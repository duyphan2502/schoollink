class GuardianPrecautionController{
    constructor(API, DialogService, PubSub){
        'ngInject';
        this.API = API;
        this.DialogService = DialogService;
        this.lang = Lang;
        this.PubSub = PubSub;
        this.sickness = {};
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('pupil-change', function () {
            self.getPrecautionsOfPupil();
        });

        this.getPrecautionsOfPupil();
    }

    getPrecautionsOfPupil(){
        this.API.one('precaution').customGET('getPrecautionsOfPupil').then((response) => {
            this.user_sicknesses = response.user_sicknesses;
            this.sicknesses = response.sicknesses;
            this.permission = response.permission;
            this.sickness_confirmed = response.sickness_confirmed;
        });
    }

    selectSickness(){
        this.sickness.name = this.sicknesses[this.option_sickness];

    }
    addSickness(){
        this.API.one('precaution').customPOST(this.sickness, 'addPrecautionOfPupil').then((response) => {
            this.user_sicknesses.push(response);
        });
    }
    deleteSickness(sickness){
        var data = {
            id: sickness.id
        };
        this.DialogService.confirm(this.lang.get('message.title_delete_precaution'), this.lang.get('message.message_delete_precaution')).then((response) => {
            this.API.one('precaution').customPUT(data, 'deletePrecautionOfPupil').then((response) => {
                var index = this.user_sicknesses.indexOf(sickness);
                this.user_sicknesses.splice(index, 1);
            });
        });
    }
    sicknessConfirm(){
        var data = {
            sickness_confirmed: this.sickness_confirmed
        }
        this.API.one('precaution').customPUT(data, 'confirmSicknessOfPupil');
    }

}

export const GuardianPrecautionComponent = {
    templateUrl: './views/app/components/schoollink/guardian/precaution/precaution.component.html',
    controller: GuardianPrecautionController,
    controllerAs: 'vm',
    bindings: {}
}
