class GuardianHeaderController{
    constructor(API, PubSub, $state, $rootScope){
        'ngInject';
        this.API = API;
        this.PubSub = PubSub;
        this.lang = Lang;
        this.$state = $state;
        this.$rootScope = $rootScope;
    }

    $onInit(){
        this.API.one('header').customGET('getHeaderSettings').then((response) => {
            this.pupils = response.roles;
            this.user = response.user;
            this.school_owner = response.school_owner;
            this.permissions = response.permissions;
            this.$rootScope.pupils = this.pupils;
        });
    }

    changePupil(pupil) {
        this.API.one('header').customPOST({id: pupil.id}, 'changeUserRoleSetting').then((response) => {
            this.permissions = response.permissions;
            if(!response.permissions.parent) {
                if(this.$state.current.name == 'app.guardian-landing' || this.$state.current.name == 'app.user-profile') {
                    this.$state.go('app.landing');
                } else {
                    this.PubSub.publish('pupil-change');
                }
            } else {
                this.$state.go('app.guardian-landing');
            }
            this.PubSub.publish('weather-change');

        });
    }
}

export const GuardianHeaderComponent = {
    templateUrl: './views/app/components/schoollink/guardian/header/header.component.html',
    controller: GuardianHeaderController,
    controllerAs: 'vm',
    bindings: {}
}
