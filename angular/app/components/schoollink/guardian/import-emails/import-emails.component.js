class GuardianImportEmailsController{
    constructor(API, PubSub){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('pupil-change', function () {
            self.getProfileImportEmails();
        });

        this.getProfileImportEmails();
    }

    getProfileImportEmails(){
        this.API.one('importEmail').customGET('getImportEmails').then((response) => {
            this.user_emails = response.user_emails;
        });
    }
}

export const GuardianImportEmailsComponent = {
    templateUrl: './views/app/components/schoollink/guardian/import-emails/import-emails.component.html',
    controller: GuardianImportEmailsController,
    controllerAs: 'vm',
    bindings: {}
}
