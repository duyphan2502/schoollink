class GuardianRemarkController{
    constructor(API, PubSub){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('pupil-change', function () {
            self.getPupilRemarks();
        });
        this.getPupilRemarks();
    }

    getPupilRemarks(){
        this.API.one('remark').customGET('getPupilRemarks').then((response) => {
            this.remarks = response.remarks;
        });
    }
}

export const GuardianRemarkComponent = {
    templateUrl: './views/app/components/schoollink/guardian/remark/remark.component.html',
    controller: GuardianRemarkController,
    controllerAs: 'vm',
    bindings: {}
}
