/**
 * Created by thuan.pham on 9/8/2016.
 */
class GuardianContactListController {
    constructor(API, PubSub) {
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
    }

    $onInit() {
        var self = this;
        this.PubSub.subscribe('pupil-change', function () {
            self.getContactList(0);
        });
        this.getContactList(0);
    }

    getContactList(subgroup_id) {
        var data = {
            subgroup_id: subgroup_id
        };
        this.API.one('contactList').customPOST(data, 'getContactList').then((response) => {
            this.subgroups = response.subgroups;
            this.classmates = response.classmates;
            this.my_subgroup_id = response.my_subgroup_id;
            this.show_my_contact = response.show_my_contact;
            this.my_id = response.my_id;
        });
    }

    updateShowMyContact(){
        var show_my_contact = this.show_my_contact == 1 ? 0 : 1;
        var data = {
            'show_my_contact': show_my_contact
        };
        this.API.one('contactList').customPUT(data, 'updateShowMyContact').then((response) => {
            this.show_my_contact = response;
        });

    }

    getLimits(classmates){
        if(classmates!=null)
        {
            return [
                Math.ceil(classmates.length / 2),
                -Math.floor(classmates.length / 2)
            ];
        }
    }
}
export const GuardianContactListComponent = {
    templateUrl: './views/app/components/schoollink/guardian/contact-list/contact-list.component.html',
    controller: GuardianContactListController,
    controllerAs: 'vm',
    bindings: {}
}
