class GuardianMessagesController{
    constructor(API, NgTableParams, DialogService, $filter){
        'ngInject';
        this.API = API;
        this.NgTableParams = NgTableParams;
        this.DialogService = DialogService;
        this.$filter = $filter;
        this.lang = Lang;
    }

    $onInit(){

    }

}

export const GuardianMessagesComponent = {
    templateUrl: './views/app/components/schoollink/guardian/messages/messages.component.html',
    controller: GuardianMessagesController,
    controllerAs: 'vm',
    bindings: {}
}
