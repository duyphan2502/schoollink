class GuardianUserProfileController{
    constructor(API, PubSub, Upload){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
        this.Upload = Upload;
        this.editing = false;
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('pupil-change', function () {
            self.getProfile();
        });

        this.getProfile();
    }

    getProfile(){
        this.API.one('user').customGET('getProfile').then((response) => {
            this.permissions = response.permissions;
            this.profile = response.profile;
            this.profile_backup = this.API.copy(this.profile);
        });
    }

    turnOnOffEditing(){
        this.editing = this.editing == true ? false : true;
        if(this.editing == false){
            // reset all value when cancel editing
            this.profile = this.profile_backup;
        } else {
            this.profile_backup = this.API.copy(this.profile);
        }
    }

    updateProfileInfo(){
        var data = {
            id: this.profile.id,
            email: this.profile.email,
            mobilephone: this.profile.mobilephone,
            telephone: this.profile.telephone,
            guardianportal_classlist_show: this.profile.guardian_portal_class_list_show
        };
        this.API.one('user').customPUT(data, 'updateProfile').then((response) => {
            this.editing = false;
        });
    }
    changeShowContact(){
        this.profile.guardian_portal_class_list_show = this.profile.guardian_portal_class_list_show == 1 ? 0 : 1;
    }

    upload(file){
        var _this = this;
        this.Upload.upload({
            url: '/img/upload',
            data: {img: file, token_image: window.frameElement.getAttribute('data-token-image')}
        }).then(function (resp) {
            _this.API.one('user').customPUT({image_id: resp.data.id}, 'uploadImage').then((response) => {
                _this.profile.image = '/img/profile/'+resp.data.id;
            });
        }, function (resp) {
            console.log('Error status: ');
        }, function (evt) {
            console.log('progress: ');
        });
    }

}

export const GuardianUserProfileComponent = {
    templateUrl: './views/app/components/schoollink/guardian/user-profile/user-profile.component.html',
    controller: GuardianUserProfileController,
    controllerAs: 'vm',
    bindings: {}
}
