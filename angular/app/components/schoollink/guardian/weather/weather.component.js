class GuardianWeatherController{
    constructor(API, PubSub){
        'ngInject';
        this.API = API;
        this.PubSub = PubSub;
        this.lang = Lang;
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('weather-change', function () {
            self.getWeather();
        });
        this.getWeather();
    }
    getWeather(){
        this.API.one('weather').customGET('getWeather').then((response) => {
            this.fore_cast = response.fore_cast;
        });
    }
}

export const GuardianWeatherComponent = {
    templateUrl: './views/app/components/schoollink/guardian/weather/weather.component.html',
    controller: GuardianWeatherController,
    controllerAs: 'vm',
    bindings: {}
}
