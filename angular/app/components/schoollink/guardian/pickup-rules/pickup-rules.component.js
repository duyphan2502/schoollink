class GuardianPickupRulesController{
    constructor(API, DialogService, PubSub){
        'ngInject';
        this.API = API;
        this.DialogService = DialogService;
        this.lang = Lang;
        this.PubSub = PubSub;
        this.editing = false;
        this.message = '';
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('pupil-change', function () {
            self.getPickupRules();
        });

        this.getPickupRules();
    }
    getPickupRules(){
        // Create lastpage for aac
        this.lastpage_active_pickup_rule = 1;
        this.lastpage_deactive_pickup_rule = 1;
        var data = {
            page: this.lastpage_active_pickup_rule
        };
        this.API.one('pickupRules').customPOST(data, 'getPickupRules').then((response) => {
            this.pickup = response.pickup;
        });
    }
    deletePickupRule(pickup_rule){
        var data = {
            id: pickup_rule.id
        };
        this.DialogService.confirm(this.lang.get('message.title_delete'), this.lang.get('message.message_delete_pickup_rule')).then((response) => {
            this.API.one('pickupRules').customPUT(data, 'deletePickupRule').then((response) => {
                if (response.active_pickup_rules) {
                    var index = this.pickup.active_pickup_rules.data.indexOf(pickup_rule);
                    this.pickup.active_pickup_rules.data.splice(index, 1);
                    this.lastpage_active_pickup_rule -=1;
                } else {
                    var index = this.pickup.deactive_pickup_rules.data.indexOf(pickup_rule);
                    this.pickup.deactive_pickup_rules.data.splice(index, 1);
                    this.lastpage_deactive_pickup_rule -=1;
                }
            });
        });
    }
    getAllActivePickupRulesOfTypeRule(type_pickup_rule) {
        this.lastpage_active_pickup_rule +=1;
        var data = {
            type_pickup_rule: type_pickup_rule,
            page: this.lastpage_active_pickup_rule
        };
        this.API.one('pickupRules').customPOST(data, 'getAllPickupRulesOfTypeRule').then((response) => {
            if (response.pickup.active_pickup_rules) {
                this.pickup.active_pickup_rules.data = this.pickup.active_pickup_rules.data.concat(response.pickup.active_pickup_rules.data);
                if (response.pickup.active_pickup_rules.to == response.pickup.active_pickup_rules.total) {
                    this.hide_active_pickup_rules = true;
                }
            } else {
                this.pickup.deactive_pickup_rules.data = this.pickup.deactive_pickup_rules.data.concat(response.pickup.deactive_pickup_rules.data);
                if (response.pickup.deactive_pickup_rules.to == response.pickup.deactive_pickup_rules.total) {
                    this.hide_deactive_pickup_rules = true;
                }
            }
        });
    }
    getAllDeactivePickupRulesOfTypeRule(type_pickup_rule) {
        this.lastpage_deactive_pickup_rule +=1;
        var data = {
            type_pickup_rule: type_pickup_rule,
            page: this.lastpage_deactive_pickup_rule
        };
        this.API.one('pickupRules').customPOST(data, 'getAllPickupRulesOfTypeRule').then((response) => {
            if (response.pickup.active_pickup_rules) {
                this.pickup.active_pickup_rules.data = this.pickup.active_pickup_rules.data.concat(response.pickup.active_pickup_rules.data);
                if (response.pickup.active_pickup_rules.to == response.pickup.active_pickup_rules.total) {
                    this.hide_active_pickup_rules = true;
                }
            } else {
                this.pickup.deactive_pickup_rules.data = this.pickup.deactive_pickup_rules.data.concat(response.pickup.deactive_pickup_rules.data);
                if (response.pickup.deactive_pickup_rules.to == response.pickup.deactive_pickup_rules.total) {
                    this.hide_deactive_pickup_rules = true;
                }
            }
        });
    }

    addPickupRule() {
        this.API.one('pickupRules').customPOST(this.sickness, 'addPickupRule').then((response) => {
            this.text = '123';
        });
    }


}

export const GuardianPickupRulesComponent = {
    templateUrl: './views/app/components/schoollink/guardian/pickup-rules/pickup-rules.component.html',
    controller: GuardianPickupRulesController,
    controllerAs: 'vm',
    bindings: {}
}
