class GuardianConsentController{
    constructor(API, PubSub){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
        this.editing = false;
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('pupil-change', function () {
            self.getPupilConsents();
        });

        this.getPupilConsents();
    }
    getPupilConsents(){
        this.API.one('consent').customGET('getPupilConsents').then((response) => {
            this.consents = response.consents;
            this.consents_backup = this.API.copy(this.consents);
            this.permission_edit = response.permission_edit;
        });
    }
    turnOnOffEditing(){
        this.editing = this.editing == true ? false : true;
        if(this.editing == false){
            // cancel button
            // reset all value have changed
            this.consents = this.consents_backup;
        }else{
            this.consents_backup = this.API.copy(this.consents);
        }
    }
    update_consents(){
        this.API.one('consent').customPUT(this.consents, 'updatePupilConsents').then((response) => {
            this.editing = false;
        });
    }

}

export const GuardianConsentComponent = {
    templateUrl: './views/app/components/schoollink/guardian/consent/consent.component.html',
    controller: GuardianConsentController,
    controllerAs: 'vm',
    bindings: {}
}
