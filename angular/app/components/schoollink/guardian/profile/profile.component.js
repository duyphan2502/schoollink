class GuardianPupilProfileController{
    constructor(API, PubSub, DialogService){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
        this.DialogService = DialogService;
        this.editing = false;
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('pupil-change', function () {
            self.getProfile();
        });

        this.getProfile();
    }

    getProfile(){
        this.API.one('user').customGET('getProfile').then((response) => {
            this.permissions = response.permissions;
            this.profile = response.profile;
            this.profile_backup = this.API.copy(this.profile);
        });
    }

    turnOnOffEditing(){
        this.editing = this.editing == true ? false : true;
        if(this.editing == false){
            // reset all value when cancel editing
            this.profile = this.profile_backup;
        }else {
            this.profile_backup = this.API.copy(this.profile);
        }
    }

    updateProfileInfo(){
        var data = {
            id: this.profile.id,
            email: this.profile.email,
            mobilephone: this.profile.mobilephone,
            telephone: this.profile.telephone
        };
        this.API.one('user').customPUT(data, 'updateProfile').then((response) => {
            this.editing = false;
        });
    }


}

export const GuardianPupilProfileComponent = {
    templateUrl: './views/app/components/schoollink/guardian/profile/profile.component.html',
    controller: GuardianPupilProfileController,
    controllerAs: 'vm',
    bindings: {}
}
