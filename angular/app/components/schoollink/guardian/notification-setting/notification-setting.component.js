class GuardianNotificationSettingController{
    constructor(API, PubSub){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('pupil-change', function () {
            self.getNotificationSettingOfPupil();
        });

        this.getNotificationSettingOfPupil();
    }

    getNotificationSettingOfPupil(){
        this.API.one('notificationSetting').customGET('getNotificationSettingOfPupil').then((response) => {
            this.notifications = response.notifications;
            this.permission = response.permission;
        });
    }

    changeNotification(notification){
        var data = {
            id: notification.login_type_id
        };

        this.API.one('notificationSetting').customPUT(data, 'updateNotificationSettingOfPupil').then((response) => {

        });
    }

}

export const GuardianNotificationSettingComponent = {
    templateUrl: './views/app/components/schoollink/guardian/notification-setting/notification-setting.component.html',
    controller: GuardianNotificationSettingController,
    controllerAs: 'vm',
    bindings: {}
}
