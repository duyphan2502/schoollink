class DksParticipantHeaderController{
    constructor(API, PubSub, $rootScope){
        'ngInject';
        this.API = API;
        this.PubSub = PubSub;
        this.lang = Lang;
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('update-pupil', function () {
            self.getHeaderSettings();
        })
        this.getHeaderSettings();
    }

    getHeaderSettings(){
        this.getHeaderGroups();
        this.API.one('header').customGET('getDksHeaderSettings').then((response) => {
            this.user = response.user;
            this.teachers = response.teachers;
            this.school_owner = response.school_owner;
            this.teacher_role_id = response.go_to_teacher_portal;
            if(Object.keys(this.teachers).length == 1){
                var selected_school = Object.keys(this.teachers)[0];
                this.selected_school_id = selected_school;
            }
        });
    }

    querySearch (query) {
        var results = query ? this.schools.filter(this.createFilterFor(query)) : this.schools;
        return results;
    }

    createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(school) {
            return (angular.lowercase(school.name).indexOf(lowercaseQuery) === 0);
        };
    }

    goToTeachPortal() {
        this.API.one('header').customPOST({id: this.teacher_role_id}, 'changeUserRoleSetting').then((response) => {
            var token = window.frameElement.getAttribute('data-id');
        parent.location.href = '/?user_set_role=' + this.teacher_role_id + '&set_parent_overview=0&set_environment=web_t&token='+ token;
    });
    }

    getHeaderGroups(){
        this.API.one('enrollmentGroup').customGET('getHeaderGroups').then((response) => {
            this.enrollment_groups = response;
    });
    }

    changeEnrollmentGroup(){
        this.PubSub.publish('change-enrollment-group', this.selectedGroup);
    }
    selectClass(){
        this.PubSub.publish('select-teacher-class', {school_id:this.selected_school_id, class_id: this.selected_group_id});
    }

}

export const DksParticipantHeaderComponent = {
    templateUrl: './views/app/components/dks/participant/header/header.component.html',
    controller: DksParticipantHeaderController,
    controllerAs: 'vm',
    bindings: {}
};
