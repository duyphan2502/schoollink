import {TeacherOverviewCourseComponent} from '../../../summerschool/teacher/course/course.component';
import {DksAddCourseController} from '../../../../../dialogs/dks-add-course/dks-add-course.dialog';
import {DksGroupActivityRoleController} from '../../../../../dialogs/dks-group-activity-role/dks-group-activity-role.dialog';

class AdminCourseController extends TeacherOverviewCourseComponent.controller {
    selectCourse(item) {
        this.DialogService.fromTemplate('dks-add-course', {
            controller: DksAddCourseController,
            controllerAs: 'vm',
            locals: {
                id: item.id,
                course_type_id: item.course_type_id
            }
        });
    }

    showRoles(type){
        this.$rootScope.dialogType = type;
        this.DialogService.fromTemplate('dks-group-activity-role', {
            controller: DksGroupActivityRoleController,
            controllerAs: 'vm'
        });
    }
}

export const AdminCourseComponent = {
    templateUrl: './views/app/components/dks/admin/course/course.component.html',
    controller: AdminCourseController,
    controllerAs: 'vm',
    bindings: {}
};