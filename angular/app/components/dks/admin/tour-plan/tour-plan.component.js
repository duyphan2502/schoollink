class AdminTourPlanComponentController {
    constructor(DialogService, API, NgTableParams, ToastService, $state) {
        'ngInject';

        this.API = API;
        this.lang = Lang;
        this.DialogService = DialogService;
        this.NgTableParams = NgTableParams;
        this.ToastService = ToastService;
        this.state = $state;

        this.enrollment_group_id = this.state.params.enrollment_group_id;
        this.course_id = this.state.params.course_id;

        this.maskOptions = {
            maskDefinitions: {
                'v': /[0-2]/,
                '9': /[0-9]/,
                'x': /[0-5]/,
                '8': /[0-9]/,
            }
        };
        this.maskDateOptions = {
            maskDefinitions: {
                'd': /[0-3]/,
                '9': /[0-9]/,
                'm': /[0-1]/,
                '8': /[0-9]/,
                'y': /[2]/,
                '7': /[0-9]/,
                '6': /[0-9]/,
                '5': /[0-9]/,
            }
        };

        this.init();
    }

    init() {
        this.weekDays = {
            '0': this.lang.get('label.monday'),
            '1': this.lang.get('label.tuesday'),
            '2': this.lang.get('label.wednesday'),
            '3': this.lang.get('label.thursday'),
            '4': this.lang.get('label.friday'),
            '5': this.lang.get('label.saturday'),
            '6': this.lang.get('label.sunday')
        };

        this.getActivities();
    }

    getActivities() {
        this.API.all('dks-activity').customGET("getTourPlan", {
            enrollment_group_id: this.enrollment_group_id,
            course_id: this.course_id
        })
            .then((response) => {
                if (response.result) {
                    this.activities = response.result.activities;
                    this.enrollment_group = response.result.enrollment_group;
                    this.course = response.result.course;

                    this.activityTableParams = new this.NgTableParams({
                        count: this.activities.length,
                        filter: {}
                    }, {
                        counts: [],
                        dataset: this.activities
                    });
                }
            });
    }

    selectTour(item) {
        this.state.go('app.activity-detail', {id: item.id, course_type_id: item.course_type_id, tour_activity: true});
    }

    toWeek(date) {
        return moment(date * 1000).week();
    }

    formatDate(date) {
        if (date) {
            return moment(date * 1000).format("DD.MM.YYYY");
        }
    }

    getDay(date) {
        if (date) {
            var dayOfWeek = moment(date * 1000).day();
            return this.weekDays[dayOfWeek].substr(0, 2);
        }
    }

    addActivity() {
        var self = this;
        this.API.all('dks-activity').customPOST({
            enrollment_group_id: self.enrollment_group_id,
            course_id: self.course_id
        }, 'add')
            .then((response) => {
                if (response.id) {
                    self.state.go('app.activity-detail', {
                        id: response.id,
                        school_id: self.enrollment_group.school_id,
                        tour_activity: true
                    });
                }
            });
    }

    cloneActivity($event, activity) {
        var self = this,
            params = {
                id: activity.id,
                enrollment_group_id: activity.enrollment_group_id
            };

        self.API.all('dks-activity').customPOST(params, 'clone').then((response) => {
            if (response.result) {
                self.getActivities();
            }
        });

        $event.stopPropagation();
    }

    deleteActivity($event, activity) {
        var self = this;
        this.DialogService.confirm('', 'Er du sikker på at du vil slette dette kurset?').then(() => {
            var self = this;
            var params = {
                id: activity.id
            };
            this.API.all('activity').customPOST(params, 'delete').then((response) => {
                self.getActivities();
            });
        });

        $event.stopPropagation();
    }

    updateActivity($event, activity) {
        let self = this;

        if(!self.checkTime(activity)) {
            return;
        }

        activity.date_start = moment(activity.date_start * 1000);
        activity.date_end = moment(activity.date_end * 1000);

        let params = {
                activity_id: activity.id,
                activity: activity
            };

        this.API.all('dks-activity').customPOST(params, 'update').then((response) => {
            if (response.result) {
                self.getActivities();
            }
        });
    }

    checkTime(activity) {
        if(activity.options.dks_school_tour_start_time && activity.options.dks_school_tour_end_time) {
            let timeEnd = activity.options.dks_school_tour_end_time.split(':'),
                timeStart = activity.options.dks_school_tour_start_time.split(':');

            let result = true;

            if(timeStart[0]*1 > timeEnd[0]*1) {
                this.ToastService.error(this.lang.get('message.timestart_cannot_greater_than_timeend'));
                result = false;
            }

            if(timeStart[0]*1 == timeEnd[0]*1) {
                if(timeStart[1]*1 > timeEnd[1]*1) {
                    this.ToastService.error(this.lang.get('message.timestart_cannot_greater_than_timeend'));
                    result = false;
                }
            }

            return result;
        }
    }

    hide() {
        this.DialogService.cancel();
    }
}

export const AdminTourPlanComponent = {
    templateUrl: './views/app/components/dks/admin/tour-plan/tour-plan.component.html',
    controller: AdminTourPlanComponentController,
    controllerAs: 'vm',
    bindings: {}
};
