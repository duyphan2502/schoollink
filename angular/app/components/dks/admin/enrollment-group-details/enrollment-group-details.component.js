import {TeacherEnrollmentGroupDetailsComponent} from '../../../summerschool/teacher/enrollment-group-details/enrollment-group-details.component';

class AdminEnrollmentGroupDetailsController extends TeacherEnrollmentGroupDetailsComponent.controller {

}

export const AdminEnrollmentGroupDetailsComponent = {
    templateUrl: './views/app/components/dks/admin/enrollment-group-details/enrollment-group-details.component.html',
    controller: AdminEnrollmentGroupDetailsController,
    controllerAs: 'vm',
    bindings: {}
};