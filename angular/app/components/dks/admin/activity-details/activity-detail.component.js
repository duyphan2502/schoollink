import {TeacherActivityDetailComponent} from '../../../summerschool/teacher/activity-details/activity-detail.component';

class AdminActivityDetailController extends TeacherActivityDetailComponent.controller {

    $onInit(){
        this.tourActivity = false;
        if(this.$state.params.tour_activity && this.$state.params.tour_activity == 'true') {
            this.tourActivity = true;
        }

        this.arrange_filter = {
            group : '',
            sex : '',
            school: ''
        };

        this.activity = {
            date_range: {},
            options: {
                dks_tour_activity: this.tourActivity
            }
        };

        this.modify_participant = false;

        this.optsSinglePicker = {
            autoUpdateInput: true,
            singleDatePicker: true,
            locale: {
                format: 'DD.MM.YYYY',
                cancelLabel: 'Avbryt',
                applyLabel: "OK",
                fromLabel: 'Fra',
                toLabel: 'Till',
                daysOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                monthNames: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"]
            },
        };

        this.opts = {
            autoUpdateInput: true,
            locale: {
                format: 'DD.MM.YYYY',
                cancelLabel: 'Avbryt',
                applyLabel: "OK",
                fromLabel: 'Fra',
                toLabel: 'Till',
                daysOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                monthNames: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"]
            },
        };

        this.maskOptions = {
            maskDefinitions: {
                'v': /[0-2]/,
                '9': /[0-9]/,
                'x': /[0-5]/,
                '8': /[0-9]/,
            }
        };

        this.getActivity();
        this.getSchools();

        this.weekdays = [
            { time_start: 'monday_time_start', time_end: 'monday_time_end', text: this.lang.get('label.monday')},
            { time_start: 'tuesday_time_start', time_end: 'tuesday_time_end', text: this.lang.get('label.tuesday')},
            { time_start: 'wednesday_time_start', time_end: 'wednesday_time_end', text: this.lang.get('label.wednesday')},
            { time_start: 'thursday_time_start', time_end: 'thursday_time_end', text: this.lang.get('label.thursday')},
            { time_start: 'friday_time_start', time_end: 'friday_time_end', text: this.lang.get('label.friday')},
            { time_start: 'saturday_time_start', time_end: 'saturday_time_end', text: this.lang.get('label.saturday')},
            { time_start: 'sunday_time_start', time_end: 'sunday_time_end', text: this.lang.get('label.sunday')}
        ];

        if(this.$state.params.edit_clone) {
            this.show_edit_view = true;
        }

        this.show_edit_view = (this.activity.date_activated != null);
    }

    querySearch (query) {
        if(query) {
            return this.API.all('group').customGET('search', { text : query, id: this.activityId}).then((response) => {
                return response;
            });
        }
    }

    updateActivity() {
        var self = this;

        this.activity.options.dks_tour_activity = this.tourActivity;

        this.activity.date_start = this.$rootScope.toUTCOffset(this.activity.date_start);

        var params = {
            activity_id: this.activityId,
            activity: this.activity
        };

        this.API.all('dks-activity').customPOST(params, 'update').then((response) => {
            if(response.result) {
                self.activity_bk = angular.copy(self.activity);
                self.show_edit_view = false;
            }
        });
    }

    changeSingleTime(item, type) {
        if(this.activity.options.dks_school_tour_start_time && this.activity.options.dks_school_tour_end_time) {
            var timeEnd = this.activity.options.dks_school_tour_end_time.split(':'),
                timeStart = this.activity.options.dks_school_tour_start_time.split(':');

            if(timeStart[0]*1 > timeEnd[0]*1) {
                this.ToastService.error(this.lang.get('message.timestart_cannot_greater_than_timeend'));
                if(type == 'start') {
                    this.activity.options.dks_school_tour_start_time = '';
                } else {
                    this.activity.options.dks_school_tour_end_time = '';
                }
            }

            if(timeStart[0]*1 == timeEnd[0]*1) {
                if(timeStart[1]*1 > timeEnd[1]*1) {
                    this.ToastService.error(this.lang.get('message.timestart_cannot_greater_than_timeend'));
                    if(type == 'start') {
                        this.activity.options.dks_school_tour_start_time = '';
                    } else {
                        this.activity.options.dks_school_tour_end_time = '';
                    }
                }
            }
        }
    }

    getActivity() {
        var self = this;
        var data = {
            id: self.activityId
        };
        if(this.activityId) {
            this.API.all('activity').customGET('getBy', data).then((response) => {
                if(response) {
                    self.activity = response.activity;
                    self.participants = response.participants;

                    if(self.activity.group && self.activity.group.direct_entry_date_start) {
                        if(moment(self.activity.group.direct_entry_date_start * 1000) <= moment()) {
                            self.modify_participant = true;
                        }
                    }

                    self.activity.date_start = moment(self.activity.date_start * 1000);

                    //TODO: workaround to select item in selection box
                    if(self.activity.options) {
                        self.activity.options.summer_school_school_id =  self.activity.options.summer_school_school_id + '';
                    } else {
                        self.activity.options = {
                            summer_school_school_id : '',
                            dks_tour_activity: self.tourActivity
                        };
                    }

                    //TODO: workaround to sent params to dialog
                    this.activity.number_of_groups = this.activity.number_of_groups + '';

                    self.$state.params['course_type_id'] = self.activity.course_type_id;

                    if(self.activity.course) {
                        self.activity['course_name'] = self.activity.course.name;
                        self.activity['course_id'] = self.activity.course.id;
                    }

                    self.participants.waiting = _.orderBy(self.participants.waiting, 'position');
                    self.arrangeParticipant();
                    self.activity_bk = angular.copy(self.activity);
                }
            });
        }
    }

    formatTourDate(date) {
        if(date) {
            return moment(date).format("DD.MM.YYYY");
        }
    }


}

export const AdminActivityDetailComponent = {
    templateUrl: './views/app/components/dks/admin/activity-details/activity-detail.component.html',
    controller: AdminActivityDetailController,
    controllerAs: 'vm',
    bindings: {}
};


