import {TeacherSimulationActivityComponent} from '../../../summerschool/teacher/simulation-activity/simulation-activity.component';

class AdminSimulationActivityController extends TeacherSimulationActivityComponent.controller {

}

export const AdminSimulationActivityComponent = {
    templateUrl: './views/app/components/dks/admin/simulation-activity/simulation-activity.component.html',
    controller: AdminSimulationActivityController,
    controllerAs: 'vm',
    bindings: {}
};