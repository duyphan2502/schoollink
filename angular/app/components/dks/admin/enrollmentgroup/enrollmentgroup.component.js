import {TeacherOverviewEnrollmentGroupComponent} from '../../../summerschool/teacher/enrollmentgroup/enrollmentgroup.component';

class AdminOverviewEnrollmentGroupController extends TeacherOverviewEnrollmentGroupComponent.controller {

}

export const AdminOverviewEnrollmentGroupComponent = {
    templateUrl: './views/app/components/dks/admin/enrollmentgroup/enrollmentgroup.component.html',
    controller: AdminOverviewEnrollmentGroupController,
    controllerAs: 'vm',
    bindings: {}
};
