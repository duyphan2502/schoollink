import {TeacherAcademicYearComponent} from '../../../summerschool/teacher/academic-year/academic-year.component';

class AdminAcademicYearController extends TeacherAcademicYearComponent.controller {

}

export const AdminAcademicYearComponent = {
    templateUrl: './views/app/components/dks/admin/academic-year/academic-year.component.html',
    controller: AdminAcademicYearController,
    controllerAs: 'vm',
    bindings: {}
};