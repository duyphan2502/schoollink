import {TeacherOverviewActivityComponent} from '../../../summerschool/teacher/activity/activity.component';
import {DksAddActivityCourseController} from './../../../../../dialogs/dks-add-activity-course/dks-add-activity-course.dialog';

class AdminActivityController extends TeacherOverviewActivityComponent.controller {
    getOverview() {
        this.activities = [];

        this.API.all('dks-activity').customGET("getActivitiesByEnrollmentGroupId", {enrollment_group_id: this.enrollment_group_id}).then((response) => {
            if(response) {
                this.activities = response.activities;
                this.enrollment_group = response.enrollment_group;
                this.activityTableParams = new this.NgTableParams({
                    count: this.activities.length,
                    filter : {
                        'school.id': ''
                    }
                }, {
                    counts: [],
                    dataset: this.activities
                });

                this.initFilter();
            }
        });
    }

    addActivity(){
        var self = this;
        this.DialogService.fromTemplate('dks-add-activity-course', {
            controller: DksAddActivityCourseController,
            controllerAs: 'vm',
            locals: {
                course_type_id: self.enrollment_group.course_type_id
            }
        }).then(function(response) {
            self.API.all('dks-activity').customPOST({ enrollment_group_id: self.enrollment_group_id , course_id : response.id}, 'add')
                .then((response) => {
                    if(response.id) {
                        self.$state.go('app.activity-detail', { id: response.id, school_id: self.school_id});
                    }
                });
        });
    }

    showTourPlan(activity) {
        this.$state.go('app.tour-plan', { course_id: activity.course_id, enrollment_group_id: activity.enrollment_group_id});
    }

    toSemesterText(semester) {
        let data = semester.split('-');
        let year = data[1];
        let season = data[0];

        if(season == 1) {
            return this.lang.get('label.spring') + ' ' + year;
        }
        return this.lang.get('label.autumn') + ' ' + year;
    }

    initFilter(){
        this.schoolFilters = {};
        this.courseFilters = {};
        this.weekFilters = {};
        this.yearGradeFilters = {};

        for (var i = 1; i < 14; i++) {
            this.yearGradeFilters[i] = {
                year_grade: i,
                count: 0
            };
        }

        for (var activity of this.activities) {
            if (activity.course) {
                if (this.courseFilters[activity.course.id] === undefined) {
                    this.courseFilters[activity.course.id] = {
                        course: activity.course,
                        name: activity.course.name,
                        semester: activity.course.dks_semester,
                        semester_label: this.toSemesterText(activity.course.dks_semester),
                        count: 0
                    };
                }

                activity.activity.year_grade = [];
                if(activity.course.year_grade_minimum && activity.course.year_grade_maximum) {
                    for (var year = activity.course.year_grade_minimum ; year <= activity.course.year_grade_maximum; year++) {
                        this.yearGradeFilters[year].count++;
                        activity.activity.year_grade[year] = year;
                    }
                }
                this.courseFilters[activity.course.id].count++;
            } else {
                activity.course = {
                    id: -1
                };
                if (this.courseFilters[0] === undefined) {
                    this.courseFilters[0] = {
                        course: activity.course,
                        count: 0
                    };
                }
                this.courseFilters[0].count++;
            }
            activity.activity.week_start = moment(activity.activity.date_start * 1000).isoWeek();
            activity.activity.week = [];
            var year = moment(activity.activity.date_start * 1000).year();
            var week_start = moment(activity.activity.date_start * 1000).isoWeek();
            var week_end = moment(activity.activity.date_end * 1000).isoWeek();
            for (var i = week_start; i <= week_end; i++) {
                if (this.weekFilters[year + '_' + i] === undefined) {
                    this.weekFilters[year + '_' + i] = {
                        year: year,
                        week: i,
                        count: 0
                    };
                }
                activity.activity.week[i] = i;
                this.weekFilters[year + '_' + i].count++;
            }
        };

    }

    changeFilter(field, value) {
        var filter = {};
        filter[field] = value;

        angular.extend(this.activityTableParams.filter(), filter);
    }

    displayDetail(activity) {
        var id = activity.activity.id,
            courseTypeId = activity.activity.course_type_id;

        if(!activity.course.book_on_course) {
            this.showTourPlan(activity.activity);
        } else {
            this.showDetail(id, courseTypeId);
        }
    }

    formatTourDate(date) {
        if(date) {
            return moment(date * 1000).format('DD.MM.YYYY');
        }
    }
}

export const AdminActivityComponent = {
    templateUrl: './views/app/components/dks/admin/activity/activity.component.html',
    controller: AdminActivityController,
    controllerAs: 'vm',
    bindings: {}
};


