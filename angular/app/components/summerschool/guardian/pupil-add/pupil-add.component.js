class GuardianPupilAddController{
    constructor(API, PubSub){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
        this.pupil = {};
    }

    $onInit(){
        this.pupil.birthdate = {
            endDate: moment().add(1, 'days'),
            startDate: moment()
        };
        this.opts = {
            autoUpdateInput: true,
            singleDatePicker: true,
            locale: {
                format: 'DD.MM.YYYY',
                cancelLabel: 'Avbryt',
                applyLabel: "OK",
                fromLabel: 'Fra',
                toLabel: 'Till',
                daysOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                monthNames: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"]
            }
        };
        this.getSchools();
    }
    addPupil(){
        var data = {
            firstname: this.pupil.firstname,
            surname: this.pupil.surname,
            postal_code: this.pupil.postal_code,
            postal_name: this.pupil.postal_name,
            mobilephone: this.pupil.mobilephone,
            address: this.pupil.address,
            email: this.pupil.email,
            birthdate: this.pupil.birthdate.startDate,
            school: this.pupil.school,
            class: this.pupil.class
        };
        this.API.one('user').customPOST(data, 'addPupil').then((response) => {
            if(response.result == true){
                this.PubSub.publish('update-pupil');
            }
        });
    }
    getSchools(){
        this.API.one('school').customGET('getSchoolsInMunicipality').then((response) => {
            this.schools = response.schools;
        });
    }
    changeSchool(){
        var data = {id: this.pupil.school};
        this.API.one('school').customPOST(data, 'getSubgroupSchool').then((response) => {
            this.pupil.class = '';
            this.subgroups = response.subgroups;
        });
    }

}

export const GuardianPupilAddComponent = {
    templateUrl: './views/app/components/summerschool/guardian/pupil-add/pupil-add.component.html',
    controller: GuardianPupilAddController,
    controllerAs: 'vm',
    bindings: {}
}
