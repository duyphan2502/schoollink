class PupilProfileController {
    constructor(API, PubSub, DialogService, Upload, $state){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
        this.$state = $state;
        this.Upload = Upload;
        this.DialogService = DialogService;
        this.editing = false;
    }
    $onInit(){
        var self = this;
        this.PubSub.subscribe('pupil-change', function () {
            self.getProfile();
        });

        this.getProfile();
        this.getPermissionDelete();
    }
    getProfile(){
        this.API.one('user').customGET('getPupilSummerProfile').then((response) => {
            this.permissions = response.permissions;
            this.profile = response.profile;
            this.profile_backup = this.API.copy(this.profile);
        });
    }

    turnOnOffEditing(){
        this.editing = this.editing == true ? false : true;
        if(this.editing == false){
            // reset all value when cancel editing
            this.profile = this.profile_backup;
        }else {
            this.profile_backup = this.API.copy(this.profile);
        }
    }
    getPermissionDelete(){
        var self = this;
        this.API.one('user').customGET('getPermissionDelete').then((response) => {
            self.permission_delete = response;
        });
    }
    updateProfileInfo(){
        var data = this.profile;
        this.API.one('user').customPUT(data, 'updateSummerPupilProfile').then((response) => {
            this.editing = false;
        });
    }
    deletePupil(id){
        this.DialogService.confirm('You want to delete this pupil?').then((response) => {
            if(response == true){
                var data = {
                    id: id
                }
                this.API.one('user').customPOST(data, 'deletePupil').then((response) => {
                    if(response == true){
                        this.PubSub.publish('update-pupil');
                        this.$state.go('app.landing');
                    }
                });
            }
        });
    }
    upload(file){
        var _this = this;
        this.Upload.upload({
            url: '/img/upload',
            data: {img: file, token_image: window.frameElement.getAttribute('data-token-image')}
        }).then(function (resp) {
            _this.API.one('user').customPUT({image_id: resp.data.id}, 'uploadImage').then((response) => {
                _this.profile.image = '/img/profile/'+resp.data.id;
        });
        }, function (resp) {
            //console.log('Error status: ');
        }, function (evt) {
            //console.log('progress: ');
        });
    }
}

export const PupilProfileComponent = {
    templateUrl: './views/app/components/summerschool/guardian/profile/profile.component.html',
    controller: PupilProfileController,
    controllerAs: 'vm',
    bindings: {}
}
