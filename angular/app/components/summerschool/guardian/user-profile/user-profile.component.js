import {GuardianUserProfileComponent} from './../../../schoollink/guardian/user-profile/user-profile.component.js';
class GuardianProfileController extends GuardianUserProfileComponent.controller {
    $onInit(){
        this.getProfile();
    }
    getProfile(){
        this.API.one('user').customGET('getGuardianSummerProfile').then((response) => {
            this.permissions = response.permissions;
            this.profile = response.profile;
            this.profile_backup = this.API.copy(this.profile);
        });
    }
    updateGuardianProfileInfo(){
        var data = {
            id: this.profile.id,
            email: this.profile.email,
            mobilephone: this.profile.mobilephone,
            firstname: this.profile.firstname,
            surname: this.profile.surname,
            address: this.profile.address,
            postal_code: this.profile.postal_code,
            postal_name: this.profile.postal_name
        };
        this.API.one('user').customPUT(data, 'updateGuardianProfile').then((response) => {
            this.editing = false;
        });
    }
}

export const GuardianProfileComponent = {
    templateUrl: './views/app/components/summerschool/guardian/user-profile/user-profile.component.html',
    controller: GuardianProfileController,
    controllerAs: 'vm',
    bindings: {}
}
