import {LocationMapController} from '../../../../../dialogs/location-map/location-map.dialog';
class GuardianSummerBookingController{
    constructor(API, PubSub, $state, DialogService, ToastService, $filter, $cookies){
        'ngInject';
        this.API = API;
        this.PubSub = PubSub;
        this.lang = Lang;
        this.DialogService = DialogService;
        this.$state = $state;
        this.ToastService = ToastService;
        this.keys = Object.keys;
        this.distance = this.max_distance = 20;
        this.$filter = $filter;
        this.view_mode = $cookies.get('teacher_mode') == "true";
        this.current_language = this.lang.locale();
    }

    $onInit(){
        var self = this;
        this.activities_interested = [];
        this.activities_registered = [];
        this.activities_waiting = [];
        this.PubSub.subscribe('pupil-change', function () {
            if(!self.enroll_groups){
                self.getEnrollGroups();
            }else {
                self.getNumberOfConfirmedWaiting();
                self.getPupilActivities();
            }
        });
        this.PubSub.subscribe('pupil-change-detail', function (pupil) {
            self.selectedItem = pupil.school;
            self.pupil = pupil;
            if(self.pupil){
                //default filter trinn of pupil
                self.grade_active.push(self.pupil.grade);
                self.grade_filter = self.filterGradeRange(self.pupil.grade);
            }
        })
        this.PubSub.subscribe('change-enrollment-group', function (group_id) {
            self.getFilterCourses(group_id);
        })
        this.PubSub.subscribe('switch-mode', function (teacher) {
            self.view_mode = teacher;
        });
        this.PubSub.subscribe('teacher-pupil-selected', function (value) {
            self.allow_access_booking = value;
        });
        this.getNumberOfConfirmedWaiting();
        this.getSchools();
        this.getEnrollGroups();
    }

    getSchools(){
        this.API.one('school').customGET('getSchoolsInMunicipality').then((response) => {
            this.schools = this.$filter('to_array')(response.schools);
        })
    }

    changeDistance(){
        var self = this;
        return function (course) {
            if(self.selectedItem) {
                for(var index in course.activities_school) {
                    if (self.selectedItem) {
                        var current_gps = course.activities_school[index].school.gps_coordinates;
                        if(self.selectedItem.gps_coordinates == null || self.selectedItem.gps_coordinates == undefined){
                            self.selectedItem.gps_coordinates = {};
                            self.selectedItem.gps_coordinates.latitude = 0;
                            self.selectedItem.gps_coordinates.longitude = 0;
                        }
                        if(current_gps) {
                            var get_distance = self.getDistance(current_gps.latitude, current_gps.longitude, self.selectedItem.gps_coordinates.latitude, self.selectedItem.gps_coordinates.longitude, 'K');
                        }else{
                            var get_distance = 0;
                        }
                        self.result = get_distance <= self.distance;
                        if (self.result == true || get_distance >= self.max_distance) {
                            return true;
                        }
                    }
                }
                return self.result;
            }else{
                return true;
            }
        }
    }
    filterCourseLocation(){
        var self = this;
        return function (course) {
            if(self.courseLocationSelected != null) {
                for(var index in course.activities_school) {
                    if(course.activities_school[index].school.id == self.courseLocationSelected.id){
                        return true;
                    }
                }
                return false;
            }else{
                return true;
            }
        }
    }
    numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        return parts.join(",");
    }

    querySearch (query) {
        var results = query ? this.schools.filter(this.createFilterFor(query)) : this.schools;
        return results;
    }
    querySearchCourseLocation (query) {
        var results = query ? this.schools.filter(this.createFilterFor(query)) : this.schools;
        return results;
    }

    createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(school) {
            return (angular.lowercase(school.name).indexOf(lowercaseQuery) === 0);
        };
    }
    checkFilterExist(){
        if(this.grade_active.length == 0 || this.week_active.length == 0 || this.subject_active.length == 0|| this.courses_available_active != ''){
            return true;
        }
        return false;
    }

    /**
     * check activities of course location have available spaces
     * @param activity_school
     * @returns {boolean}
     */
    checkAvailableInside(activity_school){
        var self = this;
        if(activity_school.activities){
            var available_exist = false;
            activity_school.activities.forEach(function(act){
                var check_correct_week = $.grep(self.schedule, function(w) {
                    return act.week >= w.start_week && act.week <= w.end_week;
                });
                if(act.available_space > 0 && act.in_activity == false && check_correct_week.length > 0){
                    available_exist = true;
                }
            })
            return available_exist;
        }
    }
    getFilterCourses(group_id){
        // reset filter selected
        this.week_active = [];
        this.grade_active = [];
        this.subject_active = [];
        this.courses_available_active = '';
        this.available_filter = '';
        this.course_filter = '';
        this.grade_filter = '';
        this.week_filter = '';

        // end reset
        if(!group_id) {
            var group_id = Object.keys(this.enroll_groups)[0];
        }
        this.group_active = group_id;
        
        this.subjects = [];
        this.schedule = [];
        this.activities_course = [];
        for(var key in this.subjects_bk){
            if( this.subjects_bk[key].group_id == group_id){
                this.subjects.push(this.subjects_bk[key]);
            }

        }
        for(var key in this.schedule_bk){
            if(this.schedule_bk[key].group_id == group_id){
                this.schedule.push(this.schedule_bk[key]);
            }
        }
        for(var key in this.activities_course_bk){
            if(key == group_id){
                for(var i in this.activities_course_bk[key]){
                    this.activities_course.push(this.activities_course_bk[key][i]);
                }
            }
        }
        var self = this;
        for(var index in this.activities_course) {
            $.each(this.activities_course[index].activities_school, function(i, v){
                if(v.activities) {
                    v.activities.forEach(function (act) {
                        var check_in_activity = self.checkInActivity(act.id);
                        if (check_in_activity.length > 0) {
                            act.in_activity = true;
                        }else{
                            act.in_activity = false;
                        }
                        if(!act.available_space && !act.on_waiting_list) {
                            act.available_space = self.number_of_confirmed_waiting[act.id].available_space;
                            act.on_waiting_list = self.number_of_confirmed_waiting[act.id].on_waiting_list;
                        }
                    })
                }else{
                    var check_in_activity = self.checkInActivity(v.id);
                    if (check_in_activity.length > 0) {
                        v.in_activity = true;
                    }else{
                        v.in_activity = false;
                    }
                    if(!v.available_space && !v.on_waiting_list) {
                        v.available_space = self.number_of_confirmed_waiting[v.id].available_space;
                        v.on_waiting_list = self.number_of_confirmed_waiting[v.id].on_waiting_list;
                    }
                }
            })
        }
    }
    getNumberOfConfirmedWaiting(){
        var self = this;
        this.API.one('activity').customGET('getNumberOfConfirmedWaiting').then((response) => {
            self.number_of_confirmed_waiting = response;
        })
    }
    getEnrollGroups(){
        this.API.one('enrollmentGroup').customGET('getEnrollmentGroups').then((response) => {
            if(response){
                this.activities_course_bk = [];
                for (var key in response.activities_course) {
                    this.activities_course_bk[key] = response.activities_course[key];
                }
                this.activities_all = response.activities_all;
                this.subjects_bk = response.course_type.subjects;
                this.schedule_bk = response.course_type.schedule;
                this.enroll_groups = response.enroll_groups;
                this.getPupilActivities();
            }
        })
    }
    getPupilActivities(){
        this.API.one('user').customGET('getPupilActivities').then((response) => {
            this.activities_interested = response.activities_interested;
            this.activities_registered = response.activities_registered;
            this.activities_waiting = response.activities_waiting;
            this.getFilterCourses();
        });
    }
    checkInActivity(activity_id){
        var check_in_activity = [];
        if(this.enroll_groups[this.group_active].direct_entry == true){
            this.activities_interested = [];
        }
        check_in_activity = $.grep(this.activities_registered, function(a) {
            return a.activity_id == activity_id;
        });
        if(check_in_activity.length == 0){
            check_in_activity = $.grep(this.activities_waiting, function(a) {
                return a.activity_id == activity_id;
            });
        }
        if(check_in_activity.length == 0){
            check_in_activity = $.grep(this.activities_interested, function(a) {
                return a.activity_id == activity_id;
            });
        }
        return check_in_activity;
    }
    filterCoursesAvailableSpaces(){
        var self = this;
        return function (course) {
            var available_space = false;
            for(var index in course.activities_school) {
                if(course.activities_school[index].activities) {
                    available_space = self.checkAvailableInside(course.activities_school[index]);
                }else{
                    //special course
                    if(course.activities_school[index].available_space > 0 && course.activities_school[index].in_activity == false){
                        return true;
                    }
                }
            }
            return available_space;
        }
    }
    filterGradeRange(grade){
        var self = this;
        return function(course){
            //return course.year_grade_maximum >= grade && course.year_grade_minimum <= grade;
            if(self.grade_active.length == 0){
                return true;
            }
            return self.checkArrayRangeInside(self.grade_active, course.year_grade_minimum, course.year_grade_maximum);
        }
    }
    filterWeek(week){
        var self = this;
        return function(course){
            if(self.week_active.length == 0){
                return true;
            }
            var result = self.checkOverlapArray(self.week_active, course.weeks);
            return result.length > 0
            //return course.weeks.indexOf(week) != -1;
        }
    }
    filterSubject(){
        var self = this;
        return function(course){
            if(self.subject_active.length == 0){
                return true;
            }
            return self.subject_active.indexOf(course.subject) !== -1;
        }
    }
    checkOverlapArray(arr1, arr2){
        var arrays = [arr1, arr2];
        return arrays.shift().filter(function(v) {
            return arrays.every(function(a) {
                return a.indexOf(v) !== -1;
            });
        });
    }
    checkArrayRangeInside(arr, start, end){
        var result = false;
        arr.forEach( function(a) {
            if(a >= start && a <= end){
                result = true;
            }
        });
        return result;
    }
    getDistance(lat1, lon1, lat2, lon2, unit) {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        var radtheta = Math.PI * theta/180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        if(!dist){
            return this.max_distance;
        }
        return dist >= this.max_distance ? this.max_distance : dist;
    }
    keySort(key) {
        return function(a,b){
            if (a[key] > b[key]) return 1;
            if (a[key] < b[key]) return -1;
            return 0;
        }
    }
    confirmActivity(id, act_school){
        var part = this.activities_all[id];
        var data = {
            data: this.activities_registered,
            id: id
        };
        this.API.one('activity').customPUT(data, 'directBooking').then((response) => {
            if(response.result == false){
                this.ToastService.error(response.message);
            }else{
                act_school.in_activity = true;
                act_school.on_waiting_list = response.on_waiting_list;
                act_school.available_space = response.available_space;
                part.status = response.status;
                part.position = response.position;
                if(response.status == 'BookedDirectly'){
                    this.activities_registered.push(part);
                } else{
                    this.activities_waiting.push(part);
                }
            }
        });
    }
    removeDirectBooking(id, status){
        var msg = 'Please confirm cancelation of booking';
        if(this.current_language != 'en'){
            msg = 'Vennligst bekreft at påmelding skal slettes';
        }
        this.DialogService.confirm(msg).then((response) => {
            if(response == true){
                var data = {
                    status: status,
                    id: id
                };
                this.API.one('activity').customPUT(data, 'removeDirectBooking').then((response) => {
                    if(response.result == true){
                        for(var index in this.activities_registered) {
                            if(this.activities_registered[index].activity_id == id){
                                var index_removed = index;
                            }
                        }
                        if(status == 'WaitingList') {
                            this.activities_waiting.splice(index_removed, 1);
                        }
                        else{
                            this.activities_registered.splice(index_removed, 1);
                        }
                        var course_id = this.activities_all[id].course_id;
                        for(var index in this.activities_course_bk) {
                            $.each(this.activities_course_bk[index], function(key, value_course){
                                if(key == course_id){
                                    $.each(value_course.activities_school, function(i, v){
                                        if(v.activities) {
                                            v.activities.forEach(function (act) {
                                                if (act.id == id) {
                                                    act.in_activity = false;
                                                    act.available_space = response.available_space;
                                                    act.on_waiting_list = response.on_waiting_list;
                                                }
                                            })
                                        }else{
                                            if(v.id == id){
                                                v.in_activity = false;
                                                v.available_space = response.available_space;
                                                v.on_waiting_list = response.on_waiting_list;
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    }
                });
            }
        });

    }
    bookingActivity(id, action, activity){
        var data = {
            id: id,
            action: action
        };
        switch (action){
            case 'Delete':
                var msg = 'Please confirm cancelation of booking';
                if(this.current_language != 'en'){
                    msg = 'Vennligst bekreft at påmelding skal slettes';
                }
                this.DialogService.confirm(msg).then((response) => {
                    for(var index in this.activities_interested) {
                        if(this.activities_interested[index].activity_id == id){
                            this.activities_participant_backup = this.API.copy(this.activities_interested);
                            var index_removed = index;
                        }
                        if(index_removed < index){
                            this.activities_interested[index].priority -= 1;
                        }
                    }
                    this.activities_interested.splice(index_removed, 1);
                    var course_id = this.activities_all[id].course_id;
                    for(var index in this.activities_course_bk) {
                        $.each(this.activities_course_bk[index], function(key, value_course){
                            if(key == course_id){
                                $.each(value_course.activities_school, function(i, v){
                                    if(v.activities) {
                                        v.activities.forEach(function (act) {
                                            if (act.id == id) {
                                                act.in_activity = false;
                                            }
                                        })
                                    }else{
                                        if(v.id == id){
                                            v.in_activity = false;
                                        }
                                    }
                                })
                            }
                        })
                    }
                    data.data = this.activities_participant_backup;
                    this.API.one('activity').customPUT(data, 'bookingActivity').then((response) =>{
                        if(response.result == false){
                            this.ToastService.error(response.message);
                        }
                    });
                });
                return false;
            case 'Decrease':
                for(var index in this.activities_interested) {
                    if(this.activities_interested[index].activity_id == id){
                        this.activities_interested[index].priority += 1;
                        this.activities_interested[parseInt(index)+1].priority -= 1;
                    }
                }
                break;
            case 'Increase':
                for(var index in this.activities_interested) {
                    if(this.activities_interested[index].activity_id == id){
                        this.activities_interested[index].priority -= 1;
                        this.activities_interested[index-1].priority += 1;
                    }
                }
                break;
            default:
                var part = this.activities_all[id];
                data.enroll_group_id = part.enroll_group_id;
                this.activities_interested_temp = this.API.copy(this.activities_interested);
                part.priority = this.activities_interested_temp.length + 1;
                this.activities_interested_temp.push(part);
                break;
        }
        this.activities_interested.sort(this.keySort('priority'));

        data.data = this.activities_interested;
        if(action == 'Book'){
            data.data = this.activities_interested_temp;
        }
        this.API.one('activity').customPUT(data, 'bookingActivity').then((response) =>{
            if(action == 'Book' && response.result == true){
                activity.in_activity = true;
                part.priority = this.activities_interested.length + 1;
                this.activities_interested.push(part);
            }
            if(response.result == false){
                this.ToastService.error(response.message);
            }
        });
    }
    getNumberOfLocation(activities_school){
        if(!activities_school){
            return 0;
        }
        // count number of course location inside course found in case
        // filter week, school, distance, courses available spaces
        if(this.week_active == '' && this.courseLocationSelected == null && this.distance == this.max_distance && this.courses_available_active == ''){
            return this.keys(activities_school).length;
        }
        this.count_location = 0;
        var self = this;
        $.each(activities_school, function(index, value){
            //incase not yet input school location
            if(self.selectedItem.gps_coordinates == null || self.selectedItem.gps_coordinates == undefined){
                self.selectedItem.gps_coordinates = {};
                self.selectedItem.gps_coordinates.latitude = 0;
                self.selectedItem.gps_coordinates.longitude = 0;
            }
            if(value.school.gps_coordinates == null || value.school.gps_coordinates == undefined){
                value.school.gps_coordinates = {};
                value.school.gps_coordinates.latitude = 0;
                value.school.gps_coordinates.longitude = 0;
            }
            if((self.week_active == '' || (value.weeks && self.checkOverlapArray(value.weeks, self.week_active).length > 0) || self.checkArrayRangeInside(self.week_active, value.week_start, value.week_end) == true)
                && (self.courseLocationSelected == null || self.courseLocationSelected.id == value.school.id)
                && (self.distance == self.max_distance || self.distance >= self.getDistance(value.school.gps_coordinates.latitude, value.school.gps_coordinates.longitude, self.selectedItem.gps_coordinates.latitude, self.selectedItem.gps_coordinates.longitude, 'K'))
                && (self.courses_available_active == '' || (value.activities && self.checkAvailableInside(value) == true) || (value.in_activity == false && value.available_space > 0))
            ){
                    self.count_location++;
            }
        })
        return this.count_location;
    }
    setActiveSubject(subject){
        if(subject == ''){
            this.subject_active = [];
        }else if(this.subject_active.indexOf(subject) != -1){
            this.subject_active = $.grep(this.subject_active, function(value) {
                return value != subject;
            });
        }else{
            this.subject_active.push(subject);
        }
    }
    setActiveGrade(grade){
        if(grade == ''){
            this.grade_active = [];
        }else if(this.grade_active.indexOf(grade) != -1){
            this.grade_active = $.grep(this.grade_active, function(value) {
                if(grade > 10){
                    var vgs = [11,12,13];
                    return vgs.indexOf(value) == -1;
                }
                return value != grade;
            });
        }else{
            if(grade > 10){
                //vgs
                this.grade_active.push(11);
                this.grade_active.push(12);
                this.grade_active.push(13);
            }else {
                this.grade_active.push(grade);
            }
        }
    }
    setActiveWeek(week){
        if(week == ''){
            this.week_active = [];
        }else if(this.week_active.indexOf(week) != -1){
            this.week_active = $.grep(this.week_active, function(value) {
                return value != week;
            });
        }else{
            this.week_active.push(week);
        }
    }
    setActiveAvailable(available){
        this.courses_available_active = available;
    }
    setActiveGroup(group_id){
        this.group_active = group_id;
    }
    showLocationMap(school_name, school_location){
        this.DialogService.fromTemplate('location-map', {
            controller: LocationMapController,
            controllerAs: 'vm',
            locals: {school_name: school_name, school_location:school_location}
        });
    }

}

export const GuardianSummerBookingComponent = {
    templateUrl: './views/app/components/summerschool/guardian/summer-booking/summer-booking.component.html',
    controller: GuardianSummerBookingController,
    controllerAs: 'vm',
    bindings: {}
}
