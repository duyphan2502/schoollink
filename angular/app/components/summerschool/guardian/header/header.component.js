import {SelectRoleController} from '../../../../../dialogs/select-role/select-role.dialog';
import {AddPupilController} from '../../../../../dialogs/pupil-add/pupil-add.dialog';

class GuardianHeaderController{
    constructor(API, PubSub, DialogService, $state, $rootScope, $cookies){
        'ngInject';
        this.API = API;
        this.PubSub = PubSub;
        this.lang = Lang;
        this.$state = $state;
        this.DialogService = DialogService;
        this.$rootScope = $rootScope;
        this.$cookies = $cookies;
        this.view_mode = this.$cookies.get('teacher_mode') == "true";
        this.teacher_only = false;
    }

    $onInit(){
        var self = this;
        this.PubSub.subscribe('update-pupil', function () {
            self.getHeaderSettings();
        })
        this.getHeaderSettings();
    }

    getHeaderSettings(){
        this.getHeaderGroups();
        this.API.one('header').customGET('getSommerSchoolHeaderSettings').then((response) => {
            this.pupils = response.roles;
            if(this.view_mode == true && response.teachers.length == 0){
                this.view_mode = false;
                this.$cookies.put('teacher_mode', this.view_mode);
            }
            // parent dont have any pupil must be go to add pupil page
            if(this.pupils.length < 1 && response.teachers.length == 0){
                this.$state.go('app.landing');
                //reset variable
                this.pupil = null;
                this.DialogService.fromTemplate('select-role', {
                    controller: SelectRoleController,
                    controllerAs: 'vm',
                    locals: {user: response.user}
                }).then(function(response) {

                });
            }else{
                if(this.view_mode == false) {
                    // this.changePupil(this.pupils[0], null);
                    this.getProfile();
                }
            }
            this.user = response.user;
            this.teachers = response.teachers;
            this.school_owner = response.school_owner;
            this.permissions = response.permissions;
            this.teacher_role_id = response.go_to_teacher_portal;
            this.$rootScope.pupils = this.pupils;
            this.is_parent = response.is_parent;
            this.is_pupil = response.is_pupil;
            // in case only have role C
            if(this.pupils.length < 1 && response.teachers.length != 0){
                this.teacher_only = true;
            }
            if(this.view_mode == true &&  Object.keys(this.teachers).length == 1){
                var selected_school = Object.keys(this.teachers)[0];
                this.selected_school_id = selected_school;
            }
        });
    }

    querySearch (query) {
        var results = query ? this.schools.filter(this.createFilterFor(query)) : this.schools;
        return results;
    }

    createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(school) {
            return (angular.lowercase(school.name).indexOf(lowercaseQuery) === 0);
        };
    }

    changePupil(child, pupil) {
        var params = { id: null, user_id: null};
        if(child != null) {
            params.id = child.id;
        } else if(pupil != null) {
            params.user_id = pupil.id;
            this.PubSub.publish('teacher-pupil-selected', true);
        }

        this.API.one('header').customPOST(params, 'changeUserSetting').then((response) => {
            this.PubSub.publish('pupil-change');
            this.getProfile();
            this.$state.go('app.landing');
        });

    }

    getProfile(){
        this.API.one('user').customGET('getSummerProfile').then((response) => {
            if(response){
                this.pupil = response.profile;
                this.PubSub.publish('pupil-change-detail', this.pupil);
                // reset selected value of enrollment group on header
                this.selectedGroup = this.enrollment_groups[0].id;
            }
        });
    }

    goToTeachPortal() {
        this.API.one('header').customPOST({id: this.teacher_role_id}, 'changeUserRoleSetting').then((response) => {
            var token = window.frameElement.getAttribute('data-id');
            parent.location.href = '/?user_set_role=' + this.teacher_role_id + '&set_parent_overview=0&set_environment=web_t&token='+ token;
        });
    }

    getHeaderGroups(){
        this.API.one('enrollmentGroup').customGET('getHeaderGroups').then((response) => {
            this.enrollment_groups = response;
        });
    }

    changeEnrollmentGroup(){
        this.PubSub.publish('change-enrollment-group', this.selectedGroup);
    }

    switchMode(){
        this.view_mode = this.view_mode == true ? false : true;
        this.PubSub.publish('switch-mode', this.view_mode);
        this.$cookies.put('teacher_mode', this.view_mode);
        // auto select active child in parent mode
        if(this.view_mode != true && this.pupils.length > 0){
            this.changePupil(this.pupils[0], null);
        }else if(this.selected_user){
            // auto select active child in teacher mode
            this.changePupil(null, this.selected_user);
        }
        // auto select school if user have only role on one school
        if(this.view_mode == true && Object.keys(this.teachers).length == 1){
            var selected_school = Object.keys(this.teachers)[0];
            this.selected_school_id = selected_school;
        }
        this.$state.go('app.landing');
    }

    loadUsers(){
        this.API.one('user').customGET('getPupilBySubgroupId' , {subgroup_id: this.selected_group_id}).then((response) => {
            this.users = response.users;
        });
    }
    addPupil(){
        this.DialogService.fromTemplate('pupil-add', {
            controller: AddPupilController,
            controllerAs: 'vm',
            locals: {user:{}}
        }).then(function(response) {

        });
    }

}

export const GuardianHeaderComponent = {
    templateUrl: './views/app/components/summerschool/guardian/header/header.component.html',
    controller: GuardianHeaderController,
    controllerAs: 'vm',
    bindings: {}
}
