class TeacherEnrollmentGroupDetailsController{
    constructor(API, NgTableParams, DialogService, $stateParams, PubSub, $state){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.NgTableParams = NgTableParams;
        this.DialogService = DialogService;
        this.$stateParams = $stateParams;
        this.activity_list =  $stateParams.editing;
        this.$state = $state;
        this.PubSub = PubSub;
    }

    $onInit(){
        this.max_participants = [];
        for (var i = 0; i < 15; i++) {
            var text = i;
            if(i == 0)  {
                this.max_participants.push({text: 'Velg', value: null});

            } else {
                this.max_participants.push({text: text, value: i});
            }
        }

        this.date_opts = {
            locale: {
                separator: ' - ',
                format: 'DD.MM.YYYY',
                showWeekNumbers: true,
                cancelLabel: 'Avbryt',
                applyLabel: "OK",
                fromLabel: 'Fra',
                toLabel: 'Till',
                daysOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                monthNames: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"]
            },
            autoUpdateInput: false
        };

        this.date_time_opts = {
            locale: {
                separator: ' - ',
                showWeekNumbers: true,
                format: 'DD.MM.YYYY HH:mm',
                cancelLabel: 'Avbryt',
                applyLabel: "OK",
                fromLabel: 'Fra',
                toLabel: 'Till',
                daysOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                monthNames: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"]
            },
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true
        };

        this.model = {
            date_range : {},
            enrollment_date_range : {},
            direct_entry_date_range: {},
            withdraw_date_range: {},
            time_sending_message_range: {},
            time_publish_allocation_range: {}
        };

        this.editing = !this.model.date_activated;

        this.getEnrollmentGroup();
    }

    getEnrollmentGroup(){
        var data = {
            id: this.$stateParams.id,
            school_id: this.$stateParams.school_id
        };

        this.API.one('enrollmentGroup').customGET('getBy', data).then((response) => {
            if(response) {

                this.activities = response.activities;
                this.enrollment_group = response.enrollment_group;

                this.model = angular.extend(this.model, response.enrollment_group);
                this.model.participant_activities_maximum = this.model.participant_activities_maximum + "";

                this.model.date_range.startDate = moment(this.model.date_start * 1000);
                this.model.date_range.endDate = moment(this.model.date_end * 1000);

                this.model.enrollment_date_range.minDate = this.model.date_range.startDate;
                this.model.enrollment_date_range.startDate = this.model.enrollment_date_start ?
                    moment(this.model.enrollment_date_start * 1000) : null;
                this.model.enrollment_date_range.endDate = this.model.enrollment_date_end ?
                    moment(this.model.enrollment_date_end * 1000) : null;

                this.model.direct_entry_date_range.startDate = this.model.direct_entry_date_start ?
                    moment(this.model.direct_entry_date_start * 1000) : null;
                this.model.direct_entry_date_range.endDate = this.model.direct_entry_date_end ?
                    moment(this.model.direct_entry_date_end * 1000) : null;

                this.model.withdraw_date_range.startDate = this.model.withdraw_date_start ?
                    moment(this.model.withdraw_date_start * 1000) : null;
                this.model.withdraw_date_range.endDate = this.model.withdraw_date_end ?
                    moment(this.model.withdraw_date_end * 1000) : null;

                this.model.time_sending_message_range = this.model.time_sending_message ?
                    moment(this.model.time_sending_message * 1000) : {};

                this.model.time_publish_allocation_range = this.model.time_publish_allocation ?
                    moment(this.model.time_publish_allocation * 1000) : {};

                this.editing = !this.model.date_activated;
                this.model_bk = angular.copy(this.model);
            }
        });
    }

    update() {
        this.API.one('enrollmentGroup').customPUT(this.model, 'update').then((response) => {
            this.editing = false;
            this.getEnrollmentGroup();
            this.PubSub.publish('enrollment-group-update');
        });
    }

    delete() {
        this.DialogService.confirm('', 'Er du sikker på at du vil slette denne påmeldingsgruppen?').then(()=> {
            var data = {
                id: this.$stateParams.id,
                school_id: this.$stateParams.school_id
            };
            this.API.one('enrollmentGroup').customDELETE('delete', data).then((response) => {
                this.$state.go('app.landing');
            });
        });
    }

    archive() {
        this.DialogService.confirm('', 'Er du sikker på at du vil arkiv denne påmeldingsgruppen?').then(()=> {
            var data = {
                id: this.$stateParams.id,
                school_id: this.$stateParams.school_id
            };
            this.API.one('enrollmentGroup').customPUT(data, 'archive').then((response) => {
                this.$state.go('app.landing');
            });
        });
    }

    restore() {
        this.DialogService.confirm('', 'Er du sikker på at du vil gjenopprett denne påmeldingsgruppen?').then(()=> {
            var data = {
                id: this.$stateParams.id,
                school_id: this.$stateParams.school_id
            };
            this.API.one('enrollmentGroup').customPUT(data, 'restore').then((response) => {
                this.$state.go('app.landing');
            });
        });
    }

    formatDate(range) {
        if(range && range.startDate && range.endDate)
            return range.startDate.format("DD.MM.YYYY") + " - " + range.endDate.format("DD.MM.YYYY");
    }

    formatDateTime(date) {
        if(!angular.equals({}, date))
            return date.format("DD.MM.YYYY HH:mm");
    }

    changeEditingMode(){
        this.editing = !this.editing;
        if(!this.editing) {
            this.model = this.model_bk;
        } else {
            this.model_bk = angular.copy(this.model);

            //TODO: fix binding value to input issue
            this.model = angular.copy(this.model_bk);
        }
    }
}

export const TeacherEnrollmentGroupDetailsComponent = {
    templateUrl: './views/app/components/summerschool/teacher/enrollment-group-details/enrollment-group-details.component.html',
    controller: TeacherEnrollmentGroupDetailsController,
    controllerAs: 'vm',
    bindings: {}
}
