class TeacherOverviewEnrollmentGroupController{
    constructor(API, NgTableParams, PubSub, DialogService, $state, $rootScope){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.NgTableParams = NgTableParams;
        this.DialogService = DialogService;
        this.$state = $state;
        this.PubSub = PubSub;
        this.$rootScope = $rootScope;
    }

    $onInit(){
        this.getOverview();
        var self = this;
        this.PubSub.subscribe('academic-year-change', function (data) {
            var year = data.year;
            self.getOverview(year);
        });
    }

    edit($event, school_id, id){
        this.$state.go('app.enrollment_group', {id: id, school_id: school_id, editing: true});
        $event.stopPropagation();
    }

    getOverview(year) {
        this.enrollment_groups = [];
        var params = {
            year: year
        };
        this.API.all('enrollmentGroup').customGET("getGroups", params).then((response) => {
            if(response) {
                this.enrollment_groups = response;

                this.groupTableParams = new this.NgTableParams({
                    count: this.enrollment_groups.length,
                    filter : {
                        'enrollment_group.is_archived': false
                    }
                }, {
                    counts: [],
                    dataset: this.enrollment_groups
                });
            }
        });
    }


    filterGroup(value) {
        var filter = {};
        filter['enrollment_group.is_archived'] = value;
        angular.extend(this.groupTableParams.filter(), filter);
    }

    showRoles(type){
        this.$rootScope.dialogType = type;
        this.DialogService.fromTemplate('group-activity-role');
    }
}

export const TeacherOverviewEnrollmentGroupComponent = {
    templateUrl: './views/app/components/summerschool/teacher/enrollmentgroup/enrollmentgroup.component.html',
    controller: TeacherOverviewEnrollmentGroupController,
    controllerAs: 'vm',
    bindings: {}
}
