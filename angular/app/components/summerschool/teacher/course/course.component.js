import {AddCourseController} from '../../../../../dialogs/add-course/add-course.dialog';

class TeacherOverviewCourseController {
    constructor(API, NgTableParams,DialogService, $filter, $state, $rootScope, PubSub){
        'ngInject';

        this.API = API;
        this.NgTableParams = NgTableParams;
        this.$filter = $filter;
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.PubSub = PubSub;
        this.DialogService = DialogService;
    }

    $onInit(){
        var self = this;
        this.courses = [];
        this.getSubjects();
        this.getCourses();

        this.PubSub.subscribe('course-change', function () {
            self.getCourses();
        });
    }

    getCourses() {
        this.API.all('course').customGET('all').then((response) => {
            if(response.courses) {
                this.courses = response.courses;
                this.courses = this.courses.map(function(course){
                    course.is_archived =  course.date_deleted != null;
                    return course;
                });
                this.courseTableParams = new this.NgTableParams({
                    count: this.courses.length,
                    sorting: { name: "asc" },
                    filter : {
                        'is_archived': false
                    }
                }, {
                    counts: [],
                    dataset: this.courses
                });
            }
        });
    }

    getSubjects() {
        var self = this;
        var params = {
            course_type_id: null
        };
        this.API.all('courseType').customGET('getSubjects', params).then(function (response) {
            if(response.result) {
                self.subjects = _.keyBy(response.result, function(item) {
                    return item.code;
                });
            }
        });
    }

    showRoles(type){
        this.$rootScope.dialogType = type;
        this.DialogService.fromTemplate('group-activity-role');
    }

    selectCourse(item) {
        this.DialogService.fromTemplate('add-course', {
            controller: AddCourseController,
            controllerAs: 'vm',
            locals: {
                id: item.id,
                course_type_id: item.course_type_id
            }
        }).then(function(response) {

        });
    }

    filterGroup(value) {
        var filter = {};
        filter['is_archived'] = value;
        angular.extend(this.courseTableParams.filter(), filter);
    }
}

export const TeacherOverviewCourseComponent = {
    templateUrl: './views/app/components/summerschool/teacher/course/course.component.html',
    controller: TeacherOverviewCourseController,
    controllerAs: 'vm',
    bindings: {}
};
