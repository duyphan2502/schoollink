import {AddActivityCourseController} from './../../../../../dialogs/add-activity-course/add-activity-course.dialog';
import {DialogActivityListController} from './../../../../../dialogs/activity-list/activity-list.dialog';
import {DialogFormListController} from './../../../../../dialogs/form-list/form-list.dialog';
import {DialogEnrollmentGroupsController} from './../../../../../dialogs/enrollment-groups/enrollment-groups.dialog';

class TeacherActivityDetailController{
    constructor(API, NgTableParams, DialogService, ToastService, $filter, $rootScope, $state){
        'ngInject';

        this.API = API;
        this.lang = Lang;
        this.NgTableParams = NgTableParams;
        this.DialogService = DialogService;
        this.ToastService = ToastService;
        this.$filter = $filter;
        this.$rootScope = $rootScope;
        this.$state = $state;

        this.activityId = this.$state.params.id;
    }

    $onInit(){
        this.arrange_filter = {
            group : '',
            sex : '',
            school: ''
        };
        this.activity = {
            date_range: {},
            options: {}
        };

        this.modify_participant = false;

        this.opts = {
            autoUpdateInput: true,
            locale: {
                format: 'DD.MM.YYYY',
                cancelLabel: 'Avbryt',
                applyLabel: "OK",
                fromLabel: 'Fra',
                toLabel: 'Till',
                daysOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                monthNames: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"]
            },
        };

        this.maskOptions = {
            maskDefinitions: {
                'v': /[0-2]/,
                '9': /[0-9]/,
                'x': /[0-5]/,
                '8': /[0-9]/,
            }
        };

        this.getActivity();
        this.getSchools();

        this.weekdays = [
            { time_start: 'monday_time_start', time_end: 'monday_time_end', text: this.lang.get('label.monday')},
            { time_start: 'tuesday_time_start', time_end: 'tuesday_time_end', text: this.lang.get('label.tuesday')},
            { time_start: 'wednesday_time_start', time_end: 'wednesday_time_end', text: this.lang.get('label.wednesday')},
            { time_start: 'thursday_time_start', time_end: 'thursday_time_end', text: this.lang.get('label.thursday')},
            { time_start: 'friday_time_start', time_end: 'friday_time_end', text: this.lang.get('label.friday')},
            { time_start: 'saturday_time_start', time_end: 'saturday_time_end', text: this.lang.get('label.saturday')},
            { time_start: 'sunday_time_start', time_end: 'sunday_time_end', text: this.lang.get('label.sunday')}
        ];

        if(this.$state.params.edit_clone) {
            this.show_edit_view = true;
        }

        this.show_edit_view = (this.activity.date_activated != null);
    }

    getActivity() {
        var self = this;
        var data = {
            id: self.activityId
        };
        if(this.activityId) {
            this.API.all('activity').customGET('getBy', data).then((response) => {
                if(response) {
                    self.activity = response.activity;
                    self.participants = response.participants;

                    if(self.activity.group && self.activity.group.direct_entry_date_start) {
                        if(moment(self.activity.group.direct_entry_date_start * 1000) <= moment()) {
                            self.modify_participant = true;
                        }
                    }

                    self.activity.date_range = {
                        startDate : moment(self.activity.date_start * 1000),
                        endDate : moment(self.activity.date_end * 1000)
                    };

                    //TODO: workaround to select item in selection box
                    if(self.activity.options) {
                        self.activity.options.summer_school_school_id =  self.activity.options.summer_school_school_id + '';
                    } else {
                        self.activity.options = {
                            summer_school_school_id : ''
                        };
                    }

                    //TODO: workaround to sent params to dialog
                    this.activity.number_of_groups = this.activity.number_of_groups + '';

                    self.$state.params['course_type_id'] = self.activity.course_type_id;

                    if(self.activity.course) {
                        self.activity['course_name'] = self.activity.course.name;
                        self.activity['course_id'] = self.activity.course.id;
                    }

                    self.participants.waiting = _.orderBy(self.participants.waiting, 'position');
                    self.arrangeParticipant();
                    self.activity_bk = angular.copy(self.activity);
                }
            });
        }
    }

    arrangeParticipant() {
        if(this.participants.confirmed) {
            this.list = [];
            for(var i = 1; i <= this.activity.number_of_groups ; i++) {
                this.list[i] = [];
                for(var participant in this.participants.confirmed) {
                    if(this.participants.confirmed[participant].group_number == i) {
                        this.list[i].push(this.participants.confirmed[participant]);
                    }
                }
            }
        }
    }

    changeGroup(group, participant) {
        if(group != participant.group_number) {
            participant.group_number = group;

            var params = {
                id: this.activity.id,
                participants : [{
                    id: participant.id,
                    resource_id : participant.user_id,
                    group: group
                }]
            };

            this.API.all('activity').customPOST(params, 'changeGroupNumberOfParticipant').then((response) => {
            });
        }
    }

    changePositionWaitingList(){
        var self = this;
        if(!this.drap_drop_execution) {
            this.drap_drop_execution = 0;
        }

        this.drap_drop_execution++;
        if(this.drap_drop_execution > this.participants.waiting.length) {
            //TODO: work around to fix drop event which happen before the list is updated
            setTimeout(function(){
                var params = {
                    id: self.activity.id,
                    user_ids : self.$filter('map')(self.participants.waiting, 'user_id')
                };

                self.API.all('activity').customPOST(params, 'changePositionOfWaitingList').then((response) => {

                });
            }, 1000);

        }
    }

    distributeParticipants() {
        var participants = this.participants.confirmed;
        var participant_groups = null;

        if(this.arrange_filter.group) {
            if(this.arrange_filter.group == 'Distribute') {
                participants = this.$filter('orderBy')(participants, 'group');
            } else {
                participant_groups = this.$filter('groupBy')(participants, 'group');
            }
        }
        else if(this.arrange_filter.sex) {
            if(this.arrange_filter.sex == 'Distribute') {
                participants = this.$filter('orderBy')(participants, 'gender');
            } else {
                participant_groups = this.$filter('groupBy')(participants, 'gender');
            }
        }
        else if(this.arrange_filter.school) {
            if(this.arrange_filter.school == 'Distribute') {
                participants = this.$filter('orderBy')(participants, 'school');
            } else {
                participant_groups = this.$filter('groupBy')(participants, 'school');
            }
        }

        if(participant_groups != null) {
            participant_groups = this.$filter('toArray')(participant_groups);
            if(participant_groups.length > this.activity.number_of_groups) {
                this.ToastService.error('Add more groups');
            } else {
                for (var index in participant_groups) {
                    for(var participant of participant_groups[index]) {
                        participant.group_number = index * 1 + 1;
                    }
                }
            }
        } else {
            for (var index in participants) {
                participants[index].group_number = (index % this.activity.number_of_groups ) + 1;
            }
        }

        this.participants.confirmed = participants;

        var group_change = [];
        for(var participant of participants) {
            group_change.push({resource_id: participant.user_id, group: participant.group_number, id: participant.id});
        }

        var params = {
            id: this.activity.id,
            participants : group_change
        };

        this.API.all('activity').customPOST(params, 'changeGroupNumberOfParticipant').then((response) => {
            this.arrangeParticipant();
        });
    }

    changeNumberOfGroup() {
        var params = {
            id: this.activity.id,
            number_of_groups: this.activity.number_of_groups
        };
        this.API.all('activity').customPOST(params, 'changeNumberOfGroups').then((response) => {
            this.arrangeParticipant();
        });
    }

    toUTCOffset(date){
        return moment(date).utc().add(date.utcOffset(), 'm');
    }

    updateActivity() {
        var self = this;

        this.activity.date_range.startDate = this.$rootScope.toUTCOffset(this.activity.date_range.startDate);

        var params = {
            activity_id: this.activityId,
            activity: this.activity
        };

        this.API.all('activity').customPOST(params, 'update').then((response) => {
            if(response.result) {
                self.activity.school = self.schools[self.activity.options.summer_school_school_id];
                self.activity_bk = angular.copy(self.activity);
                self.show_edit_view = false;
            }
        });
    }

    deleteActivity() {
        this.DialogService.confirm('', 'Er du sikker på at du vil slette dette kurset?').then(()=> {
            var self = this;
            var params = {
                id: this.activityId
            };
            this.API.all('activity').customPOST(params, 'delete').then((response) => {
                if(response.result) {
                    window.history.back();
                }
            });
        });
    }

    getSchools() {
        var self = this;
        this.API.all('school').customGET('getSchoolsInMunicipality').then((response) => {
            if(response) {
                self.schools = response.schools;
            }
        });
    }

    cancelEditActivity() {
        this.show_edit_view = false;
        this.activity = angular.copy(this.activity_bk);
    }

    cloneActivity() {
        var self = this;
        this.DialogService.fromTemplate('enrollment-groups', {
            controller: DialogEnrollmentGroupsController,
            controllerAs: 'vm',
            locals: {
                course_type_id: self.activity.course_type_id,
                default_enrollment_group_id: self.activity.enrollment_group_id
            }
        }).then(function(response) {
            var params = {
                id: self.activityId,
                enrollment_group_id: response.id
            };

            self.API.all('activity').customPOST(params, 'clone').then((response) => {
                if(response.result) {
                    var activity = response.result;
                    self.$state.go('app.activity-detail', { id: activity.id, edit_clone: true });
                }
            });
        });
    }

    addCourse() {
        var self = this;
        this.$state.params.is_add_course = true;

        this.DialogService.fromTemplate('add-activity-course', {
            controller: AddActivityCourseController,
            controllerAs: 'vm',
            locals: {
                course_type_id: self.activity.course_type_id
            }
        }).then(function(response) {
            self.activity.course_id = response.id;
            self.activity.course_name = response.name;
        });
    }

    range(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    }


    querySearch (query) {
        if(query) {
            return this.API.one('user').customGET('searchParticipantInMunicipality', { text : query, id: this.activityId}).then((response) => {
                return response;
            });
        }
    }

    addParticipant() {
        if(this.selected_participant) {
            var params = {
                id: this.activity.id,
                user_id : this.selected_participant.id
            };
            this.API.one('activity').customPOST(params, 'addParticipant').then((response) => {
                if(response.result){
                    var participant = response.result;
                    if(participant.status == 'WaitingList') {
                        this.participants.waiting.push(participant);
                    } else {
                        this.participants.confirmed.push(participant);
                    }
                    this.arrangeParticipant();
                } else {
                    this.ToastService.error(response.message);
                }
            });
        }
    }

    confirmParticipant(participant) {
        this.DialogService.confirm('', 'Er du sikker på at du vil bekrefte denne deltakeren?').then(()=> {
            var params = {
                id: this.activity.id,
                user_id : participant.user_id
            };

            this.API.one('activity').customPOST(params, 'confirmParticipant').then((response) => {
                if(response.result){
                    this.participants.confirmed.push(response.result);
                    var waiting_participant = this.$filter('filter')(this.participants.waiting,
                        {user_id: response.result.user_id})[0];
                    this.participants.waiting.splice(this.participants.waiting.indexOf(waiting_participant), 1);
                    this.arrangeParticipant();
                }
            });
        });
    }

    removeParticipant(participants, participant) {
        this.DialogService.confirm('', 'Er du sikker på at du vil slette denne deltakeren?').then(()=> {
            var params = {
                id: this.activity.id,
                user_id : participant.user_id
            };

            this.API.one('activity').customPOST(params, 'removeParticipant').then((response) => {
                if(response.result){
                    participants.splice(participants.indexOf(participant), 1);
                    if(response.participant) {
                        this.participants.confirmed.push(response.participant);
                        var waiting_participant = this.$filter('filter')(this.participants.waiting,
                            {user_id: response.participant.user_id})[0];
                        this.participants.waiting.splice(this.participants.waiting.indexOf(waiting_participant), 1);
                    }
                    this.arrangeParticipant();
                }
            });
        });
    }

    moveParticipant(participants, participant) {
        var self = this;
        this.DialogService.fromTemplate('activity-list', {
            controller: DialogActivityListController,
            controllerAs: 'vm',
            locals: {
                enrollment_group_id: self.activity.enrollment_group_id
            }
        }).then(function(response) {
            if(self.activity.id != response.id) {
                var params = {
                    user_id: participant.user_id,
                    old_activity_id: self.activity.id,
                    new_activity_id: response.id
                };

                self.API.one('activity').customPOST(params, 'moveParticipant').then((response) => {
                    if(response.result)
                    {
                        participants.splice(participants.indexOf(participant), 1);

                        if (response.participant) {
                            self.participants.confirmed.push(response.participant);
                            var waiting_participant = self.$filter('filter')(self.participants.waiting,
                                {user_id: response.participant.user_id})[0];
                            self.participants.waiting.splice(self.participants.waiting.indexOf(waiting_participant), 1);
                        }
                        self.arrangeParticipant();
                    }
                });
            }
        });
    }

    sendForm(){
        this.DialogService.fromTemplate('form-list', {
            controller: DialogFormListController,
            controllerAs: 'vm',
            locals: {
                school_id: this.activity.school_id
            }
        }).then(function(response) {

        });
    }

    changeTime(item, type) {
        if(this.activity[item.time_start] && this.activity[item.time_end]) {
            var timeEnd = this.activity[item.time_end].split(':'),
                timeStart = this.activity[item.time_start].split(':');

            if(timeStart[0]*1 > timeEnd[0]*1) {
                this.ToastService.error(this.lang.get('message.timestart_cannot_greater_than_timeend'));
                if(type == 'start') {
                    this.activity[item.time_start] = '';
                } else {
                    this.activity[item.time_end] = '';
                }
            }

            if(timeStart[0]*1 == timeEnd[0]*1) {
                if(timeStart[1]*1 > timeEnd[1]*1) {
                    this.ToastService.error(this.lang.get('message.timestart_cannot_greater_than_timeend'));
                    if(type == 'start') {
                        this.activity[item.time_start] = '';
                    } else {
                        this.activity[item.time_end] = '';
                    }
                }
            }
        }
    }

    formatDate(range) {
        if(range && range.startDate && range.endDate) {
            return range.startDate.format("DD.MM.YYYY") + " - " + range.endDate.format("DD.MM.YYYY") + " (" + this.lang.get('label.week') + " " + range.startDate.format('W') + ")";
        }
    }
}

export const TeacherActivityDetailComponent = {
    templateUrl: './views/app/components/summerschool/teacher/activity-details/activity-detail.component.html',
    controller: TeacherActivityDetailController,
    controllerAs: 'vm',
    bindings: {}
}

Date.prototype.toJSON = function(){ return moment(this).format(); }
