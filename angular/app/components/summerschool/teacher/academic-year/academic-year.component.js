class TeacherAcademicYearController {
    constructor(API, $state, PubSub){
        'ngInject';

        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
        this.$state = $state;
        this.academicYear = moment().year() + '';
    }

    $onInit(){
        this.years = this.generateAcademicYears(3);
        this.changeAcademicYear(moment().year());
    }

    changeAcademicYear() {
        this.selectedYear = !this.academicYear ? 0 : this.academicYear;
        this.PubSub.publish('academic-year-change', {year: this.selectedYear});
    }

    generateAcademicYears(max) {
        var years = [];
        var currentYear = moment().year();

        for(var i = 0; i < max; i++) {
            years.push(currentYear - i);
        }

        return years;
    }
}

export const TeacherAcademicYearComponent = {
    templateUrl: './views/app/components/summerschool/teacher/academic-year/academic-year.component.html',
    controller: TeacherAcademicYearController,
    controllerAs: 'vm',
    bindings: {}
};
