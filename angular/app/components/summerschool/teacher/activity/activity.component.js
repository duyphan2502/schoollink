import {AddActivityCourseController} from './../../../../../dialogs/add-activity-course/add-activity-course.dialog';

class TeacherOverviewActivityController{
    constructor(API, NgTableParams, DialogService, $stateParams, $state){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.NgTableParams = NgTableParams;
        this.DialogService = DialogService;
        this.enrollment_group_id = $stateParams.id;
        this.school_id = $stateParams.school_id;
        this.$state = $state;
    }

    $onInit(){
        this.getOverview();
    }

    getOverview() {
        this.activities = [];

        this.API.all('activity').customGET("getActivitiesByEnrollmentGroupId", {enrollment_group_id: this.enrollment_group_id}).then((response) => {
            if(response) {
                this.activities = response.activities;
                this.enrollment_group = response.enrollment_group;
                this.activityTableParams = new this.NgTableParams({
                    count: this.activities.length,
                    sorting: { 'activity.name' : "asc" },
                    filter : {
                        'school.id': ''
                    }
                }, {
                    counts: [],
                    dataset: this.activities
                });

                this.initFilter();
            }
        });
    }

    addActivity(){
        var self = this;
        this.DialogService.fromTemplate('add-activity-course', {
            controller: AddActivityCourseController,
            controllerAs: 'vm',
            locals: {
                course_type_id: self.enrollment_group.course_type_id
            }
        }).then(function(response) {
            self.API.all('activity').customPOST({ enrollment_group_id: self.enrollment_group_id , course_id : response.id}, 'add')
                .then((response) => {
                if(response.id) {
                    self.$state.go('app.activity-detail', { id: response.id, school_id: self.school_id});
                }
            });
        });

    }

    showDetail(id, course_type_id) {
        this.$state.go('app.activity-detail', {id: id, course_type_id: course_type_id});
    }

    initFilter(){
        this.schoolFilters = {};
        this.courseFilters = {};
        this.weekFilters = {};
        this.yearGradeFilters = {};

        for (var i = 1; i < 14; i++) {
            this.yearGradeFilters[i] = {
                year_grade: i,
                count: 0
            };
        }

        for (var activity of this.activities) {
            if (activity.summer_school.school) {
                if (this.schoolFilters[activity.summer_school.school.id] === undefined) {
                    this.schoolFilters[activity.summer_school.school.id] = {
                        school: activity.summer_school.school,
                        name: activity.summer_school.school.name,
                        count: 0
                    };
                }
                this.schoolFilters[activity.summer_school.school.id].count++;
            } else {
                activity.summer_school.school = {
                    id : -1,
                    name: 'Ingen skole'
                };
                if (this.schoolFilters[0] === undefined) {
                    this.schoolFilters[0] = {
                        school: activity.summer_school.school,
                        name: activity.summer_school.school.name,
                        count: 0
                    };
                }
                this.schoolFilters[0].count++;
            }

            if (activity.course) {
                if (this.courseFilters[activity.course.id] === undefined) {
                    this.courseFilters[activity.course.id] = {
                        course: activity.course,
                        name: activity.course.name,
                        count: 0
                    };
                }
                activity.activity.year_grade = [];
                if(activity.course.year_grade_minimum && activity.course.year_grade_maximum) {
                    for (var year = activity.course.year_grade_minimum ; year <= activity.course.year_grade_maximum; year++) {
                        this.yearGradeFilters[year].count++;
                        activity.activity.year_grade[year] = year;
                    }
                }
                this.courseFilters[activity.course.id].count++;
            } else {
                activity.course = {
                    id: -1
                };
                if (this.courseFilters[0] === undefined) {
                    this.courseFilters[0] = {
                        course: activity.course,
                        count: 0
                    };
                }
                this.courseFilters[0].count++;
            }
            activity.activity.week_start = moment(activity.activity.date_start * 1000).isoWeek();
            activity.activity.week = [];
            var year = moment(activity.activity.date_start * 1000).year();
            var week_start = moment(activity.activity.date_start * 1000).isoWeek();
            var week_end = moment(activity.activity.date_end * 1000).isoWeek();
            for (var i = week_start; i <= week_end; i++) {
                if (this.weekFilters[year + '_' + i] === undefined) {
                    this.weekFilters[year + '_' + i] = {
                        year: year,
                        week: i,
                        count: 0
                    };
                }
                activity.activity.week[i] = i;
                this.weekFilters[year + '_' + i].count++;
            }
        };

    }

    changeFilter(field, value) {
        var filter = {};
        filter[field] = value;

        angular.extend(this.activityTableParams.filter(), filter);
    }

    calculateActivityFieldTotal(data, field) {
        var result = 0;

        if(data && data.length > 0) {
            for (var activity of data) {
                result += activity.summer_school[field];
            }
        }
        return result;    
    }


    activeToggle($event, activity) {
        var data = {
            id : activity.id
        };

        this.API.one('enrollmentGroup').customPUT(data, 'toggleActivity').then((response) => {
            if(response.result) {
                activity.active = !activity.active;
            }
        });
        $event.stopPropagation();
    }
}

export const TeacherOverviewActivityComponent = {
    templateUrl: './views/app/components/summerschool/teacher/activity/activity.component.html',
    controller: TeacherOverviewActivityController,
    controllerAs: 'vm',
    bindings: {}
};
