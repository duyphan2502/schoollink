class TeacherSimulationActivityController{
    constructor(API, NgTableParams, DialogService, PubSub, $stateParams, $filter){
        'ngInject';

        this.API = API;
        this.lang = Lang;
        this.NgTableParams = NgTableParams;
        this.DialogService = DialogService;
        this.PubSub = PubSub;
        this.$stateParams = $stateParams;
        this.$filter = $filter;
    }

    $onInit(){
        var self = this;
        this.getSimulateDistribution();
        this.PubSub.subscribe('enrollment-group-update', function(){
            self.simulationDistribution();
        });
    }

    initFilter(){
        this.schoolFilters = {};
        this.courseFilters = {};
        this.weekFilters = {};
        this.yearGradeFilters = {};

        for (var i = 1; i < 14; i++) {
            this.yearGradeFilters[i] = {
                year_grade: i,
                count: 0
            };
        }

        for (var activity of this.activities) {

            if (activity.school) {
                if (this.schoolFilters[activity.school.id] === undefined) {
                    this.schoolFilters[activity.school.id] = {
                        school: activity.school,
                        name: activity.school.name,
                        count: 0
                    };
                }
                this.schoolFilters[activity.school.id].count++;
            } else {
                activity.school = {
                    id : -1,
                    name: 'Ingen skole'
                };
                if (this.schoolFilters[0] === undefined) {
                    this.schoolFilters[0] = {
                        school: activity.school,
                        name: activity.school.name,
                        count: 0
                    };
                }
                this.schoolFilters[0].count++;
            }

            if (activity.course) {
                if (this.courseFilters[activity.course.id] === undefined) {
                    this.courseFilters[activity.course.id] = {
                        course: activity.course,
                        name: activity.course.name,
                        count: 0
                    };
                }
                activity.activity.year_grade = [];
                if(activity.course.year_grade_minimum && activity.course.year_grade_maximum) {
                    for (var year = activity.course.year_grade_minimum ; year <= activity.course.year_grade_maximum; year++) {
                        this.yearGradeFilters[year].count++;
                        activity.activity.year_grade[year] = year;
                    }
                }
                this.courseFilters[activity.course.id].count++;
            } else {
                activity.course = {
                    id: -1
                };
                if (this.courseFilters[0] === undefined) {
                    this.courseFilters[0] = {
                        course: activity.course,
                        count: 0
                    };
                }
                this.courseFilters[0].count++;
            }
            activity.activity.week_start = moment(activity.activity.date_start * 1000).isoWeek();
            activity.activity.week = [];
            var year = moment(activity.activity.date_start * 1000).year();
            var week_start = moment(activity.activity.date_start * 1000).isoWeek();
            var week_end = moment(activity.activity.date_end * 1000).isoWeek();
            for (var i = week_start; i <= week_end; i++) {
                if (this.weekFilters[year + '_' + i] === undefined) {
                    this.weekFilters[year + '_' + i] = {
                        year: year,
                        week: i,
                        count: 0
                    };
                }
                activity.activity.week[i] = i;
                this.weekFilters[year + '_' + i].count++;
            }
        };

    }

    changeFilter(field, value) {
        var filter = {};
        filter[field] = value;

        angular.extend(this.activityTableParams.filter(), filter);
        this.calculateSum();
    }

    getSimulateDistribution(){
        var data = {
            id: this.$stateParams.id,
            school_id: this.$stateParams.school_id
        };

        this.API.one('enrollmentGroup').customPOST(data, 'getSimulateDistribution').then((response) => {
            if(response){
                this.activities = response.activities;
                this.enrollment_group = response.enrollment_group;

                this.activities = this.activities.map(function(item){
                    item.name = item.activity.name;
                    item.activity.participants_maximum = item.activity.options.summer_school_participants_maximum;

                    if(item.activity.options.summer_school_participants_limit)
                        item.activity.participants_maximum += item.activity.options.summer_school_participants_limit;

                    item.place_filled = !_.isEmpty(item.simulation) ? item.simulation.allocated / item.activity.participants_maximum * 100: 0;

                    return item;
                });

                $('.rowspan').attr('rowspan', 2);
                $('.colspan').attr('colspan', this.enrollment_group.maximum_course_random_registration);

                this.activityTableParams = new this.NgTableParams({
                    count: this.activities.length,
                    sorting: { 'activity.name' : "asc" }
                }, {
                    counts: [],
                    dataset: this.activities
                });

                if(this.enrollment_group.enrollment_date_start) {
                    if(moment(this.enrollment_group.enrollment_date_end * 1000) < moment() && moment(this.enrollment_group.direct_entry_date_start * 1000) > moment()) {
                        this.can_lock_allocation = true;
                    }
                }
                this.initFilter();
                this.calculateSum();
            }
        });
    }

    simulationDistribution() {
        var data = {
            id: this.$stateParams.id,
            school_id: this.$stateParams.school_id
        };

        this.API.one('enrollmentGroup').customPOST(data, 'simulateDistribution').then((response) => {
            if(response){
                this.activities = response.activities;
                this.enrollment_group = response.enrollment_group;

                this.activities = this.activities.map(function(item){
                    item.name = item.activity.name;
                    item.place_filled = item.simulation ? item.simulation.allocated / item.activity.options.summer_school_participants_maximum * 100: 0;
                    return item;
                });

                $('.rowspan').attr('rowspan', 2);
                $('.colspan').attr('colspan', this.enrollment_group.maximum_course_random_registration);

                this.activityTableParams = new this.NgTableParams({
                    count: this.activities.length,
                    sorting: { 'activity.name' : "asc" }
                }, {
                    counts: [],
                    dataset: this.activities
                });

                if(this.enrollment_group.enrollment_date_start) {
                    if(moment(this.enrollment_group.enrollment_date_end * 1000) < moment() && moment(this.enrollment_group.direct_entry_date_start * 1000) > moment()) {
                        this.can_lock_allocation = true;
                    }
                }
                this.initFilter();
                this.calculateSum();
            }
        });
    }

    saveNote(activity) {
        var data = {
            note: activity.notes,
            id: activity.id
        };

        this.API.one('enrollmentGroup').customPUT(data, 'saveNote').then((response) => {
            if(response.result) {
                activity.is_updated = false;
            }
        });
    }

    range(n) {
        return _.range(1, n * 1 + 1);
    }

    calculateSum() {
        var filters = {};
        angular.forEach(this.activityTableParams.filter(), function(val,key){
            var filter = filters;
            var parts = key.split('.');
            for (var i=0;i<parts.length;i++){
                if (i!=parts.length-1) {
                    filter[parts[i]] = {};
                    filter = filter[parts[i]];
                }
                else {
                    filter[parts[i]] = val;
                }
            }
        });

        var data = filters ?
            this.$filter('filter')(this.activities, filters) :
            this.activities;

        if(data) {
            this.total = {
                participants_maximum: 0,
                confirmed: 0,
                allocated: 0,
                levels: {},
                levels_unallocable_due_to_participant_limit: {}
            };

            for(var level = 1; level <= 5 ; level++) {
                if (!this.total.levels[level])
                    this.total.levels[level] = 0;

                if (!this.total.levels_unallocable_due_to_participant_limit[level])
                    this.total.levels_unallocable_due_to_participant_limit[level] = 0;
            }

            for(var activity_index in data) {
                var activity = data[activity_index];
                if(activity.simulation) {
                    var participants_maximum = activity.activity.options.summer_school_participants_maximum,
                        simulation = activity.simulation;
                    this.total.participants_maximum += participants_maximum;
                    this.total.confirmed += simulation.confirmed;
                    this.total.allocated += simulation.allocated;
                    for(var level = 1; level <= 5 ; level++) {
                        this.total.levels[level] += simulation.levels[level] ? simulation.levels[level] : 0;

                        this.total.levels_unallocable_due_to_participant_limit[level] +=
                            simulation.levels_unallocable_due_to_participant_limit[level] ?
                                simulation.levels_unallocable_due_to_participant_limit[level] : 0;
                    }
                }
            }
        }
    }

    confirmApplication() {
        this.DialogService.confirm('', 'Er du sikker på at du vil låse tildelinger?').then(() => {
            var data = {
                id: this.$stateParams.id,
                school_id: this.$stateParams.school_id
            };
            this.API.one('enrollmentGroup').customPOST(data, 'confirm').then((response) => {
                this.simulationDistribution();
            });
        });
    }

    changeParticipantMaximum(activity) {
        activity.editing = false;
        var maximum = activity.activity.maximum;
        if(activity.activity.options.summer_school_participants_limit) {
            maximum -= activity.activity.options.summer_school_participants_limit;
        }
        var data = {
            id: activity.activity.id,
            maximum: maximum
        };

        this.API.one('activity').customPOST(data, 'updateParticipantMaximum').then((response) => {
            if(response.result) {
                activity.activity.options.summer_school_participants_maximum = maximum;
                activity.activity.participants_maximum = activity.activity.maximum;
            }
        });
    }

    editParticipantMaximum(activity) {
        activity.editing = true;
        activity.activity.maximum = activity.activity.participants_maximum;
    }
}

export const TeacherSimulationActivityComponent = {
    templateUrl: './views/app/components/summerschool/teacher/simulation-activity/simulation-activity.component.html',
    controller: TeacherSimulationActivityController,
    controllerAs: 'vm',
    bindings: {}
}
