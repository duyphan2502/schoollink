import {AddActivityCourseController} from './../../dialogs/add-activity-course/add-activity-course.dialog';

export class DksAddActivityCourseController extends AddActivityCourseController {
    getCourses() {
        var params = {
            id: this.course_type_id
        };

        this.API.all('dks-course').customGET('getByCourseTypeId', params).then((response) => {
            if(response.courses) {
                this.courses = response.courses;
            }
        });
    }
}

