import {DialogEnrollmentGroupsController} from './../../dialogs/enrollment-groups/enrollment-groups.dialog';

export class DksDialogEnrollmentGroupsController extends DialogEnrollmentGroupsController {
    getEnrollmentGroups() {
        var self = this;
        var params = {
            id: this.course_type_id
        };

        this.API.all('dks-enrollmentGroup').customGET('getByCourseTypeId', params).then((response) => {
            if(response.result) {
                self.groups = response.result;
                self.getSelectedEnrollmentGroup(response.result);
            }
        });
    }
}

