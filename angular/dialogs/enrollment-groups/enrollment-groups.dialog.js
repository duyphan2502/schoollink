export class DialogEnrollmentGroupsController {
    constructor(DialogService, API, $state, course_type_id, default_enrollment_group_id){
        'ngInject';

        this.API = API;
        this.DialogService = DialogService;
        this.$state = $state;
        this.lang = Lang;

        this.course_type_id = course_type_id;
        this.default_enrollment_group_id = default_enrollment_group_id;

        this.init();
    }

    init() {
        this.selected_enrollment_group = {};

        this.getEnrollmentGroups();
    }

    selectEnrollmentGroup(group) {
        this.selected_enrollment_group = group;
    }

    clone() {
        this.DialogService.hide(this.selected_enrollment_group);
    }

    getEnrollmentGroups() {
        var self = this;
        var params = {
            id: this.course_type_id
        };

        this.API.all('enrollmentGroup').customGET('getByCourseTypeId', params).then((response) => {
            if(response.result) {
                self.groups = response.result;
                self.getSelectedEnrollmentGroup(response.result);
            }
        });
    }

    getSelectedEnrollmentGroup(groups) {
        var self = this;
        groups.forEach(item => {
           if(item.id == self.default_enrollment_group_id) {
               self.selected_enrollment_group = item;
               return;
           }
        });
    }

    hide(){
        this.DialogService.cancel();
    }
}

