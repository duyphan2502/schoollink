export class AddPupilController {
    constructor(API, PubSub, DialogService, $filter, user){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
        this.DialogService = DialogService;
        this.$filter = $filter;
        this.pupil = user;
        this.init();
    }

    init(){
        this.pupil.birthdate = {
            endDate: moment().add(1, 'days'),
            startDate: moment()
        };
        if(this.pupil.firstname) {
            this.edit_firstname = false;
        }
        if(this.pupil.surname) {
            this.edit_surname = false;
        }
        this.opts = {
            autoUpdateInput: true,
            singleDatePicker: true,
            locale: {
                format: 'DD.MM.YYYY',
                cancelLabel: 'Avbryt',
                applyLabel: "OK",
                fromLabel: 'Fra',
                toLabel: 'Till',
                daysOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                monthNames: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"]
            }
        };
        this.getSchools();

    }
    addPupil(){
        var data = {
            firstname: this.pupil.firstname,
            surname: this.pupil.surname,
            postal_code: this.pupil.postal_code,
            postal_name: this.pupil.postal_name,
            mobilephone: this.pupil.mobilephone,
            address: this.pupil.address,
            email: this.pupil.email,
            birthdate: this.pupil.birthdate.startDate,
            school: this.pupil.school,
            class: this.pupil.class
        };
        if(this.pupil.id){
            data.id = this.pupil.id;
            this.API.one('user').customPUT(data, 'firstUpdatePupil').then((response) => {
                if(response.result == true){
                    this.PubSub.publish('update-pupil');
                    this.hide();
                }
            });
        }else {
            this.API.one('user').customPOST(data, 'addPupil').then((response) => {
                if(response.result == true){
                    this.PubSub.publish('update-pupil');
                    this.hide();
                }
            });
        }

    }
    getSchools(){
        this.API.one('school').customGET('getSchoolsInMunicipality').then((response) => {
            this.schools = this.$filter('to_array')(response.schools);
        });
    }
    changeSchool(){
        var data = {id: this.pupil.school ? this.pupil.school.id : 'none'};
        this.API.one('school').customPOST(data, 'getSubgroupSchool').then((response) => {
            this.pupil.class = '';
            this.subgroups = response.subgroups;
        });
    }
    querySearch (query) {
        var results = query ? this.schools.filter(this.createFilterFor(query)) : this.schools;
        return results;
    }
    createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(school) {
            return (angular.lowercase(school.name).indexOf(lowercaseQuery) === 0);
        };
    }
    hide(){
        this.DialogService.cancel();
    }
}
