import {AddPupilController} from '../pupil-add/pupil-add.dialog';
import {GuardianProfileController} from '../guardian-profile/guardian-profile.dialog';
export class SelectRoleController {
    constructor(DialogService, API, $state, user){
        'ngInject';

        this.API = API;
        this.DialogService = DialogService;
        this.lang = Lang;
        this.$state = $state;
        this.user = user;

        this.init();
    }

    init() {

    }
    goAddPupil(guardian){
        var data = {user:{}};
        if(guardian == false){
            data.user = this.user;
        }
        this.DialogService.fromTemplate('pupil-add', {
            controller: AddPupilController,
            controllerAs: 'vm',
            locals: data
        }).then(function(response) {

        });
        this.hide();
    }
    guardianProfile(){
        var data = {};
        data.user = this.user;
        this.DialogService.fromTemplate('guardian-profile', {
            controller: GuardianProfileController,
            controllerAs: 'vm',
            locals: data
        }).then(function(response) {
        });
    }
    hide(){
        this.DialogService.cancel();
    }
}

