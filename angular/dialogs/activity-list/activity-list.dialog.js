export class DialogActivityListController {
    constructor(DialogService, API, NgTableParams, enrollment_group_id) {
        'ngInject';

        this.API = API;
        this.lang = Lang;
        this.DialogService = DialogService;
        this.NgTableParams = NgTableParams;
        this.enrollment_group_id = enrollment_group_id;

        this.init();
    }

    init() {
        this.getActivities();
    }

    getActivities() {
        this.API.all('activity').customGET("getActivitiesByEnrollmentGroupId", {enrollment_group_id: this.enrollment_group_id}).then((response) => {
            if(response) {
                this.activities = response.activities;

                this.activityTableParams = new this.NgTableParams({
                    count: this.activities.length,
                    filter : {
                    }
                }, {
                    counts: [],
                    dataset: this.activities
                });
            }
        });
    }

    selectActivity(activity){
        this.DialogService.hide(activity.activity);
    }

    hide(){
        this.DialogService.cancel();
    }
}

