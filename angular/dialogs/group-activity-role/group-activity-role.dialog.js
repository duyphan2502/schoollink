import {AddCourseController} from './../../dialogs/add-course/add-course.dialog';

export class GroupActivityRoleController{
    constructor(DialogService, API, $rootScope, $state){
        'ngInject';
        
        this.API = API;
        this.lang = Lang;
        this.DialogService = DialogService;
        this.type = $rootScope.dialogType;
        this.$state = $state;
        this.getRoles();
    }

    getRoles() {
        this.API.all('courseType').customGET("getCourseTypes").then((response) => {
            this.roles = response.roles;
            this.course_types = response.types;
        });
    }

    selectRole(){
        var school_id = this.active_role.split(":")[1];
        this.active_types = [];
        for (var type of this.course_types) {
            if(type.school_id == school_id) {
                this.active_types.push(type);
            }
        }

        if(this.active_types.length > 0) {
            this.selected_type = this.active_types[0].id;
        }
    }

    save(){
        this.DialogService.hide();
        if(this.type == 1) {
            this.API.all('enrollmentGroup').customPOST({ role: this.active_role , type: this.selected_type}, 'add').then((response) => {
                if(response.id) {
                    this.$state.go('app.enrollment_group', {id: response.id, school_id: this.active_role.split(":")[1]});
                }
            });
        } else if (this.type == 2) {
            this.API.all('activity').customPOST({ role: this.active_role, type: this.selected_type }, 'add').then((response) => {
                if(response.id) {
                    this.$state.go('app.activity-detail', { id: response.id, school_id: this.active_role.split(":")[1]});
                }
            });
        } else if (this.type = 3) {
            this.API.all('course').customPOST({ role: this.active_role, type: this.selected_type }, 'add').then((response) => {
                if(response.id) {
                    //this.$state.go('app.course-detail', { id: response.id, course_type_id: this.selected_type});
                    this.DialogService.fromTemplate('add-course', {
                        controller: AddCourseController,
                        controllerAs: 'vm',
                        locals: {
                            id: response.id,
                            course_type_id: this.selected_type
                        }
                    }).then(function(response) {

                    });
                }
            });
        }
    }

    hide(){
        this.DialogService.cancel();
    }
}

