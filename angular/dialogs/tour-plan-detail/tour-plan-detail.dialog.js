export class DialogTourPlanDetailController {
    constructor(DialogService, API, $state, activity_id){
        'ngInject';

        this.API = API;
        this.DialogService = DialogService;
        this.lang = Lang;
        this.$state = $state;

        this.activityId = activity_id;

        this.init();
    }

    init() {
        this.activity = {};
        this.opts = {
            autoUpdateInput: true,
            singleDatePicker: true,
            locale: {
                format: 'DD.MM.YYYY',
                cancelLabel: 'Avbryt',
                applyLabel: "OK",
                fromLabel: 'Fra',
                toLabel: 'Till',
                daysOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                monthNames: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"]
            },
        };

        this.maskOptions = {
            maskDefinitions: {
                'v': /[0-2]/,
                '9': /[0-9]/,
                'x': /[0-5]/,
                '8': /[0-9]/,
            }
        };

        this.getActivity();
        this.getSchools();
    }

    getActivity() {
        var self = this;

        if(this.activityId) {
            var data = {
                id: self.activityId
            };

            this.API.all('activity').customGET('getBy', data).then((response) => {
                if(response.result) {
                    self.activity = response.result;

                    //TODO: workaround to select item in selection box
                    if(self.activity.options) {
                        self.activity.options.summer_school_school_id =  self.activity.options.summer_school_school_id + '';
                    } else {
                        self.activity.options = {
                            summer_school_school_id : ''
                        };
                    }

                    self.activity.options.dks_school_tour_date = moment(self.activity.options.dks_school_tour_date * 1000);
                }
            });
        }
    }

    submit() {
        var self = this;

        if(this.activityId) {
            var params = {
                activity_id: this.activityId,
                activity: this.activity
            };

            this.API.all('dks-activity').customPOST(params, 'update').then((response) => {
                if(response.result) {
                    self.activity.school = self.schools[self.activity.options.summer_school_school_id];
                    self.activity_bk = angular.copy(self.activity);
                    self.show_edit_view = false;

                    self.cancel();
                }
            });
        } else {
            var params = {
                activity_id: this.activityId,
                activity: this.activity
            };

            this.API.all('dks-activity').customPOST(params, 'add').then((response) => {
                if(response.result) {
                    self.activity.school = self.schools[self.activity.options.summer_school_school_id];
                    self.activity_bk = angular.copy(self.activity);
                    self.show_edit_view = false;

                    self.cancel();
                }
            });
        }
    }

    getSchools() {
        var self = this;
        this.API.all('school').customGET('getSchoolsInMunicipality').then((response) => {
            if(response) {
                self.schools = response.schools;
            }
        });
    }

    changeTime(item, type) {
        if(this.activity.options.dks_school_tour_start_time && this.activity.options.dks_school_tour_end_time) {
            var timeEnd = this.activity.options.dks_school_tour_end_time.split(':'),
                timeStart = this.activity.options.dks_school_tour_start_time.split(':');

            if(timeStart[0]*1 > timeEnd[0]*1) {
                this.ToastService.error(this.lang.get('message.timestart_cannot_greater_than_timeend'));
                if(type == 'start') {
                    this.activity.options.dks_school_tour_start_time = '';
                } else {
                    this.activity.options.dks_school_tour_end_time = '';
                }
            }

            if(timeStart[0]*1 == timeEnd[0]*1) {
                if(timeStart[1]*1 > timeEnd[1]*1) {
                    this.ToastService.error(this.lang.get('message.timestart_cannot_greater_than_timeend'));
                    if(type == 'start') {
                        this.activity.options.dks_school_tour_start_time = '';
                    } else {
                        this.activity.options.dks_school_tour_end_time = '';
                    }
                }
            }
        }
    }

    range(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    }

    cancel(){
        this.DialogService.cancel();
    }
}

