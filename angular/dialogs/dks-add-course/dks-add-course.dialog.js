import  {AddCourseController} from './../../dialogs/add-course/add-course.dialog';

export class DksAddCourseController extends AddCourseController{
    init() {
        this.opts = {
            autoUpdateInput: true,
            locale: {
                format: 'DD.MM.YYYY',
                cancelLabel: 'Avbryt',
                applyLabel: "OK",
                fromLabel: 'Fra',
                toLabel: 'Till',
                daysOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                monthNames: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"]
            },
        };

        this.model = {
            date_range : {},
            course_type_id: this.course_type_id,
            options: {}
        };


        if(this.id) {
            this.getCourse();
        }

        this.getSubjects();
        this.getPosts();
        this.generateSemester();
    }

    generateSemester(){
        let self = this;
        let nextThreeYear = moment().add(3, 'year').year();
        let yearRange = _.range(moment().year(), nextThreeYear);
        this.semesters = [];
        for(let year of yearRange) {
            this.semesters.push({label: self.lang.get('label.spring') + ' ' + year, value: 1 + '-' + year});
            this.semesters.push({label: self.lang.get('label.autumn') + ' ' + year, value: 2 + '-' + year});
        }
    }

    setSemester() {
        if(this.model.options.dks_semester) {
            this.semester = this.findSemester(this.model.options.dks_semester);
        } else {
            this.semester = this.semesters[0];
        }
    }

    findSemester(semester) {
        for(let item of this.semesters) {
            if(item.value == semester) {
                return item;
            }
        }
    }

    getCourse() {
        let self = this;
        let data = {
            id: this.id
        };

        this.API.all('course').customGET('getBy', data).then((response) => {
            if(response.course) {
                self.model = response.course;
                self.model.post_detail_id = self.model.post_detail_id + '';

                self.model.options.dks_start_date = moment(self.model.options.dks_start_date * 1000);
                self.model.options.dks_end_date = moment(self.model.options.dks_end_date * 1000);

                self.setSemester();
                self.show_add = true;
            }
        });
    }

    save(){
        let self = this;
        if(this.id) {
            this.model.options.semester = this.semester.value;

            let params = {
                course_id: this.id,
                course: this.model
            };

            self.convertSemesterToDateRange();

            this.API.all('dks-course').customPOST(params, 'update').then((response) => {
                if(response.id) {
                    self.hide();
                    this.PubSub.publish('course-change');
                }
            });
        }
    }

    convertSemesterToDateRange() {
        let data = this.semester.value.split('-');
        let year = data[1];
        let season = data[0];

        if(season == 1) {
            this.model.options.dks_start_date =  this.$rootScope.toUTCOffset(moment(year + "-1-1", "YYYY-MM-DD"));
            this.model.options.dks_end_date = this.$rootScope.toUTCOffset(moment(year + "-7-31", "YYYY-MM-DD"));
        } else {
            this.model.options.dks_start_date = this.$rootScope.toUTCOffset(moment(year + "-8-1", "YYYY-MM-DD"));
            this.model.options.dks_end_date = this.$rootScope.toUTCOffset(moment(year + "-12-31", "YYYY-MM-DD"));
        }
    }
}

