export class AddActivityCourseController {
    constructor(DialogService, API, $state, course_type_id){
        'ngInject';

        this.API = API;
        this.DialogService = DialogService;
        this.$state = $state;
        this.course_type_id = course_type_id;

        this.init();
    }

    init() {
        this.getCourses();
    }

    selectCourse(course) {
        this.DialogService.hide(course);
    }

    getCourses() {
        var params = {
            id: this.course_type_id
        };

        this.API.all('course').customGET('getByCourseTypeId', params).then((response) => {
            if(response.courses) {
                this.courses = response.courses;
            }
        });
    }

    hide(){
        this.DialogService.cancel();
    }
}

