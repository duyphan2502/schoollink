import {AddPupilController} from '../pupil-add/pupil-add.dialog';
export class GuardianProfileController {
    constructor(API, PubSub, DialogService, $filter, user){
        'ngInject';
        this.API = API;
        this.lang = Lang;
        this.PubSub = PubSub;
        this.DialogService = DialogService;
        this.$filter = $filter;
        this.guardian = user;
        console.log(user);
        this.init();
    }

    init(){


    }
    updateGuardian(){
        var data = {
            firstname: this.guardian.firstname,
            surname: this.guardian.surname,
            postal_code: this.guardian.postal_code,
            postal_name: this.guardian.postal_name,
            mobilephone: this.guardian.mobilephone,
            address: this.guardian.address,
            email: this.guardian.email,
        };
        if(this.guardian.id){
            data.id = this.guardian.id;
            this.API.one('user').customPUT(data, 'firstUpdateGuardian').then((response) => {
                if(response.result == true){
                    var data = {user:{}};
                    this.DialogService.fromTemplate('pupil-add', {
                        controller: AddPupilController,
                        controllerAs: 'vm',
                        locals: data
                    }).then(function(response) {});
                    this.hide();
                }
            });
        }

    }
    hide(){
        this.DialogService.cancel();
    }
}
