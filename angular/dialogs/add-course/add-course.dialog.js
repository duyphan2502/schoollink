export class AddCourseController {
    constructor(DialogService, API, $state,ToastService,PubSub, $rootScope, id, course_type_id){
        'ngInject';

        this.API = API;
        this.DialogService = DialogService;
        this.ToastService = ToastService;
        this.$state = $state;
        this.$rootScope = $rootScope;
        this.lang = Lang;
        this.PubSub = PubSub;

        this.id = id;
        this.course_type_id = course_type_id;

        this.init();
    }

    init() {
        this.opts = {
            autoUpdateInput: true,
            locale: {
                format: 'DD.MM.YYYY',
                cancelLabel: 'Avbryt',
                applyLabel: "OK",
                fromLabel: 'Fra',
                toLabel: 'Till',
                daysOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                monthNames: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"]
            },
        };

        this.model = {
            date_range : {},
            course_type_id: this.course_type_id
        };


        if(this.id) {
            this.getCourse();
        }

        this.getSubjects();
        this.getPosts();
    }

    selectCourse(course) {
        this.DialogService.hide(course);
    }

    getCourse() {
        var self = this;
        var data = {
            id: this.id
        };

        this.API.all('course').customGET('getBy', data).then((response) => {
            if(response.course) {
                self.model = response.course;
                self.model.post_detail_id = self.model.post_detail_id + '';
                self.model.date_range = {
                    startDate : moment(self.model.options.summer_school_birthdate_minimum * 1000),
                    endDate : moment(self.model.options.summer_school_birthdate_maximum  * 1000)
                };
                self.show_add = true;
            }
        });
    }

    getSubjects() {
        var self = this;
        var params = {
            course_type_id: this.course_type_id
        };
        this.API.all('courseType').customGET('getSubjects', params).then(function (response) {
            if(response) {
                self.subjects = response.result;
            }
        });
    }

    getPosts() {
        var self = this;

        this.API.all('post').customGET('all').then(function (response) {
            if(response) {
                self.posts = response.posts;
            }
        });
    }

    save(){
        var self = this;
        if(this.id) {
            var params = {
                course_id: this.id,
                course: this.model
            };

            this.API.all('course').customPOST(params, 'update').then((response) => {
                if(response.id) {
                    self.hide();
                    this.PubSub.publish('course-change');
                }
            });
        }
    }

    restore(){
        var self = this;
        if(this.id) {
            var params = {
                id: this.id,
                school_id: this.model.school_id
            };

            this.API.all('course').customPOST(params, 'restore').then((response) => {
                if(response.result) {
                    self.hide();
                    this.PubSub.publish('course-change');
                }
            });
        }
    }

    delete(){
        var self = this;
        this.DialogService.confirm('', 'Er du sikker på at du vil slette kurs?').then(()=> {
            var data = {
                id: this.id
            };
            this.API.one('course').customDELETE('delete', data).then((response) => {
                self.hide();
                if(response.result == false) {
                    this.ToastService.error(response.message);
                } else {
                    this.PubSub.publish('course-change');
                }
            });
        });
    }

    hide(){
        this.DialogService.cancel();
    }

    range(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    }
}

