export class DialogFormListController {
    constructor(DialogService, API, NgTableParams, school_id) {
        'ngInject';

        this.API = API;
        this.lang = Lang;
        this.DialogService = DialogService;
        this.NgTableParams = NgTableParams;
        this.school_id = school_id;

        this.init();
    }

    init() {
        this.getForms();
    }

    getForms() {
        this.API.all('form').customGET("getBySchoolId", {school_id: this.school_id}).then((response) => {
            if(response) {
                this.forms = response.forms;

                this.formTableParams = new this.NgTableParams({
                    count: this.forms.length,
                    filter : {
                    }
                }, {
                    counts: [],
                    dataset: this.forms
                });
            }
        });
    }

    selectForm(form){
        //this.DialogService.hide(form);
        this.selected_form = form;
    }

    hide(){
        this.DialogService.cancel();
    }
}

