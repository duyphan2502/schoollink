import {DksAddCourseController} from './../../dialogs/dks-add-course/dks-add-course.dialog';
import  {GroupActivityRoleController} from './../../dialogs/group-activity-role/group-activity-role.dialog';

export class DksGroupActivityRoleController extends GroupActivityRoleController{
    save(){
        this.DialogService.hide();
        if(this.type == 1) {
            this.API.all('enrollmentGroup').customPOST({ role: this.active_role , type: this.selected_type}, 'add').then((response) => {
                if(response.id) {
                    this.$state.go('app.enrollment_group', {id: response.id, school_id: this.active_role.split(":")[1]});
                }
            });
        } else if (this.type == 2) {
            this.API.all('activity').customPOST({ role: this.active_role, type: this.selected_type }, 'add').then((response) => {
                if(response.id) {
                    this.$state.go('app.activity-detail', { id: response.id, school_id: this.active_role.split(":")[1]});
                }
            });
        } else if (this.type = 3) {
            this.API.all('course').customPOST({ role: this.active_role, type: this.selected_type }, 'add').then((response) => {
                if(response.id) {
                    this.DialogService.fromTemplate('dks-add-course', {
                        controller: DksAddCourseController,
                        controllerAs: 'vm',
                        locals: {
                            id: response.id,
                            course_type_id: this.selected_type
                        }
                    }).then(function(response) {

                    });
                }
            });
        }
    }
}

