export class LocationMapController {
    constructor(DialogService, API, school_name, school_location){
        'ngInject';

        this.API = API;
        this.DialogService = DialogService;
        this.lang = Lang;
        this.school_name = school_name;
        this.school_location = school_location;

        this.init();
    }

    init() {
    }

    hide(){
        this.DialogService.cancel();
    }
}

