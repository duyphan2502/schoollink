export class OuterLinkDirective {
    constructor() {
        this.restrict = 'A';
        this.url = '';
    }

    // optional link function
    link($scope, element, attrs) {
        element.on('click', function () {
            parent.location.href = angular.element(this).attr('outer-link');
        });
    }
}
