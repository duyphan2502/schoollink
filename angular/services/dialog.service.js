export class DialogService {
    constructor($mdDialog) {
        'ngInject';

        this.$mdDialog = $mdDialog;
        this.lang = Lang;
        this.lbl_cancel = 'Cancel';
        if(this.lang != 'en'){
            this.lbl_cancel = 'Avbryt';
        }
    }

    fromTemplate(template, options) {
        if (!template) {
            return false;
        }

        if (!options) {
            options = {};
        }

        var defaultOptions = {
            templateUrl: './views/dialogs/' + template + '/' + template + '.dialog.html'
        };
        angular.extend(defaultOptions, options);

        return this.$mdDialog.show(defaultOptions);
    }

    hide(response) {
        return this.$mdDialog.hide(response);
    }

    cancel(response) {
        return this.$mdDialog.cancel(response);
    }

    alert(title, content) {
        let alert = this.$mdDialog.alert()
            .title(title)
            .content(content)
            .ariaLabel(content)
            .ok('Ok');

        this.$mdDialog.show(alert);
    }

    confirm(title, content) {
        let confirm = this.$mdDialog.confirm()
            .title(title)
            .content(content)
            .ariaLabel(content)
            .ok('Ok')
            .cancel(this.lbl_cancel);

        return this.$mdDialog.show(confirm);
    }

    prompt(title, content, placeholder) {
        let prompt = this.$mdDialog.prompt()
            .title(title)
            .textContent(content)
            .placeholder(placeholder)
            .ariaLabel(placeholder)
            .ok('Ok')
            .cancel(this.lbl_cancel);

        return this.$mdDialog.show(prompt);
    }
}
