export function ColorRun($rootScope) {
    'ngInject';
    $rootScope.getColor = function (id) {
        let pupils = $rootScope.pupils;
        if(pupils)
        {
            for (var index in pupils) {
                if(pupils[index].user_id == id)
                    return index % 8;
            }
        }
        return 0;
    }
}


