export function DateRun($rootScope) {
    'ngInject';

    $rootScope.formatDate = function (date, format) {
        if(date)
            return moment(date * 1000).format(format);
    };

    $rootScope.getLabel = function (key) {
        return Lang.get('label.' + key);
    };

    $rootScope.getMessage = function (key) {
        return Lang.get('message.' + key);
    };

    $rootScope.toUTCOffset = function(date) {
        var momentDate = moment(date);
        return momentDate.utc().add(momentDate.utcOffset(), 'm');
    }
}


