import {RoutesRun} from '../../../run/routes.run';
import {ColorRun} from './../../../run/color.run';

angular.module('app.run')
    .run(ColorRun)
    .run(RoutesRun);
