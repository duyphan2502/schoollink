import {DksParticipantHeaderComponent} from './../../../app/components/dks/participant/header/header.component';
import {DksBookingComponent} from './../../../app/components/dks/participant/booking/booking.component';

angular.module('app.components')
    .component('dksParticipantHeader', DksParticipantHeaderComponent)
    .component('dksDksBooking', DksBookingComponent)
;

