export function RoutesConfig($stateProvider, $urlRouterProvider) {
	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}.page.html`;
	};

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('app', {
			abstract: true,
            data: {},//{auth: true} would require JWT auth
			views: {
				header: {
					templateUrl: getView('dks/admin/header/header')
				},
				footer: {
					templateUrl: getView('dks/admin/footer/footer')
				},
				main: {}
			}
		})
		.state('app.landing', {
            url: '/',
            views: {
                'main@': {
                    templateUrl: getView('dks/admin/overview/overview')
                }
            }
        })
		.state('app.enrollment_group', {
			url: '/enrollment-group/:school_id/:id/:editing',
			views: {
				'main@': {
					templateUrl: getView('dks/admin/enrollment-group/enrollment-group')
				}
			}
		})
		.state('app.activity-detail', {
			url: '/activity-detail/:id/:edit_clone/:tour_activity',
			views: {
				'main@': {
					templateUrl: getView('dks/admin/activity/activity-detail')
				}
			}
		})
		.state('app.tour-plan', {
			url: '/tour-plan/:course_id/:enrollment_group_id',
			views: {
				'main@': {
					templateUrl: getView('dks/admin/tour-plan/tour-plan')
				}
			}
		})
        ;
}
