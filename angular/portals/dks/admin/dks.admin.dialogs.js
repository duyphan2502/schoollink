import {GroupActivityRoleController} from './../../../dialogs/group-activity-role/group-activity-role.dialog';
import {DksAddActivityCourseController} from './../../../dialogs/dks-add-activity-course/dks-add-activity-course.dialog';
import {DialogTourPlanDetailController} from './../../../dialogs/tour-plan-detail/tour-plan-detail.dialog';
import {DksAddCourseController} from './../../../dialogs/dks-add-course/dks-add-course.dialog';
import {DksGroupActivityRoleController} from './../../../dialogs/dks-group-activity-role/dks-group-activity-role.dialog';
import {DksDialogEnrollmentGroupsController} from './../../../dialogs/dks-enrollment-groups/dks-enrollment-groups.dialog';

angular.module('app.dialogs')
    .controller('GroupActivityRoleController', GroupActivityRoleController)
    .controller('DksAddActivityCourseController', DksAddActivityCourseController)
    .controller('DksAddCourseController', DksAddCourseController)
    .controller('DksGroupActivityRoleController', DksGroupActivityRoleController)
    .controller('DialogTourPlanDetailController', DialogTourPlanDetailController)
    .controller('DksDialogEnrollmentGroupsController', DksDialogEnrollmentGroupsController)
;