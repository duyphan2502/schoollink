import {RoutesRun} from './../../../run/routes.run';
import {DateRun} from './../../../run/date.run';
import {InlineEdit} from './../../../run/inlineEdit.run';

angular.module('app.run')
    .run(DateRun)
    .run(RoutesRun)
    .run(InlineEdit)
;

