import {AdminOverviewEnrollmentGroupComponent} from '../../../app/components/dks/admin/enrollmentgroup/enrollmentgroup.component';
import {AdminEnrollmentGroupDetailsComponent} from '../../../app/components/dks/admin/enrollment-group-details/enrollment-group-details.component';
import {AdminActivityComponent} from '../../../app/components/dks/admin/activity/activity.component';
import {AdminActivityDetailComponent} from '../../../app/components/dks/admin/activity-details/activity-detail.component';
import {AdminSimulationActivityComponent} from '../../../app/components/dks/admin/simulation-activity/simulation-activity.component';
import {AdminCourseComponent} from '../../../app/components/dks/admin/course/course.component';
import {AdminAcademicYearComponent} from '../../../app/components/dks/admin/academic-year/academic-year.component';
import {AdminTourPlanComponent} from '../../../app/components/dks/admin/tour-plan/tour-plan.component';

angular.module('app.components')
    .component('adminOverviewEnrollmentGroup', AdminOverviewEnrollmentGroupComponent)
    .component('adminEnrollmentGroupDetails', AdminEnrollmentGroupDetailsComponent)
    .component('adminActivity', AdminActivityComponent)
    .component('adminActivityDetail', AdminActivityDetailComponent)
    .component('adminSimulationActivity', AdminSimulationActivityComponent)
    .component('adminCourse', AdminCourseComponent)
    .component('adminAcademicYear', AdminAcademicYearComponent)
    .component('adminTourPlan', AdminTourPlanComponent)
;

