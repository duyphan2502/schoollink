import {GroupActivityRoleController} from './../../../dialogs/group-activity-role/group-activity-role.dialog';

angular.module('app.dialogs')
	.controller('GroupActivityRoleController', GroupActivityRoleController);