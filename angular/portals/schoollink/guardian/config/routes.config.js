export function RoutesConfig($stateProvider, $urlRouterProvider) {
    'ngInject';

    let getView = (viewName) =>
    {
        return `./views/app/pages/${viewName}.page.html`;
    };

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('app', {
            abstract: true,
            data: {},//{auth: true} would require JWT auth
            views: {
                header: {
                    templateUrl: getView('schoollink/guardian/header/header')
                },
                footer: {
                    templateUrl: getView('schoollink/guardian/footer/footer')
                },
                main: {}
            }
        })
        .state('app.landing', {
            url: '/',
            views: {
                'main@': {
                    templateUrl: getView('schoollink/guardian/overview/overview')
                }
            }
        })
        .state('app.guardian-landing', {
            url: '/guardian-overview',
            views: {
                'main@': {
                    templateUrl: getView('schoollink/guardian/guardian-overview/guardian-overview')
                }
            }
        })
        .state('app.messages', {
            url: '/messages',
            views: {
                'main@': {
                    templateUrl: getView('schoollink/guardian/messages/messages')
                }
            }
        })
        .state('app.profile', {
            url: '/profile',
            views: {
                'main@': {
                    templateUrl: getView('schoollink/guardian/profile/profile')
                }
            }
        })

        .state('app.contact-list', {
            url: '/contact-list',
            views: {
                'main@': {
                    templateUrl: getView('schoollink/guardian/contact-list/contact-list')
                }
            }
        })

        .state('app.pickup-rules', {
            url: '/pickup-rules',
            views: {
                'main@': {
                    templateUrl: getView('schoollink/guardian/pickup-rules/pickup-rules')
                }
            }
        })

        .state('app.user-profile', {
            url: '/user-profile',
            views: {
                'main@': {
                    templateUrl: getView('schoollink/guardian/user-profile/user-profile')

                }
            }
        })
    ;
}
