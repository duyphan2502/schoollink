import {GuardianHeaderComponent} from '../../../app/components/schoollink/guardian/header/header.component';
import {GuardianMessagesComponent} from '../../../app/components/schoollink/guardian/messages/messages.component';
import {GuardianPupilProfileComponent} from '../../../app/components/schoollink/guardian/profile/profile.component';
import {GuardianContactListComponent} from '../../../app/components/schoollink/guardian/contact-list/contact-list.component';
import {GuardianUserProfileComponent} from '../../../app/components/schoollink/guardian/user-profile/user-profile.component';
import {GuardianNotificationSettingComponent} from '../../../app/components/schoollink/guardian/notification-setting/notification-setting.component';
import {GuardianPrecautionComponent} from '../../../app/components/schoollink/guardian/precaution/precaution.component';
import {GuardianImportEmailsComponent} from '../../../app/components/schoollink/guardian/import-emails/import-emails.component';
import {GuardianRemarkComponent} from '../../../app/components/schoollink/guardian/remark/remark.component';
import {GuardianConsentComponent} from '../../../app/components/schoollink/guardian/consent/consent.component';
import {GuardianPickupRulesComponent} from '../../../app/components/schoollink/guardian/pickup-rules/pickup-rules.component';
import {GuardianWeatherComponent} from '../../../app/components/schoollink/guardian/weather/weather.component';

angular.module('app.components')
	.component('guardianHeader', GuardianHeaderComponent)
	.component('guardianPupilProfile', GuardianPupilProfileComponent)
	.component('guardianUserProfile', GuardianUserProfileComponent)
	.component('guardianNotificationSetting', GuardianNotificationSettingComponent)
	.component('guardianPrecaution', GuardianPrecautionComponent)
	.component('guardianMessages', GuardianMessagesComponent)
	.component('guardianImportEmails', GuardianImportEmailsComponent)
	.component('guardianRemark', GuardianRemarkComponent)
    .component('guardianContactList', GuardianContactListComponent)
	.component('guardianConsent', GuardianConsentComponent)
	.component('guardianPickupRules', GuardianPickupRulesComponent)
	.component('guardianWeather', GuardianWeatherComponent);

