import {OuterLinkDirective} from './../../../directives/outer-link';

angular.module('app.directives')
    .directive('outerLink', () => new OuterLinkDirective);