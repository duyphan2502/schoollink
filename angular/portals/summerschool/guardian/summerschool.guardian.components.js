import {GuardianHeaderComponent} from '../../../app/components/summerschool/guardian/header/header.component';
import {PupilProfileComponent} from '../../../app/components/summerschool/guardian/profile/profile.component';
import {GuardianPrecautionComponent} from '../../../app/components/schoollink/guardian/precaution/precaution.component';
import {GuardianSummerBookingComponent} from './../../../app/components/summerschool/guardian/summer-booking/summer-booking.component';
import {GuardianProfileComponent} from '../../../app/components/summerschool/guardian/user-profile/user-profile.component';
import {GuardianPupilAddComponent} from '../../../app/components/summerschool/guardian/pupil-add/pupil-add.component';
import {GuardianSummerBookingWpComponent} from './../../../app/components/summerschool/guardian/summer-booking-wp/summer-booking-wp.component';

angular.module('app.components')
    .component('guardianHeader', GuardianHeaderComponent)
    .component('guardianPupilProfile', PupilProfileComponent)
    .component('guardianPrecaution', GuardianPrecautionComponent)
    .component('guardianSummerBooking', GuardianSummerBookingComponent)
    .component('guardianUserProfile', GuardianProfileComponent)
    .component('guardianPupilAdd', GuardianPupilAddComponent)
    .component('guardianSummerBookingWp', GuardianSummerBookingWpComponent);

