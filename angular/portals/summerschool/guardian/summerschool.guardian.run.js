import {RoutesRun} from './../../../run/routes.run';
import {ColorRun} from './../../../run/color.run';
import {DateRun} from './../../../run/date.run';

angular.module('app.run')
    .run(ColorRun)
    .run(RoutesRun)
    .run(DateRun);
