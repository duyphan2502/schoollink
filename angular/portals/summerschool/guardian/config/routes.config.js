export function RoutesConfig($stateProvider, $urlRouterProvider) {
	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}.page.html`;
	};

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('app', {
			abstract: true,
            data: {},//{auth: true} would require JWT auth
			views: {
				header: {
					templateUrl: getView('summerschool/guardian/header/header')
				},
				footer: {
					templateUrl: getView('summerschool/guardian/footer/footer')
				},
				main: {}
			}
		})
		.state('app.landing', {
            url: '/',
            views: {
                'main@': {
                    templateUrl: getView('summerschool/guardian/overview/overview')
                }
            }
        })
		.state('app.user-profile', {
			url: '/user-profile',
			views: {
				'main@': {
					templateUrl: getView('summerschool/guardian/user-profile/user-profile')
				}
			}
		})
		.state('app.messages', {
			url: '/messages',
			views: {
				'main@': {
					templateUrl: getView('guardian/messages/messages')
				}
			}
		})
		.state('app.pickup-rules', {
			url: '/pickup-rules',
			views: {
				'main@': {
					templateUrl: getView('guardian/pickup-rules/pickup-rules')
				}
			}
		})
		.state('app.profile', {
			url: '/profile',
			views: {
				'main@': {
					templateUrl: getView('summerschool/guardian/profile/profile')
				}
			}
		})
		.state('summer-booking-wp', {
			url: '/summer-booking-wp',
			views: {
				'main@': {
					templateUrl: getView('summerschool/guardian/book-landing/book-landing')
				}
			}
		})
        ;
}
