import {TeacherOverviewEnrollmentGroupComponent} from '../../../app/components/summerschool/teacher/enrollmentgroup/enrollmentgroup.component';
import {TeacherOverviewActivityComponent} from '../../../app/components/summerschool/teacher/activity/activity.component';
import {TeacherOverviewCourseComponent} from '../../../app/components/summerschool/teacher/course/course.component';
import {TeacherEnrollmentGroupDetailsComponent} from '../../../app/components/summerschool/teacher/enrollment-group-details/enrollment-group-details.component';
import {TeacherActivityDetailComponent} from '../../../app/components/summerschool/teacher/activity-details/activity-detail.component';
import {TeacherSimulationActivityComponent} from '../../../app/components/summerschool/teacher/simulation-activity/simulation-activity.component';
import {TeacherAcademicYearComponent} from '../../../app/components/summerschool/teacher/academic-year/academic-year.component';

angular.module('app.components')
    .component('teacherOverviewEnrollmentGroup', TeacherOverviewEnrollmentGroupComponent)
    .component('teacherEnrollmentGroupDetails', TeacherEnrollmentGroupDetailsComponent)
    .component('teacherOverviewActivity', TeacherOverviewActivityComponent)
    .component('teacherOverviewCourse', TeacherOverviewCourseComponent)
    .component('teacherActivityDetail', TeacherActivityDetailComponent)
    .component('teacherSimulationActivity', TeacherSimulationActivityComponent)
    .component('teacherAcademicYear', TeacherAcademicYearComponent)
;

