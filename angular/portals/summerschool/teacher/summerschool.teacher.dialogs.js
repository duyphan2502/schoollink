import {GroupActivityRoleController} from './../../../dialogs/group-activity-role/group-activity-role.dialog';
import {AddActivityCourseController} from './../../../dialogs/add-activity-course/add-activity-course.dialog';
import {AddCourseController} from './../../../dialogs/add-course/add-course.dialog';
import {DialogEnrollmentGroupsController} from './../../../dialogs/enrollment-groups/enrollment-groups.dialog';

angular.module('app.dialogs')
	.controller('GroupActivityRoleController', GroupActivityRoleController)
	.controller('AddActivityCourseController', AddActivityCourseController)
	.controller('AddCourseController', AddCourseController)
	.controller('DialogEnrollmentGroupsController', DialogEnrollmentGroupsController)
;