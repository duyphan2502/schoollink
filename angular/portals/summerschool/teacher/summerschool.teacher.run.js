import {RoutesRun} from './../../../run/routes.run';
import {DateRun} from './../../../run/date.run';

angular.module('app.run')
    .run(DateRun)
    .run(RoutesRun);
