export function RoutesConfig($stateProvider, $urlRouterProvider) {
	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}.page.html`;
	};

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('app', {
			abstract: true,
            data: {},//{auth: true} would require JWT auth
			views: {
				header: {
					templateUrl: getView('summerschool/teacher/header/header')
				},
				footer: {
					templateUrl: getView('summerschool/teacher/footer/footer')
				},
				main: {}
			}
		})
		.state('app.landing', {
            url: '/',
            views: {
                'main@': {
                    templateUrl: getView('summerschool/teacher/overview/overview')
                }
            }
        })

		.state('app.enrollment_group', {
			url: '/enrollment-group/:school_id/:id/:editing',
			views: {
				'main@': {
					templateUrl: getView('summerschool/teacher/enrollment-group/enrollment-group')
				}
			}
		})
		.state('app.activity-detail', {
			url: '/activity-detail/:id/:edit_clone',
			views: {
				'main@': {
					templateUrl: getView('summerschool/teacher/activity/activity-detail')
				}
			}
		})
		.state('app.course-detail', {
			url: '/course-detail/:id/:course_type_id',
			views: {
				'main@': {
					templateUrl: getView('summerschool/teacher/course/course-detail')
				}
			}
		})
        ;
}
