<?php

return [
    'title_delete_precaution' => 'Bekreftelse',
    'message_delete_precaution' => 'Er du sikker på at vil slette denne hensyn?',
    'message_id_validate_required' => 'ID kreves',
    'message_id_validate_integer' => 'ID må være et heltall',
    'message_email_validate_required' => 'E-post kreves',
    'message_email_validate_email' => 'E-post må være en gyldig e-postadresse',
    'message_email_validate_unique' => 'E-post er allerede tatt',
    'message_mobilephone_validate_max' => 'Mobiltelefon kan ikke være større enn :max tegn',
    'message_sickness_confirmed_validate_required' => 'Hensyn bekreftet felt kreves',
    'message_name_validate_required' => 'Navn kreves',
    'title_delete' => 'Bekreftelse',
    'message_delete_pickup_rule' => 'Er du sikker på at vil slette denne henteregler?',
    'enter_location_description' => 'Skriv inn beskrivelse av lokasjonen her',
    'enter_address_here' => 'Skriv inn adressen her',
    'enter_phone_here' => 'Skriv inn telefonnummer her',
    'send_message_to_participants' => 'Send ut spørsmåls-skjema til deltagerene',
    'add_related_questions_form_for_enrollment' => 'Legg til spørsmåls-skjema påkrevd for påmelding',
    'spread_evenly_year' => 'Spre årstrinn jevnt',
    'prefer_same_grade' => 'Foretrekk samme årstrinn',
    'prefer_same_sex' => 'Foretrekk samme kjønn',
    'spread_evenly_schools' => 'Spre skoler jevnt',
    'prefer_same_schools' => 'Prefer same schools',
    'enter_participant_name' => 'Skriv inn deltagerens navn',
    'timestart_cannot_greater_than_timeend' => 'Tid start kan ikke større enn gang slutt',
    'participant_registered' => 'Deltaker registrert',
];
