<?php

$summer_school_resource_messages = require resource_path('lang/no/summerschool/message.php');
$dks_resource_messages = require resource_path('lang/no/dks/message.php');
$schoollink_resource_messages = require resource_path('lang/en/schoollink/message.php');

return [
    'm_summer_school' => $summer_school_resource_messages,
    'm_dks' => $dks_resource_messages,
    'm_schoollink' => $schoollink_resource_messages
];
