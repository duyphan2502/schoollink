<?php

$summer_school_resource_labels = require resource_path('lang/en/summerschool/label.php');
$dks_resource_labels = require resource_path('lang/en/dks/label.php');
$schoollink_resource_labels = require resource_path('lang/en/schoollink/label.php');

return [
    'm_summer_school' => $summer_school_resource_labels,
    'm_dks' => $dks_resource_labels,
    'm_schoollink' => $schoollink_resource_labels,
];
