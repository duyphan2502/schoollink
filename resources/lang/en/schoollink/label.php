<?php

return [
    'title_delete_precaution' => 'Bekreftelse',
    'ok' => 'OK',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'home' => 'Home',
    'messages' => 'Messages',
    'pupil_profile' => 'Pupil profile',
    'my_profile' => 'My profile',
    'pickup_rules' => 'Pickup rules',
    'absence' => 'Absence',
    'registration_activities' => 'Registration activities',
    'contact_list' => 'Contact list',
    'register_absence' => 'Register absence',
    'new_message' => 'New message',
    'new_pickup_message' => 'New pickup message',
    'weather_tomorrow' => 'Weather tomorrow',
    'mobilephone' => 'Mobile phone',
    'telephone' => 'Telephone',
    'email' => 'Email',
    'address' => 'Address',
    'postal_code' => 'Postal code',
    'postal_name' => 'Postal name',
    'notification_hentet' => ' is picked up from SFO',
    'notification_sfopro_type-collected' => ' is picked up from SFO',
    'notification_sfopro_type-collected_kindergarten' => ' is picked up',
    'notification_sfopro_type-gone' => ' has gone  home from SFO',
    'notification_sfopro_type-gone_kindergarten' => ' has gone  home',
    'get_notification_when' => 'Get notification when',
    'consents_are_given_on_behalf_of_both_guardians' => 'Consents are given on behalf of both guardians',
    'precautions' => 'Precautions',
    'choose_precautions' => 'Choose precaution from the list or fill in the name of the precaution below if you can not find the precaution in the list',
    'name_of_precaution' => 'Name of the precaution',
    'description' => 'Description',
    'name_of_precaution_placeholder' => 'Fill in this field if you can not find the precaution in the list above.',
    'description_placeholder' => 'Describe how the precaution should be handled.',
    'confirm_precaution_registered' => 'Confirm that no precautions should be registered',
    'add_precaution_button' => 'Add Precaution',
    'guardian_profile_field_edit_email_and_mobilephone' => 'Edit email and mobile phone number',
    'guardian_profile_field_editmobilephone' => 'Edit mobile phone',
    'teacher_profile_import_emails' => 'Import emails',
    'import_emails_IMS_Kindergarten_Guardian' => 'Imported for kindergarten guardian',
    'import_emails_IMS_Kindergarten_Staff' => 'Imported for kindergarten staff',
    'import_emails_IMS_School_Guardian' => 'Imported for school guardian',
    'import_emails_IMS_School_Staff' => 'Imported for school staff',
    'import_emails_GoogleAppsForEducation' => 'Google Apps For Education',
    'remark_title' => 'Remarks',
    'remark_title_positive' => 'Positive remarks',
    'remark_title_negative' => 'Negative remarks',
    'remark_type' => 'Remark Type',
    'remark_description' => 'Description',
    'remark_date' => 'Date',
    'consent_value_false' => 'No',
    'consent_value_true' => 'Yes',
    'consent_value_null' => 'Undefined',
    'consent_button_edit_consents' => 'Edit consents',
    'consent_title_parent_portal' => 'Consents are  given on behalf of both guardians',
    'consent_title_header' => 'Consents',
    'contact_list_show_my_contact' => 'Press here to SHOW my contact information to guardians in my children\'s class',
    'contact_list_not_show_my_contact' => 'Press here to NOT SHOW my contact information to guardians in my children\'s class',
    'pickup_rule_title' => 'Pickup rule',
    'pickup_rule_overview_title' => 'Pickup rule overview',
    'pickup_rule_active_pickup_messages' => 'Active Pickup Messages',
    'pickup_rule_exired_pickup_messages' => 'Expired Pickup Messages',
    'pickup_rule_form_description_placeholder' => 'Engangs hentemelding kan v�re en melding av typen "Ariel Nydal vil g� hjem sammen med Peter i dag"',
    'load_more' => 'Load more',
    'send_message' => 'Send message',
];
