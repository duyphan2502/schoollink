var elixir = require('laravel-elixir');

require('./tasks/angular.task.js');
require('./tasks/bower.task.js');
require('./tasks/ngHtml2Js.task.js');
require('./tasks/scss.task.js');
require('./tasks/customJsLibs.task.js');
require('laravel-elixir-livereload');
require('laravel-elixir-karma');


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    var assets = [
            'public/js/vendor.js',
            'public/js/partials.js',
            'public/js/customJsLibs.js',
            'public/js/schoollink.guardian.app.js',
            'public/js/summerschool.guardian.app.js',
            'public/js/summerschool.teacher.app.js',
            'public/js/dks.admin.app.js',
            'public/js/dks.participant.app.js',

            'public/css/vendor.css',
            'public/css/components.css',
            'public/css/app.schoollink.guardian.css',
            'public/css/app.summerschool.teacher.css',
            'public/css/app.summerschool.parent.css',
            'public/css/app.dks.participant.css',
            'public/css/app.dks.admin.css'
        ],
        karmaJsDir = [
            'public/js/vendor.js',
            'node_modules/angular-mocks/angular-mocks.js',
            'node_modules/ng-describe/dist/ng-describe.js',
            'public/js/partials.js',
            'public/js/customJsLibs.js',
            'public/js/schoollink.guardian.app.js',
            'public/js/summerschool.guardian.app.js',
            'public/js/summerschool.teacher.app.js',
            'public/js/dks.participant.app.js',
            'public/js/dks.admin.app.js',
            'tests/angular/**/*.spec.js'
        ];

    mix
        .bower()
        .angular('./angular/')
        .ngHtml2Js('./angular/**/*.html')
        .resources()
        .customJsLibs()
        .version(assets)
        .livereload('public/build/rev-manifest.json', {
            liveCSS: true
        })
        .karma({
            jsDir: karmaJsDir
        });
});
