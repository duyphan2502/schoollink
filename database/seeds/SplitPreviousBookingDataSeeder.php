<?php

use Illuminate\Database\Seeder;

class SplitPreviousBookingDataSeeder extends Seeder
{
    public function __construct()
    {

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (file_exists(__DIR__ . '/data2016.json')) {
            $sheetData = json_decode(file_get_contents(__DIR__ . '/data2016.json'), true);
        }

        $index = 1;
        $new_data =[];
        foreach ($sheetData as $k => $v) {
            $new_data[$k] = $v;
            if(count($new_data) == 3500) {
                \Storage::disk('local')->put('data2016'.$index.'.json', json_encode($new_data));
                $new_data = [];
                $index++;
            }
        }
        \Storage::disk('local')->put('data2016'.$index.'.json', json_encode($new_data));
    }
}
