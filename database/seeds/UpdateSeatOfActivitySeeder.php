<?php

use Illuminate\Database\Seeder;

class UpdateSeatOfActivitySeeder extends Seeder
{
    private $activityRepository;
    private $schoolRepository;
    private $groupRepository;
    private $subGroupRepository;
    private $courseRepository;
    private $userRepository;
    private $userRoleRepository;
    private $participantRepository;
    private $schoolOwnerRepository;

    public function __construct(\App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface $activityRepository,
    \App\Containers\SummerSchool\Contracts\CourseRepositoryInterface $courseRepository,
    \App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface $participantRepository,
    \App\Containers\SchoolLink\Contracts\UserRepositoryInterface $userRepository,
    \App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface $userRoleRepository,
    \App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface $subGroupRepository,
    \App\Containers\SchoolLink\Contracts\GroupRepositoryInterface $groupRepository,
    \App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface $schoolOwnerRepository,
    \App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface $schoolRepository)
    {
        $this->activityRepository = $activityRepository;
        $this->schoolRepository = $schoolRepository;
        $this->courseRepository = $courseRepository;
        $this->userRepository = $userRepository;
        $this->userRoleRepository = $userRoleRepository;
        $this->groupRepository = $groupRepository;
        $this->subGroupRepository = $subGroupRepository;
        $this->participantRepository = $participantRepository;
        $this->schoolOwnerRepository = $schoolOwnerRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (file_exists(__DIR__ . '/data.json')) {
            $sheetData = json_decode(file_get_contents(__DIR__ . '/data.json'), true);

        }
        foreach ($sheetData as $row) {
            $this->findActivity($row);
        }
    }

    public function findActivity($row)
    {
        $course_name = trim($row['Course']);
        $activity_week = $row['Week'];
        $week = explode(' ', $activity_week)[1];

        if ($course_name == 'Speak! Learn English from English teachers')
            $course_name = 'Speak! Learn English with English teachers';
        else if($course_name == 'Programmering - lær å lage dataspill')
            $course_name = 'Programmering – lær å lage dataspill';
        else if($course_name == 'Sommerredaksjonen - lag avis')
            $course_name = 'Sommerredaksjonen – lag avis';
        else if(strpos($course_name,'up? Let'))
            $course_name = 'up? Let';
        else if($course_name == 'Fantasifulle fortellinger - musikk, dans og drama 1')
            $course_name = 'Fantasifulle fortellinger 1 med musikk, dans og drama';
        else if($course_name == 'Fantasifulle fortellinger med musikk, dans og drama 2')
            $course_name = 'Fantasifulle fortellinger 2 med musikk, dans og drama';
        else if(strpos($course_name,'Improve your English'))
            $course_name = 'Improve your English';
        else if(strpos($course_name,'oro med m'))
            $course_name = 'oro med m';
        else if(strpos($course_name,'ummer games'))
            $course_name = 'ummer games';
        else if($course_name == 'Kodegjengen! Matematikk og dataspill')
            $course_name = 'Kodegjengen – matematikk og dataspill ';
        else if($course_name == 'Forskere i farta!')
            $course_name = 'Forskere i farta';
        else if(strpos($course_name, 'odige monstre og trassige tryllestaver'))
            $course_name = 'odige monstre og trassige tryllestaver';
        else if(strpos($course_name, 'atterkula'))
            $course_name = 'atterkula';
        else if(strpos($course_name, 'erkelige oppfinnelser og'))
            $course_name = 'erkelige oppfinnelser og';
        else if (strpos($course_name, 'anoskolen'))
            $course_name = 'anoskolen';
        else if ($course_name == 'Sommerskole-patruljen')
            $course_name = 'Sommerskolepatruljen';
        else if ($course_name == 'Matematisk Hip hop! Regning og dans')
            $course_name = 'Matematisk Hiphop – regning og dans';

        $school_name = trim($row['Location']);
        if(strpos($school_name, 'skole')){
            $school_name = explode(' ', $school_name)[0];
        } else if (strpos($school_name, 'niversitetet i Oslo')){
            $school_name = explode('/', $school_name)[1];
        } else if(strpos($school_name, 'vgs') || strpos($school_name, 'VGS'))
            $school_name = explode(' ', $school_name)[0];
        else if($school_name == 'Kuben yrkesarena'){
            $school_name = 'Kuben videregående skole';
        }

        $school = $this->schoolRepository->findWhere([['name' , 'like', '%'.$school_name.'%'], 'date_deleted' => null,
            'owner_id' => 9])->first();

        $activity = null;

        if($school != null) {
            $activity = $this->activityRepository->findWhere([
                ['Name','like','%'.$course_name.'%'.$week.'%'],
                'Options.SummerSchoolSchoolId' => (int)$school->id
            ], ['_id', 'CourseId', 'EnrollmentGroupId', 'Name'])->first();
        }

        if($activity != null) {
            $this->activityRepository->updateOne([
                '_id' => new \MongoDB\BSON\ObjectID($activity->id)
            ], [
                'AllowWaitingQueue' => true,
                'Options.SummerSchoolParticipantsMaximum' => $row['Seat'],
                'Options.SummerSchoolParticipantsLimit' => 0,
            ]);
        }
    }
}
