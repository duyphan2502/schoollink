<?php

use Illuminate\Database\Seeder;

class ApplyStudentsSeeder extends Seeder
{
    private $activityRepository;
    private $schoolRepository;
    private $groupRepository;
    private $subGroupRepository;
    private $courseRepository;
    private $userRepository;
    private $userRoleRepository;
    private $participantRepository;

    public function __construct(\App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface $activityRepository,
    \App\Containers\SummerSchool\Contracts\CourseRepositoryInterface $courseRepository,
    \App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface $participantRepository,
    \App\Containers\SchoolLink\Contracts\UserRepositoryInterface $userRepository,
    \App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface $userRoleRepository,
    \App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface $subGroupRepository,
    \App\Containers\SchoolLink\Contracts\GroupRepositoryInterface $groupRepository,
    \App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface $schoolRepository)
    {
        $this->activityRepository = $activityRepository;
        $this->schoolRepository = $schoolRepository;
        $this->courseRepository = $courseRepository;
        $this->userRepository = $userRepository;
        $this->userRoleRepository = $userRoleRepository;
        $this->groupRepository = $groupRepository;
        $this->subGroupRepository = $subGroupRepository;
        $this->participantRepository = $participantRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $summer_school = $this->schoolRepository->findWhere(['name' => 'Sommerskolen'])->first();

        $activities = $this->activityRepository->getByCriteria(new \App\Containers\SummerSchool\Criterias\Activity\GetAllActivitiesCriteria([
            'school_owner_ids' => [$summer_school->owner_id],
            'school_ids' => [$summer_school->id],
            'owner_ors' => []
        ]))->keyBy('id');

        foreach ($activities as $activity) {
            $this->activityRepository->update(['Participants' => []], $activity->id);
        }

        $activity_keys = $activities->pluck('id')->toArray();

        $courses = $this->courseRepository->getByCriteria(new \App\Containers\SummerSchool\Criterias\Course\GetAllCoursesCriteria([
            'school_owner_ids' => [$summer_school->owner_id],
            'school_ids' => [$summer_school->id],
        ]))->keyBy('id');

        $users = $this->userRepository->getByCriteria(new \App\Containers\SchoolLink\Criterias\User\SearchParticipantsCriteria($summer_school->owner_id, '', 10000));

        foreach ($users as $user) {
            $level = (int)$user->group;

            if ($level === 0)
                continue;

            $number_of_course_to_pick = rand(1, 5);
            shuffle($activity_keys);

            $ACID = 0;
            for ($i = 0; $i < $number_of_course_to_pick; $i++) {
                $activity = null;
                $run = true;
                while ($run) {
                    if (!isset($activity_keys[$ACID])) {
                        $activity = null;
                        $run = false;
                    }
                    if ($run) {
                        $activity = $activities[$activity_keys[$ACID]];
                        if ($activity->course_id) {
                            $Course = $courses[(string)$activity->course_id];
                            if ($Course->year_grade_maximum >= $level && $Course->year_grade_minimum <= $level) {
                                $run = false;
                            }
                        }
                        $ACID++;
                    }
                }
                if ($activity) {
                    $guardian_user_role = $this->userRoleRepository->findWhere(['guardian_user_id' => $user->id, 'role_id' => 'G'])->first();

                    $participant = [
                        'ResourceId' => $user->id,
                        'Priority' => (int)($i + 1),
                        'Status' => \App\Containers\SummerSchool\Constants\Constant::PARTICIPANT_STATUS_REGISTERED,
                        'GroupNumber' => 1,
                        'RegisteredDate' => new \MongoDB\BSON\UTCDateTime(\Carbon\Carbon::now()->getTimestamp() * 1000),
                        'RegisteredBy' => isset($guardian_user_role) ? $guardian_user_role->user_id : $user->id,
                        'ActivityId' => new \MongoDB\BSON\ObjectID($activity->id),
                        'EnrollmentGroupId' => new \MongoDB\BSON\ObjectID($activity->enrollment_group_id),
                        'CourseId' => new \MongoDB\BSON\ObjectID($activity->course_id)
                    ];
                    $this->participantRepository->create($participant);

                    echo $user->firstname . ' apply to '. $activity->name .' ('.$activity->id.')';
                    echo "\n";
                }
            }
            echo "\n";
        }
    }
}
