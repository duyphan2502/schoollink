<?php

use Illuminate\Database\Seeder;

class ImportPreviousBookingSeeder extends Seeder
{
    const Batch = 1;
    const Gender_Male = 'M';
    const Gender_Female = 'F';

    private $activityRepository;
    private $schoolRepository;
    private $groupRepository;
    private $subGroupRepository;
    private $courseRepository;
    private $userRepository;
    private $userRoleRepository;
    private $participantRepository;
    private $schoolOwnerRepository;

    private $pupils;
    private $count;
    private $count_found;
    private $match_school_name;
    private $all_users;

    public function __construct(\App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface $activityRepository,
    \App\Containers\SummerSchool\Contracts\CourseRepositoryInterface $courseRepository,
    \App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface $participantRepository,
    \App\Containers\SchoolLink\Contracts\UserRepositoryInterface $userRepository,
    \App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface $userRoleRepository,
    \App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface $subGroupRepository,
    \App\Containers\SchoolLink\Contracts\GroupRepositoryInterface $groupRepository,
    \App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface $schoolOwnerRepository,
    \App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface $schoolRepository)
    {
        $this->activityRepository = $activityRepository;
        $this->schoolRepository = $schoolRepository;
        $this->courseRepository = $courseRepository;
        $this->userRepository = $userRepository;
        $this->userRoleRepository = $userRoleRepository;
        $this->groupRepository = $groupRepository;
        $this->subGroupRepository = $subGroupRepository;
        $this->participantRepository = $participantRepository;
        $this->schoolOwnerRepository = $schoolOwnerRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->count = 0;
        $this->count_found = 0;
        $this->pupils = array();
        $this->match_school_name = array();

        if (file_exists(__DIR__ . '/data2016'.self::Batch.'.json')) {
            $sheetData = json_decode(file_get_contents(__DIR__ . '/data2016'.self::Batch.'.json'), true);
        }
        $this->all_users = collect(\DB::select(\DB::raw("select users.*, schools.id school_id from users 
                        inner join user_roles on users.id = user_roles.user_id
                        inner join subgroups on subgroups.id = user_roles.subgroup_id
                        inner join groups on groups.id = subgroups.group_id
                        inner join schools on schools.id = groups.school_id
                        where schools.owner_id = 9")))->keyBy('id');

        $this->user_ids = array_keys($this->all_users->toArray());

        $guardians = collect(\DB::table('users')->join('user_roles', 'users.id', '=' ,'user_roles.user_id')
                                ->whereIn('user_roles.guardian_user_id', $this->user_ids)->get());
        $all_guardians = [];
        foreach ($guardians as $guardian) {
            if(isset($all_guardians[$guardian->guardian_user_id]))
                $all_guardians[$guardian->guardian_user_id] = [];

            $all_guardians[$guardian->guardian_user_id][] = $guardian;
        }

        $this->guardians = $all_guardians;

        $this->school_owner = $this->schoolOwnerRepository->find(9);

        $this->missing_name = [];
        foreach ($sheetData as $k => $v) {
            if ($k < 3)
                continue;
            $User = $this->getUser($v);

            if ($User) {
                echo $this->count_found . "/" . $this->count . " " . substr(($this->count_found / $this->count * 100) . "000", 0, 5) . "% FOUND/NOT-FOUND\t\t-> " . $User->id . "\n";
                $this->findActivity($v, $User);

            } else {
                echo "\n";
            }
        }

        var_dump($this->missing_name);
    }

    function getUser($row)
    {
        if (!array_key_exists($row['D'], $this->match_school_name))
            $this->match_school_name[$row['D']] = array(0, 0);

        $this->match_school_name[$row['D']][1]++;
        $pupil_id = (int)$row['E'];
        $this->count++;

        if (isset($this->pupils[$pupil_id])) {
            $this->count_found++;
            $this->match_school_name[$row['D']][0]++;
            return $this->pupils[$pupil_id];
        }


        $Users = array();

        $user_same_birth_numbers = $this->all_users->filter(function($user) use($row){
            if($user->birthdate == date('Y-m-d', strtotime($row['R'])))
                return $user;
        });
        $Scores = array();

        $Max = null;
        $SecondMax = null;

        foreach ($user_same_birth_numbers as $User) {
            try{
                $Score = 0;

                $Score = $this->scoreGender($User, $row);
                $Score += $this->scoreStudentName($User, $row);
                $Score += $this->scoreStudentSurname($User, $row);

                if($Score == 200 || $Score == 201)
                    continue;

                $Users[(int)$User->id] = $User;

                $Score += $this->scoreSchool($User, $row);
                $Score += $this->scorePhoneNumber($User, $row);

                if(isset($this->guardians[$User->id])) {
                    $User->guardians = $this->guardians[$User->id];
                    $Score += $this->scoreEmail($User, $row);
                    $Score += $this->scoreGuardianName($User, $row);
                    $Score += $this->scoreAddress($User, $row);
                }

                $Scores[(int)$User->id] = $Score;

                if ($Score > 800) {
                    if ($Score >= $Max[1]) {
                        $SecondMax = $Max;
                        $Max = array((int)$User->id, $Score);
                    }
                }
            } catch(\App\Core\Exception\Abstracts\Exception $ex) {

            }
        }


        if ($Max !== null && ($SecondMax === null || $Max[1] - $SecondMax[1] > 300)) {
            $this->count_found++;
            $this->pupils[$pupil_id] = $Users[$Max[0]];
            $this->match_school_name[$row['D']][0]++;
            return $Users[$Max[0]];
        }
        return null;
    }

    function scoreSchool($user, $row)
    {
        $SchoolName = $row['D'];

        $SchoolId = null;

        $School = $this->schoolRepository->find($user->school_id);
        if (mb_substr(mb_strtolower($School->name), 0, 3) === mb_substr(mb_strtolower($SchoolName), 0, 3)
            && $School->owner_id == 9) {
            $SchoolId = $user->school_id;
        }
        return $SchoolId !== null ? 1000 : 0;
    }



    function scoreAddress($User, $row)
    {
        $PostalCode = $row['L'];
        if (trim($PostalCode) === trim($User->postal_code)) {
            return 700;
        }

        foreach ($User->guardians as $Guardian) {
            if (trim($PostalCode) === trim($Guardian->postal_code)) {
                return 700;
            }
        }
        return 0;
    }

    function scorePhoneNumber($User, $row)
    {
        $PhoneNumber = $row['Y'];
        $PhoneNumberObject = $this->GetMobilePhoneNumberData($User);

        if ($PhoneNumberObject) {
            return $PhoneNumberObject['MobilePhoneNumber'] === trim($PhoneNumber) ? 500 : 0;
        }
    }

    function scoreEmail($User, $row)
    {
        $Email = $row['AA'];

        foreach ($User->guardians as $guardian) {
            if ($this->Email_Has($guardian, $Email)) {
                return 780;
            }
        }

        return 0;
    }

    function scoreStudentName($user, $row)
    {
        $FirstName = $row['I'];

        if (mb_substr(mb_strtolower($FirstName), 0, 4) === mb_substr(mb_strtolower($user->firstname), 0, 4)) {
            return 800;
        }
        return 0;
    }

    function scoreGuardianName($user, $row)
    {
        $FirstName = $row['T'];

        foreach ($user->guardians as $guardian) {
            if (mb_substr(mb_strtolower($FirstName), 0, 4) === mb_substr(mb_strtolower($guardian->firstname), 0, 4)) {
                return 800;
            }
        }
        return 0;
    }

    function scoreGender($user, $row)
    {
        $gender = $row['K'];
        if ($user->gender === self::Gender_Male && $gender === 'Male') {
            return 201;
        }
        if ($user->gender === self::Gender_Female && $gender === 'Female') {
            return 200;
        }
    }

    function scoreStudentSurname($user, $row)
    {
        $SurName = $row['J'];
        if (mb_strpos(mb_strtolower($user->firstname . " " . $user->surname), mb_substr(mb_strtolower($SurName), 0, 4)) !== false) {
            return 800;
        }
        return 0;
    }

    public function GetMobilePhoneNumberData($User)
    {
        // Default country code. 0047 -> Norwegian country code, and all consumer grade norwegian phone numbers are 8 digits
        $CountryCode = '47';
        $AcceptedLengths = array(8);

        /**
         * Get School Owner specific settings for country code
         */
        $SchoolOwner = $this->school_owner;
        if ($SchoolOwner && $SchoolOwner->setting_messages_sms_country_code && $this->CountryCodeInBounds($SchoolOwner->setting_messages_sms_country_code)) {
            $CountryCode = $SchoolOwner->setting_messages_sms_country_code;
        }
        if ($SchoolOwner) {
            $AcceptedLengths_Temp = $this->GetAcceptedLengths($SchoolOwner->setting_messages_sms_accepted_lengths);
            if (!empty($AcceptedLengths))
                $AcceptedLengths = $AcceptedLengths_Temp;
        }

        $MobilePhoneNumber = $this->ExtractAndVerifyPhoneNumberFromString($CountryCode, $AcceptedLengths, $User->mobilephone);

        if (!$MobilePhoneNumber)
            return false;

        return array(
            'CountryCode' => $CountryCode,
            'MobilePhoneNumber' => $MobilePhoneNumber
        );
    }

    public function CountryCodeInBounds($CountryCode)
    {
        return is_numeric($CountryCode) && (int)$CountryCode > 0;
    }

    public function GetAcceptedLengths($CommaSeperatedLengthString)
    {
        $LengthArray = explode(',', $CommaSeperatedLengthString);
        $Return = array();
        foreach($LengthArray as $Length){
            $Length = (int)$Length;
            if($Length > 2 && $Length < 50){
                $Return[] = $Length;
            }
        }
        return $Return;
    }

    public function ExtractAndVerifyPhoneNumberFromString($CountryCode, $AcceptedLengths, $PhoneNumber)
    {

        $PhoneNumber = str_replace(array(' '), array(''), $PhoneNumber);

        /**
         * Remove +47 and 0047 paddings
         */
        $CountryCode_LeadingPlus = '+' . $CountryCode;
        $CountryCode_FourDigits = str_pad($CountryCode, 4, '0', STR_PAD_LEFT);
        if (substr($PhoneNumber, 0, strlen($CountryCode_LeadingPlus)) === $CountryCode_LeadingPlus)
            $PhoneNumber = substr($PhoneNumber, strlen($CountryCode_LeadingPlus));
        if (substr($PhoneNumber, 0, strlen($CountryCode_FourDigits)) === $CountryCode_FourDigits)
            $PhoneNumber = substr($PhoneNumber, strlen($CountryCode_FourDigits));

        /**
         * Check if phone number has a valid length
         */
        if (!in_array(strlen($PhoneNumber), $AcceptedLengths, true))
            return false;

        return (string)$PhoneNumber;
    }

    public function Email_Has($user, $Email)
    {
        $UserEmails = \DB::table('user_emails')->where('user_id', $user->id)->get();
        foreach ($UserEmails as $UserEmail) {
            if ($UserEmail->date_deleted === null && $UserEmail->email === mb_strtolower($Email)) {
                return $UserEmail;
            }
        }
        return null;
    }

    public function findActivity($row, $user = null)
    {
        $course_name = trim($row['A']);
        $activity_week = $row['C'];
        $week = explode(' ', $activity_week)[1];

        if ($course_name == 'Naturfags-opplevelser i marka')
            $course_name = 'Naturfagsopplevelser i marka';
        else if ($course_name == 'Speak! Learn English from English teachers')
            $course_name = 'Speak! Learn English with English teachers';
        else if($course_name == 'Programmering - lær å lage dataspill')
            $course_name = 'Programmering – lær å lage dataspill';
        else if($course_name == 'Sommerredaksjonen - lag avis')
            $course_name = 'Sommerredaksjonen – lag avis';
        else if(strpos($course_name,'up? Let'))
            $course_name = 'up? Let';
        else if($course_name == 'Fantasifulle fortellinger - musikk, dans og drama 1')
            $course_name = 'Fantasifulle fortellinger 1 med musikk, dans og drama';
        else if($course_name == 'Fantasifulle fortellinger med musikk, dans og drama 2')
            $course_name = 'Fantasifulle fortellinger 2 med musikk, dans og drama';
        else if(strpos($course_name,'Improve your English'))
            $course_name = 'Improve your English';
        else if(strpos($course_name,'oro med m'))
            $course_name = 'oro med m';
        else if(strpos($course_name,'ummer games'))
            $course_name = 'ummer games';
        else if($course_name == 'Kodegjengen! Matematikk og dataspill')
            $course_name = 'Kodegjengen – matematikk og dataspill ';
        else if($course_name == 'Forskere i farta!')
            $course_name = 'Forskere i farta';
        else if(strpos($course_name, 'odige monstre og trassige tryllestaver'))
            $course_name = 'odige monstre og trassige tryllestaver';
        else if(strpos($course_name, 'atterkula'))
            $course_name = 'atterkula';
        else if(strpos($course_name, 'erkelige oppfinnelser og'))
            $course_name = 'erkelige oppfinnelser og';
        else if (strpos($course_name, 'anoskolen'))
            $course_name = 'anoskolen';
        else if ($course_name == 'Sommerskole-patruljen')
            $course_name = 'Sommerskolepatruljen';
        else if ($course_name == 'Matematisk Hip hop! Regning og dans')
            $course_name = 'Matematisk Hiphop – regning og dans';

        $school_name = trim($row['B']);
        if(strpos($school_name, 'skole')){
            $school_name = explode(' ', $school_name)[0];
        } else if (strpos($school_name, 'niversitetet i Oslo')){
            $school_name = explode('/', $school_name)[1];
        } else if(strpos($school_name, 'vgs'))
            $school_name = explode(' ', $school_name)[0];

        $school = $this->schoolRepository->findWhere([['name' , 'like', '%'.$school_name.'%'], 'date_deleted' => null,
            ['ims_id', '!=', null], 'owner_id' => 9])->first();

        $activity = null;

        if($school != null) {
            $activity = $this->activityRepository->findWhere([
                ['Name','like','%'.$course_name.'%'.$week.'%'],
                'Options.SummerSchoolSchoolId' => (int)$school->id
            ], ['_id', 'CourseId', 'EnrollmentGroupId', 'Name'])->first();
        }

        if($activity == null) {
            $activity = $this->activityRepository->findWhere([
                ['Name','like','%'.$course_name.'%'.$week.'%']
            ], ['_id', 'CourseId', 'EnrollmentGroupId', 'Name'])->first();
        }

        if($activity != null) {
            $participant = [
                'ResourceId' => $user->id,
                'Priority' => (int)($row['H']),
                'Status' => \App\Containers\SummerSchool\Constants\Constant::PARTICIPANT_STATUS_REGISTERED,
                'GroupNumber' => 1,
                'RegisteredDate' => new \MongoDB\BSON\UTCDateTime(\Carbon\Carbon::now()->getTimestamp() * 1000),
                'RegisteredBy' => isset($user->guardians) && count($user->guardians) > 0 ? $user->guardians[0]->id : $user->id,
                'ActivityId' => new \MongoDB\BSON\ObjectID($activity->id),
                'EnrollmentGroupId' => new \MongoDB\BSON\ObjectID($activity->enrollment_group_id),
                'CourseId' => new \MongoDB\BSON\ObjectID($activity->course_id),
                'Batch' => self::Batch
            ];

            $this->participantRepository->create($participant);
            echo $user->firstname . ' apply to '. $activity->name .' ('.$activity->id.')';
            echo "\n";
        } else {
            var_dump($course_name.' '.$week.' '.$school_name. ' ' .$school->id);
        }
    }
}
