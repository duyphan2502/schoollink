<?php

use Netpower\JsLocalization\Caching\ConfigCachingService;
use Netpower\JsLocalization\Caching\MessageCachingService;
use Netpower\JsLocalization\Utils\Helper;

App::singleton('JsLocalizationHelper', function()
{
    return new Helper;
});

App::singleton('JsLocalizationMessageCachingService', function()
{
    return new MessageCachingService;
});

App::singleton('JsLocalizationConfigCachingService', function()
{
    return new ConfigCachingService();
});
