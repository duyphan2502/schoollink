<?php


Route::group([ 'namespace' => '\Netpower\JsLocalization\Http\Controllers' ], function()
{
    Route::get('/js-localization/messages/{type}', 'JsLocalizationController@createJsMessages');
    Route::get('/js-localization/config', 'JsLocalizationController@createJsConfig');
    Route::get('/js-localization/localization.js', 'JsLocalizationController@deliverLocalizationJS');

    Route::get('/js-localization/all.js', 'JsLocalizationController@deliverAllInOne');
});
