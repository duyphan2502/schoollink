<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 22/09/2016
 * Time: 3:46 PM
 */

namespace App\Core\Utils;


class SortUtil
{
    public static function AASort(&$array, $key)
    {
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            if (is_string($key)) {
                if (is_object($va)) {
                    $sorter[$ii] = $va->$key;
                } else {
                    $sorter[$ii] = $va[$key];
                }
            } else {
                if (is_callable($key)) {
                    $sorter[$ii] = $key($ii, $va);
                }
            }

        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[] = $array[$ii];
        }
        $array = $ret;
    }

    public static function multiAASort(&$Array, $Keys)
    {

        $DataArrays = array();
        foreach($Keys as $K => $V) {
            $DataArrays[$K] = array();
        }

        foreach ($Array as $key => $row) {
            foreach($Keys as $K => $V) {
                $DataArrays[$K][$key] = $row[$K];
            }
        }

        $Parameters = array();

        foreach ($Keys as $K => $Order){
            $Parameters[] = $DataArrays[$K];
            $Parameters[] = $Order;
        }
        $Parameters[] = &$Array;
        call_user_func_array('array_multisort', $Parameters);
    }
}