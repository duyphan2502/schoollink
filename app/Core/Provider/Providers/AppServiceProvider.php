<?php

namespace App\Core\Provider\Providers;

use App\Core\Provider\Abstracts\ServiceProviderAbstract;
use App\Core\Provider\Traits\AutoRegisterServiceProvidersTrait;
use App\Core\Provider\Traits\AppServiceProviderTrait;
use App\Core\Routes\Providers\RoutesServiceProvider;


/**
 * Class AppServiceProvider
 * The main Service Provider where all Service Providers gets registered
 * this is the only Service Provider that gets injected in the Config/app.php.
 *
 * Class AppServiceProvider
 *
 */
class AppServiceProvider extends ServiceProviderAbstract
{

    use AppServiceProviderTrait;
    use AutoRegisterServiceProvidersTrait;

    /**
     * the new Models Factories Paths
     */
    const MODELS_FACTORY_PATH = '/app/Core/Factory';

    /**
     * Core internal Service Provides.
     *
     * @var array
     */
    private $engineServiceProviders = [
        RoutesServiceProvider::class
    ];

    /**
     * Perform post-registration booting of services.
     */
    public function boot()
    {
        $this->registerServiceProviders(array_merge(
            $this->getContainersServiceProviders(),
            $this->engineServiceProviders
        ));
        
        $this->autoLoadViewsFromContainers();

        $this->overrideDefaultFractalSerializer();
        if(config('app.debug')) {
            \DB::connection('mongodb')->enableQueryLog();
        }
    }

    /**
     * Register bindings in the container.
     */
    public function register()
    {
        $this->changeTheDefaultDatabaseModelsFactoriesPath(self::MODELS_FACTORY_PATH);
        $this->publishContainersMigrationsFiles();
        $this->debugDatabaseQueries(true, false);
    }

}
