<?php

namespace App\Core\Provider\Abstracts;

use App\Core\Provider\Traits\AutoRegisterServiceProvidersTrait;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

/**
 * Class ServiceProviderAbstract.
 *
 */
abstract class ServiceProviderAbstract extends LaravelServiceProvider
{
    use AutoRegisterServiceProvidersTrait;
}
