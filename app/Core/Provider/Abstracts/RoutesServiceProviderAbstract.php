<?php

namespace App\Core\Provider\Abstracts;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as LaravelRouteServiceProvider;

/**
 * Class RoutesServiceProviderAbstract.
 *
 */
abstract class RoutesServiceProviderAbstract extends LaravelRouteServiceProvider
{

}
