<?php

namespace App\Core\Provider\Traits;

use App;
use DB;
use Log;

/**
 * Class AutoRegisterServiceProvidersTrait.
 *
 */
trait AutoRegisterServiceProvidersTrait
{
    /**
     * register an array of providers.
     *
     * @param array $providers
     */
    public function registerServiceProviders(array $providers)
    {
        foreach ($providers as $provider) {
            if (class_exists($provider)) {
                $this->app->register($provider);
            }
        }
    }

}
