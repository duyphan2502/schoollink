<?php

namespace App\Core\Model\Abstracts;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Model.
 *
 */
abstract class MongoModel extends Eloquent
{
    public $timestamps = false;
	
	protected $connection = 'mongodb';
}
