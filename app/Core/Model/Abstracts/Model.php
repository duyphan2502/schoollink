<?php

namespace App\Core\Model\Abstracts;

use Illuminate\Database\Eloquent\Model as LaravelEloquentModel;

/**
 * Class Model.
 *
 */
abstract class Model extends LaravelEloquentModel
{
    public $timestamps = false;
}
