<?php

namespace App\Core\Transformer\Abstracts;

use League\Fractal\TransformerAbstract as FractalTransformerAbstract;

/**
 * Class Transformer.
 *
 */
abstract class Transformer extends FractalTransformerAbstract
{

}
