<?php

namespace App\Core\Event\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as LaravelServiceProvider;

/**
 * Class CoreEventsServiceProvider
 *
 */
abstract class CoreEventsServiceProvider extends LaravelServiceProvider
{

}
