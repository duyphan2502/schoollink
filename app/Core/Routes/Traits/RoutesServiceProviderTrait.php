<?php

namespace App\Core\Routes\Traits;

use App\Core\Butler\Portals\Facade\CoreButler;
use Dingo\Api\Routing\Router as DingoApiRouter;
use Illuminate\Routing\Router as LaravelRouter;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class RoutesServiceProviderTrait.
 *
 */
trait RoutesServiceProviderTrait
{

    /**
     * Register all the containers routes files in the framework
     */
    private function registerRoutes()
    {
        $containersPaths = CoreButler::getContainersPaths();
        $containersNamespace = CoreButler::getContainersNamespace();
        
        foreach ($containersPaths as $containerPath) {
            $this->registerContainersApiRoutes($containerPath, $containersNamespace);
            $this->registerContainersWebRoutes($containerPath, $containersNamespace);
        }
    }

    /**
     * Register the Containers API routes files
     *
     * @param $containerPath
     * @param $containersNamespace
     */
    private function registerContainersApiRoutes($containerPath, $containersNamespace)
    {
        // get the container api routes path
        $apiRoutesPath = $containerPath . '/UI/API/Routes';

        if (File::isDirectory($apiRoutesPath)) {
            // get all files from the container API routes directory
            $directories = File::directories($apiRoutesPath);
            foreach ($directories as $directory) {
                $files = File::allFiles($directory);
                $apiVersionNumber = $this->getRouteFileVersionNumberOfFolder($directory);
                foreach ($files as $file) {

                    $this->apiRouter->version('v' . $apiVersionNumber,
                        function (DingoApiRouter $router) use ($file, $containerPath, $containersNamespace) {

                            $router->group([
                                // Routes Namespace
                                'namespace'  => $containersNamespace . '\\Containers\\' . basename($containerPath) . '\\UI\API\Controllers',
                                // Enable: API Rate Limiting
                                'middleware' => ['api.throttle', 'token'],
                                // The API limit time.
                                'limit'      => Config::get('api.limit'),
                                // The API limit expiry time.
                                'expires'    => Config::get('api.limit_expires'),
                            ], function ($router) use ($file) {
                                require $file->getPathname();
                            });
                        });
                }
            }
        }
    }

    /**
     * Register the Containers WEB routes files
     *
     * @param $containerPath
     * @param $containersNamespace
     */
    private function registerContainersWebRoutes($containerPath, $containersNamespace)
    {
        // get the container web routes path
        $webRoutesPath = $containerPath . '/UI/WEB/Routes';

        if (File::isDirectory($webRoutesPath)) {
            // get all files from the container Web routes directory
            $files = File::allFiles($webRoutesPath);

            foreach ($files as $file) {
                $this->webRouter->group([
                    'namespace' => $containersNamespace . '\\Containers\\' . basename($containerPath) . '\\UI\WEB\Controllers',
                    'middleware' => ['token'],
                ], function (LaravelRouter $router) use ($file) {
                    require $file->getPathname();
                });
            }
        }

    }

    /**
     * @param \Symfony\Component\Finder\SplFileInfo $file
     *
     * @return  mixed
     */
    private function getRouteFileNameWithoutExtension(SplFileInfo $file)
    {
        $fileInfo = pathinfo($file->getFileName());

        return $fileInfo['filename'];
    }

    /**
     * @param $fileNameWithoutExtension
     */
    private function getRouteFileVersionNumber($fileNameWithoutExtension)
    {
        $fileNameWithoutExtensionExploded = explode('.', $fileNameWithoutExtension);

        $apiVersionNumber = (int)end($fileNameWithoutExtensionExploded);

        return (is_int($apiVersionNumber) ? $apiVersionNumber : 1);
    }

    private function getRouteFileVersionNumberOfFolder($folder)
    {
        $fileNameWithoutExtensionExploded = explode('.', $folder);

        $apiVersionNumber = (int)end($fileNameWithoutExtensionExploded);

        return (is_int($apiVersionNumber) ? $apiVersionNumber : 1);
    }

}
