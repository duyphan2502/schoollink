<?php

namespace App\Core\Console\Abstracts;

use Illuminate\Console\Command as LaravelCommand;

/**
 * Class ConsoleCommand
 *
 */
abstract class CoreCommand extends LaravelCommand
{

}
