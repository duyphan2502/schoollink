<?php

namespace App\Core\Console\Commands;

use App\Core\Exception\Abstracts\Exception;
use Illuminate\Console\Command;

define("ACTIONTEMPLATE", "action.txt");
define("REQUESTTEMPLATE", "request.txt");
define("BUSINESSMODELSTEMPLATE", "business.txt");
define("MODELTEMPLATE", "model.txt");
define("CONTRACTTEMPLATE", "contract.txt");
define("ELOQUENTEMPLATE", "eloquent.txt");
define("CONTROLLERTEMPLATE", "controller.txt");
define("ROUTETEMPLATE", "route.txt");
define("REGISTERREPOSITORYTEMPLATE", "binding_repository.txt");
define("REGISTERREPOSITORYCONTRACTTEMPLATE", "register_repository_contract.txt");
define("REGISTERREPOSITORYELOQUENTTEMPLATE", "register_repository_eloquent.txt");

class Netpower extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'np:create {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Netpower commands';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->base_path = base_path();
        $this->template_path = app_path().'/Core/Console/Commands/Templates';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        if($type == 'action') {
            $this->actionHandler();
        } else if($type == 'request') {
            $this->requestHandler();
        } else if ($type == 'container') {
            $this->containerHandler();
        } else if ($type == 'entity') {
            $this->entityHandler();
        }
    }

    // <editor-fold desc="Action">

    private function actionHandler() {
        $container_name = $this->ask('What is Container name?' );
        $folder_name = $this->ask('What is Action\'folder name?' );
        $action_name = $this->ask('What is Action name?');

        if($this->checkContainerExist($container_name)) {
            $action_path = $this->checkActionFolderExist($folder_name, $container_name);
            if($action_path) {
                $this->createActionClass($action_name, $folder_name, $container_name, $action_path);
            } else {
                $this->error("Action folder doesn't exist!!!");
            }
        } else {
            $this->error("Container doesn't exist!!!");
        }
    }

    private function createActionClass($action_name, $folder_name, $container_name, $action_path) {
        try {
            $filename = $action_name.'Action.php';
            $path = $action_path.'/'.$filename;
            if(\File::exists($path)) {
                $this->error("action doesn't exists");
                return false;
            }

            $contents = file_get_contents( $this->template_path.'/Action/'. ACTIONTEMPLATE );
            $contents = str_replace(array(
                '{%CONTAINER%}',
                '{%ACTION_FOLDER%}',
                '{%NAME%}'
            ), array(
                $container_name,
                $folder_name,
                $action_name
            ), $contents );

            \File::put($path, $contents);

        }catch (Exception $ex) {
            $this->error('Cannot create action class');
        }
    }

    private function checkActionFolderExist($name, $container_name) {
        if($this->isNullOrEmptyString($name)) {
            return false;
        }

        $action_path = $this->base_path.'/app/Containers/'.$container_name.'/Actions/'.$name;
        if(!\File::exists($action_path)) {
            return;
        }

        return $action_path;
    }

    private function checkContainerExist($name) {
        $container_path = $this->base_path.'/app/Containers/'.$name;

        if(\File::exists($container_path)) {
            return true;
        }
        return false;
    }

    // </editor-fold>

    // <editor-fold desc="Container">

    private function containerHandler() {
        $container_name = $this->ask('What is container name?' );

        try {
            $container_path = $this->base_path.'/app/Containers/'.$container_name;
            if(!\File::exists($container_path)) {
                \File::makeDirectory($container_path);
            }
        } catch (Exception $e) {
            $this->error('Cantnot create component folder');
        }
    }

    private function isNullOrEmptyString($question){
        return (!isset($question) || trim($question)==='');
    }

    // </editor-fold>

    // <editor-fold desc="Entity">

    private function entityHandler() {
        $container_name = $this->ask('What is Container name?' );
        $entity_name = $this->ask('What is Entity name?' );

        if($this->checkContainerExist($container_name)) {
            $this->createActionFolder($entity_name, $container_name);
            $this->createBusinessModels($entity_name, $container_name);
            $this->createEntityContract($entity_name, $container_name);
            $this->createCriteriaFolder($entity_name, $container_name);
            $this->createEntityModel($entity_name, $container_name);
            $this->createEntityRepository($entity_name, $container_name);
            $this->createEntityController($entity_name, $container_name);
            $this->createEntityRequest($entity_name, $container_name);
            $this->createEntityRoute($entity_name, $container_name);
            $this->registerEntityRepository($entity_name, $container_name);
            $this->registerEntityRepositoryContract($entity_name, $container_name);
            $this->registerEntityRepositoryEloquent($entity_name, $container_name);
        } else {
            $this->error("Container doesn't exist!!!");
        }
    }

    private function createActionFolder($name, $container_name) {
        if($this->isNullOrEmptyString($name)) {
            return;
        }

        $action_path = $this->base_path.'/app/Containers/'.$container_name.'/Actions/'.$name;
        if(!\File::exists($action_path)) {
            \File::makeDirectory($action_path);
        }
    }

    private function createBusinessModels($name, $container_name) {
        try {
            $filename = $name.'.php';
            $path = $this->base_path.'/app/Containers/'.$container_name.'/BusinessModels/'.$filename;
            if(\File::exists($path)) {
                $this->error("entity business model exists");
                return;
            }

            $contents = file_get_contents( $this->template_path .'/Models/'. BUSINESSMODELSTEMPLATE );
            $contents = str_replace(array(
                '{%CONTAINER%}',
                '{%NAME%}'
            ), array(
                $container_name,
                $name
            ), $contents );

            \File::put($path, $contents);

        }catch (Exception $ex) {
            $this->error('Cannot create action class');
        }
    }

    private function createEntityContract($name, $container_name) {
        try {
            $filename = $name.'RepositoryInterface.php';
            $path = $this->base_path.'/app/Containers/'.$container_name.'/Contracts/'.$filename;
            if(\File::exists($path)) {
                $this->error("entity contract exists");
                return;
            }

            $contents = file_get_contents( $this->template_path .'/Contracts/'. CONTRACTTEMPLATE );
            $contents = str_replace(array(
                '{%CONTAINER%}',
                '{%NAME%}'
            ), array(
                $container_name,
                $name
            ), $contents );

            \File::put($path, $contents);

        }catch (Exception $ex) {
            $this->error('Cannot create action class');
        }
    }

    private function createEntityModel($name, $container_name) {
        try {
            $filename = $name.'.php';
            $path = $this->base_path.'/app/Containers/'.$container_name.'/Models/'.$filename;
            if(\File::exists($path)) {
                $this->error("entity model exists");
                return;
            }

            $contents = file_get_contents( $this->template_path .'/Models/'. MODELTEMPLATE );
            $contents = str_replace(array(
                '{%CONTAINER%}',
                '{%NAME%}'
            ), array(
                $container_name,
                $name
            ), $contents );

            \File::put($path, $contents);

        }catch (Exception $ex) {
            $this->error('Cannot create action class');
        }
    }

    private function createEntityRepository($name, $container_name) {
        try {
            $filename = $name.'Repository.php';
            $path = $this->base_path.'/app/Containers/'.$container_name.'/Repositories/Eloquent/'.$filename;
            if(\File::exists($path)) {
                $this->error("entity repository exists");
                return;
            }

            $contents = file_get_contents( $this->template_path .'/Repository/'. ELOQUENTEMPLATE );
            $contents = str_replace(array(
                '{%CONTAINER%}',
                '{%NAME%}'
            ), array(
                $container_name,
                $name
            ), $contents );

            \File::put($path, $contents);

        }catch (Exception $ex) {
            $this->error('Cannot create action class');
        }
    }

    private function createEntityController($name, $container_name) {
        try {
            $filename = $name.'Controller.php';
            $path = $this->base_path.'/app/Containers/'.$container_name.'/UI/API/Controllers/'.$filename;
            if(\File::exists($path)) {
                $this->error("entity controller exists");
                return;
            }

            $contents = file_get_contents( $this->template_path .'/Controller/'. CONTROLLERTEMPLATE );
            $contents = str_replace(array(
                '{%CONTAINER%}',
                '{%NAME%}'
            ), array(
                $container_name,
                $name
            ), $contents );

            \File::put($path, $contents);

        }catch (Exception $ex) {
            $this->error('Cannot create action class');
        }
    }

    private function createEntityRequest($name, $container_name) {
        try {
            if($this->isNullOrEmptyString($name)) {
                return;
            }

            $action_path = $this->base_path.'/app/Containers/'.$container_name.'/UI/API/Requests/'.$name;
            if(!\File::exists($action_path)) {
                \File::makeDirectory($action_path);
            }

        }catch (Exception $ex) {
            $this->error('Cannot create action class');
        }
    }

    private function createEntityRoute($name, $container_name) {
        try {
            $filename = lcfirst($name).'.php';
            $path = $this->base_path.'/app/Containers/'.$container_name.'/UI/API/Routes/v.1/'.$filename;
            if(\File::exists($path)) {
                $this->error("route entity exists");
                return;
            }

            $contents = file_get_contents( $this->template_path .'/Route/'. ROUTETEMPLATE );
            $contents = str_replace(array(
                '{%CONTAINER%}',
                '{%NAME%}'
            ), array(
                $container_name,
                $name
            ), $contents );

            \File::put($path, $contents);

        }catch (Exception $ex) {
            $this->error('Cannot create action class');
        }
    }

    private function registerEntityRepository($name, $container_name) {
        $path = $this->base_path.'/app/Containers/'.$container_name.'/Providers/'.$container_name.'ServiceProvider.php';
        $template_content = file_get_contents( $this->template_path .'/'. REGISTERREPOSITORYTEMPLATE);
        $template_content = str_replace(array(
            '{%CONTAINER%}',
            '{%NAME%}'
        ), array(
            $container_name,
            $name
        ), $template_content );

        $original_content = file($path);
        foreach($original_content as $lineNumber => &$lineContent) {
            if(str_contains($lineContent, '#binding_repository_flag')) {
                $lineContent .= $template_content . PHP_EOL;
                break;
            }
        }

        $allContent = implode("", $original_content);
        \File::put($path, $allContent);
    }

    private function registerEntityRepositoryContract($name, $container_name) {
        $path = $this->base_path.'/app/Containers/'.$container_name.'/Providers/'.$container_name.'ServiceProvider.php';

        $template_content = file_get_contents( $this->template_path .'/'. REGISTERREPOSITORYCONTRACTTEMPLATE);
        $template_content = str_replace(array(
            '{%CONTAINER%}',
            '{%NAME%}'
        ), array(
            $container_name,
            $name
        ), $template_content );

        $original_content = file($path);
        foreach($original_content as $lineNumber => &$lineContent) {
            if(str_contains($lineContent, '#register_repository_contract')) {
                $lineContent .= $template_content . PHP_EOL;
                break;
            }
        }

        $allContent = implode("", $original_content);
        \File::put($path, $allContent);
    }

    private function registerEntityRepositoryEloquent($name, $container_name) {
        $path = $this->base_path.'/app/Containers/'.$container_name.'/Providers/'.$container_name.'ServiceProvider.php';

        $template_content = file_get_contents( $this->template_path .'/'. REGISTERREPOSITORYELOQUENTTEMPLATE);
        $template_content = str_replace(array(
            '{%CONTAINER%}',
            '{%NAME%}'
        ), array(
            $container_name,
            $name
        ), $template_content );

        $original_content = file($path);
        foreach($original_content as $lineNumber => &$lineContent) {
            if(str_contains($lineContent, '#register_repository_eloquent')) {
                $lineContent .= $template_content . PHP_EOL;
                break;
            }
        }

        $allContent = implode("", $original_content);
        \File::put($path, $allContent);
    }

    private function createCriteriaFolder($name, $container_name) {
        if($this->isNullOrEmptyString($name)) {
            return;
        }

        $action_path = $this->base_path.'/app/Containers/'.$container_name.'/Criterias/'.$name;
        if(!\File::exists($action_path)) {
            \File::makeDirectory($action_path);
        }
    }

    // </editor-fold>

    // <editor-fold desc="Request">

    private function requestHandler() {
        $container_name = $this->ask('What is Container name?' );
        $folder_name = $this->ask('What is Request\'folder name?' );
        $action_name = $this->ask('What is Request name?');

        if($this->checkContainerExist($container_name)) {
            $request_path = $this->checkRequestFolderExist($folder_name, $container_name);
            if($request_path) {
                $this->createRequestClass($action_name, $folder_name, $container_name, $request_path);
            } else {
                $this->error("Request folder doesn't exist!!!");
            }
        } else {
            $this->error("Container doesn't exist!!!");
        }
    }

    private function createRequestClass($request_name, $folder_name, $container_name, $request_path) {
        try {
            $filename = $request_name.'Request.php';
            $path = $request_path.'/'.$filename;

            if(\File::exists($path)) {
                $this->error("request doesn't exists");
                return false;
            }

            $contents = file_get_contents( $this->template_path.'/Request/'. REQUESTTEMPLATE );
            $contents = str_replace(array(
                '{%CONTAINER%}',
                '{%REQUEST_FOLDER%}',
                '{%NAME%}'
            ), array(
                $container_name,
                $folder_name,
                $request_name
            ), $contents );

            \File::put($path, $contents);

        }catch (Exception $ex) {
            $this->error('Cannot create action class');
        }
    }

    private function checkRequestFolderExist($name, $container_name) {
        if($this->isNullOrEmptyString($name)) {
            return false;
        }
        $request_path = $this->base_path.'/app/Containers/'.$container_name.'/UI/API/Requests/'.$name;

        if(!\File::exists($request_path)) {
            return;
        }
        return $request_path;
    }

    // </editor-fold>
}
