<?php

namespace App\Containers\{%CONTAINER%}\BusinessModels;

use App\Core\Model\Abstracts\BusinessModel;

/**
 * Class {%NAME%}.
 *
 */
class {%NAME%} extends BusinessModel
{

}
