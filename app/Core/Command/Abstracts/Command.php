<?php

namespace App\Core\Commands\Abstracts;

use App\Core\Commands\Traits\DispatcherTrait;
use Illuminate\Foundation\Bus\DispatchesJobs as LaravelDispatchesJobs;

/**
 * Class Command.
 *
 */
abstract class Command
{
    use LaravelDispatchesJobs;
    use DispatcherTrait;
}
