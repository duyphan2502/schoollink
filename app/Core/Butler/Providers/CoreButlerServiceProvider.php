<?php

namespace App\Core\Butler\Providers;

use App\Core\Provider\Abstracts\ServiceProviderAbstract;
use App\Core\Butler\Portals\CoreButler;

/**
 * Class CoreButlerServiceProvider
 *
 */
class CoreButlerServiceProvider extends ServiceProviderAbstract
{

    /**
     * Register bindings in the container.
     */
    public function register()
    {
        $this->app->bind('CoreButler', function () {
            return $this->app->make(CoreButler::class);
        });
    }
}
