<?php

namespace App\Core\Butler\Portals\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Class CoreButler
 *
 */
class CoreButler extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'CoreButler';
    }

}

