<?php

namespace App\Core\Criterias\Abstracts;

use Prettus\Repository\Contracts\CriteriaInterface as PrettusCriteriaInterface;

/**
 * Class Criteria.
 *
 */
abstract class Criteria implements PrettusCriteriaInterface
{

}
