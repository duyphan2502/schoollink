<?php

namespace App\Core\Controller\Abstracts;

use App\Core\Controller\Contracts\WebControllerInterface;

/**
 * Class CoreWebController.
 *
 */
abstract class CoreWebController extends CoreController implements WebControllerInterface
{

    /**
     * CoreWebController constructor.
     */
    public function __construct()
    {

    }

}
