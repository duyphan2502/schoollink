<?php

namespace App\Core\Controller\Abstracts;

use App\Core\Controller\Contracts\ApiControllerInterface;
use Dingo\Api\Routing\Helpers as DingoApiHelper;

/**
 * Class CoreApiController.
 *
 */
abstract class CoreApiController extends CoreController implements ApiControllerInterface
{
    use DingoApiHelper;
}
