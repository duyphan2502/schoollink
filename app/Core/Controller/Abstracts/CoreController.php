<?php

namespace App\Core\Controller\Abstracts;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as LaravelController;

/**
 * Class CoreController.
 *
 * A.K.A (app/Http/Controllers/Controller.php)
 *
 * You are not allowed to extend from this class directly.
 *
 */
abstract class CoreController extends LaravelController
{
    use DispatchesJobs, ValidatesRequests;
}
