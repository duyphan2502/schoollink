<?php

namespace App\Core\Middleware\Middlewares;

/**
 * Class Kernel
 *
 */
class Kernel
{

    /**
     * The application's global HTTP middleware stack.
     *
     * @return  array
     */
    public function applicationMiddlewares()
    {
        return [
            // Laravel default middleware's
            \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
            \App\Core\Middleware\Middlewares\Http\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \App\Core\Middleware\Middlewares\Http\VerifyCsrfToken::class,
            // CORS Package middleware
            \Barryvdh\Cors\HandleCors::class,
        ];
    }

    /**
     * The application's route middleware.
     *
     * @return  array
     */
    public function routeMiddlewares()
    {
        return [
            // JWT Package middleware's
            'jwt.auth'    => \Tymon\JWTAuth\Middleware\GetUserFromToken::class,
            'jwt.refresh' => \Tymon\JWTAuth\Middleware\RefreshToken::class,
            'token' => \App\Containers\SchoolLink\Middlewares\AuthenticateToken::class
        ];

    }

}
