<?php

namespace App\Core\Middleware;

use App\Core\Middleware\Middlewares\Kernel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Http\Kernel as LaravelHttpKernel;
use Illuminate\Routing\Router;

/**
 * Class CoreHttpMiddleware
 *
 * A.K.A (app/Http/Core.php)
 *
 */
class CoreHttpMiddleware extends LaravelHttpKernel
{

    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [];


    protected $middlewareGroups = [
        'web' => [
            \App\Core\Middleware\Middlewares\Http\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Core\Middleware\Middlewares\Http\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * CoreHttpMiddleware constructor.
     *
     * @param \Illuminate\Contracts\Foundation\Application $app
     * @param \Illuminate\Routing\Router                   $router
     */
    public function __construct(Application $app, Router $router, Kernel $kernel)
    {
        $this->middleware = $kernel->applicationMiddlewares();
        $this->pushMiddleware('Barryvdh\Debugbar\Middleware\Debugbar');
        $this->routeMiddleware = $kernel->routeMiddlewares();

        parent::__construct($app, $router);
    }


}
