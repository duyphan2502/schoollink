<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 04/10/2016
 * Time: 10:03 AM
 */

namespace App\Core\Contracts;


interface MongoRepositoryInterface
{
    public function customWhere(array $custom_rules, $columns);
    public function push($parameters, $id);
    public function pull($wheres, $column, $values = null);
    public function updateOne($wheres, $values);
    public function count($wheres);
}