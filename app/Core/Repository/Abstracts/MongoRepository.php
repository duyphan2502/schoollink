<?php

namespace App\Core\Repository\Abstracts;

use App\Core\Contracts\MongoRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;
use Prettus\Repository\Criteria\RequestCriteria as PrettusRequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository as PrettusRepository;
use Prettus\Repository\Events\RepositoryEntityDeleted;

/**
 * Class Repository.
 *
 */
abstract class MongoRepository extends PrettusRepository implements MongoRepositoryInterface
{
    private static $cacheKeys = [];

    abstract public function business_model();

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(PrettusRequestCriteria::class));
    }

    public function find($id, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->find($id, $columns);
        $this->resetModel();

        return $this->parserResult($model);
    }

    public function delete($id)
    {
        $this->applyScope();

        $temporarySkipPresenter = $this->skipPresenter;
        $this->skipPresenter(true);

        $model = $this->model->find($id);
        $originalModel = clone $model;

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        $deleted = $model->delete();

        event(new RepositoryEntityDeleted($this, $originalModel));

        return $deleted;
    }

    public function deleteWhere(array $where)
    {
        $this->applyScope();

        $temporarySkipPresenter = $this->skipPresenter;
        $this->skipPresenter(true);

        $this->applyConditions($where);

        $deleted = $this->model->delete();

        event(new RepositoryEntityDeleted($this, $this->model->getModel()));

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        return $deleted;
    }

    public function customWhere(array $custom_rules, $columns = ['*']){
        $this->applyCriteria();
        $this->applyScope();

        foreach ($custom_rules as $rule => $custom_rule) {
            if($rule == 'where') {
                $this->applyConditions($custom_rule);
            } elseif ($rule == 'whereNotNull' || $rule == 'whereNull') {
                foreach ($custom_rule as $field) {
                    $this->model = $this->model->$rule($field);
                }
            } else {
                list($field, $values) = $custom_rule;

                $this->model = $this->model->$rule($field, $values);
            }
        }

        $model = $this->model->get($columns);
        $this->resetModel();
        return $this->parserResult($model);
    }

    protected function applyConditions(array $where)
    {
        foreach ($where as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                if($condition == 'in') {
                    $this->model = $this->model->whereIn($field, $val);
                } else if ($condition == 'notIn') {
                    $this->model = $this->model->whereNotIn($field, $val);
                } else if($condition == 'nn') {
                    $this->model = $this->model->$value($field);
                } else {
                    $this->model = $this->model->where($field, $condition, $val);
                }
            } else {
                $this->model = $this->model->where($field, '=', $value);
            }
        }
    }

    public function push($parameters, $id)
    {
        $this->applyCriteria();
        $this->applyScope();

        $unique = true;
        if (count($parameters) == 3) {
            list($column, $values, $unique) = $parameters;
        } else {
            list($column, $values) = $parameters;
        }

        $result = $this->model->where('_id', new ObjectID($id))
            ->push($column, $values, $unique);

        $this->resetModel();
        return $result;
    }

    public function pull($wheres, $column, $values = null)
    {
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($wheres);
        $result = $this->model->pull($column, $values);

        $this->resetModel();
        return $result;
    }

    public function count($wheres)
    {
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($wheres);

        $result = $this->model->count();

        $this->resetModel();
        return $result;
    }

    public function drop($wheres, $column)
    {
        $this->applyCriteria();
        $this->applyConditions($wheres);

        $result = $this->model->drop($column);

        $this->resetModel();
        return $result;
    }

    public function updateOne($wheres, $values)
    {
        $this->applyScope();
        $this->applyConditions($wheres);

        $result = $this->model->update($values);


        $this->resetModel();
        return $result;
    }

    public function parserResult($result)
    {
        if($result instanceof Collection) {
            $result = $this->convertCollection($result);
        } else if($result instanceof LengthAwarePaginator){
            $items = $this->convertCollection(Collection::make($result->items()));
            $result = new LengthAwarePaginator($items, $result->total(),$result->perPage(),$result->currentPage());
        } else {
            $result = $this->convertMongoModelToBusinessModel($result);
        }
        return $result;
    }

    public function convert($result)
    {
        if($result instanceof Collection) {
            $result = $result->map(function ($model) {
                return (object) $model;
            });
        } else if($result instanceof LengthAwarePaginator){
            $items = $items->map(function ($model) {
                return (object) $model;
            });
            $result = new LengthAwarePaginator($items, $result->total(),$result->perPage(),$result->currentPage());
        } else {
            return (object) $result;
        }
        return $result;
    }

    protected function convertCollection($result)
    {
        $result = $result->map(function ($model) {
            $model = $this->convertMongoModelToBusinessModel($model);
            return $model;
        });
        return $result;
    }

    protected function convertMongoModelToBusinessModel($model)
    {
        if(!isset($model)) {
            return null;
        }

        $business_model = $this->app->make($this->business_model());
        $business_model->id = $model->getKey();

        $fields = $model->attributesToArray();
        foreach ($fields as $field=>$value) {
            //$value = $model->$field;
            $new_name = $this->formatName($field);

            if(is_array($value)) {
                if($this->isAssoc($value)){
                    $new_value = [];
                    foreach ($value as $item) {
                        $object = $this->convertArrayToObject($item);
                        $new_value[] = $object;
                    }
                    $value = $new_value;
                } else {
                    $value = $this->convertArrayToObject($value);
                }
            } else if ($value instanceof UTCDatetime){
                $value = $value->toDateTime()->getTimeStamp();
            } else if($value instanceof ObjectID) {
                $value = (string)$value;
            }
            $business_model->$new_name = $value;
        }

        return $business_model;
    }

    protected function convertArrayToObject($array)
    {
        if(!is_array($array)) {
            return $array;
        }

        $item = new \stdClass();
        foreach ($array as $key => $value) {
            $key_name = $this->formatName($key);
            if(is_array($value)) {
                if(empty($value)) {
                    $item->$key_name = $value;
                    continue;
                }

                if($this->isAssoc($value)){
                    $new_value = [];
                    foreach ($value as $item) {
                        $object = $this->convertArrayToObject($item);
                        $new_value[] = $object;
                    }
                    $value = $new_value;
                } else {
                    $value = $this->convertArrayToObject($value);
                }
            } else if ($value instanceof UTCDatetime){
                $value = $value->toDateTime()->getTimeStamp();
            } else if($value instanceof ObjectID) {
                $value = (string)$value;
            }

            if(!empty($key_name)){
                $item->$key_name = $value;
            }
        }

        return $item;
    }

    protected function formatName($field)
    {
        if($field == '_id') {
            return 'id';
        }

        if(isset(self::$cacheKeys[$field])) {
            return self::$cacheKeys[$field];
        }

        $name_uppercase_explodes = preg_split('/(?=[A-Z])/', $field);
        $name_uppercase = [];
        foreach ($name_uppercase_explodes as $name_uppercase_explode) {
            if($name_uppercase_explode != '') {
                if(strpos($name_uppercase_explode, '__') > -1){
                    $name_uppercase [] = str_replace('__', '__', $name_uppercase_explode);
                } else {
                    $name_uppercase [] = str_replace('_', '', $name_uppercase_explode);
                }
            }
        }

        $new_name = strtolower(implode('_', $name_uppercase));

        self::$cacheKeys[$field] = $new_name;
        return $new_name;
    }

    private function isAssoc($arr)
    {
        return isset($arr[0]);
    }
}
