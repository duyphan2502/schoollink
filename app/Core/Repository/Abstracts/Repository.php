<?php

namespace App\Core\Repository\Abstracts;

use App\Core\Contracts\EloquentRepositoryInterface;
use App\Core\Model\Abstracts\BusinessModel;
use App\Core\Model\Abstracts\MongoModel;
use App\Core\Repository\Cache\EloquentCacheableRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;
use Monolog\Handler\Mongo;
use Prettus\Repository\Contracts\CacheableInterface as PrettusCacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria as PrettusRequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository as PrettusRepository;
use Prettus\Repository\Events\RepositoryEntityDeleted;
use Prettus\Repository\Traits\CacheableRepository as PrettusCacheableRepository;

/**
 * Class Repository.
 *
 */
abstract class Repository extends PrettusRepository implements PrettusCacheableInterface, EloquentRepositoryInterface
{
    use PrettusCacheableRepository;
    //use EloquentCacheableRepository;

    abstract public function business_model();

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(PrettusRequestCriteria::class));
    }

    /**
     * Applies the given where conditions to the model.
     *
     * @param array $where
     * @return void
     */
    protected function applyConditions(array $where)
    {
        foreach ($where as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                if($condition == 'in') {
                    $this->model = $this->model->whereIn($field, $val);
                } else {
                    $this->model = $this->model->where($field, $condition, $val);
                }
            } else {
                $this->model = $this->model->where($field, '=', $value);
            }
        }
    }

    public function deleteWhere(array $where)
    {
        $this->applyScope();

        $temporarySkipPresenter = $this->skipPresenter;
        $this->skipPresenter(true);

        $this->applyConditions($where);

        $deleted = $this->model->delete();

        event(new RepositoryEntityDeleted($this, $this->model->getModel()));

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        return $deleted;
    }

    public function delete($id)
    {
        $this->applyScope();

        $temporarySkipPresenter = $this->skipPresenter;
        $this->skipPresenter(true);

        $model = $this->model->find($id);
        $originalModel = clone $model;

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        $deleted = $model->delete();

        event(new RepositoryEntityDeleted($this, $originalModel));

        return $deleted;
    }

    public function parserResult($result)
    {
        if ($this->presenter instanceof PresenterInterface) {
            if ($result instanceof Collection || $result instanceof LengthAwarePaginator) {
                $result->each(function ($model) {
                    if ($model instanceof Presentable) {
                        $model->setPresenter($this->presenter);
                    }

                    return $model;
                });
            } elseif ($result instanceof Presentable) {
                $result = $result->setPresenter($this->presenter);
            }

            if (!$this->skipPresenter) {
                return $this->presenter->present($result);
            }
        }

        if($result instanceof Collection) {
            $result = $this->convertCollection($result);
        } else if($result instanceof LengthAwarePaginator){
            $items = $this->convertCollection(Collection::make($result->items()));
            $result = new LengthAwarePaginator($items, $result->total(),$result->perPage(),$result->currentPage());
        } else {
            $result = $this->convertToBusinessModel($result);
        }

        return $result;
    }

    protected function convertCollection($result)
    {
        $result = $result->map(function ($model) {
            $model = $this->convertToBusinessModel($model);
            return $model;
        });
        return $result;
    }


    protected function convertToBusinessModel($model)
    {
        if(isset($model)) {
            $attributes = $model->attributesToArray();
            $business_model = $this->app->make($this->business_model());
            foreach ($attributes as $key => $value) {
                if($key == 'ID') {
                    $business_model->id = $value;
                } else {
                    $business_model->$key = $value;
                }
            }

            $fields = $model->attributesToArray();
            foreach ($fields as $field=>$value) {
                if(!isset($business_model->$field)){
                    $business_model->$field = null;
                }
            }
            return $business_model;
        } else {
            return null;
        }
    }
}
