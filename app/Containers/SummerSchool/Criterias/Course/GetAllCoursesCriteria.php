<?php

namespace App\Containers\SummerSchool\Criterias\Course;

use App\Core\Criterias\Abstracts\Criteria;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetAllCoursesCriteria extends Criteria
{
	private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        $query = $model

            ->where('Active', true)
            ->whereIn('SchoolOwnerId', $this->params['school_owner_ids'])
            ->whereIn('SchoolId', $this->params['school_ids']);

        if(isset($this->params['year'])) {
            $query
                ->where('DateCreated', '>=', Carbon::createFromDate($this->params['year'], 1, 1))
                ->where('DateCreated', '<=', Carbon::createFromDate($this->params['year'], 12, 31));
        }

        if(!isset($this->params['include_archive'])) {
            $query->where('DateDeleted', null)
                ->where('DeletedBy', null);
        }

        if(!isset($this->params['course_ids'])) {
            return $query;
        }

    	return $query->whereIn('_id', $this->params['course_ids']);
    }
}