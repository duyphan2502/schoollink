<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 20/10/2016
 * Time: 10:02 AM
 */

namespace App\Containers\SummerSchool\Criterias\Course;

use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetCourseCriteria extends Criteria
{
    private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        $query = $model
            ->where('DateDeleted', null)
            ->where('DeletedBy', null)
            ->where('_id', $this->params['id']);

        return $query;
    }
}