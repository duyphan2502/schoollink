<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/10/2016
 * Time: 2:45 PM
 */

namespace App\Containers\SummerSchool\Criterias\Course;

use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetCoursesByActivityIdCriteria extends Criteria
{
    private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        $query = $model
            ->where('DateDeleted', null)
            ->where('DeletedBy', null)
            ->where('Active', true)
            ->where('Owner.Type', $this->params['owner_type'])
            ->where('CourseTypeId', $this->params['course_type_id'])
            ->where('SchoolOwnerId', $this->params['school_owner_id'])
            ->where('SchoolId', $this->params['school_id']);

        return $query;
    }
}