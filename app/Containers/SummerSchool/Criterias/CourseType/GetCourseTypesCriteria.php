<?php

namespace App\Containers\SummerSchool\Criterias\CourseType;

use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetCourseTypesCriteria extends Criteria
{
    private $school_owner_ids;
    private $school_ids;

    public function __construct($school_owner_ids, $school_ids)
    {
        $this->school_owner_ids = $school_owner_ids;
        $this->school_ids = $school_ids;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
    	return $model->whereIn('SchoolOwnerId', $this->school_owner_ids)
                                    ->whereIn('SchoolId', $this->school_ids);
    }
}