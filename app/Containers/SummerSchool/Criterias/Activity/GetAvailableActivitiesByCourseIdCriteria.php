<?php

namespace App\Containers\SummerSchool\Criterias\Activity;

use App\Core\Criterias\Abstracts\Criteria;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetAvailableActivitiesByCourseIdCriteria extends Criteria
{
	private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
    	return $model
            ->where('CourseId', new ObjectID($this->params['course_id']))
            ->whereNotNull('DateActivated')
            ->whereNull('DateDeleted');
    }
}