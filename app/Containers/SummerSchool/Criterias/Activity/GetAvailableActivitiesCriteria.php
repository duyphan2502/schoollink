<?php

namespace App\Containers\SummerSchool\Criterias\Activity;

use App\Core\Criterias\Abstracts\Criteria;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetAvailableActivitiesCriteria extends Criteria
{
	private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
    	return $model->where('SchoolOwnerId', (int)$this->params['school_owner_id'])
                    ->where('SchoolId', (int)$this->params['school_id'])
                    ->where('CourseTypeId', new ObjectID($this->params['course_type_id']))
                    ->whereNotNull('DateActivated')
                    ->where('Owner.Type', $this->params['owner_type'])
                    ->where('Active', true)
                    ->where('Owner.SchoolId', (int)$this->params['owner_school_id'])
                    //->where('DateEnd', '>', Carbon::createFromTimestampUTC($this->params['date']))
                    ->where('EnrollmentGroupId', new ObjectID($this->params['enrollment_group_id']))
                    ->whereNull('DateDeleted');
    }
}