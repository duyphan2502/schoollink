<?php

namespace App\Containers\SummerSchool\Criterias\Activity;

use App\Core\Criterias\Abstracts\Criteria;
use MongoDB\BSON\ObjectID;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetAllActivitiesCriteria extends Criteria
{
	private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
    	$owner_ors = $this->params['owner_ors'];
    	$query = $model->whereIn('SchoolOwnerId', $this->params['school_owner_ids'])
                                    ->whereIn('SchoolId', $this->params['school_ids'])
                                    ->whereNotNull('DateActivated')
                                    ->whereNull('DateDeleted')
                                    ->where(function($query) use($owner_ors) {
                                        foreach ($owner_ors as $owner_or) {
                                            $query->orWhere(function($query) use($owner_or) {
                                                foreach ($owner_or as $key => $owner) {
                                                    $query->where($key, $owner);
                                                }
                                            });
                                        }
                                    });

        if(isset($this->params['enrollment_group_id'])) {
            $query = $query->where('EnrollmentGroupId', new ObjectID($this->params['enrollment_group_id']));
        }

        return $query;
    }
}