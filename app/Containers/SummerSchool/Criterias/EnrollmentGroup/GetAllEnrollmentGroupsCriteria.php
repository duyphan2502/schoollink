<?php

namespace App\Containers\SummerSchool\Criterias\EnrollmentGroup;

use App\Core\Criterias\Abstracts\Criteria;
use Carbon\Carbon;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetAllEnrollmentGroupsCriteria extends Criteria
{
	private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
    	 $owner_ors = $this->params['owner_ors'];

    	 $query = $model->whereIn('SchoolOwnerId', $this->params['school_owner_ids'])
                                    ->whereIn('SchoolId', $this->params['school_ids'])
                                    ->whereNotNull('DateActivated')
                                    ->whereNull('DateDeleted')
                                    ->orWhere(function($query) use($owner_ors) {
                                        foreach ($owner_ors as $owner_or) {
                                            $query->orWhere(function($query) use($owner_or) {
                                                foreach ($owner_or as $key => $owner) {
                                                    $query->where($key, $owner);
                                                }    
                                            });
                                        }
                                    });

        if(isset($this->params['year']) && $this->params['year'] != 0) {
            $query
                ->where('DateStart', '>=', Carbon::createFromDate($this->params['year'] - 1, 12, 31))
                ->where('DateEnd', '<=', Carbon::createFromDate($this->params['year'] + 1, 1, 1));
        }

        return $query;
    }
}