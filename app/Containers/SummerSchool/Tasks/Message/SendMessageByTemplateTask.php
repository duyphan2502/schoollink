<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/11/2016
 * Time: 2:46 PM
 */

namespace App\Containers\SummerSchool\Tasks\Message;


use App\Containers\SchoolLink\Contracts\MessageTemplateRepositoryInterface;
use App\Core\Task\Abstracts\Task;
use GuzzleHttp\Client;

class SendMessageByTemplateTask extends Task
{
    private $message_template_repository;

    public function __construct(MessageTemplateRepositoryInterface $message_template_repository)
    {
        $this->message_template_repository = $message_template_repository;
    }

    public function run(array $recipients, $template, $data)
    {
        $app = \Session::get('app');
        $special_school = $app->special_school;

        $message = $this->message_template_repository->orderBy('id', 'desc')->findWhere([
            'sub_school' => $special_school->id,
            'deleted' => false,
            'date_replaced' => null,
            'name' => $template
        ])->first();

        $data = [
            'data' => $data,
            'recipients' => $recipients,
            'delivery_method' => 'Email',
            'message_template ' => $message->message_template
        ];

        $client = new Client();
        $res = $client->request('POST', config('app.url').'/Api/Web/Messages/SendMessage', [
            'form_params' => $data
        ]);
        $status = $res->getStatusCode();

        if($status == 200)
            return true;

        return false;
    }
}