<?php

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 24/11/2016
 * Time: 2:18 PM
 */

namespace App\Containers\SummerSchool\Tasks\Audit;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SummerSchool\Contracts\NewsFeedRepositoryInterface;
use App\Core\Exception\Abstracts\Exception;
use App\Core\Task\Abstracts\Task;
use Carbon\Carbon;
use MongoDB\BSON\UTCDateTime;

class AddGeneralAuditLogTask extends Task
{
    private $news_feed_repository;
    private $user_role_repository;
    public function __construct(NewsFeedRepositoryInterface $news_feed_repository,
        UserRoleRepositoryInterface $user_role_repository)
    {
        $this->news_feed_repository = $news_feed_repository;
        $this->user_role_repository = $user_role_repository;
    }

    public function run($comment, $data = null)
    {
        try {
            $app = \Session::get('app');
            $current_user = $app->current_user;
            $special_school = $app->special_school;
            $user_ids = $this->getSchoolAdminUserIdsOfSchool($special_school->id);

            $log = [
                'EntityId' => null,
                'EntityType' => 'UserStaff',
                'Data' => (object) [
                    'DataType' => 'GenericFeedDataType',
                    'Data' => (object)[
                        'DataType' => 'Audit',
                        'Data' => (object)[
                            'UpdatedBy' => $current_user->id,
                            'UserId' => null,
                            'Comment' => $comment,
                            'Data' => $data
                        ]
                    ]
                ],
                'DateUpdated' => new UTCDateTime(Carbon::now()->getTimestamp() * 1000),
                'DateForSorting' => new UTCDateTime(Carbon::now()->getTimestamp() * 1000),
                'DateCreated' => new UTCDateTime(Carbon::now()->getTimestamp() * 1000),
                'DateHidden' => null
            ];
            foreach ($user_ids as $user_id) {
                $log['EntityId'] = $user_id;
                $log['Data']->Data->Data->UserId = $user_id;
                $this->news_feed_repository->create($log);
            }
            return true;
        } catch (Exception $ex) {
            \Log::error($ex);
            return false;
        }
    }

    private function getSchoolAdminUserIdsOfSchool($school_id)
    {
        $user_ids = $this->user_role_repository->findWhere(['school_id' => $school_id,
            'role_id' => Constant::ROLE_SCHOOL_ADMIN])->pluck('user_id');

        return $user_ids;
    }
}