<?php

namespace App\Containers\SummerSchool\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CourseRepositoryInterface.
 *
 */
interface CustomFieldRepositoryInterface extends RepositoryInterface
{

}
