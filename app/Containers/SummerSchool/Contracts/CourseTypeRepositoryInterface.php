<?php

namespace App\Containers\SummerSchool\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CourseTypeRepositoryInterface.
 *
 */
interface CourseTypeRepositoryInterface extends RepositoryInterface
{

}
