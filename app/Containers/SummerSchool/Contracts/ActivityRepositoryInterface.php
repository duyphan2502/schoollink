<?php

namespace App\Containers\SummerSchool\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ActivityRepositoryInterface.
 *
 */
interface ActivityRepositoryInterface extends RepositoryInterface
{
}
