<?php

namespace App\Containers\SummerSchool\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsFeedRepositoryInterface.
 *
 */
interface NewsFeedRepositoryInterface extends RepositoryInterface
{
}
