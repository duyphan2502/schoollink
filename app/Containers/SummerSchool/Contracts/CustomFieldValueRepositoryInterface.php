<?php

namespace App\Containers\SummerSchool\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CustomFieldValueRepositoryInterface.
 *
 */
interface CustomFieldValueRepositoryInterface extends RepositoryInterface
{

}
