<?php

namespace App\Containers\SummerSchool\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EnrollmentGroupRepositoryInterface.
 *
 */
interface EnrollmentGroupRepositoryInterface extends RepositoryInterface
{

}
