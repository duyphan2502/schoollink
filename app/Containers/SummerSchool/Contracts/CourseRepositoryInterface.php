<?php

namespace App\Containers\SummerSchool\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CourseRepositoryInterface.
 *
 */
interface CourseRepositoryInterface extends RepositoryInterface
{

}
