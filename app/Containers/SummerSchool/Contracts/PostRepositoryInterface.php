<?php

namespace App\Containers\SummerSchool\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PostRepositoryInterface.
 *
 */
interface PostRepositoryInterface extends RepositoryInterface
{
}
