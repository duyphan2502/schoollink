<?php

namespace App\Containers\SummerSchool\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ParticipantRepositoryInterface.
 *
 */
interface ParticipantRepositoryInterface extends RepositoryInterface
{
}
