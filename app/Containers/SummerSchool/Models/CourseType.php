<?php

namespace App\Containers\SummerSchool\Models;

use App\Core\Model\Abstracts\MongoModel as Model;

/**
 * Class CourseType.
 *
 */
class CourseType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'Activities_CourseType';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Name', 'Code', 'Schedules', 'Subjects', 'SchoolOwnerId', 'SchoolId'];

}
