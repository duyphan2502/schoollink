<?php

namespace App\Containers\SummerSchool\Models;

use App\Core\Model\Abstracts\MongoModel as Model;

/**
 * Class CustomField.
 *
 */
class CustomField extends Model
{
    protected $table = 'CustomField';

    protected $fillable = ['Code', 'Name', 'SchoolId', 'SchoolOwnerId', 'Owners', 'ResourceType', 'FieldType', 'DefaultValue', 'Order', 'Mandatory'];
}
