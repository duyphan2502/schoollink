<?php

namespace App\Containers\SummerSchool\Models;

use App\Core\Model\Abstracts\MongoModel as Model;

/**
 * Class CustomFieldValue.
 *
 */
class CustomFieldValue extends Model
{
    protected $table = 'CustomField_Value';

    protected $fillable = ['CustomFieldId', 'ResourceId', 'Value'];
}
