<?php

namespace App\Containers\SummerSchool\Models;

use App\Core\Model\Abstracts\MongoModel as Model;

/**
 * Class EnrollmentGroup.
 *
 */
class EnrollmentGroup extends Model 
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'Activities_EnrollmentGroup';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['SchoolOwnerId', 'SchoolId', 'Name', 'CourseType', 'Owner', 'Participant_Activities_Maximum',
                            'DateStart', 'DateEnd', 'CreatedBy', 'DateCreated', 'DateActivated', 'ActivatedBy',
                            'Enrollment_DateStart', 'Enrollment_DateEnd', 'TimeSendingMessage',
                            'DirectEntryDateStart', 'DirectEntryDateEnd', 'WithdrawDateStart', 'WithdrawDateEnd',
                            'MaximumCourseRandomRegistration', 'MaximumCourseRandomDistribution', 'ParticipantActivitiesMaximum',
                            'MaximumCourseDirectRegistration', 'TimePublishAllocation', 'CourseTypeId', 'ConfirmApplications',
                            'DeletedBy', 'DateDeleted', 'IsDefault', 'DateArchived', 'ArchivedBy', 'DescriptionRandomAllocation',
                            'DescriptionDirectEntry'
                            ];
}
