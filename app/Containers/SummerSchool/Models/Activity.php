<?php

namespace App\Containers\SummerSchool\Models;

use App\Core\Model\Abstracts\MongoModel as Model;

/**
 * Class Activity.
 *
 */
class Activity extends Model 
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'Activities_Activity';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['SchoolOwnerId', 'SchoolId', 'Owner', 'Name', 'Description', 'DateStart', 'DateEnd',
            'MondayTimeStart', 'MondayTimeEnd', 'TuesdayTimeStart' , 'TuesdayTimeEnd', 'WednesdayTimeStart',
            'WednesdayTimeEnd', 'ThursdayTimeStart', 'ThursdayTimeEnd', 'FridayTimeStart', 'FridayTimeEnd',
            'SaturdayTimeStart', 'SaturdayTimeEnd', 'SundayTimeStart', 'SundayTimeEnd', 'Participants', 'Options',
            'CourseId', 'NumberOfGroups', 'ActivatedBy', 'DateActivated', 'Notes', 'Active', 'EnrollmentGroupId',
            'CreatedBy', 'DateCreated', 'DeletedBy', 'DateDeleted', 'CourseTypeId', 'AllowWaitingQueue', 'IsDefault', 'NumberOfConfirmed', 'NumberOfWaitingList', 'Simulation'];
}
