<?php

namespace App\Containers\SummerSchool\Models;

use App\Core\Model\Abstracts\MongoModel as Model;

/**
 * Class Participant.
 *
 */
class Participant extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'Activities_Participant';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ResourceId', 'Priority', 'Status', 'GroupNumber', 'RegisteredDate',
                            'RegisteredBy', 'ActivityId', 'Position', 'EnrollmentGroupId', 'CourseId', 'Batch'];
}
