<?php

namespace App\Containers\SummerSchool\Models;

use App\Core\Model\Abstracts\MongoModel as Model;

/**
 * Class NewsFeed.
 *
 */
class NewsFeed extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'Collaboration_NewsFeed';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['EntityId', 'EntityType', 'Data', 'DateUpdated', 'DateForSorting', 'DateHidden', 'DateCreated'];
}
