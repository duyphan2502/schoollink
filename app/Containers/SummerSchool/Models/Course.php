<?php

namespace App\Containers\SummerSchool\Models;

use App\Core\Model\Abstracts\MongoModel as Model;

/**
 * Class Course.
 *
 */
class Course extends Model 
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'Activities_Course';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['SchoolOwnerId', 'SchoolId', 'Name', 'Owner','Name', 'Subject', 'Description', 'Active',
                            'DateCreated', 'CreatedBy', 'YearGradeMaximum', 'YearGradeMinimum',
                            'Options', 'CourseTypeId', 'DeletedBy', 'DateDeleted', 'PostDetailId', 'BookOnCourse'];
}
