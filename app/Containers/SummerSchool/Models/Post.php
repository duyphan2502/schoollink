<?php

namespace App\Containers\SummerSchool\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class Post.
 *
 */
class Post extends Model
{
    /**
     * The connection string.
     *
     * @var string
     */
    protected $connection = 'wordpress';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    protected $primaryKey = 'ID';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['post_title', 'post_content', 'post_excerpt', 'post_status', 'post_name'];
}
