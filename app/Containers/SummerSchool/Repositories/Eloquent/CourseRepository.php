<?php

namespace App\Containers\SummerSchool\Repositories\Eloquent;

use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Models\Course;
use App\Containers\SummerSchool\BusinessModels\Course as BusinessCourse;
use App\Core\Repository\Abstracts\MongoRepository;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class CourseRepository.
 *
 */
class CourseRepository extends MongoRepository  implements CourseRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Course::class;
    }

    public function business_model()
    {
        return BusinessCourse::class;
    }

    public $skip_parsing = true;
}
