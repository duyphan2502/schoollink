<?php

namespace App\Containers\SummerSchool\Repositories\Eloquent;

use App\Containers\SummerSchool\BusinessModels\NewsFeed as BusinessNewsFeed;
use App\Containers\SummerSchool\Contracts\NewsFeedRepositoryInterface;
use App\Containers\SummerSchool\Models\NewsFeed;
use App\Core\Repository\Abstracts\MongoRepository;

/**
 * Class NewsFeedRepository.
 *
 */
class NewsFeedRepository extends MongoRepository implements NewsFeedRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return NewsFeed::class;
    }

    public function business_model()
    {
        return BusinessNewsFeed::class;
    }
}
