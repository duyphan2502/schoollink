<?php

namespace App\Containers\SummerSchool\Repositories\Eloquent;

use App\Containers\SummerSchool\BusinessModels\Post as BusinessPost;
use App\Containers\SummerSchool\Contracts\PostRepositoryInterface;
use App\Containers\SummerSchool\Models\Post;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class PostRepository.
 *
 */
class PostRepository extends Repository implements PostRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Post::class;
    }

    public function business_model()
    {
        return BusinessPost::class;
    }
}
