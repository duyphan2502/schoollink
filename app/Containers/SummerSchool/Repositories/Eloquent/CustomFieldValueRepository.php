<?php

namespace App\Containers\SummerSchool\Repositories\Eloquent;

use App\Containers\SummerSchool\BusinessModels\CustomFieldValue as BusinessCustomFieldValue;
use App\Containers\SummerSchool\Contracts\CustomFieldValueRepositoryInterface;
use App\Containers\SummerSchool\Models\CustomFieldValue;
use App\Core\Repository\Abstracts\MongoRepository;

/**
 * Class CustomFieldValueRepository.
 *
 */
class CustomFieldValueRepository extends MongoRepository  implements CustomFieldValueRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return CustomFieldValue::class;
    }

    public function business_model()
    {
        return BusinessCustomFieldValue::class;
    }
}
