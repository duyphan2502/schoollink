<?php

namespace App\Containers\SummerSchool\Repositories\Eloquent;

use App\Containers\SummerSchool\Contracts\CourseTypeRepositoryInterface;
use App\Containers\SummerSchool\Models\CourseType;
use App\Containers\SummerSchool\BusinessModels\CourseType as BusinessCourseType;
use App\Core\Repository\Abstracts\MongoRepository;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class CourseTypeRepository.
 *
 */
class CourseTypeRepository extends MongoRepository  implements CourseTypeRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return CourseType::class;
    }

    public function business_model()
    {
        return BusinessCourseType::class;
    }
}
