<?php

namespace App\Containers\SummerSchool\Repositories\Eloquent;

use App\Containers\SummerSchool\Contracts\CustomFieldRepositoryInterface;
use App\Containers\SummerSchool\BusinessModels\CustomField as BusinessCustomField;
use App\Containers\SummerSchool\Models\CustomField;
use App\Core\Repository\Abstracts\MongoRepository;

/**
 * Class CustomFieldRepository.
 *
 */
class CustomFieldRepository extends MongoRepository  implements CustomFieldRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return CustomField::class;
    }

    public function business_model()
    {
        return BusinessCustomField::class;
    }
}
