<?php

namespace App\Containers\SummerSchool\Repositories\Eloquent;

use App\Containers\SummerSchool\BusinessModels\Participant as BusinessParticipant;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Containers\SummerSchool\Models\Participant;
use App\Core\Repository\Abstracts\MongoRepository;

/**
 * Class ParticipantRepository.
 *
 */
class ParticipantRepository extends MongoRepository implements ParticipantRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Participant::class;
    }

    public function business_model()
    {
        return BusinessParticipant::class;
    }
}
