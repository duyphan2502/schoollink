<?php

namespace App\Containers\SummerSchool\Repositories\Eloquent;

use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Containers\SummerSchool\Models\EnrollmentGroup;
use App\Containers\SummerSchool\BusinessModels\EnrollmentGroup as BusinessEnrollmentGroup;
use App\Core\Repository\Abstracts\MongoRepository;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class EnrollmentGroupRepository.
 *
 */
class EnrollmentGroupRepository extends MongoRepository  implements EnrollmentGroupRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return EnrollmentGroup::class;
    }

    public function business_model()
    {
        return BusinessEnrollmentGroup::class;
    }
}
