<?php

namespace App\Containers\SummerSchool\Repositories\Eloquent;

use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Models\Activity;
use App\Containers\SummerSchool\BusinessModels\Activity as BusinessActivity;
use App\Core\Contracts\MongoRepositoryInterface;
use App\Core\Repository\Abstracts\MongoRepository;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class ActivityRepository.
 *
 */
class ActivityRepository extends MongoRepository implements ActivityRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Activity::class;
    }

    public function business_model()
    {
        return BusinessActivity::class;
    }
}
