<?php
namespace App\Containers\SummerSchool\Constants;

class Constant
{
	const TYPE_SCHOOL_SFO = 'SchoolSFO';
    const TYPE_SCHOOL_ADMIN = 'SchoolAdministration';
    const TYPE_TEACHER = 'Teacher';

    const COURSE_TYPE_DKS = 'DKS';
    const COURSE_TYPE_SOMMERSKOLEN = 'SommerSkolen';
    const COURSE_TYPE_CODE_NORMAL = 'SOMMERSKOLEN_ORDINARY_COURSE';
    const COURSE_TYPE_CODE_SPECIAL = 'SOMMERSKOLEN_SPECIAL_COURSE';

    const ACTIVITY_PARTICIPANT_ACTION_DELETE = 'Delete';
    const ACTIVITY_PARTICIPANT_ACTION_BOOK = 'Book';

    const PARTICIPANT_STATUS_REGISTERED = 'Registered';
    const PARTICIPANT_STATUS_ALLOCATED = 'Allocated';
    const PARTICIPANT_STATUS_BOOKED_DIRECTLY = 'BookedDirectly';
    const PARTICIPANT_STATUS_WAITING_LIST = 'WaitingList';
    const CUSTOM_FIELD_GPS_COORDINATES = 'gps_coordinates';
    const CACHE_FOR_ACTIVITIES = 'CacheForActivities';
}