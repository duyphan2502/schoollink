<?php
namespace App\Containers\SummerSchool\Actions\User;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

class GetPupilsBySubgroupIdAction extends Action
{
    private $user_repository;
    private $user_role_repository;

    /**
     * AddPupilAction constructor.
     *
     */
    public function __construct(
        UserRepositoryInterface $user_repository,
        UserRoleRepositoryInterface $user_role_repository
    ) {
        $this->user_repository = $user_repository;
        $this->user_role_repository = $user_role_repository;
    }


    public function run($subgroup_id)
    {
        try {
            $user_roles = $this->user_role_repository->findWhere([
                'role_id' => Constant::ROLE_PUPIL,
                'subgroup_id' => $subgroup_id
            ], ['user_id']);

            $user_ids =  $user_roles->pluck('user_id');

            $users = $this->user_repository->findWhereIn('id', $user_ids->toArray());
            return [ 'users' => $users];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}