<?php
namespace App\Containers\SummerSchool\Actions\User;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\UTCDatetime;

class AddPupilAction extends Action
{
    private $user_repository;
    private $school_repository;
    private $user_role_repository;

    /**
     * AddPupilAction constructor.
     *
     */
    public function __construct(
        UserRepositoryInterface $user_repository,
        SchoolRepositoryInterface $school_repository,
        UserRoleRepositoryInterface $user_role_repository
    ) {
        $this->user_repository = $user_repository;
        $this->school_repository = $school_repository;
        $this->user_role_repository = $user_role_repository;
    }


    public function run($data)
    {
        try {
            $app = \Session::get('app');
            if(isset($data['firstname'], $data['surname'])){
                $data_save = [
                    'firstname' => $data['firstname'],
                    'surname' => $data['surname'],
                    'address' => $data['address'],
                    'email' => isset($data['email']) ? $data['email'] : '',
                    'postal_code' => $data['postal_code'],
                    'postal_name' => $data['postal_name'],
                    'birthdate' => new UTCDatetime(Carbon::parse($data['birthdate'])->getTimestamp() * 1000),
                    'mobilephone' => isset($data['mobilephone']) ? $data['mobilephone'] : ''

                ];
                $pupil = $this->user_repository->create($data_save);
                $pupil->addRole(Constant::ROLE_PUPIL, $data['class']);
                if(isset($app->current_user)){
                    $parent_role = $app->current_user->addRole(Constant::ROLE_GUARDIAN, $pupil->id);
                    $guardian_access_levels = $app->school_owner->getSchoolOwnerGuardianAccessLevels();
                    if($guardian_access_levels) {
                        $this->user_role_repository->update(['school_owner_role_access_level' => $guardian_access_levels[0]->school_owner_role_access_level], $parent_role->id);
                    }
                    $app->current_user->roles[$parent_role->id] = $parent_role;
                }
                return ['result' => true];
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}