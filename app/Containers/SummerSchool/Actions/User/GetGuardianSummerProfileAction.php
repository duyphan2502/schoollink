<?php

namespace App\Containers\SummerSchool\Actions\User;

use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;

use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

/**
 * Class GetGuardianSummerProfileAction.
 *
 */
class GetGuardianSummerProfileAction extends Action
{
	private $user_role_repository;

	public function __construct(
        UserRoleRepositoryInterface $user_role_repository
    ) {
        $this->user_role_repository = $user_role_repository;
    }

    public function run()
    {
        $app = \Session::get('app');
        $user = $app->current_user;

		$return = [];
        $profile = [];
        $permissions = [];
        $profile['image'] = $user->getImage('profile');
        $profile['firstname'] = $user->firstname;
        $profile['surname'] = $user->surname;
        $profile['email'] = $user->email;
        $profile['mobilephone'] = $user->mobilephone;
        $profile['telephone'] = $user->telephone;
        $profile['address'] = $user->address;
        $profile['postal_code'] = $user->postal_code;
        $profile['postal_name'] = $user->postal_name;
        $profile['id'] = $user->id;

        $permissions['image'] = $user->permissionEdit(Constant::PERMISSION_EDIT_IMAGE_ID)
            && !$user->hasRole(Constant::ROLE_PUPIL);

        $permissions['guardianportal_main'] = $user->permissionEdit(Constant::PERMISSION_EDIT_GUARDIANPORTAL);

        $return['permissions'] = $permissions;
        $return['profile'] = $profile;
		return $return;
    }
}