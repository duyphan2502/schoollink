<?php
namespace App\Containers\SummerSchool\Actions\User;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

class DeletePupilAction extends Action
{
    private $user_repository;
    private $school_repository;
    private $user_role_repository;

    /**
     * DeletePupilAction constructor.
     *
     */
    public function __construct(
        UserRepositoryInterface $user_repository,
        SchoolRepositoryInterface $school_repository,
        UserRoleRepositoryInterface $user_role_repository
    ) {
        $this->user_repository = $user_repository;
        $this->school_repository = $school_repository;
        $this->user_role_repository = $user_role_repository;
    }


    public function run($id)
    {
        try {
            $app = \Session::get('app');
            $pupil = $this->user_repository->find($id);
            $role = $pupil->getRole(Constant::ROLE_PUPIL, new RoleParam(['current_year' => true]));
            if($role && $role->source_ims == false){
                $parent_role = $app->current_user->getRole(Constant::ROLE_GUARDIAN, $id);
                $this->user_role_repository->delete($role->id);
                unset($app->current_user->roles[$parent_role->id]);
                $pupils = [];
                foreach ($app->current_user->roles as $role) {
                    if ($role->loginActive()) {
                        switch ($role->role_id) {
                            case Constant::ROLE_GUARDIAN:
                                $pupils[] = array(
                                    'user_id' => $role->guardian_user_id,
                                );
                                break;
                            default:
                                break;
                        }
                    }
                }
                if(!empty($pupils)){
                    $app->child = $this->user_repository->find($pupils[0]['user_id']);
                }else{
                    $app->child = null;
                }

                return true;
            }
            return false;

        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}