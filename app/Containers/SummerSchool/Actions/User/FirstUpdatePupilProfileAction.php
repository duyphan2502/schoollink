<?php

namespace App\Containers\SummerSchool\Actions\User;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;

use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class FirstUpdatePupilProfileAction.
 *
 */
class FirstUpdatePupilProfileAction extends Action
{

    private $user_repository;
	public function __construct(
        UserRepositoryInterface $user_repository
    ) {
        $this->user_repository = $user_repository;
    }

    public function run($data)
    {
        // first update pupil profile
        try{
            $app = \Session::get('app');
            if(isset($data['id'])) {
                $data_update = [];
                foreach ($data as $k => $v) {
                    $data_update[$k] = $v;
                    $app->current_user->$k = $data_update[$k];
                }
                unset($data_update['id']);
                $pupil = $this->user_repository->update($data_update, $data['id']);
                $role = $pupil->addRole(Constant::ROLE_PUPIL, $data['class']);
                $app->current_user->roles[$role->id] = $role;
                return ['result' => true];
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}