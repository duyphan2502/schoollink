<?php

namespace App\Containers\SummerSchool\Actions\User;

use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;

use App\Containers\SchoolLink\Models\RoleParam;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

/**
 * Class GetGuardianSummerProfileAction.
 *
 */
class GetPupilSummerProfileAction extends Action
{
	private $user_role_repository;

	public function __construct(
        UserRoleRepositoryInterface $user_role_repository
    ) {
        $this->user_role_repository = $user_role_repository;
    }

    public function run()
    {
        $app = \Session::get('app');
        $user = $app->child;
        if(!$app->user_role){
            $user = $app->current_user;
        }

        $return = [];
        $profile = [];
        $permissions = [];
        $profile['image'] = $user->getImage('profile');
        $profile['firstname'] = $user->firstname;
        $profile['surname'] = $user->surname;
        $profile['email'] = $user->email;
        $profile['mobilephone'] = $user->mobilephone;
        $profile['address'] = $user->address;
        $profile['postal_code'] = $user->postal_code;
        $profile['postal_name'] = $user->postal_name;
        $profile['id'] = $user->id;

        $role = $user->getRole(Constant::ROLE_PUPIL, new RoleParam(['current_year' => true]));
        if(!$role){
            return;
        }
        if($role->source_ims == false){
            //permissions
            $permissions['email'] = true;
            $permissions['mobilephone'] = true;
            $permissions['address'] = true;
            $permissions['postal_code'] = true;
            $permissions['postal_name'] = true;
        }else{
            $permissions['email'] = true;
            $permissions['mobilephone'] = true;
        }

        if($app->current_user->id == $app->child->id){
            $permissions['image'] = true;
        }

        $return['permissions'] = $permissions;
        $return['profile'] = $profile;
        return $return;
    }
}