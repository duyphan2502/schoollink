<?php

namespace App\Containers\SummerSchool\Actions\User;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant;
use App\Containers\SchoolLink\Constants\Constant as SchoollinkConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseTypeRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Containers\SummerSchool\Contracts\PostRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

/**
 * Class GetPupilActivitiesAction.
 *
 */
class GetPupilActivitiesAction extends Action
{
    private $enroll_group_repository;
    private $activity_repository;
    private $course_repository;
    private $school_repository;
    private $course_type_repository;
    private $post_repository;
    private $participant_repository;

    public function __construct(
        EnrollmentGroupRepositoryInterface $enroll_group_repository,
        ActivityRepositoryInterface $activity_repository,
        CourseRepositoryInterface $course_repository,
        SchoolRepositoryInterface $school_repository,
        CourseTypeRepositoryInterface $course_type_repository,
        PostRepositoryInterface $post_repository,
        ParticipantRepositoryInterface $participant_repository
    )
    {
        $this->enroll_group_repository = $enroll_group_repository;;
        $this->activity_repository = $activity_repository;
        $this->course_repository = $course_repository;
        $this->school_repository = $school_repository;
        $this->course_type_repository = $course_type_repository;
        $this->post_repository = $post_repository;
        $this->participant_repository = $participant_repository;
    }

    public function run()
    {
        try {
            $app = \Session::get('app');
            if(empty($app->child) || empty($app->special_school)){
                return false;
            }

            $enroll_groups = [];
            $courses = [];
            $interested = $this->participant_repository->findWhere([
                'ResourceId' => $app->child->id,
                'Status' => Constant::PARTICIPANT_STATUS_REGISTERED
            ]);
            $registered = $this->participant_repository->findWhere([
                'ResourceId' => $app->child->id,
                'Status' => Constant::PARTICIPANT_STATUS_BOOKED_DIRECTLY
            ]);
            $waiting = $this->participant_repository->findWhere([
                'ResourceId' => $app->child->id,
                'Status' => Constant::PARTICIPANT_STATUS_WAITING_LIST
            ]);
            $activity_projection = ['_id', 'Name','CourseTypeId','DateStart','DateEnd','CourseId','EnrollmentGroupId','AllowWaitingQueue','Options'];

            // get schools
            $schools = $this->school_repository
                ->findWhere(['owner_id' => $app->school_owner->id, 'type_id' => SchoollinkConstant::SCHOOL_TYPE_SCHOOL], ['id','name','location'])
                ->keyBy('id')->toArray();


            $activities_interested = $this->getParticipants($interested, $courses, $enroll_groups, $schools, $activity_projection);
            $activities_registered = $this->getParticipants($registered, $courses, $enroll_groups, $schools, $activity_projection);
            $activities_waiting = $this->getParticipants($waiting, $courses, $enroll_groups, $schools, $activity_projection);

            if (!empty($activities_interested)) {
                usort($activities_interested, function ($a, $b) {
                    return $a['priority'] > $b['priority'];
                });
            }
            return [
                'activities_interested' => $activities_interested,
                'activities_registered' => $activities_registered,
                'activities_waiting' => $activities_waiting,
            ];
        } catch (Exception $ex) {
            \Log::error($ex);
        }

    }
    protected function getParticipants($participants, &$courses, &$enroll_groups, &$schools, $activity_projection){
        $activities_participants = [];
        foreach ($participants as $participant){
            $activity = $this->activity_repository->findWhere(
                [
                    '_id' => new ObjectID($participant->activity_id),
                    'Active' => true,
                    ['DateActivated', '<>', NULL],
                    ['CourseId', '<>', NULL],
                    'DateDeleted' => NULL,
                ],$activity_projection
            )->first();
            if($activity){
                //get course of activity
                if(!isset($courses[$activity->course_id])) {
                    $course = $this->course_repository->find($activity->course_id);
                    if (!empty($course->post_detail_id)) {
                        $post = $this->post_repository->findWhere(['ID' => $course->post_detail_id])->first();
                        $course->description = $post->post_content;
                    }
                    $courses[$activity->course_id] = $course;
                }
                //get group of activity
                if(!isset($enroll_groups[$activity->enrollment_group_id])){
                    $group = $this->enroll_group_repository->find($activity->enrollment_group_id);
                    $enroll_groups[$activity->enrollment_group_id] = $this->transformGroup($group);
                }
                $school = $schools[$activity->options->summer_school_school_id];
                $school->location = is_string($school->location) ? json_decode($school->location) : $school->location;
                $week_start = (int)Carbon::createFromTimeStamp($activity->date_start)->format('W');
                $activities_participants[] = $this->transformActivityParticipant($courses, $activity, $week_start, $participant, $enroll_groups, $school);
            }
        }
        return $activities_participants;
    }

    protected function transformGroup($group)
    {
        $current_date = Carbon::now()->getTimestamp();
        return [
            'id' => $group->id,
            'name' => $group->name,
            'description_random_allocation' => $group->description_random_allocation,
            'description_direct_entry' => $group->description_direct_entry,
            'enrollment_date_start' => $group->enrollment_date_start,
            'enrollment_date_end' => $group->enrollment_date_end,
            'direct_entry_date_start' => $group->direct_entry_date_start,
            'direct_entry_date_end' => $group->direct_entry_date_end,
            'random_allocation' => $group->enrollment_date_start < $current_date && $current_date < $group->enrollment_date_end,
            'direct_entry' => $group->direct_entry_date_start < $current_date && $current_date < $group->direct_entry_date_end,
            'withdraw' => $group->withdraw_date_start < $current_date && $current_date < $group->withdraw_date_end,
            'allow_booking' => $current_date <= $group->direct_entry_date_end,
            'maximum_course_registration' => $group->maximum_course_random_registration,
            'maximum_course_direct_registration' => $group->maximum_course_direct_registration,
            'is_default' => $group->is_default

        ];
    }

    protected function getDateOfWeek($week, $year)
    {
        $dto = Carbon::now();
        $dto->setISODate($year, $week);
        $ret['date_start'] = $dto->format('d/m');
        $dto->modify('+6 days');
        $ret['date_end'] = $dto->format('d/m');
        return $ret;
    }

    protected function transformActivityParticipant($courses, $act, $week_start, $participant, $enroll_groups, $school)
    {
        $now = Carbon::now();
        return [
            'course_name' => $courses[$act->course_id]->name,
            'course_description' => $courses[$act->course_id]->description,
            'course_subject' => $courses[$act->course_id]->subject,
            'activity_id' => $act->id,
            'activity_name' => $act->name,
            'week' => $week_start,
            'date_of_week' => $this->getDateOfWeek($week_start, $now->year),
            'date_start' => $act->date_start,
            'date_end' => $act->date_end,
            'school_name' => $school->name,
            'school_location' => $school->location,
            'priority' => $participant->priority,
            'status' => $participant->status,
            'year_grade_maximum' => $courses[$act->course_id]->year_grade_maximum,
            'year_grade_minimum' => $courses[$act->course_id]->year_grade_minimum,
            'allow_withdraw' => $enroll_groups[$act->enrollment_group_id]['withdraw'],
            'enroll_group_id' => $act->enrollment_group_id,
            'position' => !empty($participant->position) ? $participant->position : 0
        ];
    }
}