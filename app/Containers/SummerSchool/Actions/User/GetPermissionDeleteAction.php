<?php
namespace App\Containers\SummerSchool\Actions\User;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

class GetPermissionDeleteAction extends Action
{
    private $user_repository;
    private $user_role_repository;

    /**
     * GetPermissionDeleteAction constructor.
     *
     */
    public function __construct(
        UserRepositoryInterface $user_repository,
        UserRoleRepositoryInterface $user_role_repository
    ) {
        $this->user_repository = $user_repository;
        $this->user_role_repository = $user_role_repository;
    }


    public function run()
    {
        try {
            $app = \Session::get('app');
            $pupil = $this->user_repository->find($app->child->id);
            $role = $pupil->getRole(Constant::ROLE_PUPIL, new RoleParam(['current_year' => true]));
            if($role && $role->source_ims == false && $app->current_user->id != $app->child->id){
                return true;
            }
            return false;

        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}