<?php

namespace App\Containers\SummerSchool\Actions\User;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;

use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class UpdatePupilProfileAction.
 *
 */
class UpdatePupilProfileAction extends Action
{

    private $user_repository;
	public function __construct(
        UserRepositoryInterface $user_repository
    ) {
        $this->user_repository = $user_repository;
    }

    public function run($data)
    {
        try{
            $app = \Session::get('app');
            if(isset($data['id'])) {
                $data_update = [];
                foreach ($data as $k => $v) {
                    $data_update[$k] = $v;
                    if($app->current_user->id == $data['id']){
                        $app->current_user->$k = $data_update[$k];
                    }else{
                        $app->child->$k = $data_update[$k];
                    }
                }
                unset($data_update['id']);
                $this->user_repository->update($data_update, $data['id']);

                return ['result' => true];
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}