<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Course;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

class AddCourseAction extends Action
{
    protected $course_repository;
    protected $activity_repository;
    protected $school_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        CourseRepositoryInterface $course_repository,
        SchoolRepositoryInterface $school_repository,
        ActivityRepositoryInterface $activity_repository
    )
    {
        $this->course_repository = $course_repository;
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
    }


    public function run($data)
    {
        try {
            $role = $data['role'];
            $course_type_id = $data['type'];
            $id = $this->addCourse($role, $course_type_id);
            return ['id' => $id];
        } catch (Exception $e) {
            \Log::error($e);
            return [];
        }
    }

    protected function addCourse($role, $course_type_id)
    {
        $role_params = explode(":", $role);
        $school_owner_type = $role_params[0];
        $school_id = $role_params[1];

        $school = $this->school_repository->find($school_id);
        if (!$school) {
            return false;
        }

        $app = \Session::get('app');
        $current_user = $app->current_user;

        switch ($school_owner_type) {
            case SummerSchoolConstant::TYPE_SCHOOL_SFO:
                if (!$current_user->hasRole(Constant::ROLE_SFO, $school->id))
                    return false;
                break;
            case SummerSchoolConstant::TYPE_SCHOOL_ADMIN:
                if (!$current_user->hasRole(Constant::ROLE_SCHOOL_ADMIN, $school->id))
                    return false;
                break;
            case SummerSchoolConstant::TYPE_TEACHER:
                if (!$current_user->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $school->id])))
                    return false;
                break;
            default:
                return false;
        }

        $academic_year = $school->academicYear();

        $now = Carbon::now();

        $course_data = array(
            'SchoolOwnerId' => (int)$school->owner_id,
            'SchoolId' => (int)$school->id,
            'Owner' => array(
                'Type' => $school_owner_type,
                'SchoolId' => $school_owner_type === SummerSchoolConstant::TYPE_SCHOOL_SFO || $school_owner_type ===
                SummerSchoolConstant::TYPE_SCHOOL_ADMIN ? (int)$school->id : null
            ),
            'Name' => null,
            'CourseTypeId' => new ObjectID($course_type_id),
            'Description' => null,
            'Subject' => null,
            'DateCreated' => new UTCDatetime($now->getTimestamp() * 1000),
            'CreatedBy' => (int)$current_user->id,
            'YearGradeMaximum' => null,
            'YearGradeMinimum' => null,
            'Options' => array(
                'SummerSchoolBirthdateMinimum' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000),
                'SummerSchoolBirthdateMaximum' => new UTCDatetime(Carbon::parse($academic_year->date_end)->getTimestamp() * 1000)
            )
        );

        $course = $this->course_repository->create($course_data);
        return $course->id;
    }
}