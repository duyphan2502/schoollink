<?php

namespace App\Containers\SummerSchool\Actions\Course;

use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Criterias\Course\GetCourseCriteria;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class GetAllAction.
 *
 */
class GetCourseAction extends Action
{
    private $course_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(CourseRepositoryInterface $course_repository)
    {
        $this->course_repository = $course_repository;
    }

    public function run($data)
    {
        try {
            $id = $data['id'];
            $course = $this->course_repository->find($id);
            return ['course' => $course];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
