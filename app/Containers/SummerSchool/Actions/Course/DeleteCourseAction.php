<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 19/10/2016
 * Time: 5:05 PM
 */

namespace App\Containers\SummerSchool\Actions\Course;


use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Criterias\Activity\GetAvailableActivitiesByCourseIdCriteria;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

class DeleteCourseAction
{
    private $course_repository;
    private $activity_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(CourseRepositoryInterface $course_repository,
                                ActivityRepositoryInterface $activity_repository)
    {
        $this->course_repository = $course_repository;
        $this->activity_repository = $activity_repository;
    }

    public function run($data)
    {
        try {
            $id = $data['id'];

            if ($this->isUsingByActivities($id)) {
                return ['result' => false, 'message' => 'You cannot delete it because it is using by some activities'];
            }
            $result = $this->deleteCourse($id);
            return $result;
        } catch (Exception $e) {
            \Log::error($e);
            return ['result' => false, 'message' => $e . getMessage()];
        }
    }

    protected function isUsingByActivities($id)
    {
        $params = [
            'course_id' => $id
        ];
        $activities = $this->activity_repository->getByCriteria(new GetAvailableActivitiesByCourseIdCriteria($params));
        if (count($activities) == 0) {
            return false;
        }
        return true;
    }


    protected function deleteCourse($id)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;

        $updatedData = [
            'DeletedBy' => $current_user->id,
            'DateDeleted' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000)
        ];

        $result = $this->course_repository->update($updatedData, $id);
        return ['result' => $result];
    }
}