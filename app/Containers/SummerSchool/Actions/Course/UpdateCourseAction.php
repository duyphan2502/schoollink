<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 29/09/2016
 * Time: 1:56 PM
 */

namespace App\Containers\SummerSchool\Actions\Course;

use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use Carbon\Carbon;
use MongoDB\BSON\UTCDatetime;
use App\Core\Exception\Abstracts\Exception;

class UpdateCourseAction extends Action
{
    protected $course_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(CourseRepositoryInterface $course_repository)
    {
        $this->course_repository = $course_repository;

    }

    public function run($data)
    {
        try {
            $course_id = $data['course_id'];
            $course = $data['course'];

            $id = $this->updateCourse($course_id, $course);
            return ['id' => $id];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function updateCourse($course_id, $course)
    {
        $course_data = array(
            'Name' => $course['name'],
            'Active' => true,
            'BookOnCourse' => isset($course['book_on_course']) ? $course['book_on_course'] : false,
            'Description' => $course['description'],
            'Subject' => $course['subject'],
            'YearGradeMaximum' => $course['year_grade_maximum'],
            'YearGradeMinimum' => $course['year_grade_minimum'],
            'PostDetailId' => (int)$course['post_detail_id'],
            'Options' => array(
                'SummerSchoolBirthdateMinimum' => isset($course['date_range']['startDate']) ? new UTCDatetime(Carbon::parse($course['date_range']['startDate'])->getTimestamp() * 1000) : null,
                'SummerSchoolBirthdateMaximum' => isset($course['date_range']['endDate']) ? new UTCDatetime(Carbon::parse($course['date_range']['endDate'])->getTimestamp() * 1000) : null
            )
        );

        $course = $this->course_repository->update($course_data, $course_id);
        return $course->id;
    }
}