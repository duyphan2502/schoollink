<?php

namespace App\Containers\SummerSchool\Actions\Course;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

/**
 * Class RestoreCourseAction.
 *
 */
class RestoreCourseAction extends Action
{

    /**
     * @var \App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface
     */
    private $course_repository;
    private $school_repository;

    /**
     * RestoreCourseAction constructor.
     *
     */
    public function __construct(
        CourseRepositoryInterface $course_repository,
        SchoolRepositoryInterface $school_repository
    )
    {
        $this->course_repository = $course_repository;
        $this->school_repository = $school_repository;
    }

    public function run($id, $school_id)
    {
        try {
            $app = \Session::get('app');
            $current_user = $app->current_user;

            $school = $this->school_repository->find($school_id);

            $course = $this->course_repository
                ->findWhere(['_id' => $id, 'SchoolOwnerId' => (int)$school->owner_id, 'SchoolId' => (int)$school->id])->first();

            if (!isset($course) || !$course->permissionEdit($current_user))
                return ['result' => false];

            $this->course_repository->updateOne([
                '_id' => new ObjectID($course->id)
            ],[
                'DateDeleted' => null,
                'DeletedBy' => null
            ]);

            return ['result' => true];
        } catch (Exception $ex) {
            \Log::error($ex);
        }
    }
}
