<?php

namespace App\Containers\SummerSchool\Actions\Course;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Criterias\Course\GetAllCoursesCriteria;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;

/**
 * Class GetAllAction.
 *
 */
class GetAllAction extends Action
{
    private $course_repository;
    private $school_repository;
    private $subgroup_repository;
    private $group_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        CourseRepositoryInterface $course_repository,
        SchoolRepositoryInterface $school_repository,
        GroupRepositoryInterface $group_repository,
        SubGroupRepositoryInterface $subgroup_repository
    )
    {
        $this->course_repository = $course_repository;
        $this->school_repository = $school_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
    }

    public function run($data)
    {
        try {
            $school_owner_id = null;
            $school_id = null;
            $owner_ors = [];

            $this->initParams($school_owner_id, $school_id, $owner_ors);

            $academic_year = isset($data['year']) ? $data['year'] : Carbon::now()->year;
            $courses = $this->getCourses($school_owner_id, $school_id, $academic_year);
            return $courses;
        } catch (Exception $e) {
            \Log::error($e);
            return [];
        }
    }

    protected function initParams(&$school_owner_id, &$school_id, &$owner_ors)
    {
        $app = \Session::get('app');
        $user_role = $app->user_role;

        switch ($user_role->role_id) {
            case Constant::ROLE_SFO:
                $school = $this->school_repository->find($user_role->school_id);
                $school_owner_id = (int)$school->owner_id;
                $school_id = (int)$school->id;
                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_SCHOOL_SFO,
                    'Owner.SchoolId' => (int)$school->id
                );
                break;

            case Constant::ROLE_SCHOOL_ADMIN:
                $school = $this->school_repository->find($user_role->school_id);
                $school_owner_id = (int)$school->owner_id;
                $school_id = (int)$school->id;
                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_SCHOOL_ADMIN,
                    'Owner.SchoolId' => (int)$school->id
                );
                break;

            case Constant::ROLE_TEACHER:
                $subgroup = $this->subgroup_repository->find($user_role->subgroup_id);
                $group = $this->group_repository->find($subgroup->group_id);
                $school = $this->school_repository->find($group->school_id);

                $school_owner_id= (int)$school->owner_id;
                $school_id = (int)$school->id;

                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_TEACHER,
                    'Owner.GroupId' => (int)$group->id
                );
                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_TEACHER,
                    'Owner.SubgroupId' => (int)$subgroup->id
                );
                break;
        }
    }

    protected function getCourses($school_owner_id, $school_id, $academic_year)
    {
        $course_params = array(
            'school_owner_ids' => [$school_owner_id],
            'school_ids' => [$school_id],
            'year' => $academic_year,
            'include_archive' => true
        );

        $courses = $this->course_repository
            ->getByCriteria(new GetAllCoursesCriteria($course_params));

        return ['courses' => $courses];
    }
}
