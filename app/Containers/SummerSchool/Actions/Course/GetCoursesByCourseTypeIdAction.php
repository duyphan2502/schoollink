<?php

namespace App\Containers\SummerSchool\Actions\Course;

use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseTypeRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use MongoDB\BSON\ObjectID;

/**
 * Class GetAllAction.
 *
 */
class GetCoursesByCourseTypeIdAction extends Action
{
    protected $course_repository;
    protected $course_type_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        CourseTypeRepositoryInterface $course_type_repository,
        CourseRepositoryInterface $course_repository
    )
    {
        $this->course_repository = $course_repository;
        $this->course_type_repository = $course_type_repository;
    }

    public function run($course_type_id)
    {
        try {
            $courses = $this->getCourses($course_type_id);
            return $courses;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function getCourses($course_type_id)
    {
        $course_type = $this->course_type_repository->find($course_type_id);
        if (!isset($course_type)) {
            return null;
        }

        $courses = $this->course_repository->findWhere([
            'DateDeleted' => null,
            'DeletedBy' => null,
            ['Name', '<>', null],
            'CourseTypeId' => new ObjectID($course_type_id),
            'SchoolOwnerId' => (int)$course_type->school_owner_id,
            'SchoolId' => (int)$course_type->school_id
        ]);

        return ['courses' => $courses];
    }
}
