<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 30/09/2016
 * Time: 11:26 AM
 */

namespace App\Containers\SummerSchool\Actions\CourseType;

use App\Containers\SummerSchool\Contracts\CourseTypeRepositoryInterface;
use App\Core\Action\Abstracts\Action;

class GetSubjectsAction extends Action
{
    private $course_type_repository;

    /**
     * GetSubjectsAction constructor.
     */
    public function __construct(CourseTypeRepositoryInterface $course_type_repository)
    {
        $this->course_type_repository = $course_type_repository;
    }

    public function run($data)
    {
        $course_type_id = isset($data['course_type_id']) ? $data['course_type_id'] : null;
        $result = $this->getSubjects($course_type_id);
        return ['result' => $result];
    }

    protected function getSubjects($course_type_id = null)
    {
        if($course_type_id != null) {
            $course_type = $this->course_type_repository->find($course_type_id);
            if (isset($course_type)) {
                return $course_type->subjects;
            }
        } else {
            $app = \Session::get('app');
            $owner = $app->school_owner;
            $course_types = $this->course_type_repository->findWhere(['SchoolOwnerId' => (int)$owner->id]);
            $subjects = [];
            foreach ($course_types as $course_type) {
                $subjects = array_merge($course_type->subjects, $subjects);
            }
            return $subjects;
        }
    }
}