<?php

namespace App\Containers\SummerSchool\Actions\CourseType;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseTypeRepositoryInterface;
use App\Containers\SummerSchool\Criterias\CourseType\GetCourseTypesCriteria;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class GetCourseTypesAction.
 *
 */
class GetCourseTypesAction extends Action
{
    private $course_type_repository;
    private $school_repository;
    private $subgroup_repository;
    private $group_repository;

    public function __construct(
        CourseTypeRepositoryInterface $course_type_repository,
        SchoolRepositoryInterface $school_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository
    )
    {
        $this->course_type_repository = $course_type_repository;
        $this->school_repository = $school_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
    }

    public function run()
    {
        try {
            $school_owner_ids = [];
            $school_ids = [];
            $roles = $this->getRoles($school_owner_ids, $school_ids);

            $types = $this->course_type_repository->getByCriteria(new GetCourseTypesCriteria($school_owner_ids, $school_ids));
            return ['roles' => $roles, 'types' => $types];
        } catch (Exception $ex) {
            \Log::error($ex);
        }

    }

    protected function getRoles(&$school_owner_ids, &$school_ids)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;

        foreach ($current_user->roles as $role) {
            switch ($role->role_id) {
                case Constant::ROLE_SFO:
                    $school = $this->school_repository->find($role->school_id);
                    $school_owner_ids[] = (int)$school->owner_id;
                    $school_ids[] = (int)$school->id;

                    if (!isset($return['schools'][(int)$school->id])) {
                        $return['schools'][(int)$school->id] = array(
                            'school_id' => (int)$school->id,
                            'school_name' => $school->name,
                            'school_SFO' => false,
                            'school_administration' => false,
                            'teacher' => false
                        );
                    }
                    $return['schools'][(int)$school->id]['school_SFO'] = true;

                    break;

                case Constant::ROLE_SCHOOL_ADMIN:
                    $school = $this->school_repository->find($role->school_id);
                    $school_owner_ids[] = (int)$school->owner_id;
                    $school_ids[] = (int)$school->id;

                    if (!isset($return['schools'][(int)$school->id])) {
                        $return['schools'][(int)$school->id] = array(
                            'school_id' => (int)$school->id,
                            'school_name' => $school->name,
                            'school_SFO' => false,
                            'school_administration' => false,
                            'teacher' => false
                        );
                    }
                    $return['schools'][(int)$school->id]['school_administration'] = true;

                    break;

                case Constant::ROLE_TEACHER:
                    $subgroup = $this->subgroup_repository->find($role->subgroup_id);
                    $group = $this->group_repository->find($subgroup->group_id);
                    $school = $this->school_repository->find($group->school_id);

                    $school_owner_ids[] = (int)$school->owner_id;
                    $school_ids[] = (int)$school->id;

                    if (!isset($return['schools'][(int)$school->id])) {
                        $return['schools'][(int)$school->id] = array(
                            'school_id' => (int)$school->id,
                            'school_name' => $school->name,
                            'school_SFO' => false,
                            'school_administration' => false,
                            'teacher' => false
                        );
                    }
                    $return['schools'][(int)$school->id]['teacher'] = true;

                    break;
            }
        }
        return $return['schools'];
    }
}