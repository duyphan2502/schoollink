<?php
namespace App\Containers\SummerSchool\Actions\School;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\UTCDatetime;

class GetSubgroupSchoolAction extends Action
{
    private $group_repository;
    private $subgroup_repository;

    /**
     * GetSubgroupSchoolAction constructor.
     *
     */
    public function __construct(
        GroupRepositoryInterface $group_repository,
        SubGroupRepositoryInterface $subgroup_repository
    ) {
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
    }


    public function run($school_id)
    {
        try {
            $app = \Session::get('app');
            $groups = $this->group_repository->findWhere([
                'school_id' => (int)$school_id,
                'academic_year_id' => $app->academic_year->id,
                'date_deleted' => NULL
            ]);
            if($groups){
                $subgroups = [];
                foreach ($groups as $group){
                    $subgroup = $this->subgroup_repository->findWhere(['group_id' => $group->id])->toArray();
                    $subgroups = array_merge($subgroups, $subgroup);
                }
                if (!empty($subgroups)) {
                    usort($subgroups, function ($a, $b) {
                        return $a->name > $b->name;
                    });
                }
                return ['subgroups' => $subgroups];
            }
            return [];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}