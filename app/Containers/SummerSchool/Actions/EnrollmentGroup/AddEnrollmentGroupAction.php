<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

/**
 * Class AddEnrollmentGroupAction.
 *
 */
class AddEnrollmentGroupAction extends Action
{

    /**
     * @var \App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface
     */
    private $enrollment_group_repository;
    private $school_repository;
    private $school_owner_repository;
    private $subgroup_repository;
    private $group_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        SchoolRepositoryInterface $school_repository,
        SchoolOwnerRepositoryInterface $school_owner_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository
    )
    {
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->school_repository = $school_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->school_owner_repository = $school_owner_repository;
        $this->group_repository = $group_repository;
    }

    public function run($role, $course_type)
    {
        try {
            $role_params = explode(":", $role);
            $school_owner_type = $role_params[0];
            $school_id = $role_params[1];

            $school = $this->school_repository->find($school_id);
            if (!$school)
                return false;

            $app = \Session::get('app');
            $current_user = $app->current_user;

            switch ($school_owner_type) {
                case SummerSchoolConstant::TYPE_SCHOOL_SFO:
                    if (!$current_user->hasRole(Constant::ROLE_SFO, $school->id))
                        return false;
                    break;
                case SummerSchoolConstant::TYPE_SCHOOL_ADMIN:
                    if (!$current_user->hasRole(Constant::ROLE_SCHOOL_ADMIN, $school->id))
                        return false;
                    break;
                case SummerSchoolConstant::TYPE_TEACHER:
                    if (!$current_user->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $school->id])))
                        return false;
                    break;
                default:
                    return false;
            }

            $academic_year = $school->academicYear();

            $data = [
                'SchoolOwnerId' => (int)$school->owner_id,
                'SchoolId' => (int)$school->id,
                'CourseTypeId' => new ObjectID($course_type),
                'Name' => '',
                'Owner' => array(
                    'Type' => $school_owner_type,
                    'SchoolId' => $school_owner_type === SummerSchoolConstant::TYPE_SCHOOL_SFO || $school_owner_type ===
                    SummerSchoolConstant::TYPE_SCHOOL_ADMIN ? (int)$school->id : null),
                'DateStart' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000),
                'DateEnd' => new UTCDatetime(Carbon::parse($academic_year->date_end)->getTimestamp() * 1000),
                'CreatedBy' => (int)$current_user->id,
                'DateCreated' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000),
                'MaximumCourseRandomRegistration' => 5,
                'MaximumCourseRandomDistribution' => 1,
                'MaximumCourseDirectRegistration' => 5,
                'ParticipantActivitiesMaximum' => 5,
                'ArchivedBy' => null,
                'DateArchived' => null,
                'DescriptionRandomAllocation' => null,
                'DescriptionDirectEntry' => null
            ];

            $enrollment_group = $this->enrollment_group_repository->create($data);

            return ['id' => $enrollment_group->id];
        } catch (Exception $ex) {
            \Log::error($ex);
        }
    }
}
