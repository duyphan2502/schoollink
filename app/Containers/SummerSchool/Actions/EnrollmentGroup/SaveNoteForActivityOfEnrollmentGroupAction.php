<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class SaveNoteForActivityOfEnrollmentGroupAction.
 *
 */
class SaveNoteForActivityOfEnrollmentGroupAction extends Action
{

    /**
     * @var \App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface
     */
    private $activity_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository
    )
    {
        $this->activity_repository = $activity_repository;
    }

    public function run($id, $note)
    {
        try {
            $app = \Session::get('app');
            $current_user = $app->current_user;

            $activity = $this->activity_repository->find($id);

            if (!isset($activity) || !$activity->permissionEdit($current_user)) {
                return null;
            }

            $updatedData = [
                'Notes' => $note
            ];

            $result = $this->activity_repository->update($updatedData, $id);

            return ['result' => $result];
        } catch (Exception $ex) {
            \Log::error($ex);
        }
    }
}
