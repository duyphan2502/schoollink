<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class ToggleActivityOfEnrollmentGroupAction.
 *
 */
class ToggleActivityOfEnrollmentGroupAction extends Action
{

    /**
     * @var \App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface
     */
    private $enrollment_group_repository;
    private $activity_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        ActivityRepositoryInterface $activity_repository
    )
    {
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->activity_repository = $activity_repository;
    }

    public function run($id)
    {
        try {
            $app = \Session::get('app');
            $current_user = $app->current_user;

            $activity = $this->activity_repository->find($id);

            if (!isset($activity) || !$activity->permissionEdit($current_user)) {
                return null;
            }

            $updatedData = [
                'Active' => !$activity->active
            ];

            $result = $this->activity_repository->update($updatedData, $id);

            return ['result' => $result];
        } catch (Exception $ex) {
            \Log::error($ex);
        }
    }
}
