<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\UTCDatetime;

/**
 * Class GetHeaderGroupsAction.
 *
 */
class GetHeaderGroupsAction extends Action
{

    private $enrollment_group_repository;

    /**
     * GetHeaderGroupsAction constructor.
     *
     */
    public function __construct(
        EnrollmentGroupRepositoryInterface $enrollment_group_repository
    )
    {
        $this->enrollment_group_repository = $enrollment_group_repository;
    }

    public function run()
    {
        try {
            $app = \Session::get('app');
            $current_date = new UTCDatetime(Carbon::now()->getTimestamp() * 1000);
            $enrollment_groups = $this->enrollment_group_repository->orderBy('IsDefault', 'desc')->findWhere([
                'SchoolId' => $app->special_school->id,
                'SchoolOwnerId' => $app->school_owner->id,
                'DateDeleted' => NULL,
                'DateArchived' => NULL,
                ['DateActivated', '<>', NULL],
                ['DateStart', '<=', $current_date],
                ['DateEnd', '>=', $current_date],
             ], ['_id', 'Name', 'IsDefault'])->toArray();

            return $enrollment_groups;
        } catch (Exception $ex) {
            \Log::error($ex);
        }
    }
}
