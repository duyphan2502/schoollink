<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\UTCDatetime;

/**
 * Class UpdateEnrollmentGroupAction.
 *
 */
class UpdateEnrollmentGroupAction extends Action
{

    /**
     * @var \App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface
     */
    private $enrollment_group_repository;
    private $school_repository;
    private $school_owner_repository;
    private $subgroup_repository;
    private $group_repository;
    private $activity_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        ActivityRepositoryInterface $activity_repository,
        SchoolRepositoryInterface $school_repository,
        SchoolOwnerRepositoryInterface $school_owner_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository
    )
    {
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->school_owner_repository = $school_owner_repository;
        $this->group_repository = $group_repository;
    }

    public function run($data)
    {
        try {
            $app = \Session::get('app');
            $current_user = $app->current_user;

            $school = $this->school_repository->find($data['school_id']);

            $enrollment_group = $this->enrollment_group_repository
                ->findWhere(['_id' => $data['id'], 'SchoolOwnerId' => (int)$school->owner_id, 'SchoolId' => (int)$school->id])->first();

            if (!isset($enrollment_group) || !$enrollment_group->permissionEdit($current_user))
                return false;

            $set_data = [];

            if (isset($data['name'])) {
                $set_data['Name'] = $data['name'];
            }

            if (isset($data['date_range']['startDate'])) {
                $set_data['DateStart'] = new UTCDatetime(Carbon::parse($data['date_range']['startDate'])->getTimestamp() * 1000);
            }

            if (isset($data['date_range']['endDate'])) {
                $set_data['DateEnd'] = new UTCDatetime(Carbon::parse($data['date_range']['endDate'])->getTimestamp() * 1000);
            }

            if (isset($data['enrollment_date_range']['startDate'])) {
                $set_data['Enrollment_DateStart'] = new UTCDatetime(Carbon::parse($data['enrollment_date_range']['startDate'])->getTimestamp() * 1000);
            }

            if (isset($data['enrollment_date_range']['endDate'])) {
                $set_data['Enrollment_DateEnd'] = new UTCDatetime(Carbon::parse($data['enrollment_date_range']['endDate'])->getTimestamp() * 1000);
            }

            if ($data['time_sending_message_range'] != null) {
                $set_data['TimeSendingMessage'] = new UTCDatetime(Carbon::parse($data['time_sending_message_range'])->getTimestamp() * 1000);
            }

            if ($data['time_publish_allocation_range'] != null) {
                $set_data['TimePublishAllocation'] = new UTCDatetime(Carbon::parse($data['time_publish_allocation_range'])->getTimestamp() * 1000);
            }

            if (isset($data['direct_entry_date_range']['startDate'])) {
                $set_data['DirectEntryDateStart'] = new UTCDatetime(Carbon::parse($data['direct_entry_date_range']['startDate'])->getTimestamp() * 1000);
            }

            if (isset($data['direct_entry_date_range']['endDate'])) {
                $set_data['DirectEntryDateEnd'] = new UTCDatetime(Carbon::parse($data['direct_entry_date_range']['endDate'])->getTimestamp() * 1000);
            }

            if (isset($data['withdraw_date_range']['startDate'])) {
                $set_data['WithdrawDateStart'] = new UTCDatetime(Carbon::parse($data['withdraw_date_range']['startDate'])->getTimestamp() * 1000);
            }

            if (isset($data['withdraw_date_range']['endDate'])) {
                $set_data['WithdrawDateEnd'] = new UTCDatetime(Carbon::parse($data['withdraw_date_range']['endDate'])->getTimestamp() * 1000);
            }

            $set_data['MaximumCourseRandomRegistration'] = $data['maximum_course_random_registration'];
            $set_data['MaximumCourseRandomDistribution'] = $data['maximum_course_random_distribution'];
            $set_data['MaximumCourseDirectRegistration'] = $data['maximum_course_direct_registration'];
            $set_data['ParticipantActivitiesMaximum'] = $data['participant_activities_maximum'];
            $set_data['DescriptionRandomAllocation'] = $data['description_random_allocation'];
            $set_data['DescriptionDirectEntry'] = $data['description_direct_entry'];
            $set_data['ActivatedBy'] = $current_user->id;
            $set_data['DateActivated'] = new UTCDatetime(time() * 1000);

            if(isset($data['is_default']) && $data['is_default'] == true) {
                $this->enrollment_group_repository->updateOne(['DateDeleted' => null], ['IsDefault' => (boolean)false]);
            }
            $set_data['IsDefault'] = isset($data['is_default']) && $data['is_default'] == 'true' ? true: false;

            $this->enrollment_group_repository->update($set_data, $enrollment_group->id);

            return ['result' => true];
        } catch (Exception $ex) {
            \Log::error($ex);
        }
    }
}
