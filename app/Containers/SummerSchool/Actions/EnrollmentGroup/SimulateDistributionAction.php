<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use App\Core\Utils\SortUtil;
use MongoDB\BSON\ObjectID;

/**
 * Class SimulateDistributionAction.
 *
 */
class SimulateDistributionAction extends Action
{

    /**
     * @var \App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface
     */
    private $enrollment_group_repository;
    private $activity_repository;
    private $school_repository;
    private $course_repository;
    private $participant_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        SchoolRepositoryInterface $school_repository,
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        CourseRepositoryInterface $course_repository,
        ActivityRepositoryInterface $activity_repository,
        ParticipantRepositoryInterface $participant_repository
    )
    {
        $this->school_repository = $school_repository;
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->course_repository = $course_repository;
        $this->activity_repository = $activity_repository;
        $this->participant_repository = $participant_repository;
    }

    public function run($id, $school_id, $confirm = false)
    {
        try {

            $school = $this->school_repository->find($school_id);

            $enrollment_group = $this->enrollment_group_repository
                ->findWhere(['SchoolOwnerId' => $school->owner_id, 'SchoolId' => $school->id, '_id' => $id])->first();

            $mongo_activities = $this->activity_repository
                ->findWhere([
                    'EnrollmentGroupId' => new ObjectID($enrollment_group->id),
                    'Active' => true,
                    'DateDeleted' => null,
                    ['DateActivated', '!=', null]
                ]);

            /** @var Activity[] $Activities */
            $activities = array();
            $activity_course_ids = array();
            $activity_ids = array();

            foreach ($mongo_activities as $activity) {
                if ($activity->course_id)
                    $activity_course_ids[] = $activity->course_id;
                $activities[(string)$activity->id] = $activity;
                $activity_ids[] = new ObjectID($activity->id);
            }

            $mongo_courses = $this->course_repository->findWhereIn('_id', $activity_course_ids);

            $courses = array();
            foreach ($mongo_courses as $course) {
                $courses[(string)$course->id] = $course;
            }

            $participants = $this->participant_repository->findWhereIn('ActivityId', $activity_ids,
                ['ResourceId', 'Status', 'ActivityId', 'Priority']);

            $users = array();
            $activity_counts = array();

            foreach ($participants as $participant) {
                if (!array_key_exists($participant->resource_id, $users)) {
                    $users[(int)$participant->resource_id] = array(
                        'participant_count' => 0,
                        'participants' => array(),
                        'selected_participant' => []
                    );
                }
                $activity_id = $participant->activity_id;

                $activity = $activities[$activity_id];

                $users[(int)$participant->resource_id]['participant_count']++;
                $users[(int)$participant->resource_id]['participants'][$participant->priority] = array(
                    'participant' => $participant,
                    'activity' => $activity
                );

                if (!isset($activity_counts[$activity_id])) {
                    $activity_counts[$activity_id] = array(
                        'allocated' => 0,
                        'confirmed' => 0,
                        'levels' => array(),
                        'levels_unallocable_in_any_course' => array(),
                        'levels_total_applied' => array(),
                        'levels_unallocable_due_to_participant_limit' => array()
                    );
                }

                if ($participant->status === Constant::PARTICIPANT_STATUS_ALLOCATED ||
                    $participant->status === Constant::PARTICIPANT_STATUS_BOOKED_DIRECTLY) {
                    $activity_counts[$activity_id]['allocated']++;
                    $activity_counts[$activity_id]['confirmed']++;
                    $users[$participant->resource_id]['selected_participant'][] = array(
                        'participant' => $participant,
                        'activity' => $activity
                    );
                }
            }
            shuffle($users);

            SortUtil::AASort($users, 'participant_count');

            $level = 1;

            do {
                $rerun = false;

                foreach ($users as $user_key => $user) {
                    if(isset($user['participants'][$level])){
                        $rerun = true;

                        $user_participant = $user['participants'][$level];
                        $activity = $user_participant['activity'];

                        $activity_id = $activity->id;
                        if (!isset($activity_counts[$activity_id]['levels_total_applied'][$level]))
                            $activity_counts[$activity_id]['levels_total_applied'][$level] = 0;

                        $activity_counts[$activity_id]['levels_total_applied'][$level]++;
                        $is_duplicated = $this->checkDuplicateTimeSlot($user['selected_participant'], $activity);

                        if (count($user['selected_participant']) < $enrollment_group->maximum_course_random_distribution &&
                            $user_participant['participant']->status == Constant::PARTICIPANT_STATUS_REGISTERED && !$is_duplicated
                        ) {
                            $limit = $activity->options->summer_school_participants_maximum;
                            if(isset($activity->options->summer_school_participants_limit))
                                $limit += $activity->options->summer_school_participants_limit;

                            if ($limit > $activity_counts[(string)$activity_id]['allocated']) {
                                $activity_counts[$activity_id]['allocated']++;
                                if (!isset($activity_counts[$activity_id]['levels'][$level]))
                                    $activity_counts[$activity_id]['levels'][$level] = 0;

                                $activity_counts[$activity_id]['levels'][$level]++;
                                $users[$user_key]['selected_participant'][] = $user_participant;
                            } else {
                                if (!isset($activity_counts[$activity_id]['levels_unallocable_due_to_participant_limit'][$level]))
                                    $activity_counts[$activity_id]['levels_unallocable_due_to_participant_limit'][$level] = 0;
                                $activity_counts[$activity_id]['levels_unallocable_due_to_participant_limit'][$level]++;

                            }
                        }
                    }
                }

                $level++;
            } while ($rerun && $level <= $enrollment_group->maximum_course_random_registration);



            foreach ($users as $user_key => $user) {
                if (count($user['selected_participant']) === 0) {
                    foreach ($user['participants'] as $user_participant) {

                        /** @var Activity $Activity */
                        $activity = $user_participant['activity'];
                        /** @var Activity__Participant $Participant */
                        $participant = $user_participant['participant'];

                        if (isset($activity_counts[$activity->id])) {
                            if (!isset($activity_counts[$activity->id]['levels_unallocable_in_any_course'][$participant->priority]))
                                $activity_counts[$activity->id]['levels_unallocable_in_any_course'][$participant->priority] = 0;
                            $activity_counts[$activity->id]['levels_unallocable_in_any_course'][$participant->priority]++;
                        }
                    }
                }
            }

            if ($confirm) {
                foreach ($users as $user_key => $user) {
                    if (count($user['selected_participant']) > 0) {
                        foreach ($user['selected_participant'] as $user_participant) {
                            $activity = $user_participant['activity'];
                            $participant = $user_participant['participant'];

                            if ($participant->status == Constant::PARTICIPANT_STATUS_REGISTERED) {
//                                $this->participant_repository->updateOne(
//                                    [
//                                        'ActivityId' => new ObjectID($activity->id),
//                                        'ResourceId' => (int)$participant->resource_id
//                                    ],
//                                    ['Participants.$.Status' => Constant::PARTICIPANT_STATUS_ALLOCATED]
//                                );
//
//                                if ($participant->priority != 1) {
//                                    $first_priority = $this->participant_repository->findWhere(
//                                        [
//                                            'ResourceId' => $participant->user_id,
//                                            'Priority' => 1
//                                        ])->first();
//
//                                    $first_priority_activity = $this->activity_repository->find($first_priority->activity_id);
//
//                                    if (isset($first_priority_activity) && $first_priority_activity->allow_waiting_queue) {
//                                        if (($activity->date_start >= $first_priority_activity->date_start
//                                                && $activity->date_start >= $first_priority_activity->date_end)
//                                            || ($activity->date_end <= $first_priority_activity->date_start
//                                                && $activity->date_end <= $first_priority_activity->date_end)
//                                        ) {
//                                            $this->participant_repository->updateOne(
//                                                [
//                                                    '_id' => new ObjectID($first_priority->id)
//                                                ],
//                                                ['Status' => Constant::PARTICIPANT_STATUS_WAITING_LIST]
//                                            );
//                                        }
//                                    }
//                                }
                            }

                        }
                    }
                }

                foreach ($activities as $activity) {
                    $this->activity_repository->updateOne([
                        '_id' => new ObjectID($activity->id)
                    ], [
                        'NumberOfWaitingList' => $activity->getNumberOfWaitingList(),
                        'NumberOfConfirmed' => $activity->getNumberOfConfirmed()
                    ]);
                }
            }

            $schools = $this->school_repository->findWhere(['owner_id' => $school->owner_id, 'date_deleted' => null])->keyBy('id');

            $return = [];
            foreach ($activities as $activity) {
                $course = $activity->course_id ? $courses[$activity->course_id] : null;
                $school = $activity->options->summer_school_school_id != null  && isset($schools[$activity->options->summer_school_school_id]) ? $schools[$activity->options->summer_school_school_id] : null;

                $simulation_data = isset($activity_counts[$activity->id]) ? $activity_counts[$activity->id] : null;
                $return[] = array(
                    'activity' => $activity,
                    'course' => $course,
                    'school' => $school,
                    'simulation' => $simulation_data
                );
                if($simulation_data != null) {
                    $simulation = [
                        'Allocated' => $simulation_data['allocated'],
                        'Confirmed' => $simulation_data['confirmed'],
                        'Levels' => $simulation_data['levels'],
                        'LevelsTotalApplied' => $simulation_data['levels_total_applied'],
                        'LevelsUnallocableDueToParticipantLimit' => $simulation_data['levels_unallocable_due_to_participant_limit'],
                        'LevelsUnallocableInAnyCourse' => $simulation_data['levels_unallocable_in_any_course']
                    ];

                    $this->activity_repository->updateOne(['_id' => new ObjectID($activity->id)],
                        ['Simulation' => $simulation]);
                } else {
                    $this->activity_repository->updateOne(['_id' => new ObjectID($activity->id)],
                        ['Simulation' => null]);
                }
            }

            return ['activities' => $return, 'enrollment_group' => $enrollment_group];
        } catch (Exception $ex) {
            \Log::error($ex);
        }
    }


    protected function checkDuplicateTimeSlot($selected_participant, $activity)
    {
        $result = false;
        if (count($selected_participant) > 0) {
            foreach ($selected_participant as $participant) {
                $selected_activity = $participant['activity'];
                if (($selected_activity->date_start <= $activity->date_start && $selected_activity->date_end >= $activity->date_start)
                    || ($selected_activity->date_start <= $activity->date_end && $selected_activity->date_end >= $activity->date_end)
                ) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }
}
