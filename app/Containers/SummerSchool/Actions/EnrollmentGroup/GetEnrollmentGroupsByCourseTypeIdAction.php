<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use Exception;
use MongoDB\BSON\ObjectID;

use App\Core\Action\Abstracts\Action;

class GetEnrollmentGroupsByCourseTypeIdAction extends Action
{
    private $enrollment_group_repository;

    /**
     * GetEnrollmentGroupsByCourseTypeIdAction constructor.
     *
     */
    public function __construct(EnrollmentGroupRepositoryInterface $enroll_group_repository) {
        $this->enrollment_group_repository = $enroll_group_repository;
    }


    public function run($data)
    {
        try {
            $course_type_id = $data->id;
            $result = $this->getEnrollmentGroups($course_type_id);
            return $result;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    private function getEnrollmentGroups($course_type_id){
        $groups = $this->enrollment_group_repository->findWhere([
            'CourseTypeId' => new ObjectID($course_type_id),
            ['DateActivated', '<>', NULL],
            'DateArchived' => NULL,
            'DateDeleted' => NULL
        ]);
        return ['result' => $groups];
    }
}