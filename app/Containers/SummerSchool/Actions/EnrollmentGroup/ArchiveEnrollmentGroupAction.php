<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use MongoDB\BSON\UTCDatetime;

/**
 * Class ArchiveEnrollmentGroupAction.
 *
 */
class ArchiveEnrollmentGroupAction extends Action
{

    /**
     * @var \App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface
     */
    private $enrollment_group_repository;
    private $activity_repository;
    private $school_repository;

    /**
     * ArchiveEnrollmentGroupAction constructor.
     *
     */
    public function __construct(
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        ActivityRepositoryInterface $activity_repository,
        SchoolRepositoryInterface $school_repository
    )
    {
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
    }

    public function run($id, $school_id)
    {
        try {
            $app = \Session::get('app');
            $current_user = $app->current_user;

            $school = $this->school_repository->find($school_id);

            $enrollment_group = $this->enrollment_group_repository
                ->findWhere(['_id' => $id, 'SchoolOwnerId' => (int)$school->owner_id, 'SchoolId' => (int)$school->id])->first();

            if (!isset($enrollment_group) || !$enrollment_group->permissionEdit($current_user))
                return ['result' => false];

            $this->enrollment_group_repository
                ->update(['DateArchived' => new UTCDatetime(time() * 1000),
                    'ArchivedBy' => $current_user->id], $enrollment_group->id);

            return ['result' => true];
        } catch (Exception $ex) {
            \Log::error($ex);
        }
    }
}
