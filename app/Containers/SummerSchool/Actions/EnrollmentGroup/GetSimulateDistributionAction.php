<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use App\Core\Utils\SortUtil;
use MongoDB\BSON\ObjectID;

/**
 * Class GetSimulateDistributionAction.
 *
 */
class GetSimulateDistributionAction extends Action
{

    /**
     * @var \App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface
     */
    private $enrollment_group_repository;
    private $activity_repository;
    private $school_repository;
    private $course_repository;
    private $participant_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        SchoolRepositoryInterface $school_repository,
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        CourseRepositoryInterface $course_repository,
        ActivityRepositoryInterface $activity_repository,
        ParticipantRepositoryInterface $participant_repository
    )
    {
        $this->school_repository = $school_repository;
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->course_repository = $course_repository;
        $this->activity_repository = $activity_repository;
        $this->participant_repository = $participant_repository;
    }

    public function run($id, $school_id, $confirm = false)
    {
        try {

            $school = $this->school_repository->find($school_id);

            $enrollment_group = $this->enrollment_group_repository
                ->findWhere([
                    'SchoolOwnerId' => $school->owner_id,
                    'SchoolId' => $school->id,
                    '_id' => $id
                ],[
                    'Enrollment_DateStart',
                    'Enrollment_DateEnd',
                    'DirectEntryDateStart',
                    'MaximumCourseRandomRegistration'
                ])->first();

            $mongo_activities = $this->activity_repository
                ->findWhere([
                    'EnrollmentGroupId' => new ObjectID($enrollment_group->id),
                    'Active' => true,
                    'DateDeleted' => null,
                    ['DateActivated', '!=', null]
                ],[
                    '_id',
                    'Options',
                    'Name',
                    'SchoolId',
                    'DateStart',
                    'DateEnd',
                    'Notes',
                    'Simulation',
                    'CourseId'
                ]);

            /** @var Activity[] $Activities */
            $activities = array();
            $activity_course_ids = array();
            $activity_ids = array();

            foreach ($mongo_activities as $activity) {
                if ($activity->course_id)
                    $activity_course_ids[] = $activity->course_id;
                $activities[(string)$activity->id] = $activity;
                $activity_ids[] = new ObjectID($activity->id);
            }

            $mongo_courses = $this->course_repository->findWhereIn('_id', $activity_course_ids);

            $courses = array();
            foreach ($mongo_courses as $course) {
                $courses[(string)$course->id] = $course;
            }

            $schools = $this->school_repository->findWhere(['owner_id' => $school->owner_id, 'date_deleted' => null])->keyBy('id');

            $return = [];
            foreach ($activities as $activity) {
                $course = $activity->course_id ? $courses[$activity->course_id] : null;
                $school = $activity->options->summer_school_school_id != null  && isset($schools[$activity->options->summer_school_school_id]) ? $schools[$activity->options->summer_school_school_id] : null;

                $return[] = array(
                    'activity' => $activity,
                    'course' => $course,
                    'school' => $school,
                    'simulation' => isset($activity->simulation) ? $activity->simulation : null
                );
            }

            return ['activities' => $return, 'enrollment_group' => $enrollment_group];
        } catch (Exception $ex) {
            \Log::error($ex);
        }
    }


    protected function checkDuplicateTimeSlot($selected_participant, $activity)
    {
        $result = false;
        if (count($selected_participant) > 0) {
            foreach ($selected_participant as $participant) {
                $selected_activity = $participant['activity'];
                if (($selected_activity->date_start <= $activity->date_start && $selected_activity->date_end >= $activity->date_start)
                    || ($selected_activity->date_start <= $activity->date_end && $selected_activity->date_end >= $activity->date_end)
                ) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }
}
