<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Containers\SummerSchool\Criterias\Activity\GetAvailableActivitiesCriteria;
use App\Containers\SummerSchool\Criterias\Course\GetAllCoursesCriteria;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class GetEnrollmentGroupAction.
 *
 */
class GetEnrollmentGroupAction extends Action
{

    /**
     * @var \App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface
     */
    private $enrollment_group_repository;
    private $course_repository;
    private $activity_repository;
    private $school_repository;
    private $school_owner_repository;
    private $subgroup_repository;
    private $group_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        ActivityRepositoryInterface $activity_repository,
        CourseRepositoryInterface $course_repository,
        SchoolRepositoryInterface $school_repository,
        SchoolOwnerRepositoryInterface $school_owner_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository
    )
    {
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->course_repository = $course_repository;
        $this->school_repository = $school_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->school_owner_repository = $school_owner_repository;
        $this->group_repository = $group_repository;
        $this->activity_repository = $activity_repository;
    }

    public function run($id, $school_id)
    {
        try {
            $app = \Session::get('app');
            $current_user = $app->current_user;

            $school = $this->school_repository->find($school_id);

            $enrollment_group = $this->enrollment_group_repository
                ->findWhere(['_id' => $id, 'SchoolOwnerId' => (int)$school->owner_id, 'SchoolId' => (int)$school_id])->first();

            if (!isset($enrollment_group) || !$enrollment_group->permissionAccess($current_user))
                return false;

            return ['enrollment_group' => $enrollment_group];
        } catch (Exception $ex) {
            \Log::error($ex);
        }
    }
}
