<?php

namespace App\Containers\SummerSchool\Actions\EnrollmentGroup;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Containers\SummerSchool\Criterias\Activity\GetAvailableActivitiesCriteria;
use App\Containers\SummerSchool\Criterias\EnrollmentGroup\GetAllEnrollmentGroupsCriteria;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;

/**
 * Class GetAllAction.
 *
 */
class GetAllAction extends Action
{

    /**
     * @var \App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface
     */
    private $enrollment_group_repository;
    private $activity_repository;
    private $school_repository;
    private $subgroup_repository;
    private $group_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        ActivityRepositoryInterface $activity_repository,
        SchoolRepositoryInterface $school_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository
    )
    {
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
    }

    public function run($data)
    {
        try {
            $school_owner_id = [];
            $school_id = [];
            $owner_ors = [];
            $this->initParams($school_owner_id, $school_id, $owner_ors);

            $academic_year = isset($data['year']) ? $data['year'] : Carbon::now()->year;

            $groups = $this->getEnrollmentGroups($school_owner_id, $school_id, $owner_ors, $academic_year);
            return $groups;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function initParams(&$school_owner_id, &$school_id, &$owner_ors)
    {
        $app = \Session::get('app');
        $user_role = $app->user_role;

        switch ($user_role->role_id) {
            case Constant::ROLE_SFO:
                $school = $this->school_repository->find($user_role->school_id);
                $school_owner_id = (int)$school->owner_id;
                $school_id = (int)$school->id;
                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_SCHOOL_SFO,
                    'Owner.SchoolId' => (int)$school->id
                );
                break;

            case Constant::ROLE_SCHOOL_ADMIN:
                $school = $this->school_repository->find($user_role->school_id);
                $school_owner_id = (int)$school->owner_id;
                $school_id = (int)$school->id;
                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_SCHOOL_ADMIN,
                    'Owner.SchoolId' => (int)$school->id
                );
                break;

            case Constant::ROLE_TEACHER:
                $subgroup = $this->subgroup_repository->find($user_role->subgroup_id);
                $group = $this->group_repository->find($subgroup->group_id);
                $school = $this->school_repository->find($group->school_id);

                $school_owner_id = (int)$school->owner_id;
                $school_id = (int)$school->id;

                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_TEACHER,
                    'Owner.SubgroupId' => (int)$subgroup->id
                );
                break;
        }
    }


    protected function getEnrollmentGroups($school_owner_id, $school_id, $owner_ors, $academic_year)
    {
        $group_params = array(
            'school_owner_ids' => [$school_owner_id],
            'school_ids' => [$school_id],
            'owner_ors' => $owner_ors,
            'year' => $academic_year
        );

        $mongo_enrollment_groups = $this->enrollment_group_repository
            ->getByCriteria(new GetAllEnrollmentGroupsCriteria($group_params));

        $enrollment_groups = [];

        foreach ($mongo_enrollment_groups as $enrollment_group) {
            $number_activated_activities = $this->getActivitiesOfEnrollmentGroup($enrollment_group);

            $enrollment_groups[] = array(
                'school' => array(
                    'id' => (int)$enrollment_group->school_id
                ),
                'enrollment_group' => array(
                    'school_id' => $enrollment_group->school_id,
                    'id' => $enrollment_group->id,
                    'name' => $enrollment_group->name,
                    'participant_activities_maximum' => $enrollment_group->participant_activities_maximum,
                    'date_start' => $enrollment_group->date_start,
                    'date_end' => $enrollment_group->date_end,
                    'enrollment_date_start' => isset($enrollment_group->enrollment_date_start) ? $enrollment_group->enrollment_date_start : null,
                    'enrollment_date_end' => isset($enrollment_group->enrollment_date_end) ? $enrollment_group->enrollment_date_end : null,
                    'is_default' => isset($enrollment_group->is_default) ? $enrollment_group->is_default: false,
                    'is_archived' => isset($enrollment_group->archived_by) ? $enrollment_group->archived_by : false
                ),
                'number_activated_activities' => $number_activated_activities
            );
        }

        return $enrollment_groups;
    }

    protected function getActivitiesOfEnrollmentGroup($enrollment_group)
    {
        $params = [
            'SchoolOwnerId' => $enrollment_group->school_owner_id,
            'SchoolId' => $enrollment_group->school_id,
            'Owner.Type' => $enrollment_group->owner->type,
            'Owner.SchoolId' => $enrollment_group->owner->school_id,
            //'date' => $enrollment_group->date_start,
            'CourseTypeId' => new ObjectID($enrollment_group->course_type_id),
            'EnrollmentGroupId' => new ObjectID($enrollment_group->id),
            'Active' => true,
            ['DateActivated', '<>', null],
            ['DateDeleted', '=', null]
        ];

        $mongo_activities = $this->activity_repository->count($params);
        return $mongo_activities;
    }
}
