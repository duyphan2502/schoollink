<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 30/09/2016
 * Time: 11:26 AM
 */

namespace App\Containers\SummerSchool\Actions\Post;

use App\Containers\SummerSchool\Contracts\PostRepositoryInterface;
use App\Core\Action\Abstracts\Action;

class GetAllPostsAction extends Action
{
    private $post_repository;

    /**
     * GetSubjectsAction constructor.
     */
    public function __construct(PostRepositoryInterface $post_repository)
    {
        $this->post_repository = $post_repository;
    }

    public function run()
    {
        $posts = $this->post_repository->findWhere(['post_status' => 'publish', 'post_type' => 'post']);
        return ['posts' => $posts];
    }
}