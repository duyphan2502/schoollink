<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

class UpdateActivityAction extends Action
{
    protected $activity_repository;
    protected $course_repository;
    protected $school_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        SchoolRepositoryInterface $school_repository,
        CourseRepositoryInterface $course_repository
    )
    {
        $this->activity_repository = $activity_repository;
        $this->course_repository = $course_repository;
        $this->school_repository = $school_repository;
    }


    public function run($data)
    {
        try {
            $activity = $data['activity'];
            $activity_id = $data['activity_id'];

            $result = $this->updateActivity($activity_id, $activity);
            return ['result' => $result];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function setData(&$data, $resource, $data_key, $input_key){
        if(isset($resource[$input_key])) {
            $data[$data_key] =  $resource[$input_key];
        } else {
            $data[$data_key] = null;
        }
    }

    protected function updateActivity($activity_id, $activity)
    {
        $data = array();
        $exist_activity = $this->activity_repository->find($activity_id);

        $app = \Session::get('app');
        $current_user = $app->current_user;

        if (!isset($exist_activity) || !$exist_activity->permissionEdit($current_user)) {
            return null;
        }

        $data['ActivatedBy'] = $current_user->id;
        $data['DateActivated'] = new UTCDatetime(Carbon::now()->getTimestamp() * 1000);

        if (isset($activity['date_range'])) {
            $data['DateStart'] = isset($activity['date_range']['startDate']) ? new UTCDatetime(Carbon::parse($activity['date_range']['startDate'])->getTimestamp() * 1000) : null;
            $data['DateEnd'] = isset($activity['date_range']['endDate']) ? new UTCDatetime(Carbon::parse($activity['date_range']['endDate'])->getTimestamp() * 1000) : null;
        }

        if (isset($activity['course_id'])) {
            $course = $this->course_repository
                ->find($activity['course_id']);
            if ($course) {
                $data['CourseId'] = new ObjectID($course->id);
            }
        }

        $this->setData($data, $activity, 'Name', 'name');
        $this->setData($data, $activity, 'Description', 'description');
        $this->setData($data, $activity, 'AllowWaitingQueue', 'allow_waiting_queue');

        $this->setData($data, $activity, 'MondayTimeStart', 'monday_time_start');
        $this->setData($data, $activity, 'MondayTimeEnd', 'monday_time_end');

        $this->setData($data, $activity, 'TuesdayTimeStart', 'tuesday_time_start');
        $this->setData($data, $activity, 'TuesdayTimeEnd', 'tuesday_time_end');

        $this->setData($data, $activity, 'WednesdayTimeStart', 'wednesday_time_start');
        $this->setData($data, $activity, 'WednesdayTimeEnd', 'wednesday_time_end');

        $this->setData($data, $activity, 'ThursdayTimeStart', 'thursday_time_start');
        $this->setData($data, $activity, 'ThursdayTimeEnd', 'thursday_time_end');

        $this->setData($data, $activity, 'FridayTimeStart', 'friday_time_start');
        $this->setData($data, $activity, 'FridayTimeEnd', 'friday_time_end');

        $this->setData($data, $activity, 'SaturdayTimeStart', 'saturday_time_start');
        $this->setData($data, $activity, 'SaturdayTimeEnd', 'saturday_time_end');

        $this->setData($data, $activity, 'SundayTimeStart', 'sunday_time_start');
        $this->setData($data, $activity, 'SundayTimeEnd', 'sunday_time_end');

        $this->setData($data, $activity, 'NumberOfGroups', 'number_of_groups');

        $options_data = array();
        if (isset($activity['options']['summer_school_school_id'])) {
            $summer_school = $this->school_repository->find($activity['options']['summer_school_school_id']);

            if ($summer_school && $summer_school->owner_id == $activity['school_owner_id']) {
                $options_data['SummerSchoolSchoolId'] = (int)$summer_school->id;
            }
        }

        if(isset($activity['options']['summer_school_allow_withdrawal_date_start'])) {
            $options_data['SummerSchoolAllowWithdrawalDateStart'] = new UTCDatetime($activity['options']['summer_school_allow_withdrawal_date_start'] * 1000);
        }

        if(isset($activity['options']['summer_school_allow_withdrawal_date_end'])) {
            $options_data['SummerSchoolAllowWithdrawalDateEnd'] = new UTCDatetime($activity['options']['summer_school_allow_withdrawal_date_end'] * 1000);
        }

        if(isset($activity['options']['summer_school_participants_maximum'])) {
            $options_data['SummerSchoolParticipantsMaximum'] = $activity['options']['summer_school_participants_maximum'];
        }

        if(isset($activity['options']['summer_school_participants_limit'])) {
            $options_data['SummerSchoolParticipantsLimit'] = $activity['options']['summer_school_participants_limit'];
        }

        $data['Options'] = $options_data;

        $result = $this->activity_repository->update($data, $activity_id);
        return $result;
    }
}