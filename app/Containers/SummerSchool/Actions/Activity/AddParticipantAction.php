<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;

class AddParticipantAction extends Action
{
    private $activity_repository;
    private $school_repository;
    private $user_repository;
    private $subgroup_repository;
    private $group_repository;
    private $participant_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        ParticipantRepositoryInterface $participant_repository,
        UserRepositoryInterface $user_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository
    ) {
        $this->activity_repository = $activity_repository;
        $this->user_repository = $user_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->school_repository = $school_repository;
        $this->participant_repository = $participant_repository;
    }


    public function run($id, $user_id)
    {
        try {
            $app = \Session::get('app');

            $activity = $this->getActivity($id);
            if($activity == null)
                return ['result' => false];

            $existed = $activity->checkRegisteredActivityOfParticipant($user_id);
            if($existed) {
                return ['result' => false, 'message' => trans('message.m_summer_school.participant_registered')];
            }

            $user = $this->user_repository->find($user_id);

            if($user->hasRole(Constant::ROLE_PUPIL,
                new RoleParam(['school_owner_id' => $activity->school_owner_id, 'current_year' => true]))) {

                $number_of_registrations = $this->participant_repository->count([
                    'ActivityId' => new ObjectID($id),
                    ['Status', '!=', SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED],
                    ['Status', '!=', SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST]
                ]);

                $maximum = $activity->options->summer_school_participants_maximum;

                if($number_of_registrations < $maximum) {
                    $participant = [
                        'ResourceId' => $user_id,
                        'Priority' => null,
                        'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY,
                        'GroupNumber' => 1,
                        'RegisteredDate' => new UTCDateTime(Carbon::now()->getTimestamp() * 1000),
                        'RegisteredBy' => $app->current_user->id,
                        'ActivityId' => new ObjectID($activity->id),
                        'EnrollmentGroupId' => new ObjectID($activity->enrollment_group_id),
                        'CourseId' => new ObjectID($activity->course_id)
                    ];
                } else if($activity->allow_waiting_queue == true){
                    $position = $activity->getNextPositionOfWaitingList();
                    $participant = [
                        'ResourceId' => $user_id,
                        'Priority' => null,
                        'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST,
                        'GroupNumber' => 1,
                        'RegisteredDate' => new UTCDateTime(Carbon::now()->getTimestamp() * 1000),
                        'RegisteredBy' => $app->current_user->id,
                        'ActivityId' => new ObjectID($activity->id),
                        'EnrollmentGroupId' => new ObjectID($activity->enrollment_group_id),
                        'CourseId' => new ObjectID($activity->course_id),
                        'Position' => $position
                    ];
                }

                if(isset($participant)) {
                    $this->participant_repository->deleteWhere([
                        'ActivityId' => new ObjectID($activity->id),
                        'ResourceId' => (int)$user_id
                    ]);

                    $result = $this->participant_repository->create($participant);

                    if($result != null) {
                        $participant_information = $activity->getParticipantInformationByUserId($user_id);
                        $activity->updateNumberOfConfirmOrWaiting($participant['Status']);

                        return ['result' => $participant_information];
                    }
                }
            }
            return ['result' => false];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function getActivity($activity_id) {

        $activity = $this->activity_repository
            ->find($activity_id);

        $app = \Session::get('app');
        $current_user = $app->current_user;

        if(!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }

        return $activity;
    }
}