<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

class CopyActivityAction extends Action
{
    private $activity_repository;
    private $school_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        SchoolRepositoryInterface $school_repository
    )
    {
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
    }


    public function run($data)
    {
        try {
            $activity = $this->copyActivity($data['id'], $data['enrollment_group_id']);
            return ['result' => $activity];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function copyActivity($id, $enrollment_group_id)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;

        $activity = $this->activity_repository
            ->find($id);

        if (!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }

        $copy_model = $this->mapping($activity, $enrollment_group_id);
        $copy_activity = $this->activity_repository->create($copy_model);
        return $copy_activity;
    }

    protected function mapping($activity, $enrollment_group_id)
    {
        $now = Carbon::now();

        $result = array();
        $this->setData($result, $activity->school_owner_id, 'SchoolOwnerId');
        $this->setData($result, $activity->school_id, 'SchoolId');
        $this->setData($result, $activity->course_type_id, 'CourseTypeId');
        $this->setData($result, $activity->description, 'Description');
        $this->setData($result, $activity->name, 'Name');
        $this->setData($result, $activity->created_by, 'CreatedBy');
        $this->setData($result, $activity->activated_by, 'ActivatedBy');
        $this->setData($result, $activity->date_activated, 'DateActivated');
        $this->setData($result, $activity->monday_time_start, 'MondayTimeStart');
        $this->setData($result, $activity->monday_time_end, 'MondayTimeEnd');
        $this->setData($result, $activity->tuesday_time_start, 'TuesdayTimeStart');
        $this->setData($result, $activity->tuesday_time_end, 'TuesdayTimeEnd');
        $this->setData($result, $activity->wednesday_time_start, 'WednesdayTimeStart');
        $this->setData($result, $activity->wednesday_time_end, 'WednesdayTimeEnd');
        $this->setData($result, $activity->thursday_time_start, 'ThursdayTimeStart');
        $this->setData($result, $activity->thursday_time_end, 'ThursdayTimeEnd');
        $this->setData($result, $activity->friday_time_start, 'FridayTimeStart');
        $this->setData($result, $activity->friday_time_end, 'FridayTimeEnd');
        $this->setData($result, $activity->saturday_time_start, 'SaturdayTimeStart');
        $this->setData($result, $activity->saturday_time_end, 'SaturdayTimeEnd');
        $this->setData($result, $activity->sunday_time_start, 'SundayTimeStart');
        $this->setData($result, $activity->sunday_time_end, 'SundayTimeEnd');
        $this->setData($result, $activity->number_of_groups, 'NumberOfGroups');
        $this->setData($result, $activity->allow_waiting_queue, 'AllowWaitingQueue');
        $this->setData($result, $activity->notes, 'Notes');

        $result['Active'] = true;
        $result['DateStart'] = isset($activity->date_start) ? new UTCDatetime($activity->date_start * 1000) : null;
        $result['DateEnd'] = isset($activity->date_end) ? new UTCDatetime($activity->date_end * 1000) : null;
        $result['DateCreated'] = new UTCDatetime($now->getTimestamp() * 1000);
        $result['CourseId'] = isset($activity->course_id) ? new ObjectID($activity->course_id) : null;
        $result['EnrollmentGroupId'] = isset($enrollment_group_id) ? new ObjectID($enrollment_group_id) : new ObjectID($activity->enrollment_group_id);

        if (isset($activity->owner)) {
            $result['Owner'] = array(
                'Type' => (string)$activity->owner->type,
                'SchoolId' => (int)$activity->owner->school_id
            );
        }

        if (isset($activity->options)) {
            $result['Options'] = array();

            if(isset($activity->options->summer_school_school_id)) {
                $result['Options']['SummerSchoolSchoolId'] = $activity->options->summer_school_school_id;
            }

            if(isset($activity->options->summer_school_year_grade_minimum)) {
                $result['Options']['SummerSchoolYearGradeMinimum'] = $activity->options->summer_school_year_grade_minimum;
            }

            if(isset($activity->options->summer_school_year_grade_maximum)) {
                $result['Options']['SummerSchoolYearGradeMaximum'] = $activity->options->summer_school_year_grade_maximum;
            }

            if(isset($activity->options->summer_school_birthdate_maximum)) {
                $result['Options']['SummerSchoolBirthdateMaximum'] = $activity->options->summer_school_birthdate_maximum;
            }

            if(isset($activity->options->summer_school_birthdate_minimum)) {
                $result['Options']['SummerSchoolBirthdateMinimum'] = $activity->options->summer_school_birthdate_minimum;
            }

            if(isset($activity->options->summer_school_allow_withdrawal_date_start)) {
                $result['Options']['SummerSchoolAllowWithdrawalDateStart'] = $activity->options->summer_school_allow_withdrawal_date_start;
            }

            if(isset($activity->options->summer_school_allow_withdrawal_date_end)) {
                $result['Options']['SummerSchoolAllowWithdrawalDateEnd'] = $activity->options->summer_school_allow_withdrawal_date_end;
            }

            if(isset($activity->options->summer_school_participants_maximum)) {
                $result['Options']['SummerSchoolParticipantsMaximum'] = $activity->options->summer_school_participants_maximum;
            }

            if(isset($activity->options->summer_school_participants_limit)) {
                $result['Options']['SummerSchoolParticipantsLimit'] = $activity->options->summer_school_participants_limit;
            }
        }

        return $result;
    }

    protected function setData(&$data, $value, $data_key){
        if(isset($value)) {
            $data[$data_key] =  $value;
        }
    }
}