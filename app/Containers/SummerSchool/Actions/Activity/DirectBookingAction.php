<?php
namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

class DirectBookingAction extends Action
{
    private $activity_repository;
    private $school_repository;
    private $enrollment_group_repository;
    private $participant_repository;

    /**
     * DirectBookingAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        SchoolRepositoryInterface $school_repository,
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        ParticipantRepositoryInterface $participant_repository
    )
    {
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->participant_repository = $participant_repository;
    }


    public function run($data)
    {
        try {
            $app = \Session::get('app');
            if (empty($data)) {
                return ['result' => false];
            }
            $activity = $this->activity_repository->find($data['id'],[
                '_id','DateStart','DateEnd','EnrollmentGroupId',
                'AllowWaitingQueue','Options', 'NumberOfWaitingList', 'NumberOfConfirmed'
            ]);

            //check period time valid direct entry time
            $group = $this->enrollment_group_repository->find($activity->enrollment_group_id);
            $current_date = Carbon::now()->getTimestamp();
            if(($group->direct_entry_date_start <= $current_date && $current_date <= $group->direct_entry_date_end) == false){
                return ['result' => false, 'message' => 'Period of direct entry has changed, please refresh page.'];
            }
            
            // validation limit booking in enrollment group
            $valid_maximum_course = $this->validationMaximumCourseDirectRegistration($group, $app->child->id);
            if ($valid_maximum_course == false) {
                return ['result' => false, 'message' => 'Over booked for this enrollment group'];
            }
            //end validation limit booking in enrollment group

            $limit_booking = $activity->options->summer_school_participants_maximum;
            
            $participants = $this->countParticipants($activity->id);

            $status = SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY;

            // validation limit booking in activity
            if ($limit_booking <= $participants && $activity->allow_waiting_queue == false) {
                return ['result' => false, 'message' => 'Over booked for this activity'];
            }

            // validation do not allow booking overlap date time
            foreach ($data['data'] as $act) {
                if ($act['date_start'] >= $activity->date_start && $act['date_start'] <= $activity->date_end
                    || $act['date_end'] >= $activity->date_start && $act['date_end'] <= $activity->date_end
                ) {
                    return ['result' => false, 'message' => 'Already registered for this date time'];
                }
            }

            // validation do not allow direct booking in case activity allocated
            $check_allocated = $this->participant_repository->findWhere([
                'ActivityId' => new ObjectID($data['id']),
                'ResourceId' => $app->child->id,
                'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_ALLOCATED
            ], ['_id'])->first();
            if ($check_allocated) {
                return ['result' => false, 'message' => 'Already allocated this activity'];
            }

            if ($limit_booking <= $participants && $activity->allow_waiting_queue == true) {
                $status = SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST;
            }

            $check_registered = $this->participant_repository->findWhere([
                'ActivityId' => new ObjectID($data['id']),
                'ResourceId' => $app->child->id,
                'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED
            ], ['_id'])->first();

            $next_position_waiting = NULL;
            if($status == SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST){
                $next_position_waiting = $activity->getNextPositionOfWaitingList();
            }
            if ($check_registered) {
                $participant_exc = $this->participant_repository->updateOne([
                    'ActivityId' => new ObjectID($data['id']),
                    'ResourceId' => $app->child->id,
                    'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED
                ], ['Status' => $status, 'Position' => $next_position_waiting]);
            } else {
                $participant = [
                    'ResourceId' => $app->child->id,
                    'Priority' => NULL,
                    'Status' => $status,
                    'GroupNumber' => 1,
                    'RegisteredDate' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000),
                    'RegisteredBy' => $app->current_user->id,
                    'Position' => $next_position_waiting,
                    'ActivityId' => new ObjectID($data['id']),
                    'EnrollmentGroupId' => new ObjectID($group->id)
                ];
                $participant_exc = $this->participant_repository->create($participant);
            }

            $activity->updateNumberOfConfirmOrWaiting($status);

            return [
                'participant' => $participant_exc,
                'result' => true,
                'status' => $status,
                'position' => $next_position_waiting,
                'on_waiting_list' => $status == SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST ? ($activity->number_of_waiting_list + 1) : $activity->number_of_waiting_list,
                'available_space' => $status == SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY ? ($activity->getAvailableSpace() - 1) : $activity->getAvailableSpace(),
            ];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * maximum course allow this user booking on group
     * @param $group
     * @param $user
     * @return bool
     */
    protected function validationMaximumCourseDirectRegistration($group, $user)
    {
        $activities = $this->participant_repository->count([
            'EnrollmentGroupId' => new ObjectID($group->id),
            'ResourceId' => $user,
            ['Status' , '<>', SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED],
            ['Status' , '<>', SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST]
        ]);
        if ($group && (int)$group->maximum_course_direct_registration > $activities) {
            return true;
        }
        return false;
    }

    /**
     * maximum participants in activity
     * @param $activity_id
     * @return int
     */
    protected function countParticipants($activity_id)
    {
        return $this->participant_repository->count([
            'ActivityId' => new ObjectID($activity_id),
            ['Status' , '<>', SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED],
            ['Status' , '<>', SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST]
        ]);
    }
}