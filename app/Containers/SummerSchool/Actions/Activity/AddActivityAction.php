<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

class AddActivityAction extends Action
{
    protected $enrollment_group_repository;
    protected $activity_repository;
    protected $school_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        ActivityRepositoryInterface $activity_repository,
        SchoolRepositoryInterface $school_repository
    ) {
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
    }


    public function run($enrollment_group_id, $course_id)
    {
        try {
            $id = $this->addActivity($enrollment_group_id, $course_id);
            return ['id' => $id];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function addActivity($enrollment_group_id, $course_id) {
        $enrollment_group = $this->enrollment_group_repository->find($enrollment_group_id);
        if($enrollment_group == null)
            return false;

        $school_owner_type = $enrollment_group->owner->type;
        $school_id = $enrollment_group->school_id;

        $school = $this->school_repository->find($school_id);
        if (!$school) {
            return false;
        }

        $app = \Session::get('app');
        $current_user = $app->current_user;

        switch ($school_owner_type) {
            case SummerSchoolConstant::TYPE_SCHOOL_SFO:
                if (!$current_user->hasRole(Constant::ROLE_SFO, $school->id))
                    return false;
                break;
            case SummerSchoolConstant::TYPE_SCHOOL_ADMIN:
                if (!$current_user->hasRole(Constant::ROLE_SCHOOL_ADMIN, $school->id))
                    return false;
                break;
            case SummerSchoolConstant::TYPE_TEACHER:
                if (!$current_user->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $school->id])))
                    return false;
                break;
            default:
                return false;
        }

        $academic_year = $school->academicYear();

        $data = [
            'EnrollmentGroupId' => new ObjectID($enrollment_group_id),
            'Active' => true,
            'Notes' => '',
            'SchoolOwnerId' => (int)$school->owner_id,
            'SchoolId' => (int)$school->id,
            'CourseTypeId' => new ObjectID($enrollment_group->course_type_id),
            'Description' => '',
            'Name' => '',
            'Owner' => array(
                'Type' => $school_owner_type,
                'SchoolId' => $school_owner_type === SummerSchoolConstant::TYPE_SCHOOL_SFO || $school_owner_type ===
                SummerSchoolConstant::TYPE_SCHOOL_ADMIN ? (int)$school->id : null),
            'DateStart' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000),
            'DateEnd' => new UTCDatetime(Carbon::parse($academic_year->date_end)->getTimestamp() * 1000),
            'CreatedBy' => (int)$current_user->id,
            'DateCreated' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000),
            'NumberOfGroups' => 3,
            'NumberOfConfirmed' => 0,
            'NumberOfWaitingList' => 0,
            'Simulation' => null,
            'CourseId' => new ObjectID($course_id),
        ];

        $activity = $this->activity_repository->create($data);

        return $activity->id;
    }
}