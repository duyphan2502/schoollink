<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;

class ChangePositionOfWaitingListAction extends Action
{
    private $activity_repository;
    private $school_repository;
    private $user_repository;
    private $subgroup_repository;
    private $group_repository;
    private $participant_repository;

    /**
     * ChangePositionOfWaitingListAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        ParticipantRepositoryInterface $participant_repository,
        UserRepositoryInterface $user_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository
    ) {
        $this->activity_repository = $activity_repository;
        $this->participant_repository = $participant_repository;
        $this->user_repository = $user_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->school_repository = $school_repository;
    }


    public function run($id, $user_ids)
    {
        try {
            $activity = $this->getActivity($id);
            if($activity == null)
                return ['result' => false];

            if(count($user_ids) > 0) {
                $index = 1;
                foreach ($user_ids as $user_id) {
                    $this->participant_repository->updateOne([
                        'ActivityId' => new ObjectID($activity->id),
                        'ResourceId' => $user_id
                    ],[
                        'Position' => $index
                    ]);
                    $index++;
                }
            }
            return ['result' => true];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function getActivity($activity_id) {

        $activity = $this->activity_repository
            ->find($activity_id);

        $app = \Session::get('app');
        $current_user = $app->current_user;

        if(!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }

        return $activity;
    }
}