<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use MongoDB\BSON\ObjectID;

class ChangeGroupNumberOfParticipantAction extends Action
{
    private $activity_repository;
    private $school_repository;
    private $participant_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        ParticipantRepositoryInterface $participant_repository,
        SchoolRepositoryInterface $school_repository
    ) {
        $this->participant_repository = $participant_repository;
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
    }


    public function run($id, $participants)
    {
        try {
            $activity = $this->getActivitiy($id);
            if($activity == null)
                return ['result' => false];

            foreach ($participants as $participant) {
                $participant = (object)$participant;
                $this->participant_repository->updateOne([
                    '_id' => new ObjectID($participant->id),
                    'ResourceId' => (int)$participant->resource_id
                ], [
                    'GroupNumber' => $participant->group
                ]);
            }

            return ['result' => true];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $id
     * @return mixed|null
     */
    protected function getActivitiy($id)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;

        $activity = $this->activity_repository->find($id);

        if(!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }
        return $activity;
    }
}