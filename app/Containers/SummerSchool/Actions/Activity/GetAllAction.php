<?php

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Criterias\Activity\GetAllActivitiesCriteria;
use App\Containers\SummerSchool\Criterias\Course\GetAllCoursesCriteria;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;


/**
 * Class GetAllAction.
 *
 */
class GetAllAction extends Action
{
    private $activity_repository;
    private $course_repository;
    private $school_repository;
    private $subgroup_repository;
    private $group_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        CourseRepositoryInterface $course_repository,
        SchoolRepositoryInterface $school_repository,
        GroupRepositoryInterface $group_repository,
        SubGroupRepositoryInterface $subgroup_repository
    )
    {
        $this->activity_repository = $activity_repository;
        $this->course_repository = $course_repository;
        $this->school_repository = $school_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
    }

    public function run()
    {
        try {
            $school_owner_id = null;
            $school_id = null;
            $owner_ors = [];
            $this->initParams($school_owner_id, $school_id, $owner_ors);
            $activities = $this->getActivities($school_owner_id, $school_id, $owner_ors);
            return $activities;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function initParams(&$school_owner_id, &$school_id, &$owner_ors)
    {
        $app = \Session::get('app');
        $user_role = $app->user_role;

        switch ($user_role->role_id) {
            case Constant::ROLE_SFO:
                $school = $this->school_repository->find($user_role->school_id);
                $school_owner_id = (int)$school->owner_id;
                $school_id = (int)$school->id;
                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_SCHOOL_SFO,
                    'Owner.SchoolId' => (int)$school->id
                );
                break;

            case Constant::ROLE_SCHOOL_ADMIN:
                $school = $this->school_repository->find($user_role->school_id);
                $school_owner_id = (int)$school->owner_id;
                $school_id = (int)$school->id;
                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_SCHOOL_ADMIN,
                    'Owner.SchoolId' => (int)$school->id
                );
                break;

            case Constant::ROLE_TEACHER:
                $subgroup = $this->subgroup_repository->find($user_role->subgroup_id);
                $group = $this->group_repository->find($subgroup->group_id);
                $school = $this->school_repository->find($group->school_id);

                $school_owner_id = (int)$school->owner_id;
                $school_id = (int)$school->id;

                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_TEACHER,
                    'Owner.GroupId' => (int)$group->id
                );
                $owner_ors[] = array(
                    'Owner.Type' => SummerSchoolConstant::TYPE_TEACHER,
                    'Owner.SubgroupId' => (int)$subgroup->id
                );
                break;
        }
    }

    protected function getActivities($school_owner_id, $school_id, $owner_ors)
    {
        //Get mongo activities
        $activity_params = array(
            'school_owner_ids' => [$school_owner_id],
            'school_ids' => [$school_id],
            'owner_ors' => $owner_ors,

        );

        $mongo_activities = $this->activity_repository
            ->getByCriteria(new GetAllActivitiesCriteria($activity_params));

        //GET courses by list ids
        $course_ids = [];
        $activities = [];

        foreach ($mongo_activities as $activity) {
            $activities[(string)$activity->id] = $activity;
            if ($activity->course_id != '') {
                $course_ids[(string)$activity->course_id] = $activity->course_id;
            }
        }

        $course_params = array(
            'school_owner_ids' => [$school_owner_id],
            'school_ids' => [$school_id],
            'course_ids' => $course_ids,
        );

        $mongo_courses = $this->course_repository
            ->getByCriteria(new GetAllCoursesCriteria($course_params));

        $courses = [];
        foreach ($mongo_courses as $mongo_course) {
            $courses[(string)$mongo_course->id] = $mongo_course;
        }

        $final_activities = [];


        foreach ($activities as $id => $activity) {
            $school = $this->school_repository->find($activity->school_id);
            $return_object = array(
                'activity' => array(
                    'school_id' => $activity->school_id,
                    'id' => (string)$activity->id,
                    'name' => $activity->name,
                    'course_type_id' => $activity->course_type_id,
                    'date_start' => $activity->date_start,
                    'date_end' => $activity->date_end,
                    'number_of_groups' => $activity->number_of_groups ? $activity->number_of_groups : 1
                ),
                'school' => array(
                    'id' => (int)$school->id,
                    'name' => $school->name
                ),
                'summer_school' => array(
                    'school' => null,
                    'participants_maximum' => $activity->options->summer_school_participants_maximum,
                    'participants_confirmed' => 0,
                    'participants_waiting' => 0,
                )
            );


            if (isset($activity->participants) && is_array($activity->participants)) {
                foreach ($activity->participants as $participant) {
                    if ($participant->status === SummerSchoolConstant::PARTICIPANT_STATUS_ALLOCATED ||
                        $participant->status === SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY) {
                        $return_object['summer_school']['participants_confirmed']++;
                    }
                    if ($participant->status === SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST) {
                        $return_object['summer_school']['participants_waiting']++;
                    }
                }
            }

            if (isset($activity->options->summer_school_school_id)) {
                $summer_school = $this->school_repository->find($activity->options->summer_school_school_id);
                $return_object['summer_school']['school'] = array(
                    'id' => (int)$summer_school->id,
                    'name' => $summer_school->name
                );
            }

            if (isset($activity->course_id)) {
                $course = $courses[(string)$activity->course_id];
                $return_object['activity']['year_grade_minimum'] = $course->year_grade_minimum;
                $return_object['activity']['year_grade_maximum'] = $course->year_grade_maximum;
                $return_object['course'] = array(
                    'id' => (string)$course->id,
                    'name' => $course->name,
                    'year_grade_minimum' => $course->year_grade_minimum,
                    'year_grade_maximum' => $course->year_grade_maximum,
                    'birthdate_minimum' => $course->options->summer_school_birthdate_minimum,
                    'birthdate_maximum' => $course->options->summer_school_birthdate_maximum,
                );
            }
            $final_activities[] = $return_object;
        }

        return $final_activities;
    }
}
