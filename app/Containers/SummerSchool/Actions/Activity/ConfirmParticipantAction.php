<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Containers\SummerSchool\Tasks\Message\SendAllocatedMessageTask;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use MongoDB\BSON\ObjectID;

class ConfirmParticipantAction extends Action
{
    private $activity_repository;
    private $school_repository;
    private $user_repository;
    private $subgroup_repository;
    private $group_repository;
    private $participant_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        ParticipantRepositoryInterface $participant_repository,
        UserRepositoryInterface $user_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository
    ) {
        $this->activity_repository = $activity_repository;
        $this->participant_repository = $participant_repository;
        $this->user_repository = $user_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->school_repository = $school_repository;
    }


    public function run($id, $user_id)
    {
        try {
            $activity = $this->getActivity($id);

            if($activity == null)
                return ['result' => false];

            $existed = $activity->checkRegisteredActivityOfParticipant($user_id);
            if($existed)
                return ['result' => false];

            $user = $this->user_repository->find($user_id);

            if($user->hasRole(Constant::ROLE_PUPIL,
                new RoleParam(['school_owner_id' => $activity->school_owner_id, 'current_year' => true]))) {
                $result = $this->participant_repository->updateOne([
                    'ActivityId' =>  new ObjectID($activity->id),
                    'ResourceId' => $user_id
                ],[
                    'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY
                ]);

                if((bool)$result) {
                    $activity->updateNumberOfConfirmOrWaiting(SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY);
                    $participant = $activity->getParticipantInformationByUserId($user_id);
                    return ['result' => $participant];
                }
            }
            return ['result' => false];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function getActivity($activity_id) {

        $activity = $this->activity_repository
            ->find($activity_id);

        $app = \Session::get('app');
        $current_user = $app->current_user;

        if(!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }

        return $activity;
    }
}