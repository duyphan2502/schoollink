<?php

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class GetAllAction.
 *
 */
class GetActivityAction extends Action
{
    private $enrollment_group_repository;
    private $activity_repository;
    private $school_repository;
    private $group_repository;
    private $subgroup_repository;
    private $course_repository;
    private $user_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        ActivityRepositoryInterface $activity_repository,
        CourseRepositoryInterface $course_repository,
        GroupRepositoryInterface $group_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        SchoolRepositoryInterface $school_repository,
        UserRepositoryInterface $user_repository
    )
    {
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
        $this->course_repository = $course_repository;
        $this->group_repository = $group_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->user_repository = $user_repository;
    }

    public function run($id, $school_id)
    {
        try {
            $activity = $this->getActivitiy($id, $school_id);
            return $activity;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function getActivitiy($id)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;

        $activity = $this->activity_repository->find($id);

        if (!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }

        $school = $this->school_repository->find($activity->school_id);
        if (!$school) {
            return null;
        }

        $activity->school = isset($activity->options) && $activity->options->summer_school_school_id != null ?
            $this->school_repository->find($activity->options->summer_school_school_id) : null;

        if ($activity->course_id != '') {
            $course_params = array(
                'SchoolOwnerId' => (int)$school->owner_id,
                'SchoolId' => (int)$school->id,
                '_id' => (string)$activity->course_id
            );

            $course = $this->course_repository->findWhere($course_params)->first();
            $activity->course = $course;
        }


        $participants = $activity->getParticipantsInformation();
        $activity->group = $this->enrollment_group_repository->find($activity->enrollment_group_id);

        return ['activity' => $activity, 'participants' => $participants];
    }
}
