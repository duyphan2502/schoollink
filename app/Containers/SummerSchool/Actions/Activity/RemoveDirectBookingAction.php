<?php
namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use MongoDB\BSON\ObjectID;

class RemoveDirectBookingAction extends Action
{
    private $activity_repository;
    private $school_repository;
    private $participant_repository;

    /**
     * RemoveDirectBookingAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        SchoolRepositoryInterface $school_repository,
        ParticipantRepositoryInterface $participant_repository
    )
    {
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
        $this->participant_repository = $participant_repository;
    }


    public function run($data)
    {
        try {
            $app = \Session::get('app');

            if (empty($data)) {
                return ['result' => false];
            }
            $activity = $this->activity_repository->find($data['id'], ['Options', 'NumberOfConfirmed', 'NumberOfWaitingList', '_id']);
            $limit_booking = isset($activity->options->summer_school_participants_limit) ?
                ($activity->options->summer_school_participants_limit + $activity->options->summer_school_participants_maximum) : $activity->options->summer_school_participants_maximum;

            $participants = $this->countParticipants($activity->id);

            $this->participant_repository->deleteWhere([
                'ActivityId' => new ObjectID($data['id']),
                'ResourceId' => $app->child->id
            ]);


            if ($limit_booking >= $participants && ($data['status'] == SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY
                    || $data['status'] == SummerSchoolConstant::PARTICIPANT_STATUS_ALLOCATED)) {
                $user_id = $activity->getNextWaitingParticipant();
                $this->participant_repository->updateOne([
                    'ActivityId' => new ObjectID($data['id']),
                    'ResourceId' => $user_id,
                    'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST
                ], ['Status' => SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY]);

                $activity->updateNumberOfConfirmOrWaiting(SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST);
            }

            $activity->updateNumberOfConfirmOrWaiting($data['status']);
            $activity = $this->activity_repository->find($data['id'], ['Options', 'NumberOfConfirmed', 'NumberOfWaitingList', '_id']);

            return [
                'result' => true,
                'available_space' => $activity->getAvailableSpace(),
                'on_waiting_list' => $activity->number_of_waiting_list
            ];
        } catch (Exception $e) {
            \Log::error($e);
            return ['result' => false, 'message' => $e];
        }
    }

    /**
     * maximum participants in activity
     * @param $activity_id
     * @return int
     */
    protected function countParticipants($activity_id)
    {
        return $this->participant_repository->count([
            'ActivityId' => new ObjectID($activity_id),
            ['Status' , '<>', SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED],
            ['Status' , '<>', SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST]
        ]);
    }
}