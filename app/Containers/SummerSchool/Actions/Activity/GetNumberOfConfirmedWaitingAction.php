<?php

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class GetNumberOfConfirmedWaitingAction.php.
 *
 */
class GetNumberOfConfirmedWaitingAction extends Action
{
    private $activity_repository;

    /**
     * GetAllAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository
    )
    {
        $this->activity_repository = $activity_repository;
    }

    public function run()
    {
        try {
            $app = \Session::get('app');
            $activities_all = $this->activity_repository->findWhere(
                [
                    'Active' => true,
                    ['DateActivated', '<>', NULL],
                    ['CourseId', '<>', NULL],
                    'DateDeleted' => NULL,
                    'SchoolId' => $app->special_school->id,
                    'SchoolOwnerId' => $app->school_owner->id
                ]
                ,[
                    'Options', 'NumberOfWaitingList', 'NumberOfConfirmed'
                ]
            )->keyBy('id');
            $activities = [];
            foreach($activities_all as $key => $activity){
                $activities[$key] = [
                    'on_waiting_list' => isset($activity->number_of_waiting_list) ? $activity->number_of_waiting_list : 0,
                    'available_space' => $activity->getAvailableSpace()
                ];
            }
            return $activities;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

}
