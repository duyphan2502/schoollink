<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use MongoDB\BSON\ObjectID;

class UpdateParticipantMaximumAction extends Action
{
    private $activity_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository
    )
    {
        $this->activity_repository = $activity_repository;
    }


    public function run($id, $maximum)
    {
        try {
            $activity = $this->getActivitiy($id);
            if ($activity == null)
                return ['result' => false];

            $this->activity_repository->updateOne(['_id' => new ObjectID($activity->id)],
                ['Options.SummerSchoolParticipantsMaximum' => $maximum]);
            return ['result' => true];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function getActivitiy($id)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;

        $activity = $this->activity_repository->find($id);

        if (!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }
        return $activity;
    }
}