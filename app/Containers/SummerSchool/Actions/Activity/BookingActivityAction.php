<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

use App\Core\Action\Abstracts\Action;
use App\Containers\SummerSchool\Constants\ActivityOwner;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

class BookingActivityAction extends Action
{
    private $activity_repository;
    private $school_repository;
    private $enrollment_group_repository;
    private $participant_repository;

    /**
     * BookingActivityAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        SchoolRepositoryInterface $school_repository,
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        ParticipantRepositoryInterface $participant_repository
    )
    {
        $this->activity_repository = $activity_repository;
        $this->school_repository = $school_repository;
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->participant_repository = $participant_repository;
    }

    /**
     * book, change priority interested activity
     * @param $data
     * @return array
     */
    public function run($data)
    {
        try {
            $app = \Session::get('app');
            if (empty($data['data'])) {
                return ['result' => false];
            }

            // validation maximum course registration
            if ($data['action'] == SummerSchoolConstant::ACTIVITY_PARTICIPANT_ACTION_BOOK && !empty($data['enroll_group_id'])) {
                //check period time valid direct entry time
                $group = $this->enrollment_group_repository->find($data['enroll_group_id']);
                $current_date = Carbon::now()->getTimestamp();
                if (($group->direct_entry_date_start < $current_date && $current_date < $group->direct_entry_date_end) == true) {
                    return ['result' => false, 'message' => 'Period of direct entry has changed, please refresh page.'];
                }
                $validation = $this->validationMaximumCourseRegistration($group, $app->child->id);
                if ($validation == false) {
                    return ['result' => false, 'message' => 'Over registered for this enrollment group'];
                }
            }

            $priority = 1;
            foreach ($data['data'] as $activity_participant) {
                $activity_id = $activity_participant['activity_id'];
                if (isset($data['action']) && $data['action'] == SummerSchoolConstant::ACTIVITY_PARTICIPANT_ACTION_DELETE && $activity_id == $data['id']) {
                    $this->participant_repository->deleteWhere([
                        'ActivityId' => new ObjectID($activity_id),
                        'ResourceId' => $app->child->id,
                        'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED
                    ]);
                    $priority--;
                } else {
                    $check_exist = $this->participant_repository->findWhere([
                        'ActivityId' => new ObjectID($activity_id),
                        'ResourceId' => $app->child->id,
                        'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED
                    ])->first();
                    if ($check_exist) {
                        $this->participant_repository->updateOne([
                            '_id' => new ObjectID($check_exist->id)
                        ], ['Priority' => $priority]);
                    } else {
                        $participant = [
                            'ResourceId' => $app->child->id,
                            'Priority' => $priority,
                            'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED,
                            'GroupNumber' => 1,
                            'RegisteredDate' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000),
                            'RegisteredBy' => $app->current_user->id,
                            'ActivityId' => new ObjectID($activity_id),
                            'EnrollmentGroupId' => $data['enroll_group_id']
                        ];
                        $this->participant_repository->create($participant);
                    }
                }
                $priority++;
            }
            return ['result' => true];
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    /**
     * Maximum number of courses a student can be enrolled in the random registration
     * @param $group
     * @param $user
     * @return bool
     */
    protected function validationMaximumCourseRegistration($group, $user)
    {
        $participants = $this->participant_repository->count([
           'ResourceId' => $user,
            'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED,
            'EnrollmentGroupId' => $group->id
        ]);
        if ($group && (int)$group->maximum_course_random_registration > $participants) {
            return true;
        }
        return false;
    }
}