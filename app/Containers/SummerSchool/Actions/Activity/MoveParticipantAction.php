<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;

class MoveParticipantAction extends Action
{
    private $activity_repository;
    private $school_repository;
    private $user_repository;
    private $subgroup_repository;
    private $group_repository;
    private $participant_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        ParticipantRepositoryInterface $participant_repository,
        UserRepositoryInterface $user_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository
    ) {
        $this->activity_repository = $activity_repository;
        $this->user_repository = $user_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->school_repository = $school_repository;
        $this->participant_repository = $participant_repository;
    }


    public function run($user_id, $old_activity_id , $new_activity_id)
    {
        try {
            $old_activity = $this->getActivity($old_activity_id);
            if($old_activity == null)
                return ['result' => false];

            $new_activity = $this->getActivity($new_activity_id);
            if($new_activity == null)
                return ['result' => false];

            $existed = $new_activity->checkRegisteredActivityOfParticipant($user_id, [$old_activity->id]);
            if($existed)
                return ['result' => false];

            $user = $this->user_repository->find($user_id);

            if($user->hasRole(Constant::ROLE_PUPIL,
                new RoleParam(['school_owner_id' => $new_activity->school_owner_id, 'current_year' => true]))) {

                $participant = $this->participant_repository->findWhere([
                    'ResourceId' => $user_id,
                    'ActivityId' => new ObjectID($old_activity_id)
                ])->first();

                $this->participant_repository->updateOne(
                    ['_id' => new ObjectID($participant->id)],
                    [
                        'ActivityId' => new ObjectID($new_activity->id),
                        'CourseId' => new ObjectID($new_activity->course_id)
                    ]
                );

                $new_participant = null;
                if($participant->status == SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY ||
                    $participant->status == SummerSchoolConstant::PARTICIPANT_STATUS_ALLOCATED) {

                    $next_participant_id = $old_activity->getNextWaitingParticipant();
                    if ($next_participant_id != null) {
                        $this->participant_repository->updateOne(
                            [
                                'ActivityId' => new ObjectID($old_activity->id),
                                'ResourceId' => $next_participant_id
                            ],
                            ['Status' => SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY]
                        );

                        $new_participant = $old_activity->getParticipantInformationByUserId($next_participant_id);

                        $old_activity->updateNumberOfConfirmOrWaiting(SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST);

                    }
                }

                $old_activity->updateNumberOfConfirmOrWaiting($participant->status);
                $new_activity->updateNumberOfConfirmOrWaiting($participant->status);

                return ['result' => true, 'participant' => $new_participant];
            }
            return ['result' => false];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function getActivity($activity_id) {

        $activity = $this->activity_repository
            ->find($activity_id);

        $app = \Session::get('app');
        $current_user = $app->current_user;

        if(!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }

        return $activity;
    }
}