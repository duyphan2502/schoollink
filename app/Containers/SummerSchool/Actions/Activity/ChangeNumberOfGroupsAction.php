<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Tasks\Audit\AddGeneralAuditLogTask;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

class ChangeNumberOfGroupsAction extends Action
{
    private $activity_repository;
    private $audit_task;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        AddGeneralAuditLogTask $audit_task
    ) {
        $this->activity_repository = $activity_repository;
        $this->audit_task = $audit_task;
    }


    public function run($id, $number_of_group)
    {
        try {
            $activity = $this->getActivitiy($id);
            if($activity == null)
                return ['result' => false];

            $this->activity_repository->update(['NumberOfGroups' => $number_of_group], $activity->id);
            $this->audit_task->run('Change number of group to '.$number_of_group);
            return ['result' => true];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $id
     * @return mixed|null
     */
    protected function getActivitiy($id)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;

        $activity = $this->activity_repository->find($id);

        if(!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }
        return $activity;
    }
}