<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\SummerSchool\Actions\Activity;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\UTCDatetime;

class DeleteActivityAction extends Action
{
    private $activity_repository;
    private $enrollment_group_repository;
    private $school_repository;

    /**
     * AddActivityAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        EnrollmentGroupRepositoryInterface $enrollment_group_repository,
        SchoolRepositoryInterface $school_repository
    )
    {
        $this->activity_repository = $activity_repository;
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->school_repository = $school_repository;
    }


    public function run($data)
    {
        try {
            $result = $this->deleteActivity($data['id']);
            return ['result' => $result];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function deleteActivity($id)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;

        $activity = $this->activity_repository->find($id);

        if (!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }

        $updatedData = [
            'DeletedBy' => $current_user->id,
            'DateDeleted' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000)
        ];

        $result = $this->activity_repository->update($updatedData, $id);

        return $result;
    }
}