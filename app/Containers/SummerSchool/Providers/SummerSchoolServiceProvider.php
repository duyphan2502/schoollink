<?php

namespace App\Containers\SummerSchool\Providers;

#register_repository_contract
use App\Containers\SummerSchool\Contracts\CourseTypeRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CustomFieldRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CustomFieldValueRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;

#register_repository_eloquent
use App\Containers\SummerSchool\Contracts\NewsFeedRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Containers\SummerSchool\Contracts\PostRepositoryInterface;
use App\Containers\SummerSchool\Repositories\Eloquent\CourseTypeRepository;
use App\Containers\SummerSchool\Repositories\Eloquent\CustomFieldRepository;
use App\Containers\SummerSchool\Repositories\Eloquent\CustomFieldValueRepository;
use App\Containers\SummerSchool\Repositories\Eloquent\EnrollmentGroupRepository;
use App\Containers\SummerSchool\Repositories\Eloquent\ActivityRepository;
use App\Containers\SummerSchool\Repositories\Eloquent\CourseRepository;
use App\Containers\SummerSchool\Repositories\Eloquent\NewsFeedRepository;
use App\Containers\SummerSchool\Repositories\Eloquent\ParticipantRepository;
use App\Containers\SummerSchool\Repositories\Eloquent\PostRepository;
use App\Core\Provider\Abstracts\ServiceProviderAbstract;

/**
 * Class SummerSchoolServiceProvider.
 *
 * The Main Service Provider of this Module.
 * Will be automatically registered in the framework after
 * adding the Module name to containers config file.
 *
 */
class SummerSchoolServiceProvider extends ServiceProviderAbstract
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Container internal Service Provides.
     *
     * @var array
     */
    private $containerServiceProviders = [
    ];

    /**
     * Perform post-registration booting of services.
     */
    public function boot()
    {
        $this->registerServiceProviders($this->containerServiceProviders);
    }

    /**
     * Register bindings in the container.
     */
    public function register()
    {
        #binding_repository_flag
        $this->app->bind(EnrollmentGroupRepositoryInterface::class, EnrollmentGroupRepository::class);
        $this->app->bind(ActivityRepositoryInterface::class, ActivityRepository::class);
        $this->app->bind(CourseRepositoryInterface::class, CourseRepository::class);
        $this->app->bind(CourseTypeRepositoryInterface::class, CourseTypeRepository::class);
        $this->app->bind(CustomFieldRepositoryInterface::class, CustomFieldRepository::class);
        $this->app->bind(CustomFieldValueRepositoryInterface::class, CustomFieldValueRepository::class);
        $this->app->bind(PostRepositoryInterface::class, PostRepository::class);
        $this->app->bind(ParticipantRepositoryInterface::class, ParticipantRepository::class);
    }
}
