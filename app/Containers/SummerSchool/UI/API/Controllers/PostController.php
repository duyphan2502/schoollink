<?php

namespace App\Containers\SummerSchool\UI\API\Controllers;


use App\Containers\SummerSchool\Actions\Post\GetAllPostsAction;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class PostController.
 *
 */
class PostController extends CoreApiController
{
    public function all(GetAllPostsAction $action)
    {
        $posts = $action->run();
        return $this->response->array($posts);
    }
}
