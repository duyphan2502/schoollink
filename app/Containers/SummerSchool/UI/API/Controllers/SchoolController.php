<?php

namespace App\Containers\SummerSchool\UI\API\Controllers;

use App\Containers\SummerSchool\Actions\School\GetSubgroupSchoolAction;
use App\Containers\SummerSchool\UI\API\Requests\School\GetSubgroupSchoolRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class SchoolController.
 *
 */
class SchoolController extends CoreApiController
{
    public function getSubgroupSchool(GetSubgroupSchoolAction $action, GetSubgroupSchoolRequest $request)
    {
        $school = $action->run($request->id);
        return $this->response->array($school);
    }
}
