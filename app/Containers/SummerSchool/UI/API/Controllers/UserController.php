<?php

namespace App\Containers\SummerSchool\UI\API\Controllers;

use App\Containers\SummerSchool\Actions\User\FirstUpdateGuardianProfileAction;
use App\Containers\SummerSchool\Actions\User\GetPupilActivitiesAction;
use App\Containers\SummerSchool\Actions\User\AddPupilAction;
use App\Containers\SummerSchool\Actions\User\GetPupilsBySubgroupIdAction;
use App\Containers\SummerSchool\Actions\User\DeletePupilAction;
use App\Containers\SummerSchool\Actions\User\GetGuardianSummerProfileAction;
use App\Containers\SummerSchool\Actions\User\GetPermissionDeleteAction;
use App\Containers\SummerSchool\Actions\User\GetPupilSummerProfileAction;
use App\Containers\SummerSchool\Actions\User\UpdateGuardianProfileAction;
use App\Containers\SummerSchool\Actions\User\FirstUpdatePupilProfileAction;
use App\Containers\SummerSchool\Actions\User\UpdatePupilProfileAction;
use App\Containers\SummerSchool\UI\API\Requests\User\AddPupilRequest;
use App\Containers\SummerSchool\UI\API\Requests\User\FirstUpdateGuardianRequest;
use App\Containers\SummerSchool\UI\API\Requests\User\GetPupilsBySubgroupIdRequest;
use App\Containers\SummerSchool\UI\API\Requests\User\DeletePupilRequest;
use App\Containers\SummerSchool\UI\API\Requests\User\UpdateGuardianProfileRequest;
use App\Containers\SummerSchool\UI\API\Requests\User\FirstUpdatePupilRequest;
use App\Containers\SummerSchool\UI\API\Requests\User\UpdatePupilRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class UserController.
 *
 */
class UserController extends CoreApiController
{
    public function addPupil(AddPupilRequest $request, AddPupilAction $action)
    {
        $pupil = $action->run($request->all());
        return $this->response->array($pupil);
    }
    public function firstUpdatePupil(FirstUpdatePupilRequest $request, FirstUpdatePupilProfileAction $action)
    {
        $pupil = $action->run($request->all());
        return $this->response->array($pupil);
    }
    public function firstUpdateGuardian(FirstUpdateGuardianRequest $request, FirstUpdateGuardianProfileAction $action)
    {
        $guardian = $action->run($request->all());
        return $this->response->array($guardian);
    }
    public function updateSummerPupilProfile(UpdatePupilRequest $request, UpdatePupilProfileAction $action)
    {
        $pupil = $action->run($request->all());
        return $this->response->array($pupil);
    }
    public function getPermissionDelete(GetPermissionDeleteAction $action)
    {
        $pupil = $action->run();
        return $this->response->array($pupil);
    }
    public function deletePupil(DeletePupilAction $action, DeletePupilRequest $request)
    {
        $pupil = $action->run($request->id);
        return $this->response->array($pupil);
    }
    public function getGuardianSummerProfile(GetGuardianSummerProfileAction $action)
    {
        $profile = $action->run();
        return  $this->response->array($profile);
    }
    public function getPupilSummerProfile(GetPupilSummerProfileAction $action)
    {
        $profile = $action->run();
        return  $this->response->array($profile);
    }

    public function getPupilBySubgroupId(GetPupilsBySubgroupIdRequest $request, GetPupilsBySubgroupIdAction $action){
        $pupils = $action->run($request->subgroup_id);
        return $this->response->array($pupils);
    }
    public function updateGuardianProfile(UpdateGuardianProfileRequest $request, UpdateGuardianProfileAction $action){
        $guardian = $action->run($request->all());
        return $this->response->array($guardian);
    }
    public function getPupilActivities(GetPupilActivitiesAction $action)
    {
        $profile = $action->run();
        return  $this->response->array($profile);
    }

}
