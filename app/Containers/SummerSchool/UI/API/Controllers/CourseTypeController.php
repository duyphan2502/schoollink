<?php

namespace App\Containers\SummerSchool\UI\API\Controllers;


use App\Containers\SummerSchool\Actions\CourseType\GetCourseTypesAction;
use App\Containers\SummerSchool\Actions\CourseType\GetSubjectsAction;
use App\Containers\SummerSchool\UI\API\Requests\CourseType\GetSubjectsRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class CourseTypeController.
 *
 */
class CourseTypeController extends CoreApiController
{
    public function getCourseTypes(GetCourseTypesAction $action)
    {
        $course_types = $action->run();
        return  $this->response->array($course_types);
    }

    public function getSubjects(GetSubjectsRequest $request, GetSubjectsAction $action) {
        $result = $action->run($request->all());
        return  $this->response->array($result);
    }
}
