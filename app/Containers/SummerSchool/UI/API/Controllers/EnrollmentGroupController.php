<?php

namespace App\Containers\SummerSchool\UI\API\Controllers;


use App\Containers\SummerSchool\Actions\EnrollmentGroup\AddEnrollmentGroupAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\ArchiveEnrollmentGroupAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\DeleteEnrollmentGroupAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\GetEnrollmentGroupAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\GetEnrollmentGroupsByCourseTypeIdAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\GetHeaderGroupsAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\GetSimulateDistributionAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\RestoreEnrollmentGroupAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\SaveNoteForActivityOfEnrollmentGroupAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\SimulateDistributionAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\ToggleActivityOfEnrollmentGroupAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\UpdateEnrollmentGroupAction;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\AddEnrollmentGroupRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\ArchiveEnrollmentGroupRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\ConfirmEnrollmentGroupRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\DeleteEnrollmentGroupRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\GetEnrollmentGroupRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\GetEnrollmentGroupsByCourseTypeIdRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\GetGroupsRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\GetSimulateDistributionRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\RestoreEnrollmentGroupRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\SaveNoteForActivityOfEnrollmentGroupRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\SimulateDistributionRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\ToggleActivityOfEnrollmentGroupRequest;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\UpdateEnrollmentGroupRequest;
use App\Core\Controller\Abstracts\CoreApiController;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\GetAllAction;
use App\Containers\SummerSchool\Actions\EnrollmentGroup\GetEnrollmentGroupsAction;

/**
 * Class EnrollmentGroupController.
 *
 */
class EnrollmentGroupController extends CoreApiController
{
    public function getGroups(GetGroupsRequest $request, GetAllAction $action)
    {
    	$groups = $action->run($request);
    	return  $this->response->array($groups);
    }

    public function getBy(GetEnrollmentGroupRequest $request, GetEnrollmentGroupAction $action)
    {
        $group = $action->run($request->id, $request->school_id);
        return  $this->response->array($group);
    }

    public function getByCourseTypeId(GetEnrollmentGroupsByCourseTypeIdRequest $request, GetEnrollmentGroupsByCourseTypeIdAction $action)
    {
        $groups = $action->run($request);
        return  $this->response->array($groups);
    }

    public function add(AddEnrollmentGroupRequest $request, AddEnrollmentGroupAction $action)
    {
        $group = $action->run($request->role, $request->type);
        return  $this->response->array($group);
    }

    public function update(UpdateEnrollmentGroupRequest $request, UpdateEnrollmentGroupAction $action)
    {
        $group = $action->run($request->all());
        return  $this->response->array($group);
    }

    public function saveNote(SaveNoteForActivityOfEnrollmentGroupRequest $request, SaveNoteForActivityOfEnrollmentGroupAction $action)
    {
        $note = $action->run($request->id, $request->note);
        return  $this->response->array($note);
    }

    public function toggleActivity(ToggleActivityOfEnrollmentGroupRequest $request, ToggleActivityOfEnrollmentGroupAction $action)
    {
        $toggle = $action->run($request->id);
        return  $this->response->array($toggle);
    }

    public function simulateDistribution(SimulateDistributionRequest $request, SimulateDistributionAction $action)
    {
        $toggle = $action->run($request->id, $request->school_id);
        return  $this->response->array($toggle);
    }

    public function getSimulateDistribution(GetSimulateDistributionRequest $request, GetSimulateDistributionAction $action)
    {
        $toggle = $action->run($request->id, $request->school_id);
        return  $this->response->array($toggle);
    }

    public function confirm(ConfirmEnrollmentGroupRequest $request, SimulateDistributionAction $action)
    {
        $active = $action->run($request->id, $request->school_id, true);
        return  $this->response->array($active);
    }

    public function delete(DeleteEnrollmentGroupRequest $request, DeleteEnrollmentGroupAction $action)
    {
        $result = $action->run($request->id, $request->school_id);
        return  $this->response->array($result);
    }

    public function archive(ArchiveEnrollmentGroupRequest $request, ArchiveEnrollmentGroupAction $action)
    {
        $result = $action->run($request->id, $request->school_id);
        return  $this->response->array($result);
    }

    public function restore(RestoreEnrollmentGroupRequest $request, RestoreEnrollmentGroupAction $action)
    {
        $result = $action->run($request->id, $request->school_id);
        return  $this->response->array($result);
    }

	/**
     * get groups on parent portal
     */
    public function getEnrollGroups(GetEnrollmentGroupsAction $action)
    {
        $groups = $action->run();
        return  $this->response->array($groups);
    }
    public function getHeaderGroups(GetHeaderGroupsAction $action)
    {
        $enroll_groups = $action->run();
        return  $this->response->array($enroll_groups);
    }
}
