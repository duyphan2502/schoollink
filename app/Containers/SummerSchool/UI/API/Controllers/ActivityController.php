<?php

namespace App\Containers\SummerSchool\UI\API\Controllers;


use App\Containers\SummerSchool\Actions\Activity\AddActivityAction;
use App\Containers\SummerSchool\Actions\Activity\AddParticipantAction;
use App\Containers\SummerSchool\Actions\Activity\BookingActivityAction;
use App\Containers\SummerSchool\Actions\Activity\ChangeGroupNumberOfParticipantAction;
use App\Containers\SummerSchool\Actions\Activity\ChangeNumberOfGroupsAction;
use App\Containers\SummerSchool\Actions\Activity\ChangePositionOfWaitingListAction;
use App\Containers\SummerSchool\Actions\Activity\ConfirmParticipantAction;
use App\Containers\SummerSchool\Actions\Activity\CopyActivityAction;
use App\Containers\SummerSchool\Actions\Activity\DeleteActivityAction;
use App\Containers\SummerSchool\Actions\Activity\DirectBookingAction;
use App\Containers\SummerSchool\Actions\Activity\GetActivityAction;
use App\Containers\SummerSchool\Actions\Activity\GetAllAction;
use App\Containers\SummerSchool\Actions\Activity\GetAllByEnrollmentGroupIdAction;
use App\Containers\SummerSchool\Actions\Activity\GetAvailableActivitiesAction;
use App\Containers\SummerSchool\Actions\Activity\GetNumberOfConfirmedWaitingAction;
use App\Containers\SummerSchool\Actions\Activity\MoveParticipantAction;
use App\Containers\SummerSchool\Actions\Activity\RemoveDirectBookingAction;
use App\Containers\SummerSchool\Actions\Activity\RemoveParticipantAction;
use App\Containers\SummerSchool\Actions\Activity\UpdateActivityAction;
use App\Containers\SummerSchool\Actions\Activity\UpdateParticipantMaximumAction;
use App\Containers\SummerSchool\UI\API\Requests\Activity\AddActivityRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\AddPartcipantRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\BookingActivityRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\ChangeGroupNumberOfParticipantRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\ChangeNumberOfGroupsRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\ChangePositionOfWaitingListRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\ConfirmPartcipantRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\CopyActivityRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\DeleteActivityRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\DirectBookingRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\GetActivityRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\GetAllByEnrollmentGroupIdRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\MovePartcipantRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\RemoveDirectBookingRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\RemovePartcipantRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\UpdateActivityRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\UpdateParticipantMaximumRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class ActivityController.
 *
 */
class ActivityController extends CoreApiController
{
    public function add(AddActivityRequest $request, AddActivityAction $action)
    {
        $activityId = $action->run($request->enrollment_group_id, $request->course_id);
        return $this->response->array($activityId);
    }

    public function getBy(GetActivityRequest $request, GetActivityAction $action)
    {
        $activity = $action->run($request->id, $request->school_id);
        return $this->response->array($activity);
    }

    public function update(UpdateActivityRequest $request, UpdateActivityAction $action)
    {
        $activity = $action->run($request->all());
        return $this->response->array($activity);
    }

    public function copy(CopyActivityRequest $request, CopyActivityAction $action)
    {
        $activity = $action->run($request->all());
        return $this->response->array($activity);
    }

    public function delete(DeleteActivityRequest $request, DeleteActivityAction $action)
    {
        $result = $action->run($request->all());
        return $this->response->array($result);
    }

    public function getActivities(GetAllAction $action)
    {
        $activities = $action->run();
        return $this->response->array($activities);
    }

    public function getActivitiesByEnrollmentGroupId(GetAllByEnrollmentGroupIdRequest $request, GetAllByEnrollmentGroupIdAction $action)
    {
        $activities = $action->run($request->enrollment_group_id);
        return $this->response->array($activities);
    }

    public function getAvailableActivities(GetAvailableActivitiesAction $action)
    {
        $groups = $action->run();
        return $this->response->array($groups);
    }

    public function updateParticipantMaximum(UpdateParticipantMaximumRequest $request, UpdateParticipantMaximumAction $action)
    {
        $result = $action->run($request->id, $request->maximum);
        return $this->response->array($result);
    }

    public function changeGroupNumberOfParticipant(ChangeGroupNumberOfParticipantRequest $request, ChangeGroupNumberOfParticipantAction $action)
    {
        $result = $action->run($request->id, $request->participants);
        return $this->response->array($result);
    }

    public function changeNumberOfGroups(ChangeNumberOfGroupsRequest $request, ChangeNumberOfGroupsAction $action)
    {
        $result = $action->run($request->id, $request->number_of_groups);
        return $this->response->array($result);
    }

    public function addParticipant(AddPartcipantRequest $request, AddParticipantAction $action)
    {
        $result = $action->run($request->id, $request->user_id);
        return $this->response->array($result);
    }

    public function confirmParticipant(ConfirmPartcipantRequest $request, ConfirmParticipantAction $action)
    {
        $result = $action->run($request->id, $request->user_id);
        return $this->response->array($result);
    }

    public function removeParticipant(RemovePartcipantRequest $request, RemoveParticipantAction $action)
    {
        $result = $action->run($request->id, $request->user_id);
        return $this->response->array($result);
    }

    public function moveParticipant(MovePartcipantRequest $request, MoveParticipantAction $action)
    {
        $result = $action->run($request->user_id, $request->old_activity_id, $request->new_activity_id);
        return $this->response->array($result);
    }

    public function changePositionOfWaitingList(ChangePositionOfWaitingListRequest $request, ChangePositionOfWaitingListAction $action)
    {
        $result = $action->run($request->id, $request->user_ids);
        return $this->response->array($result);
    }

    public function bookingActivity(BookingActivityRequest $request, BookingActivityAction $action)
    {
        $activity = $action->run($request->all());
        return  $this->response->array($activity);
    }
    public function directBooking(DirectBookingRequest $request, DirectBookingAction $action)
    {
        $activity = $action->run($request->all());
        return  $this->response->array($activity);
    }
    public function removeDirectBooking(RemoveDirectBookingRequest $request, RemoveDirectBookingAction $action)
    {
        $activity = $action->run($request->all());
        return  $this->response->array($activity);
    }
    public function getNumberOfConfirmedWaiting(GetNumberOfConfirmedWaitingAction $action)
    {
        $activities = $action->run();
        return $this->response->array($activities);
    }
}
