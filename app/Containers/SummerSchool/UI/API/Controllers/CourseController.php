<?php

namespace App\Containers\SummerSchool\UI\API\Controllers;

use App\Containers\SummerSchool\Actions\Course\AddCourseAction;
use App\Containers\SummerSchool\Actions\Course\DeleteCourseAction;
use App\Containers\SummerSchool\Actions\Course\GetAllAction;
use App\Containers\SummerSchool\Actions\Course\GetCourseAction;
use App\Containers\SummerSchool\Actions\Course\GetCoursesByCourseTypeIdAction;
use App\Containers\SummerSchool\Actions\Course\UpdateCourseAction;
use App\Containers\SummerSchool\Actions\Course\RestoreCourseAction;
use App\Containers\SummerSchool\UI\API\Requests\Course\AddCourseRequest;
use App\Containers\SummerSchool\UI\API\Requests\Course\DeleteCourseRequest;
use App\Containers\SummerSchool\UI\API\Requests\Course\GetAllRequest;
use App\Containers\SummerSchool\UI\API\Requests\Course\GetCourseRequest;
use App\Containers\SummerSchool\UI\API\Requests\Course\GetCoursesByCourseTypeIdRequest;
use App\Containers\SummerSchool\UI\API\Requests\Course\RestoreCourseRequest;
use App\Containers\SummerSchool\UI\API\Requests\Course\UpdateCourseRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class CourseController.
 *
 */
class CourseController extends CoreApiController
{
	public function add(AddCourseRequest $request, AddCourseAction $action)
    {
        $course_id = $action->run($request->all());
        return  $this->response->array($course_id);
    }

    public function delete(DeleteCourseRequest $request, DeleteCourseAction $action)
    {
        $result = $action->run($request->all());
        return  $this->response->array($result);
    }

    public function update(UpdateCourseRequest $request, UpdateCourseAction $action)
    {
        $course_id = $action->run($request->all());
        return  $this->response->array($course_id);
    }

    public function getBy(GetCourseRequest $request, GetCourseAction $action) 
    {
    	$course = $action->run($request->all());
        return  $this->response->array($course);
    }

    public function getByCourseTypeId(GetCoursesByCourseTypeIdRequest $request, GetCoursesByCourseTypeIdAction $action)
    {
        $courses = $action->run($request->id);
        return  $this->response->array($courses);
    }

	public function all(GetAllRequest $request, GetAllAction $action) {
		$courses =  $action->run($request->all());
		return $this->response->array($courses); 
	}

    public function restore(RestoreCourseRequest $request, RestoreCourseAction $action) {
        $courses =  $action->run($request->id, $request->school_id);
        return $this->response->array($courses);
    }
}
