<?php

namespace App\Containers\SummerSchool\UI\API\Requests\CourseType;

use App\Core\Request\Abstracts\Request;

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 30/09/2016
 * Time: 11:35 AM
 */
class GetSubjectsRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
            'course_type_id.required' => trans('message.message_id_validate_required')
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}