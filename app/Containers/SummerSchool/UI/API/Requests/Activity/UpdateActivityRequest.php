<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 20/09/2016
 * Time: 11:54 AM
 */

namespace App\Containers\SummerSchool\UI\API\Requests\Activity;

use App\Core\Request\Abstracts\Request;

class UpdateActivityRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'activity_id' => 'required',
            'activity' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'activity_id.required' => trans('message.message_id_validate_required'),
            'activity.required' => trans('message.message_id_validate_required')
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}