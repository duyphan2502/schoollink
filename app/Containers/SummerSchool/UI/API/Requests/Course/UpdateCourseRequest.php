<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 20/09/2016
 * Time: 11:54 AM
 */

namespace App\Containers\SummerSchool\UI\API\Requests\Course;

use App\Core\Request\Abstracts\Request;

class UpdateCourseRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_id' => 'required',
            'course' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'course_id.required' => trans('message.message_id_validate_required'),
            'course.required' => trans('message.message_id_validate_required')
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}