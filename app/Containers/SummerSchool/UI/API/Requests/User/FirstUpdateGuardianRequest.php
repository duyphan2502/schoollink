<?php

namespace App\Containers\SummerSchool\UI\API\Requests\User;

use App\Core\Request\Abstracts\Request;

class FirstUpdateGuardianRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'email' => 'email|unique:users,email,'.$this->id,
            'firstname' => 'required',
            'surname' => 'required',
            'address' => 'required',
            'postal_code' => 'required',
            'postal_name' => 'required',
            'mobilephone' => 'required|max:25'
        ];
    }

    public function messages()
    {
        return [];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}