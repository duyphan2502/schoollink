<?php

$router->get('activity/getActivities', ['uses' => 'ActivityController@getActivities']);
$router->get('activity/getActivitiesByEnrollmentGroupId', ['uses' => 'ActivityController@getActivitiesByEnrollmentGroupId']);
$router->get('activity/getBy', ['uses' => 'ActivityController@getBy']);
$router->post('activity/add', [ 'uses' => 'ActivityController@add']);
$router->post('activity/update', [ 'uses' => 'ActivityController@update']);
$router->post('activity/clone', [ 'uses' => 'ActivityController@copy']);
$router->post('activity/delete', [ 'uses' => 'ActivityController@delete']);
$router->post('activity/updateParticipantMaximum', [ 'uses' => 'ActivityController@updateParticipantMaximum']);
$router->post('activity/changeGroupNumberOfParticipant', [ 'uses' => 'ActivityController@changeGroupNumberOfParticipant']);
$router->post('activity/changeNumberOfGroups', [ 'uses' => 'ActivityController@changeNumberOfGroups']);
$router->post('activity/addParticipant', [ 'uses' => 'ActivityController@addParticipant']);
$router->post('activity/confirmParticipant', [ 'uses' => 'ActivityController@confirmParticipant']);
$router->post('activity/removeParticipant', [ 'uses' => 'ActivityController@removeParticipant']);
$router->post('activity/moveParticipant', [ 'uses' => 'ActivityController@moveParticipant']);
$router->post('activity/changePositionOfWaitingList', [ 'uses' => 'ActivityController@changePositionOfWaitingList']);
$router->put('activity/bookingActivity', [ 'uses' => 'ActivityController@bookingActivity']);
$router->put('activity/directBooking', [ 'uses' => 'ActivityController@directBooking']);
$router->put('activity/removeDirectBooking', [ 'uses' => 'ActivityController@removeDirectBooking']);
$router->get('activity/getNumberOfConfirmedWaiting', ['uses' => 'ActivityController@getNumberOfConfirmedWaiting']);
