<?php
$router->get('courseType/getCourseTypes', ['uses' => 'CourseTypeController@getCourseTypes']);
$router->get('courseType/getSubjects', ['uses' => 'CourseTypeController@getSubjects']);