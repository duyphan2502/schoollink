<?php
$router->post('user/addPupil', [ 'uses' => 'UserController@addPupil']);
$router->put('user/firstUpdatePupil', [ 'uses' => 'UserController@firstUpdatePupil']);
$router->put('user/updateSummerPupilProfile', [ 'uses' => 'UserController@updateSummerPupilProfile']);
$router->get('user/getPupilBySubgroupId', [ 'uses' => 'UserController@getPupilBySubgroupId']);
$router->get('user/getPermissionDelete', [ 'uses' => 'UserController@getPermissionDelete']);
$router->post('user/deletePupil', [ 'uses' => 'UserController@deletePupil']);
$router->get('user/getGuardianSummerProfile', ['uses' => 'UserController@getGuardianSummerProfile']);
$router->get('user/getPupilSummerProfile', ['uses' => 'UserController@getPupilSummerProfile']);
$router->put('user/updateGuardianProfile', ['uses' => 'UserController@updateGuardianProfile']);
$router->get('user/getPupilActivities', ['uses' => 'UserController@getPupilActivities']);
$router->put('user/firstUpdateGuardian', [ 'uses' => 'UserController@firstUpdateGuardian']);