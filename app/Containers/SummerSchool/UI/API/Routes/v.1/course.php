<?php

$router->get('course/all', ['uses' => 'CourseController@all']);
$router->get('course/getBy', ['uses' => 'CourseController@getBy']);
$router->get('course/getByCourseTypeId', ['uses' => 'CourseController@getByCourseTypeId']);

$router->post('course/add', [ 'uses' => 'CourseController@add']);
$router->post('course/update', [ 'uses' => 'CourseController@update']);
$router->post('course/restore', [ 'uses' => 'CourseController@restore']);
$router->delete('course/delete', [ 'uses' => 'CourseController@delete']);