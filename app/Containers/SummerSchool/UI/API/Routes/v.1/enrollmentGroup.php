<?php

$router->get('enrollmentGroup/getGroups', [ 'uses' => 'EnrollmentGroupController@getGroups']);
$router->get('enrollmentGroup/getEnrollmentGroups', [ 'uses' => 'EnrollmentGroupController@getEnrollGroups']);
$router->get('enrollmentGroup/getBy', [ 'uses' => 'EnrollmentGroupController@getBy']);
$router->get('enrollmentGroup/getByCourseTypeId', [ 'uses' => 'EnrollmentGroupController@getByCourseTypeId']);
$router->post('enrollmentGroup/add', [ 'uses' => 'EnrollmentGroupController@add']);
$router->post('enrollmentGroup/simulateDistribution', [ 'uses' => 'EnrollmentGroupController@simulateDistribution']);
$router->post('enrollmentGroup/getSimulateDistribution', [ 'uses' => 'EnrollmentGroupController@getSimulateDistribution']);
$router->post('enrollmentGroup/confirm', [ 'uses' => 'EnrollmentGroupController@confirm']);
$router->put('enrollmentGroup/update', [ 'uses' => 'EnrollmentGroupController@update']);
$router->put('enrollmentGroup/saveNote', [ 'uses' => 'EnrollmentGroupController@saveNote']);
$router->put('enrollmentGroup/toggleActivity', [ 'uses' => 'EnrollmentGroupController@toggleActivity']);
$router->put('enrollmentGroup/archive', [ 'uses' => 'EnrollmentGroupController@archive']);
$router->put('enrollmentGroup/restore', [ 'uses' => 'EnrollmentGroupController@restore']);
$router->delete('enrollmentGroup/delete', [ 'uses' => 'EnrollmentGroupController@delete']);
$router->get('enrollmentGroup/getHeaderGroups', [ 'uses' => 'EnrollmentGroupController@getHeaderGroups']);
