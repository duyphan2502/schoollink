<?php

$router->get('/summerschool', [
    'uses' => 'SummerSchoolController@index',
]);

$router->get('/summerschool/teacher', [
    'uses' => 'SummerSchoolController@teacher',
]);

