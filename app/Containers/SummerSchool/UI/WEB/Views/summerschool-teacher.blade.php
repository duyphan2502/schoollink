@include('js-localization::head', ['type' => 'm_summer_school'])
<!doctype html>
<html ng-app="app" ng-strict-di>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="/sl/{!! elixir('css/vendor.css') !!}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/sl/{!! elixir('css/components.css') !!}">
    <link rel="stylesheet" href="/sl/{!! elixir('css/app.summerschool.teacher.css') !!}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <title>Summerschool</title>

    @yield('js-localization.head')
</head>
<body>
<div ui-view="header"></div>
<div ui-view="main"></div>
<div ui-view="footer"></div>

<script src="http://maps.google.com/maps/api/js?key=AIzaSyBCCJuPoo5K6a6N1BKHEJdjABNAMgkVnRE"></script>
<script src="/sl/{!! elixir('js/vendor.js') !!}"></script>
<script src="/sl/{!! elixir('js/customJsLibs.js') !!}"></script>
<script src="/sl/{!! elixir('js/partials.js') !!}"></script>
<script src="/sl/{!! elixir('js/summerschool.teacher.app.js') !!}"></script>


{{--livereload--}}
@if ( env('APP_ENV') === 'local' )
    <script type="text/javascript">
        document.write('<script src="'+ location.protocol + '//' + (location.host.split(':')[0] || 'localhost') +':35729/livereload.js?snipver=1" type="text/javascript"><\/script>')
    </script>
@endif
<script>
    angular.module("app").constant("CSRF_TOKEN", '{{ csrf_token() }}');
</script>
</body>
</html>
