<?php

namespace App\Containers\SummerSchool\UI\WEB\Controllers;

use App\Containers\SchoolLink\Constants\Constant;
use App\Core\Controller\Abstracts\CoreWebController;
use Carbon\Carbon;

/**
 * Class SummerSchoolController
 *
 */
class SummerSchoolController extends CoreWebController
{

    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $app = \Session::get('app');
        if($app != null && (($app->user_role != null && $app->user_role->role_id != Constant::ROLE_GUARDIAN) || $app->user_role == null)) {
            \Session::put('app', null);
        }

        return view('summerschool-index');
    }

    public function teacher()
    {
        $app = \Session::get('app');
        if($app != null && (($app->user_role != null && $app->user_role->role_id == Constant::ROLE_GUARDIAN) || $app->user_role == null)) {
            \Session::put('app', null);
        }
        return view('summerschool-teacher');
    }
}
