<?php

namespace App\Containers\SummerSchool\BusinessModels;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Core\Model\Abstracts\BusinessModel;
use App\Core\Utils\SortUtil;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;


/**
 * Class Participant.
 *
 */
class Participant extends BusinessModel
{

}
