<?php

namespace App\Containers\SummerSchool\BusinessModels;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ParticipantRepositoryInterface;
use App\Core\Model\Abstracts\BusinessModel;
use App\Core\Utils\SortUtil;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;


/**
 * Class Activity.
 *
 */
class Activity extends BusinessModel
{
    public function permissionEdit($user)
    {
        switch ($this->owner->type) {
            case SummerSchoolConstant::TYPE_SCHOOL_SFO:
                if ($user->hasRole(Constant::ROLE_SFO, $this->owner->school_id))
                    return true;
                break;

            case SummerSchoolConstant::TYPE_SCHOOL_ADMIN:
                if ($user->hasRole(Constant::ROLE_SCHOOL_ADMIN, $this->owner->school_id))
                    return true;
                break;

            case SummerSchoolConstant::TYPE_TEACHER:
                if ($user->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $this->owner->school_id])))
                    return true;
                break;
        }
        return false;
    }

    public function getParticipantsInformation()
    {
        $user_repository = app()->make(UserRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $participant_repository = app()->make(ParticipantRepositoryInterface::class);

        $user_participants = [
            'waiting' => [],
            'confirmed' => []
        ];

        $participants = $participant_repository->findWhere([
            ['Status' , '!=' , SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED],
            'ActivityId' => new ObjectID($this->id)
        ]);

        if (count($participants) == 0)
            return $user_participants;

        $user_ids = $participants->pluck('resource_id')->toArray();

        $users = $user_repository->findWhereIn('id', $user_ids)->keyBy('id')->toArray();
        $subgroup_ids = array();
        foreach ($users as $user) {
            $user_role = $user->getRole(Constant::ROLE_PUPIL);
            if ($user_role != null && !in_array($user_role->subgroup_id, $subgroup_ids)) {
                $subgroup_ids[] = $user_role->subgroup_id;
            }
        }

        $group_ids = array();
        $subgroups = $subgroup_repository->findWhereIn('id', $subgroup_ids)->keyBy('id')->toArray();
        foreach ($subgroups as $subgroup) {
            if (!in_array($subgroup->group_id, $group_ids)) {
                $group_ids[] = $subgroup->group_id;
            }
        }
        $school_ids = array();
        $groups = $group_repository->findWhereIn('id', $group_ids)->keyBy('id')->toArray();
        foreach ($groups as $group) {
            if (!in_array($group->school_id, $school_ids)) {
                $school_ids[] = $group->school_id;
            }
        }

        $schools = $school_repository->findWhereIn('id', $school_ids)->keyBy('id')->toArray();

        foreach ($participants as $participant) {
            $user = $users[$participant->resource_id];
            $user_role = $user->getRole(Constant::ROLE_PUPIL);
            $subgroup = $user_role != null && isset($subgroups[$user_role->subgroup_id]) ? $subgroups[$user_role->subgroup_id] : null;
            $group = $subgroup != null && isset($groups[$subgroup->group_id]) ? $groups[$subgroup->group_id] : null;
            $school = $group != null && isset($schools[$group->school_id]) ? $schools[$group->school_id] : null;
            $user_participant = array(
                'id' => $participant->id,
                'user_id' => $user->id,
                'gender' => $user->gender,
                'name' => $user->firstname . ' ' . $user->surname,
                'image_id' => $user->image_id,
                'school' => $school != null ? $school->name : null,
                'group' => $group != null ? $group->name : null,
                'priority' => $participant->priority,
                'notes' => isset($participant->notes) ? $participant->notes : '',
                'group_number' => isset($participant->group_number) ? $participant->group_number : 1,
                'position' => isset($participant->position) ? $participant->position: null
            );

            if ($participant->status == SummerSchoolConstant::PARTICIPANT_STATUS_ALLOCATED ||
                $participant->status == SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY
            ) {

                $user_participants['confirmed'][] = $user_participant;
            } else {
                $user_participants['waiting'][] = $user_participant;
            }

        }

        SortUtil::AASort($user_participants['confirmed'], 'name');
        SortUtil::AASort($user_participants['confirmed'], 'school');

        SortUtil::AASort($user_participants['waiting'], 'name');
        SortUtil::AASort($user_participants['waiting'], 'school');
        return $user_participants;
    }

    public function getParticipantInformationByUserId($user_id)
    {
        $participant_repository = app()->make(ParticipantRepositoryInterface::class);

        $participant = $participant_repository->findWhere([
            ['Status' , '!=' , SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED],
            'ResourceId' => $user_id,
            'ActivityId' => new ObjectID($this->id)
        ])->first();

        $user_repository = app()->make(UserRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);
        $school_repository = app()->make(SchoolRepositoryInterface::class);


        $user = $user_repository->find($participant->resource_id);
        $user_role = $user->getRole(Constant::ROLE_PUPIL);

        $subgroup = $subgroup_repository->find($user_role->subgroup_id);
        $group = $group_repository->find($subgroup->group_id);
        $school = $school_repository->find($group->school_id);

        $user_participant = array(
            'id' => $participant->id,
            'user_id' => $user->id,
            'gender' => $user->gender,
            'name' => $user->firstname . ' ' . $user->surname,
            'image_id' => $user->image_id,
            'school' => $school != null ? $school->name : null,
            'group' => $group != null ? $group->name : null,
            'notes' => isset($participant->notes) ? $participant->notes : '',
            'status' => $participant->status,
            'group_number' => isset($participant->group_number) ? $participant->group_number : 1,
            'position' => isset($participant->position) ? $participant->position: null
        );

        return $user_participant;
    }

    public function getNextWaitingParticipant()
    {
        $user_id = null;
        $participant_repository = app()->make(ParticipantRepositoryInterface::class);

        $participants = $participant_repository->findWhere([
            ['Status' , '!=' , SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED],
            'ActivityId' => new ObjectID($this->id)
        ], ['ResourceId', 'Position', 'Status']);

        if ($participants != null) {
            $number_of_registered_participants = collect($participants)->filter(function ($participant) {
                return $participant->status == SummerSchoolConstant::PARTICIPANT_STATUS_ALLOCATED ||
                $participant->status == SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY;
            })->count();

            $maximum = $this->options->summer_school_participants_maximum;
            if (isset($this->options->summer_school_participants_limit)) {
                $maximum += $this->options->summer_school_participants_limit;
            }

            if ($number_of_registered_participants < $maximum) {
                $waiting_participants = collect($participants)->filter(function ($participant) {
                    return $participant->status == SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST;
                })->toArray();

                $keys = array_map(
                    function($participant){
                        return $participant->position;
                    }, $waiting_participants);

                array_multisort($keys, SORT_ASC, $waiting_participants);
                foreach ($waiting_participants as $participant) {
                    $existed = $this->checkRegisteredActivityOfParticipant($participant->resource_id);
                    if ($existed)
                        continue;

                    $user_id = $participant->resource_id;
                    break;
                }
            }
        }
        return $user_id;
    }

    public function checkRegisteredActivityOfParticipant($user_id, $exclude_activity_ids = null)
    {
        $activity_repository = app()->make(ActivityRepositoryInterface::class);
        $participant_repository = app()->make(ParticipantRepositoryInterface::class);

        $participant_activities = $participant_repository->findWhere([
            ['Status', '!=', SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED],
            ['Status', '!=', SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST],
            'ResourceId' => $user_id
        ], ['ActivityId'])->pluck('activity_id')->toArray();

        $registered_activities = $activity_repository->findWhere(
            [
                'SchoolOwnerId' => $this->school_owner_id,
                'SchoolId' => $this->school_id,
                'CourseTypeId' => new ObjectID($this->course_type_id),
                'DateDeleted' => null,
                ['_id', 'in', $participant_activities]
            ]);

        $result = false;
        foreach ($registered_activities as $activity) {
            if (($activity->date_start <= $this->date_start && $activity->date_end >= $this->date_start)
                || ($activity->date_start <= $this->date_end && $activity->date_end >= $this->date_end)
            ) {
                if($exclude_activity_ids != null){
                    if(in_array($activity->id, $exclude_activity_ids)) {
                        continue;
                    }
                }
                $result = true;
                break;
            }
        }

        return $result;
    }

    public function getNextPositionOfWaitingList(){
        $participant_repository = app()->make(ParticipantRepositoryInterface::class);

        $array_position = $participant_repository->findWhere([
            'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST,
            'ActivityId' => new ObjectID($this->id)
        ], ['Position'])->pluck('position')->toArray();

        if(empty($array_position)){
            return 1;
        }else {
            return max($array_position) + 1;
        }
    }
    public function getAvailableSpace(){
        if($this->options == NULL){
            return 0;
        }
        $maximum = $this->options->summer_school_participants_maximum;
        if (!empty($this->number_of_confirmed)) {
            return $maximum - $this->number_of_confirmed;
        }
        return $maximum;
    }

    public function getNumberOfConfirmed()
    {
        $participant_repository = app()->make(ParticipantRepositoryInterface::class);
        return $participant_repository->count([
            'ActivityId' => new ObjectID($this->id),
            ['Status' , '!=', SummerSchoolConstant::PARTICIPANT_STATUS_REGISTERED],
            ['Status' , '!=', SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST],
        ]);
    }

    public function getNumberOfWaitingList()
    {
        $participant_repository = app()->make(ParticipantRepositoryInterface::class);
        return $participant_repository->count([
            'ActivityId' => new ObjectID($this->id),
            'Status' => SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST
        ]);
    }

    public function updateNumberOfConfirmOrWaiting($status)
    {
        $activity_repository = app()->make(ActivityRepositoryInterface::class);

        if($status == SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST) {
            $number_of_waiting_list = $this->getNumberOfWaitingList();
            $activity_repository->updateOne([
                '_id' => new ObjectID($this->id)
            ], [
                'NumberOfWaitingList' => $number_of_waiting_list
            ]);
        } else {
            $number_of_confirmed = $this->getNumberOfConfirmed();
            $activity_repository->updateOne([
                '_id' => new ObjectID($this->id)
            ], [
                'NumberOfConfirmed' => $number_of_confirmed
            ]);
        }
    }

}