<?php

namespace App\Containers\SummerSchool\BusinessModels;
use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use App\Core\Model\Abstracts\BusinessModel;
use MongoDB\BSON\ObjectID;

/**
 * Class EnrollmentGroup.
 *
 */
class EnrollmentGroup extends BusinessModel
{
    public function permissionAccess($user)
    {
        switch ($this->owner->type) {
            case SummerSchoolConstant::TYPE_SCHOOL_SFO:
                if ($user->hasRole(Constant::ROLE_SFO, $this->owner->school_id))
                    return true;
                break;

            case SummerSchoolConstant::TYPE_SCHOOL_ADMIN:
                if ($user->hasRole(Constant::ROLE_SCHOOL_ADMIN, $this->owner->school_id))
                    return true;
                break;

            case SummerSchoolConstant::TYPE_TEACHER:
                if ($user->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $this->owner->school_id])))
                    return true;
                break;
        }
        return false;
    }

    public function permissionEdit($user)
    {
        switch ($this->owner->type) {
            case SummerSchoolConstant::TYPE_SCHOOL_SFO:
                if ($user->hasRole(Constant::ROLE_SFO, $this->owner->school_id))
                    return true;
                break;

            case SummerSchoolConstant::TYPE_SCHOOL_ADMIN:
                if ($user->hasRole(Constant::ROLE_SCHOOL_ADMIN, $this->owner->school_id))
                    return true;
                break;

            case SummerSchoolConstant::TYPE_TEACHER:
                if ($user->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $this->owner->school_id])))
                    return true;
                break;
        }
        return false;
    }

}
