<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 01/09/2016
 * Time: 2:32 PM
 */

namespace App\Containers\SchoolLink\Criterias\Session;


use App\Core\Criterias\Abstracts\Criteria;
use Carbon\Carbon;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetActiveSessionCriteria extends Criteria
{
    private $key;

    public function __construct($key)
    {
        $this->key = $key;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        //TODO: refactor code when SL fix issue expire time
        return $model->where('key', $this->key)
            ->where('date_replaced', NULL)
            //->where('expire', '>' , Carbon::now())
            ->orderBy('id', 'desc');
    }
}