<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 01/09/2016
 * Time: 2:32 PM
 */

namespace App\Containers\SchoolLink\Criterias\Consent;


use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetConsentsBySchoolInfoCriteria extends Criteria
{
    private $school_id;
    private $school_owner_id;

    public function __construct($school_owner_id, $school_id)
    {
        $this->school_id = $school_id;
        $this->school_owner_id = $school_owner_id;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        $school_id = $this->school_id;
        $school_owner_id = $this->school_owner_id;

        return $model
            ->whereNULL('date_replaced')
            ->where(function($query) use($school_owner_id, $school_id) {
                $query->where('sub_school_owner', $school_owner_id)
                    ->orwhere('sub_school', $school_id);
            })
            ->orderBy('id', 'desc');
    }
}