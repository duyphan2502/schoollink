<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 01/09/2016
 * Time: 2:32 PM
 */

namespace App\Containers\SchoolLink\Criterias\Form;


use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetFormsBySchoolIdCriteria extends Criteria
{
    private $school_id;
    private $user_id;

    public function __construct($user_id, $school_id)
    {
        $this->school_id = $school_id;
        $this->user_id = $user_id;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        $user_id = $this->user_id;

        return $model
            ->where('school_id', $this->school_id)
            ->where('deleted', 0)
            ->where(function($query) use($user_id) {
                $query->where('created_by', $user_id)
                    ->orwhere('share', 1);
            })
            ->orderBy('created_date');
    }
}