<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 24/08/2016
 * Time: 6:00 PM
 */

namespace App\Containers\SchoolLink\Criterias\Sickness;

use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetSicknessesByUserIdCriteria extends Criteria
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('user_id', $this->id)->whereNull('date_deleted');
    }
}