<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 24/08/2016
 * Time: 6:00 PM
 */

namespace App\Containers\SchoolLink\Criterias\User;

use App\Containers\SchoolLink\Constants\Constant;
use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetPupilIdsOfGroupsCriteria extends Criteria
{
    private $subgroup_ids;

    public function __construct($subgroup_ids)
    {
        $this->subgroup_ids = $subgroup_ids;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->whereIn('subgroup_id', $this->subgroup_ids)->where('role_id', Constant::ROLE_PUPIL);
    }
}