<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 24/08/2016
 * Time: 6:00 PM
 */

namespace App\Containers\SchoolLink\Criterias\User;

use App\Containers\SchoolLink\Constants\Constant;
use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class SearchParticipantsCriteria extends Criteria
{
    private $name;
    private $owner_id;
    private $pagination;

    public function __construct($owner_id, $name, $pagination = 10)
    {
        $this->name = $name;
        $this->owner_id = $owner_id;
        $this->pagination = $pagination;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        $name = $this->name;
        return $model->join('user_roles', 'users.id', '=', 'user_roles.user_id')
            ->join('subgroups', 'subgroups.id', '=', 'user_roles.subgroup_id')
            ->join('groups', 'groups.id', '=', 'subgroups.group_id')
            ->join('schools', 'schools.id', '=', 'groups.school_id')
            ->where('role_id', Constant::ROLE_PUPIL)
            ->where('owner_id', $this->owner_id)
            ->where(function($query) use($name) {
                $query->where('firstname', 'like', '%'.$name.'%')
                        ->orWhere('surname', 'like', '%'.$name.'%');
            })->select(['firstname','surname', 'users.image_id', 'users.id', 'subgroups.name as subgroup', 'groups.name as group', 'schools.name as school'])
            //->orderBy('firstname')
            ->take($this->pagination);
    }
}