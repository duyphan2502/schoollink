<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 01/09/2016
 * Time: 2:32 PM
 */

namespace App\Containers\SchoolLink\Criterias\UserEmail;


use App\Containers\SchoolLink\Constants\Constant;
use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class GetUserEmailByUserIdCriteria extends Criteria
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('user_id', $this->id)
            ->whereNULL('date_deleted')
            ->where('type', '!=', Constant::USER_EMAIL_SOURCE_APPLICATION)
            ->orderBy('id', 'desc');
    }
}