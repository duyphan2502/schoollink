<?php
/**
 * Created by PhpStorm.
 * User: An Huynh
 * Date: 15/09/2016
 * Time: 11:35 AM
 */

namespace App\Containers\SchoolLink\Criterias\PickupRules;


use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;
use Carbon\Carbon;

class GetActivePickupRulesCriteria extends Criteria
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('user_id', $this->id)
            ->where('type_id', 'M')
            ->whereNULL('date_deleted')
            ->where('date_end', '>=', Carbon::now())
            ->orderBy('id', 'desc');
    }
}