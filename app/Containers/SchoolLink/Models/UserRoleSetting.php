<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class UserRoleSetting.
 *
 */
class UserRoleSetting extends Model
{
    protected $table = 'user_role_settings';

    protected $fillable = ['role_id', 'key', 'value', 'date_expire'];
}
