<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class LoginType.
 *
 */
class LoginType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'login_types';

    protected $fillable = ['name', 'name_lower', 'school_id', 'default', 'teacher_option', 'teacher_default', 
                    'sort', 'icon', 'color_code', 'color_class', 'collect_rule_show', 'collect_rule_cross',
                    'activity_student_show', 'activity_logged_in', 'guardian_email_notice','date_deleted',
                    'collect_message_require_confirmation_logout', 'hour_count', 'morning_hours',
                    'forgot_notice', 'guardian_portal_show', 'visible_on_bulk_change_page'];

	protected $dates = ['date_deleted'];
}
