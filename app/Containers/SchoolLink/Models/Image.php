<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class Image.
 *
 */
class Image extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';

    protected $fillable = ['id', 'user_id', 'date_created', 'rotate'];

    protected $dates = ['date_created'];
}
