<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class MessageTemplate.
 *
 */
class MessageTemplate extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'messages_v2_templates';

    protected $fillable = ['message_template', 'name', 'body', 'sub_school', 'sub_school_owner', 'sub_user',
        'for_school_owners', 'for_school_administration', 'for_school_sfo', 'for_contact_teachers', 'deleted', 'updater',
        'date', 'date_replaced'];

	protected $dates = ['date', 'date_replaced'];
}
