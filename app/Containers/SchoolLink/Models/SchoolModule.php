<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class SchoolModule.
 *
 */
class SchoolModule extends Model 
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'school_modules';

    protected $primary_key = null;

    protected $fillable = ['school_id', 'module_id'];
}
