<?php
namespace App\Containers\SchoolLink\Models;
 
class RoleParam
{
 	function __construct($args)
 	{
 		$args += [
 			'school_owner_id' => null,
 			'school_id' => null,
 			'group_id' => null,
 			'subgroup_id' => null,
 			'school_type' => null,
 			'permission' => null,
 			'module' => null,
 			'school_sfo_subscription' => null,
 			'academic_year_id' => null,
 			'active' => null,
			'current_year' => null
 		];

 		extract($args);

 		$this->school_owner_id 	= $args['school_owner_id'];
 		$this->school_id 		= $args['school_id'];
 		$this->group_id 		= $args['group_id'];
 		$this->subgroup_id 		= $args['subgroup_id'];
 		$this->school_type 		= $args['school_type'];
 		$this->permission 		= $args['permission'];
 		$this->module 			= $args['module'];
 		$this->school_sfo_subscription = $args['school_sfo_subscription'];
 		$this->academic_year_id = $args['academic_year_id'];
		$this->active 			= $args['active'];
		$this->current_year 	= $args['current_year'];
 	}
}