<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\MongoModel as Model;

/**
 * Class Remark.
 *
 */
class Remark extends Model
{

    protected $collection = 'Remarks';

    protected $fillable = ['Remarks', 'AcademicYearId', 'Student', 'RoleId', 'SubgroupName'];

	protected $dates = [];
}
