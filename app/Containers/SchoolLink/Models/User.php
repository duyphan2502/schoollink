<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = ['firstname', 'surname', 'gender', 'birthdate', 'birthnumber', 'email', 'email_staff', 'source_email', 
    						'email_bounce', 'image_id', 'mobilephone', 'source_mobilephone', 'telephone', 'source_telephone', 
    						'address', 'postal_code', 'postal_name', 'password', 'password_restore', 'password_blocked_bruteforce', 
    						'date_created', 'last_login_date', 'user_role_id', 'language', 'temp_lang', 'sickness_confirmed', 
    						'sickness_confirmed_updater', 'sickness_confirmed_date', 'guardianportal_classlist_show',
    						'guardianportal_access_blocked', 'notification_settings', 'message_delivery_need_sms', 
    						'adfs_id', 'setting_student_overview_view', 'setting_chat_widget', 'ShardSchoolId'];

	protected $dates = ['sickness_confirmed_date'];
}
