<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class SchoolOwner.
 *
 */
class SchoolOwner extends Model
{
    protected $table = 'school_owners';

    protected $fillable = ['name', 'description', 'phone', 'image_id', 'email', 'sfo_name', 'sfo_name_abbreviation', 'setting_login_methods', 
    					'setting_include_system_sicknesses', 'setting_messages_sms_country_code', 'setting_messages_sms_accepted_lengths', 
    					'date_deleted', 'web_protocol', 'web_domain', 'customer_type', 'brand', 'web_design', 'web_allow_password_reset', 
    					'default_language_Code', 'message_sms_sender', 'sms_notification_template', 'message_email_copy_theme'];

	protected $dates = ['date_deleted'];
}