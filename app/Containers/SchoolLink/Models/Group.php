<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class Group.
 *
 */
class Group extends Model 
{
    protected $table = 'groups';

    protected $fillable = ['name', 'school_id', 'date_deleted', 'academic_year_id', 'ims_id'];
}
