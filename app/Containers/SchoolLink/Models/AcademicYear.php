<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class AcademicYear.
 *
 */
class AcademicYear extends Model 
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'academic_years';

    protected $fillable = ['name', 'school_owner_id', 'date_start', 'date_end', 'date_deleted'];

	protected $dates = ['date_start', 'date_end', 'date_deleted'];
}
