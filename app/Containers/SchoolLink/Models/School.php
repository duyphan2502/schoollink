<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class School.
 *
 */
class School extends Model 
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'schools';

    protected $fillable = ['phone', 'description', 'yr', 'image_id', 'name', 'name_sfo', 'email', 
    						'owner_id', 'type_id', 'sms_sender', 'date_deleted', 'ims_id', 'location',
                            'location_for_dks', 'location_for_sommerskolen', 'school_type'];

}
