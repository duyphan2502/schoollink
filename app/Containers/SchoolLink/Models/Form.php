<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class Form.
 *
 */
class Form extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forms_v2';

    protected $fillable = ['title', 'description', 'school_id', 'created_by', 'created_date', 'updated_date', 'updated_by',
                            'form', 'sent', 'archived_date', 'deleted', 'share'];

	protected $dates = ['created_date', 'updated_date', 'archived_date'];
}
