<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

class UserEmail extends Model
{
    protected $table = 'user_emails';

    protected $fillable = ['user_id', 'type', 'email', 'date_created', 'created_by', 'date_bounced', 'deleted_by', 'date_deleted'];

	protected $dates = ['date_created', 'date_bounced', 'date_deleted'];
}
