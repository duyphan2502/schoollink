<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class ChangeLog.
 *
 */
class ChangeLog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'change_log';

    protected $fillable = ['editor_id', 'related_user_id', 'data_type', 'old_value', 'new_value',
                            'action', 'date_created'];

	protected $dates = ['date_created'];
}
