<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

class UserRole extends Model
{
    protected $table = 'user_roles';    

 	protected $fillable = ['user_id', 'role_id', 'guardian_user_id', 'subgroup_id', 'school_department_id', 'school_id', 
     						'school_owner_id', 'school_owner_role_access_level', 'school_sfo_subscription', 'access_settings', 
     						'source_ims', 'source_ims_kindergarten', 'comment', 'comment_date_updated', 'comment_updated_by'];
}
