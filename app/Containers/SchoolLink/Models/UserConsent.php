<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class UserConsent.
 *
 */
class UserConsent extends Model
{

    protected $table = 'user_consents';

    protected $fillable = ['user_consent', 'consent', 'user_id', 'value', 'granted_by', 'date', 'updater', 'date_replaced'];

	protected $dates = ['date', 'date_replaced'];
}
