<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class UserMessages.
 *
 */
class UserMessages extends Model
{

    protected $table = 'user_messages';

    protected $fillable = ['user_id', 'school_id', 'date_start', 'date_end', 'time', 'date_registered', 'date_deleted',
                    'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'type_id', 'text',
                    'reminder', 'require_logout_confirmation'];

	protected $dates = [];
}
