<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class SubGroup.
 *
 */
class SubGroup extends Model 
{
    protected $table = 'subgroups';

    protected $fillable = ['name', 'group_id', 'date_deleted', 'is_displayed', 'ims_id'];
}
