<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class Sickness.
 *
 */
class Sickness extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sicknesses';

    protected $attributes = ['deleted'];

    protected $fillable = ['sickness', 'school_owner_id', 'name_no_NB', 'name_en_GB', 'deleted',
                            'updater', 'date', 'date_replaced'];

	protected $dates = ['date', 'date_replaced'];
}
