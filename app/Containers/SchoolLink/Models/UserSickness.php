<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class UserSickness.
 *
 */
class UserSickness extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_sicknesses';

    protected $fillable = ['user_id', 'created_by', 'deleted_by', 'date_deleted', 'custom_id',
                            'system_registered_id', 'name', 'comment', 'date_created'];

	protected $dates = ['date_created'];
}
