<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class Consent.
 *
 */
class Consent extends Model
{

    protected $table = 'consents';

    protected $fillable = ['consent', 'sub_school_owner', 'sub_school', 'sub_activity', 'for_sfo', 'for_kindergarten', 'description',
                            'date_expire', 'deleted', 'updater', 'date', 'date_replaced'];

	protected $dates = ['date_expire', 'date', 'date_replaced'];
}
