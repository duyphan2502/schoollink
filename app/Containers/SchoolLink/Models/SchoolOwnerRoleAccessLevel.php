<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class SchoolOwnerRoleAccessLevel.
 *
 */
class SchoolOwnerRoleAccessLevel extends Model
{
    protected $table = 'school_owner_role_access_levels';

    protected $attributes = ['deleted'];

    protected $fillable = ['school_owner_role_access_level', 'school_owner_id', 'role_id', 'type_id', 'name', 
    						'deleted', 'date', 'updater', 'sort'];
}