<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;
use App\Containers\SchoolLink\Models\UserRole;

class Session extends Model
{
    protected $table = 'sessions';

    protected $fillable = ['session', 'key', 'environment', 'user_id', 'user_role_id', 'role_child_role_id',
                            'remote_ip', 'http_user_agent', 'language', 'date', 'expire', 'date_replaced',
                            'accessibility_background', 'accessibility_color', 'accessibility_font_size',
                            'setting_guardianportal_classlist_show_remind_me_later', 'source_id'];
}
