<?php

namespace App\Containers\SchoolLink\Models;

use App\Core\Model\Abstracts\Model as Model;

/**
 * Class SchoolOwnerRoleAccessLevelPermission.
 *
 */
class SchoolOwnerRoleAccessLevelPermission extends Model
{
    protected $table = 'school_owner_role_access_level_permissions';

    protected $fillable = ['school_owner_role_access_level', 'permission_id', 'value', 'updater', 'date'];
}