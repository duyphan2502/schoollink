<?php

namespace App\Containers\SchoolLink\Providers;

use App\Containers\SchoolLink\Contracts\AcademicYearRepositoryInterface;
use App\Containers\SchoolLink\Contracts\ChangeLogRepositoryInterface;
use App\Containers\SchoolLink\Contracts\ConsentRepositoryInterface;
use App\Containers\SchoolLink\Contracts\ForeCastServiceInterface;
use App\Containers\SchoolLink\Contracts\FormRepositoryInterface;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\ImageRepositoryInterface;
use App\Containers\SchoolLink\Contracts\LoginTypeRepositoryInterface;
use App\Containers\SchoolLink\Contracts\MessageTemplateRepositoryInterface;
use App\Containers\SchoolLink\Contracts\RemarkRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolModuleRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRoleAccessLevelPermissionRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRoleAccessLevelRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SessionRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SicknessRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserConsentRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserEmailRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserMessagesRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleSettingRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserSicknessRepositoryInterface;
use App\Containers\SchoolLink\Repositories\Eloquent\AcademicYearRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\ChangeLogRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\ConsentRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\FormRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\GroupRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\ImageRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\LoginTypeRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\MessageTemplateRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\RemarkRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\SchoolModuleRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\SchoolOwnerRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\SchoolOwnerRoleAccessLevelPermissionRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\SchoolOwnerRoleAccessLevelRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\SchoolRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\SessionRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\SicknessRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\SubGroupRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\UserConsentRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\UserEmailRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\UserMessagesRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\UserRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\UserRoleRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\UserRoleSettingRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\UserSicknessRepository;
use App\Containers\SchoolLink\Services\ForeCastService;
use App\Core\Provider\Abstracts\ServiceProviderAbstract;

/**
 * Class SchoolLinkServiceProvider.
 *
 * The Main Service Provider of this Module.
 * Will be automatically registered in the framework after
 * adding the Module name to containers config file.
 *
 */
class SchoolLinkServiceProvider extends ServiceProviderAbstract
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Container internal Service Provides.
     *
     * @var array
     */
    private $containerServiceProviders = [
    ];

    /**
     * Perform post-registration booting of services.
     */
    public function boot()
    {
        $this->registerServiceProviders($this->containerServiceProviders);
    }

    /**
     * Register bindings in the container.
     */
    public function register()
    {
        /** Service bindings **/
        $this->app->bind(ForeCastServiceInterface::class, ForeCastService::class);

        /** Repository bindings **/
        $this->app->bind(AcademicYearRepositoryInterface::class, AcademicYearRepository::class);
        $this->app->bind(GroupRepositoryInterface::class, GroupRepository::class);
        $this->app->bind(SessionRepositoryInterface::class, SessionRepository::class);
        $this->app->bind(SchoolOwnerRepositoryInterface::class, SchoolOwnerRepository::class);
        $this->app->bind(SchoolRepositoryInterface::class, SchoolRepository::class);
        $this->app->bind(SchoolModuleRepositoryInterface::class, SchoolModuleRepository::class);
        $this->app->bind(SubGroupRepositoryInterface::class, SubGroupRepository::class);
        $this->app->bind(SchoolOwnerRoleAccessLevelRepositoryInterface::class, SchoolOwnerRoleAccessLevelRepository::class);
        $this->app->bind(SchoolOwnerRoleAccessLevelPermissionRepositoryInterface::class, SchoolOwnerRoleAccessLevelPermissionRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(UserRoleRepositoryInterface::class, UserRoleRepository::class);
        $this->app->bind(LoginTypeRepositoryInterface::class, LoginTypeRepository::class);
        $this->app->bind(UserRoleSettingRepositoryInterface::class, UserRoleSettingRepository::class);
        $this->app->bind(UserSicknessRepositoryInterface::class, UserSicknessRepository::class);
        $this->app->bind(SicknessRepositoryInterface::class, SicknessRepository::class);
        $this->app->bind(ChangeLogRepositoryInterface::class, ChangeLogRepository::class);
        $this->app->bind(UserEmailRepositoryInterface::class, UserEmailRepository::class);
        $this->app->bind(RemarkRepositoryInterface::class, RemarkRepository::class);
        $this->app->bind(ConsentRepositoryInterface::class, ConsentRepository::class);
        $this->app->bind(UserConsentRepositoryInterface::class, UserConsentRepository::class);
		$this->app->bind(ImageRepositoryInterface::class, ImageRepository::class);
		$this->app->bind(UserMessagesRepositoryInterface::class, UserMessagesRepository::class);
        $this->app->bind(FormRepositoryInterface::class, FormRepository::class);
        $this->app->bind(MessageTemplateRepositoryInterface::class, MessageTemplateRepository::class);
    }
}
