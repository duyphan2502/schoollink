<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface GroupRepositoryInterface.
 *
 */
interface GroupRepositoryInterface extends RepositoryInterface
{

}
