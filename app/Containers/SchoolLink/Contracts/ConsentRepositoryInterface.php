<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConsentRepositoryInterface.
 *
 */
interface ConsentRepositoryInterface extends RepositoryInterface
{

}
