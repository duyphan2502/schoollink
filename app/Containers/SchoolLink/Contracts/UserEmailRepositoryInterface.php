<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserEmailRepositoryInterface.
 *
 */
interface UserEmailRepositoryInterface extends RepositoryInterface
{

}
