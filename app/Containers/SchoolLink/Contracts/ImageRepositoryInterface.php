<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ImageRepositoryInterface.
 *
 */
interface ImageRepositoryInterface extends RepositoryInterface
{

}
