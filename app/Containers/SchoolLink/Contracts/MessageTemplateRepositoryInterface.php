<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MessageTemplateRepositoryInterface.
 *
 */
interface MessageTemplateRepositoryInterface extends RepositoryInterface
{

}
