<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FormRepositoryInterface.
 *
 */
interface FormRepositoryInterface extends RepositoryInterface
{

}
