<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ChangeLogRepositoryInterface.
 *
 */
interface ChangeLogRepositoryInterface extends RepositoryInterface
{

}
