<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RemarkRepositoryInterface.
 *
 */
interface RemarkRepositoryInterface extends RepositoryInterface
{

}
