<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SchoolOwnerRoleAccessLevelPermissionRepositoryInterface.
 *
 */
interface SchoolOwnerRoleAccessLevelPermissionRepositoryInterface extends RepositoryInterface
{

}
