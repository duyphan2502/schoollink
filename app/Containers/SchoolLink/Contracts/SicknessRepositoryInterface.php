<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SicknessRepositoryInterface.
 *
 */
interface SicknessRepositoryInterface extends RepositoryInterface
{

}
