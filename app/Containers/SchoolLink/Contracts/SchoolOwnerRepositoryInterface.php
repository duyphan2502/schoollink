<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SchoolOwnerRepositoryInterface.
 *
 */
interface SchoolOwnerRepositoryInterface extends RepositoryInterface
{

}
