<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRoleSettingRepositoryInterface.
 *
 */
interface UserRoleSettingRepositoryInterface extends RepositoryInterface
{

}
