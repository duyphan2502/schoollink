<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserMessagesRepositoryInterface.
 *
 */
interface UserMessagesRepositoryInterface extends RepositoryInterface
{

}
