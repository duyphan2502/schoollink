<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SessionRepositoryInterface.
 *
 */
interface SessionRepositoryInterface extends RepositoryInterface
{

}
