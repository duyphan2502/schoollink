<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AcademicYearRepositoryInterface.
 *
 */
interface AcademicYearRepositoryInterface extends RepositoryInterface
{

}
