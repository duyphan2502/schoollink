<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRoleRepositoryInterface.
 *
 */
interface UserRoleRepositoryInterface extends RepositoryInterface
{

}
