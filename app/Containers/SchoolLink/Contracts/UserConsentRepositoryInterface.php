<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserConsentRepositoryInterface.
 *
 */
interface UserConsentRepositoryInterface extends RepositoryInterface
{

}
