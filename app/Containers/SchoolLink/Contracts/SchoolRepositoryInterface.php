<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SchoolRepositoryInterface.
 *
 */
interface SchoolRepositoryInterface extends RepositoryInterface
{

}
