<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepositoryInterface.
 *
 */
interface UserRepositoryInterface extends RepositoryInterface
{

}
