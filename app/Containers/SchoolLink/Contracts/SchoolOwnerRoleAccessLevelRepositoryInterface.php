<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SchoolOwnerRoleAccessLevelRepositoryInterface.
 *
 */
interface SchoolOwnerRoleAccessLevelRepositoryInterface extends RepositoryInterface
{

}
