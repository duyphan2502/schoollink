<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserSicknessRepositoryInterface.
 *
 */
interface UserSicknessRepositoryInterface extends RepositoryInterface
{

}
