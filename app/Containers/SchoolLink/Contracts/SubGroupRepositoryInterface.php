<?php

namespace App\Containers\SchoolLink\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SubGroupRepositoryInterface.
 *
 */
interface SubGroupRepositoryInterface extends RepositoryInterface
{

}
