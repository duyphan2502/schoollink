<?php
/**
 * Created by PhpStorm.
 * User: phamt
 * Date: 9/16/2016
 * Time: 12:44 AM
 */

namespace App\Containers\SchoolLink\Actions\Guardian\ContactList;


use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Core\Action\Abstracts\Action;

class UpdateShowMyContactAction extends Action
{
    private $user_repository;

    public function __construct(UserRepositoryInterface $user_repository)
    {
        $this->user_repository = $user_repository;
    }

    public function run($data)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;
        try {
            if (isset($data['show_my_contact'])) {
                $data_update['guardianportal_classlist_show'] = $data['show_my_contact'];
                $result = $this->user_repository->update($data_update, $current_user->id);
                if ($result != null) {
                    $app->current_user->guardianportal_classlist_show = $data_update['guardianportal_classlist_show'];
                    return $result->guardianportal_classlist_show;
                }
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}