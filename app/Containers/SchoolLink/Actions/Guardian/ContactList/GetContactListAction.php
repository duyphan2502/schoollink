<?php
/**
 * Created by PhpStorm.
 * User: thuan.pham
 * Date: 9/9/2016
 * Time: 11:40 AM
 */
namespace App\Containers\SchoolLink\Actions\Guardian\ContactList;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Repositories\Eloquent\GroupRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\SubGroupRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\UserRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\UserRoleRepository;
use App\Core\Action\Abstracts\Action;

class GetContactListAction extends Action
{
    private $user_role_repository;
    private $sub_group_repository;
    private $group_repository;
    private $user_repository;

    public function __construct(UserRoleRepository $userRoleRepository, SubGroupRepository $subGroupRepository, GroupRepository $groupRepository, UserRepository $userRepository)
    {
        $this->user_role_repository = $userRoleRepository;
        $this->sub_group_repository = $subGroupRepository;
        $this->group_repository = $groupRepository;
        $this->user_repository = $userRepository;
    }

    public function run($data)
    {
        $return = [];
        $app = \Session::get('app');
        $subgroup_id = $data['subgroup_id'];
        //default subgroup_id
        if ($subgroup_id == 0) {
            $subgroup_id = $app->subgroup->id;
        }
        $my_profile = $app->current_user;
        $group = $app->group;
        if ($group->academic_year_id == $app->academic_year->id) {
            $subgroups = $this->sub_group_repository->findWhere(['group_id' => $group->id]);
            $classmates = $this->user_role_repository->findWhere(['subgroup_id' => $subgroup_id, 'role_id' => Constant::ROLE_PUPIL]);
            $return['classmates'] = [];
            $classmate_guardians = [];
            foreach ($classmates as $classmate) {
                $guardians = $this->user_role_repository->findWhere(['role_id' => Constant::ROLE_GUARDIAN, 'guardian_user_id' => $classmate->user_id]);
                $classmate_ids[] = $classmate->user_id;
                foreach ($guardians as $guardian) {
                    $guardian_ids[] = $guardian->user_id;
                    $classmate_guardians[$classmate->user_id][] = $guardian->user_id;
                }
            }

            if (!empty($classmate_ids)) {
                $classmate_profiles = $this->user_repository->orderBy('firstname')->findWhereIn('id', $classmate_ids, ['id', 'firstname', 'surname']);
                if(!empty($guardian_ids))
                {
                    $guardian_profiles = $this->user_repository->orderBy('firstname')->findWhereIn('id', $guardian_ids, ['id', 'firstname', 'surname', 'image_id', 'email', 'mobilephone', 'telephone','guardianportal_classlist_show']);
                }
                $guardian_profiles_by_key = [];
                foreach ($guardian_profiles as $guardian_profile) {
                    $guardian_profiles_by_key[$guardian_profile->id] = $guardian_profile;
                }
                foreach ($classmate_profiles as $profile) {
                    $guardian_list = [];
                    if(isset($classmate_guardians[$profile->id]))
                    {
                        foreach ($classmate_guardians[$profile->id] as $guardian) {
                            $guardian_profile = $guardian_profiles_by_key[$guardian];
                            $guardian_list[] = $guardian_profile;
                        }
                    }
                    $profile->guardians = $guardian_list;
                    $return['classmates'][] = $profile;
                }
            }
            $return['subgroups'] = $subgroups;
        }
        $return['show_my_contact'] = $my_profile->guardianportal_classlist_show;
        $return['my_subgroup_id'] = $subgroup_id;
        $return['my_id'] = $my_profile->id;
        return $return;
    }
}