<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Weather;

use App\Containers\SchoolLink\Contracts\ForeCastServiceInterface;
use App\Core\Action\Abstracts\Action;

/**
 * Class GetWeatherAction.
 *
 */
class GetWeatherAction extends Action
{
    private $fore_cast_service;

	public function __construct(
        ForeCastServiceInterface $fore_cast_service
    ) {
        $this->fore_cast_service = $fore_cast_service;
    }

    public function run()
    {
        $app = \Session::get('app');
        if($app->school != null )
            return [
                'fore_cast' => $this->fore_cast_service->getForeCast($app->school)
            ];

        return ['fore_cast' => null];
    }
}