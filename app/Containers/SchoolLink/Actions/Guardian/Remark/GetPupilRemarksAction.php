<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Remark;

use App\Containers\SchoolLink\Contracts\RemarkRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

/**
 * Class GetPupilRemarksAction.
 *
 */
class GetPupilRemarksAction extends Action
{
    private $remark_repository;

	public function __construct(
        RemarkRepositoryInterface $remark_repository
    ) {
        $this->remark_repository = $remark_repository;
    }

    public function run()
    {
        $app = \Session::get('app');
        $child = $app->child;
        $academic_year = $app->academic_year;

        $user_role = $child->getRole(Constant::ROLE_PUPIL, new RoleParam(['current_year' => true]));

        $return['remarks'] = [
            'negative_remarks' => [],
            'positive_remarks' => [],
            'negative_remarks_number' => 0,
            'positive_remarks_number' => 0
        ];
        if($user_role){
            $id = $child->id.$user_role->id.'_'.$academic_year->id;
            $remarks = $this->remark_repository->find($id);
            if(isset($remarks->remarks)){
                $arr_date_negative = [];
                $arr_time_negative = [];
                $arr_date_positive = [];
                $arr_time_positive = [];
                foreach ($remarks->remarks as $key => $remark) {
                    if ($remark->status == Constant::REMARK_STATUS_FINAL) {
                        if($remark->category == ucfirst(Constant::REMARK_NEGATIVE)){
                            $arr_date_negative[$key] = $remark->date;
                            $arr_time_negative[$key] = $remark->time;
                            $return['remarks']['negative_remarks_number']++;
                            $return['remarks']['negative_remarks'][] = $remark;
                        }else{
                            $arr_date_positive[$key] = $remark->date;
                            $arr_time_positive[$key] = $remark->time;
                            $return['remarks']['positive_remarks_number']++;
                            $return['remarks']['positive_remarks'][] = $remark;
                        }

                    }
                }
                if($return['remarks']['negative_remarks_number'] > 0){
                    array_multisort($arr_date_negative, SORT_DESC, $arr_time_negative, SORT_DESC, $return['remarks']['negative_remarks']);
                }
                if( $return['remarks']['positive_remarks_number'] > 0){
                    array_multisort($arr_date_positive, SORT_DESC, $arr_time_positive, SORT_DESC, $return['remarks']['positive_remarks']);
                }
            }
        }
        return $return;
    }
}