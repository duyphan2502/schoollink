<?php

namespace App\Containers\SchoolLink\Actions\Guardian\NotificationSetting;

use App\Containers\SchoolLink\BusinessModels\LoginType;
use App\Containers\SchoolLink\Contracts\LoginTypeRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

/**
 * Class GetNotificationSettingOfPupilAction.
 *
 */
class GetNotificationSettingOfPupilAction extends Action
{
	private $loginTypeRepository;

	public function __construct(
        LoginTypeRepositoryInterface $loginTypeRepository
    ) {
        $this->loginTypeRepository = $loginTypeRepository;
    }

    public function run()
    {
        $app = \Session::get('app');

        $school = $app->school;
        $user = $app->current_user;
        $child = $app->child;

		$return = [];
        $permission = $school->hasModule(Constant::MODULE_NOTIFICATION_WHEN_LEAVE_PICKED_UP)
            && $user->hasRole(Constant::ROLE_GUARDIAN, $child->id)
            && $child->hasRole(Constant::ROLE_PUPIL,
                    new RoleParam(['current_year' => true, 'school_sfo_subscription' => true]));

        $return['permission'] = $permission;

        if($permission) {
            $login_types = $this->loginTypeRepository->findWhere(['school_id' => $school->id]);
            $notifications = [];
            $role = $user->getRole(Constant::ROLE_GUARDIAN, $child->id);

            foreach ($login_types as $login_type) {
                if (in_array($login_type->icon, LoginType::$notification_slider_login_types[$school->type_id]) && empty($login_type->date_deleted) && $login_type->guardian_email_notice == true)
                {
                    $setting = $role->getSetting('email_sign_in_status[' . $role->guardian_user_id . '][' . $login_type->id . ']');

                    $notification = [];
                    $notification['receiving'] = isset($setting) ? $setting->value : 0;
                    $notification['login_type_id'] = $login_type->id;
                    $notification['user_name'] = $child->getShortName();
                    $notification['user_id'] = $child->id;
                    $notification['user_image'] = $child->getImage('profile');
                    $notification['icon'] = $login_type->icon;
                    $notification['school_type'] = $school->type_id;

                    $notifications[] = $notification;
                }
            }
            $return['notifications'] = $notifications;

        }
        return $return;
    }
}