<?php

namespace App\Containers\SchoolLink\Actions\Guardian\NotificationSetting;

use App\Containers\SchoolLink\Contracts\UserRoleSettingRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

/**
 * Class UpdateNotificationSettingOfPupilAction.
 *
 */
class UpdateNotificationSettingOfPupilAction extends Action
{
	private $userRoleSettingRepository;

	public function __construct(
        UserRoleSettingRepositoryInterface $userRoleSettingRepository
    ) {
        $this->userRoleSettingRepository = $userRoleSettingRepository;
    }

    public function run($login_type_id)
    {
        $app = \Session::get('app');
        $user = $app->current_user;
        $child = $app->child;

        $role = $user->getRole(Constant::ROLE_GUARDIAN, $child->id);

        $setting = $role->getSetting('email_sign_in_status[' . $role->guardian_user_id . '][' . $login_type_id . ']');

        $result = $role->setSetting('email_sign_in_status[' . $role->guardian_user_id . '][' . $login_type_id . ']',
            $setting != null ? (int)!$setting->value : true);
        return ['result' => $result];
    }
}