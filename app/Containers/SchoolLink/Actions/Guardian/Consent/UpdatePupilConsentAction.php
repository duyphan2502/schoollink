<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Consent;


use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\UserConsentRepositoryInterface;
use App\Containers\SchoolLink\Contracts\ChangeLogRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * Class UpdatePupilConsentAction.
 *
 */
class UpdatePupilConsentAction extends Action
{
    private $user_consent_repository;
    private $change_log_repository;

	public function __construct(
        UserConsentRepositoryInterface $user_consent_repository,
        ChangeLogRepositoryInterface $change_log_repository
    ) {
        $this->user_consent_repository = $user_consent_repository;
        $this->change_log_repository = $change_log_repository;
    }

    public function run($data)
    {
        try{
            if(is_array($data)){
                $user = \Session::get('app')->current_user;
                foreach ($data as $consent){
                    $json_string_old = '';
                    if ($consent['user_consent_id'] != NULL){
                        $data_update = [
                            'value' => $consent['value'],
                            'updater' => $user->id
                        ];
                        $consent_query = $this->user_consent_repository->update($data_update, $consent['user_consent_id']);
                        $json_string_old = json_encode([
                            'consent' => $consent['description'],
                            'user_id' => $consent['user_id'],
                            'value' => $consent['value_before_update']
                        ]);
                    }else if( $consent['value'] != NULL){
                        //setup user consent value
                        $chars  = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D',
                            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
                            'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                            'Y', 'Z');
                        $serial = '';
                        $max    = count($chars) - 1;
                        for ($i = 0; $i < 20; $i++) {
                            $serial .= (! ($i % 5) && $i ? '-' : '') . $chars[rand(
                                    0, $max
                                )];
                        }
                        $data_insert = [
                            'consent' => $consent['consent'],
                            'user_id' => $consent['user_id'],
                            'value'   => $consent['value'],
                            'granted_by' => $user->id,
                            'date' => Carbon::now(),
                            'user_consent' => $serial
                        ];
                        $consent_query = $this->user_consent_repository->create($data_insert);
                    }
                    if(isset($consent_query)) {
                        $json_string_new = json_encode([
                                'consent' => $consent['description'],
                                'user_id' => $consent['user_id'],
                                'value' => $consent['value']
                            ]
                        );
                        $this->change_log_repository->create([
                            'editor_id' => $user->id,
                            'related_user_id' => $consent['user_id'],
                            'data_type' => Constant::CHANGE_LOG_TYPE_USER_CONSENT,
                            'old_value' => $json_string_old,
                            'new_value' => $json_string_new,
                            'action' => Constant::CHANGE_LOG_UPDATE,
                        ]);
                    }
                }
                return ['result' => true];
            }
        } catch (Exception $e) {
            Log::error($e);
            return ['result' => false];
        }
    }
}