<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Consent;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\ConsentRepositoryInterface;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserConsentRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Criterias\Consent\GetConsentsBySchoolInfoCriteria;
use App\Core\Action\Abstracts\Action;
use Carbon\Carbon;


/**
 * Class GetPupilConsentsAction.
 *
 */
class GetPupilConsentsAction extends Action
{
    private $user_role_repository;
    private $subgroup_repository;
    private $group_repository;
    private $school_repository;
    private $consent_repository;
    private $user_consent_repository;
	public function __construct(
        UserRoleRepositoryInterface $user_role_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository,
        ConsentRepositoryInterface $consent_repository,
        UserConsentRepositoryInterface $user_consent_repository
    ) {
        $this->user_role_repository = $user_role_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->school_repository = $school_repository;
        $this->consent_repository = $consent_repository;
        $this->user_consent_repository = $user_consent_repository;
    }

    public function run()
    {
        $child = \Session::get('app')->child;
        $return = [];
        $user_roles = $this->user_role_repository->findWhere(['user_id' => $child->id, 'role_id' => Constant::ROLE_PUPIL]);
        $consents_consent = [];
        $consents_value = [];
        if($user_roles){
            foreach ($user_roles as $role){
                $subgroup = $this->subgroup_repository->find($role->subgroup_id);
                $group = $this->group_repository->find($subgroup->group_id);
                $school = $this->school_repository->find($group->school_id);
                if($subgroup->date_deleted === NULL && $group->date_deleted == NULL && $school->date_deleted == NULL){
                    $consents = $this->consent_repository->getByCriteria(new GetConsentsBySchoolInfoCriteria($school->owner_id, $school->id));
                    foreach ($consents as $consent){
                        if(!in_array($consent->consent, $consents_consent)
                            && (($school->type_id== Constant::SCHOOL_TYPE_KINDERGARTEN && $consent->for_kindergarten == true)
                                || ($school->type_id != Constant::SCHOOL_TYPE_KINDERGARTEN && $consent->for_kindergarten == false && ($consent->for_sfo == false || $role->school_sfo_subscription !== null)))){
                            $consents_consent[] = $consent->consent;
                            $consents_value[] = $consent;
                        }
                    }
                }
            }
        }
        $user_consents_value = $this->user_consent_repository->orderBy('id', 'DESC')->findWhere(['user_id' => $child->id, 'date_replaced' => NULL]);
        $user_consents = [];
        foreach ($user_consents_value as $user_consent){
            if ( ! isset($user_consents[$user_consent->consent])
                || $user_consent->id > $user_consents[$user_consent->consent]['id']
            ) {
                $user_consents[$user_consent->consent] = array(
                    'id'           => $user_consent->id,
                    'user_consent' => $user_consent->user_consent,
                    'value'        => $user_consent->value
                );
            }
        }
        $return['consents'] = [];
        $child_name = $child->getShortName();
        $child_image = $child->getImage('profile');
        foreach ($consents_value as $consent) {
            if ($consent->deleted == false && ($consent->date_expire == NULL || $consent->date_expire >Carbon::now())) {
                $return['consents'][] = array(
                    'consent'     => $consent->consent,
                    'description' => $consent->description,
                    'date_expire' => $consent->date_expire != NULL ? Carbon::parse($consent->date_expire)->getTimestamp(): NULL,
                    'value'       => isset($user_consents[$consent->consent])
                        ? $user_consents[$consent->consent]['value'] : NULL,
                    'value_before_update' => isset($user_consents[$consent->consent])
                        ? $user_consents[$consent->consent]['value'] : NULL,
                    'user_name' => $child_name,
                    'user_id' => $child->id,
                    'user_image' => $child_image,
                    'user_consent_id' => isset($user_consents[$consent->consent])
                        ? $user_consents[$consent->consent]['id'] : ''

                );
            }
        }
        $return['permission_edit'] = $child->permissionEdit(Constant::PERMISSION_EDIT_STUDENT_PERMISSIONS);
        return $return;
    }
}