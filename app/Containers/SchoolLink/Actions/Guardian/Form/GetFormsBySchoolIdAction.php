<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 11/08/2016
 */
namespace App\Containers\SchoolLink\Actions\Guardian\Form;

use App\Containers\SchoolLink\Contracts\FormRepositoryInterface;
use App\Containers\SchoolLink\Criterias\Form\GetFormsBySchoolIdCriteria;
use App\Containers\SchoolLink\Repositories\Eloquent\GroupRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\SubGroupRepository;
use App\Containers\SchoolLink\Repositories\Eloquent\UserRepository;
use App\Core\Action\Abstracts\Action;

class GetFormsBySchoolIdAction extends Action
{
    private $form_repository;
    private $sub_group_repository;
    private $group_repository;
    private $user_repository;

    public function __construct(FormRepositoryInterface $form_repository, SubGroupRepository $subGroupRepository, GroupRepository $groupRepository, UserRepository $userRepository)
    {
        $this->form_repository = $form_repository;
        $this->sub_group_repository = $subGroupRepository;
        $this->group_repository = $groupRepository;
        $this->user_repository = $userRepository;
    }

    public function run($school_id = null)
    {
        $app = \Session::get('app');

        if($school_id == null && $app->user_role->subgroup_id != null) {
            $subgroup = $this->sub_group_repository->find($app->user_role->subgroup_id);
            $group = $this->group_repository->find($subgroup->group_id);
            $school_id = isset($group->school_id) && $group->school_id != '' ? $group->school_id : null;
        }

        if($school_id == null)
            return;

        $forms = $this->form_repository->getByCriteria(new GetFormsBySchoolIdCriteria($app->current_user->id, $school_id));

        return ['forms' => $forms];
    }
}