<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Profile;

use App\Containers\SchoolLink\Contracts\ImageRepositoryInterface;

use App\Core\Action\Abstracts\Action;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * Class UploadImageProfileAction.
 *
 */
class UploadImageProfileAction extends Action
{
    private $image_repository;

	public function __construct(
        ImageRepositoryInterface $image_repository
    ) {
        $this->image_repository = $image_repository;
    }

    public function run($data)
    {
        /*try{
            $img = $this->addImage();
            $root_dir = \Config::get('schoollink.path_upload_image');
            $data->move($root_dir, $img['id'].'.jpg');
        } catch (Exception $e) {
            Log::error($e);
        }*/
        $app = \Session::get('app');
        try{
            if(isset($data['image_id'])) {
                $data_update = [];
                foreach ($data as $k => $v) {
                    $data_update[$k] = $v;
                    $app->current_user->$k = $data_update[$k];
                }
            }
        } catch (Exception $e) {
            Log::error($e);
        }


    }
    protected function addImage(){

        try{
            $app = \Session::get('app');
            $chars = array(0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
            $serial = '';
            $max = count($chars)-1;
            for($i=0;$i<20;$i++){
                $serial .= (!($i % 5) && $i ? '-' : '').$chars[rand(0, $max)];
            }
            $data_insert = [
                'id' => $serial,
                'user_id' => $app->current_user->id,
                'date_created' => Carbon::now()
            ];
            $this->image_repository->create($data_insert);
            return $data_insert;
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}