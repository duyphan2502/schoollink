<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Profile;

use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;

use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

/**
 * Class GetProfileAction.
 *
 */
class GetProfileAction extends Action
{
	private $user_role_repository;

	public function __construct(
        UserRoleRepositoryInterface $user_role_repository
    ) {
        $this->user_role_repository = $user_role_repository;
    }

    public function run()
    {
        $app = \Session::get('app');
        $user = $app->child;
        if(!$app->user_role){
            $user = $app->current_user;
        }

		$return = [];
        $profile = [];
        $permissions = [];
        $profile['image'] = $user->getImage('profile');
        $profile['firstname'] = $user->firstname;
        $profile['surname'] = $user->surname;
        $profile['email'] = $user->email;
        $profile['mobilephone'] = $user->mobilephone;
        $profile['telephone'] = $user->telephone;
        $profile['address'] = $user->address;
        $profile['postal_code'] = $user->postal_code;
        $profile['postal_name'] = $user->postal_name;
        $profile['id'] = $user->id;
        $profile['guardian_portal_class_list_show'] = $user->guardianportal_classlist_show;

        $permissions['image'] = $user->permissionEdit(Constant::PERMISSION_EDIT_IMAGE_ID)
            && !$user->hasRole(Constant::ROLE_PUPIL);

        $permissions['first_name'] = $user->permissionEdit(Constant::PERMISSION_EDIT_FIRSTNAME);
        $permissions['surname'] = $user->permissionEdit(Constant::PERMISSION_EDIT_SURNAME);
        $permissions['email'] = $user->permissionEdit(Constant::PERMISSION_EDIT_EMAIL);
        $permissions['mobilephone'] = $user->permissionEdit(Constant::PERMISSION_EDIT_MOBILEPHONE);
        $permissions['telephone'] = $user->permissionEdit(Constant::PERMISSION_EDIT_TELEPHONE);
        $permissions['address'] = $user->permissionEdit(Constant::PERMISSION_EDIT_ADDRESS);
        $permissions['postal_code'] = $user->permissionEdit(Constant::PERMISSION_EDIT_POSTAL_CODE);
        $permissions['postal_name'] =$user->permissionEdit(Constant::PERMISSION_EDIT_POSTAL_NAME);
        $permissions['guardianportal_main'] = $user->permissionEdit(Constant::PERMISSION_EDIT_GUARDIANPORTAL);

        $return['permissions'] = $permissions;
        $return['profile'] = $profile;
		return $return;
    }
}