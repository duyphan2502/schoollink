<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Profile;

use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;

use App\Core\Action\Abstracts\Action;
use Illuminate\Support\Facades\Log;

/**
 * Class UpdateProfileAction.
 *
 */
class UpdateProfileAction extends Action
{

	public function __construct(
        UserRepositoryInterface $user_repository
    ) {
        $this->user_repository = $user_repository;
    }

    public function run($data)
    {
        try{
            $app = \Session::get('app');
            if(isset($data['id'])) {
                $data_update = [];
                foreach ($data as $k => $v) {
                    if (in_array($k, array('email', 'mobilephone', 'guardianportal_classlist_show'))) {
                        $data_update[$k] = $v;
                        // this field only apply for parent profile
                        if($k == 'guardianportal_classlist_show'){
                            $data_update[$k] = $v == 1 ? 0 : 1;
                        }
                        if(!$app->user_role){
                            $app->current_user->$k = $data_update[$k];
                        }else{
                            $app->child->$k = $data_update[$k];
                        }
                    }
                }
                return $this->user_repository->update($data_update, $data['id']);
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}