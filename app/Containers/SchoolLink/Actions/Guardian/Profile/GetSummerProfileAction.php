<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Profile;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Containers\SummerSchool\Contracts\CustomFieldRepositoryInterface;

use App\Containers\SummerSchool\Contracts\CustomFieldValueRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Containers\SummerSchool\Constants\Constant as SummerConstant;
use MongoDB\BSON\ObjectID;

/**
 * Class GetSummerProfileAction.
 *
 */
class GetSummerProfileAction extends Action
{
	private $user_role_repository;
    private $custom_field_repository;
    private $custom_field_value_repository;
    private $subgroups_repository;

	public function __construct(
        UserRoleRepositoryInterface $user_role_repository,
        CustomFieldRepositoryInterface $custom_field_repository,
        CustomFieldValueRepositoryInterface $custom_field_value_repository,
        SubGroupRepositoryInterface $subgroups_repository
    ) {
        $this->user_role_repository = $user_role_repository;
        $this->custom_field_repository = $custom_field_repository;
        $this->custom_field_value_repository = $custom_field_value_repository;
        $this->subgroups_repository = $subgroups_repository;
    }

    public function run()
    {
        $app = \Session::get('app');
        $user = $app->child;
        if(!$app->user_role){
            $user = $app->current_user;
        }

        if(empty($user) || !isset($app->school)){
            return false;
        }
		$return = [];
        $profile = [];
        $profile['image'] = $user->getImage('profile');
        $profile['firstname'] = $user->firstname;
        $profile['surname'] = $user->surname;
        $profile['id'] = $user->id;
        if(isset($app->school)) {
            $app->school->gps_coordinates = json_decode($app->school->location);
            $current_school = $app->school;
        }

        if(!empty($app->child)){
            $role = $app->child->getRole(Constant::ROLE_PUPIL, new RoleParam(['current_year' => true]));
            $subgroup = $this->subgroups_repository->find($role->subgroup_id);
            if($subgroup){
                $profile['grade'] = (int)preg_replace("/[^0-9]/","",$subgroup->name);
            }
        }
        $profile['school'] = $current_school;
        $return['profile'] = $profile;
		return $return;
    }
}