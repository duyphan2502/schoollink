<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Precaution;

use App\Containers\SchoolLink\Contracts\UserSicknessRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;
use App\Core\Exception\Abstracts\Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class DeletePrecautionOfPupilAction.
 *
 */
class DeletePrecautionOfPupilAction extends Action
{
    private $user_sickness_repository;

    public function __construct(
        UserSicknessRepositoryInterface $user_sickness_repository
    ) {
        $this->user_sickness_repository = $user_sickness_repository;
    }

    public function run($id)
    {
        try{
            $app = \Session::get('app');
            $child = $app->child;
            $user = $app->current_user;

            $sickness = $this->user_sickness_repository->find($id);
            if (!$sickness || !$child->permissionEdit(Constant::PERMISSION_EDIT_SICKNESSES)) {
                return;
            }
            $data_update = [
                'date_deleted' => date('Y-m-d H:i:s'),
                'deleted_by' => $user->id
            ];

            $sickness_delete = $this->user_sickness_repository->update($data_update, $id);
            $sickness->addChangeLog($user, Constant::CHANGE_LOG_DELETE);
            return $sickness_delete;
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}