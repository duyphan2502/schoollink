<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Precaution;

use App\Containers\SchoolLink\BusinessModels\Sickness;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserSicknessRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SicknessRepositoryInterface;
use App\Containers\SchoolLink\Contracts\ChangeLogRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

/**
 * Class AddPrecautionOfPupilAction.
 *
 */
class AddPrecautionOfPupilAction extends Action
{
    private $user_role_repository;
    private $user_sickness_repository;
    private $subgroup_repository;
    private $group_repository;
    private $school_repository;
    private $school_owner_repository;
    private $sickness_repository;
    private $change_log_repository;

    public function __construct(
        UserRoleRepositoryInterface $user_role_repository,
        UserSicknessRepositoryInterface $user_sickness_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository,
        SchoolOwnerRepositoryInterface $school_owner_repository,
        SicknessRepositoryInterface $sickness_repository,
        ChangeLogRepositoryInterface $change_log_repository
    ) {
        $this->user_role_repository = $user_role_repository;
        $this->user_sickness_repository = $user_sickness_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->school_repository = $school_repository;
        $this->school_owner_repository = $school_owner_repository;
        $this->sickness_repository = $sickness_repository;
        $this->change_log_repository = $change_log_repository;
    }

    public function run($name, $description = NULL)
    {
        try {
            $app = \Session::get('app');

            $user = $app->current_user;
            $child = $app->child;

            if(!$child || !$child->permissionEdit(Constant::PERMISSION_EDIT_SICKNESSES)){
                return null;
            }
            $user_roles = $this->user_role_repository->findWhere(['user_id' => $child->id]);
            $sicknesses = array();
            $setting_include_system_sicknesses = true;
            $sickness_custom_id = null;
            $sickness_system_registered_id = null;
            foreach ($user_roles as $role) {
                if ($role->role_id == Constant::ROLE_PUPIL) {
                    $subgroup = $this->subgroup_repository->find($role->subgroup_id);
                    $group = $this->group_repository->find($subgroup->group_id);
                    $school = $this->school_repository->find($group->school_id);
                    $school_owner = $this->school_owner_repository->find($school->owner_id);
                    if($school_owner->permissionAccessSicknesses()){
                        $sickness_ids = $this->sickness_repository->orderBy('name_no_NB')
                            ->findWhere(['school_owner_id' => $school_owner->id,
                            'date_replaced' => null], ['sickness']);

                        $sickness_ids = $sickness_ids->map(function ($item) {
                            return $item->sickness;
                        })->toArray();

                        $sicknesses = array_merge($sicknesses, $sickness_ids);
                        if ($school_owner->setting_include_system_sicknesses === false) {
                            $setting_include_system_sicknesses = false;
                        }
                    }
                }
            }
            $lang = App::getLocale();
            $lang = $lang === 'en' ? 'en_GB' : 'no_NB';
            $variable = 'name_' . $lang;
            $sicknesses = $this->sickness_repository->findWhereIn('sickness', $sicknesses);
            $query = mb_strtolower($name);
            foreach ($sicknesses as $sickness) {
                if (mb_strtolower($sickness->$variable) === $query) {
                    $sickness_custom_id = $sickness->sickness;
                }
            }

            if ($setting_include_system_sicknesses === true) {
                foreach (Sickness::$sicknesses as $key => $sickness) {
                    if (mb_strtolower($sickness['name'][$lang]) === $query) {
                        $sickness_system_registered_id = $key;
                    }
                }
            }
            $data_sickness = [
                'date_created' => Carbon::now(),
                'created_by' => $user->id,
                'comment' => $description,
                'user_id' => $child->id
            ];
            if ($sickness_custom_id !== null) {
                $data_sickness['custom_id'] = $sickness_custom_id;
            } elseif ($sickness_system_registered_id !== null) {
                $data_sickness['system_registered_id'] = $sickness_system_registered_id;
            } else {
                $data_sickness['name'] = $name;
            }
            $sickness_add = $this->user_sickness_repository->create(
                $data_sickness
            );
            if ($sickness_add) {
                $sickness_add->addChangeLog($user, Constant::CHANGE_LOG_ADD);
            }
            $sickness_add->name = $sickness_add->getName();
            return $sickness_add;
        } catch (Exception $e) {
            Log::error($e);
        }

    }
}