<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Precaution;

use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;

/**
 * Class ConfirmSicknessOfPupilAction.
 *
 */
class ConfirmSicknessOfPupilAction extends Action
{
    private $user_repository;

    public function __construct(
        UserRepositoryInterface $user_repository
    ) {
        $this->user_repository = $user_repository;
    }

    public function run($sickness_confirmed)
    {
        try {
            $app = \Session::get('app');

            $child = $app->child;
            $user = $app->current_user;

            if (!$child->permissionEdit(Constant::PERMISSION_EDIT_SICKNESSES)) {
                return;
            }

            $data_update = [
                'sickness_confirmed' => (int)$sickness_confirmed == 1 ? 0 : 1,
                'sickness_confirmed_updater' => $user->id,
                'sickness_confirmed_date' => Carbon::now()
            ];
            $this->user_repository->update($data_update, $child->id);
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}