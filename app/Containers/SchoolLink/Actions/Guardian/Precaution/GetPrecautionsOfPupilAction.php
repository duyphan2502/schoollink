<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Precaution;

use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserSicknessRepositoryInterface;
use App\Containers\SchoolLink\Criterias\Sickness\GetSicknessesByUserIdCriteria;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

/**
 * Class GetPrecautionsOfPupilAction.
 *
 */
class GetPrecautionsOfPupilAction extends Action
{
    private $user_role_repository;
    private $user_sickness_repository;

	public function __construct(
        UserRoleRepositoryInterface $user_role_repository,
        UserSicknessRepositoryInterface $user_sickness_repository
    ) {
        $this->user_role_repository = $user_role_repository;
        $this->user_sickness_repository = $user_sickness_repository;
    }

    public function run()
    {
		$return = [];
        $app = \Session::get('app');
        $user = $app->child;
        
        $permission = $user->permissionEdit(Constant::PERMISSION_EDIT_SICKNESSES);

        $return['permission'] = $permission;

        $return['user_sicknesses'] = [];
        $sicknesses = $this->user_sickness_repository->getByCriteria(new GetSicknessesByUserIdCriteria($user->id));
        foreach ($sicknesses as $sickness) {
            $return['user_sicknesses'][] = [
                'id' => $sickness->id,
                'name' =>  $sickness->getName(),
                'comment' => $sickness->comment
            ];
        }
        $return['sicknesses'] = $user->getSicknessListOfUserRoles();
        $return['sickness_confirmed'] = (boolean)$user->sickness_confirmed;

		return $return;
    }
}