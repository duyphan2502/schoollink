<?php

namespace App\Containers\SchoolLink\Actions\Guardian\School;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CustomFieldRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CustomFieldValueRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use App\Containers\SummerSchool\Constants\Constant as SummerConstant;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use MongoDB\BSON\ObjectID;

/**
 * Class GetSchoolsInMunicipalityAction.
 *
 */
class GetSchoolsInMunicipalityAction extends Action
{
	private $school_repository;
    private $custom_field_repository;
    private $custom_field_value_repository;

	public function __construct(
        SchoolRepositoryInterface $school_repository,
        CustomFieldRepositoryInterface $custom_field_repository,
        CustomFieldValueRepositoryInterface $custom_field_value_repository
    ) {
        $this->school_repository = $school_repository;
        $this->custom_field_repository = $custom_field_repository;
        $this->custom_field_value_repository = $custom_field_value_repository;
    }

    public function run()
    {
        try {
            $app = \Session::get('app');
            $school_owner = $app->school_owner;
            $schools = $this->school_repository
                ->findWhere(['owner_id' => $school_owner->id, 'type_id' => Constant::SCHOOL_TYPE_SCHOOL, 'date_deleted' => null])
                ->keyBy('id');

            foreach ($schools as $school){
                if($school) {
                    $school->gps_coordinates = json_decode($school->location);
                }
            }

            return [
                'schools' => $schools,
            ];
        } catch (Exception $ex){
            Log::error($ex);
        }

    }
}