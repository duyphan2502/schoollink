<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Header;

use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\UTCDatetime;

/**
 * Class GetHeaderSettingsAction.
 *
 */
class GetHeaderSettingsAction extends Action
{
	private $subgroup_repository;
	private $group_repository;
	private $user_repository;
	private $school_repository;
	private $user_role_repository;
	private $enrollment_group_repository;

	public function __construct(
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository,
        UserRepositoryInterface $user_repository,
        UserRoleRepositoryInterface $user_role_repository,
		EnrollmentGroupRepositoryInterface $enrollment_group_repository
    ) {
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->user_repository = $user_repository;
        $this->school_repository = $school_repository;
		$this->user_role_repository = $user_role_repository;
		$this->enrollment_group_repository = $enrollment_group_repository;
    }

    public function run()
    {
		try {
			$app = \Session::get('app');

			$user = $app->current_user;

			$return = array(
				'go_to_teacher_portal' => false,
				'roles' => array(),
				'user' => $user,
				'permissions' => [],
			);
			$return['roles'][] = array(
				'id' => null,
				'user_id' => $user->id,
				'title' => $user->getShortName(),
				'name' => explode(' ', $user->firstname)[0],
				'image_id' => $user->getImage('profile')
			);

			foreach ($user->roles as $role) {
				if ($role->loginActive()) {
					if ($role->role_id === Constant::ROLE_GUARDIAN) {
						$child_user = $this->user_repository->find($role->guardian_user_id);
						$return['roles'][] = array(
							'id' => (int) $role->id,
							'user_id' => $role->guardian_user_id,
							'title' => $child_user->getShortName(),
							'name' => explode(' ', $child_user->firstname)[0],
							'image_id' => $child_user->getImage('profile')
						);
					} else if ($role->role_id === Constant::ROLE_SFO || $role->role_id === Constant::ROLE_SCHOOL_ADMIN ||
						$role->role_id === Constant::ROLE_TEACHER || $role->role_id === Constant::ROLE_MUNICIPALITY) {
                        $return['go_to_teacher_portal'] = (int) $role->id;
                        $return['role'] = $role->role_id;
					}
				}
			}

			$return['school_owner'] = $app->school_owner;

			if (isset($app->user_role))
			{
				$child_role = $app->child_role;
				$school = $app->school;

				$permissions = [
					'school_type' => $school->type_id,
					'school' => false,
					'school_sfo_subscription' => false,
					'report_absence' => false,
					'activities' => false
				];

				if (isset($child_role) && $child_role->school_sfo_subscription != null) {
					$permissions['school_sfo_subscription'] = true;
				}

				if(isset($school)){
					$permissions['school'] = true;

					if($school->hasModule(Constant::MODULE_GUARDIANPOTAL_REPORT_ABSENCE)) {
						$permissions['report_absence'] = true;
					}

					if($school->hasModule(Constant::MODULE_GUARDIANPOTAL_ACTIVITIES)) {
						$permissions['activities'] = true;
					}
				}
			} else {
				$permissions = [
					'parent' => true,
					'school' => false,
					'school_sfo_subscription' => false,
					'report_absence' => false,
					'activities' => false,
					'school_type' => '',
				];
			}

			$return['permissions'] = $permissions;
			
			return (object)$return;
		} catch (Exception $ex) {
			Log::error($ex);
		}
    }
}