<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Header;

use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;

use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\UTCDatetime;

/**
 * Class GetHeaderSettingsAction.
 *
 */
class GetSummerSchoolHeaderSettingsAction extends Action
{
	private $subgroup_repository;
	private $group_repository;
	private $user_repository;
	private $school_repository;
	private $user_role_repository;
	private $enrollment_group_repository;

	public function __construct(
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository,
        UserRepositoryInterface $user_repository,
        UserRoleRepositoryInterface $user_role_repository,
		EnrollmentGroupRepositoryInterface $enrollment_group_repository
    ) {
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->user_repository = $user_repository;
        $this->school_repository = $school_repository;
		$this->user_role_repository = $user_role_repository;
		$this->enrollment_group_repository = $enrollment_group_repository;
    }

    public function run()
    {
		try {
			$app = \Session::get('app');

			$user = $app->current_user;

			$return = array(
				'go_to_teacher_portal' => false,
				'roles' => array(),
                'teachers' => array(),
                'user' => $user,
				'permissions' => [],
                'is_parent' =>false,
                'is_pupil' => false
			);

			foreach ($user->roles as $role) {
				if ($role->loginActive()) {
				    switch ($role->role_id) {
                        case Constant::ROLE_GUARDIAN:
                            $child_user = $this->user_repository->find($role->guardian_user_id, ['surname', 'firstname', 'image_id']);
                            $return['roles'][] = array(
                                'id' => (int) $role->id,
                                'user_id' => $role->guardian_user_id,
                                'title' => $child_user->getShortName(),
                                'name' => explode(' ', $child_user->firstname)[0],
                                'image_id' => $child_user->getImage('profile')
                            );
                            $return['is_parent'] = true;
                            break;

                        case Constant::ROLE_SCHOOL_ADMIN:
                            $special_school = $app->special_school;

                            if($special_school->id == $role->school_id) {
                                $return['go_to_teacher_portal'] = (int) $role->id;
                            } else {
                                $academic_year = $app->school_owner->activeAcademicYear();
                                $school = $this->school_repository->find($role->school_id);
                                $groups = $this->group_repository->findWhere(['school_id' => $role->school_id,
                                    'academic_year_id' => $academic_year->id],['id'])->pluck('id')->toArray();
                                $subgroups = $this->subgroup_repository->findWhereIn('group_id', $groups);

                                foreach ($subgroups as $subgroup) {
                                    if(isset($return['teachers'][$school->id])) {
                                        $return['teachers'][$school->id]['classes'][$subgroup->id] = $subgroup;
                                    } else {
                                        $return['teachers'][$school->id] = [
                                            'school' => $school,
                                            'classes' => [$subgroup->id => $subgroup]
                                        ];
                                    }
                                }
                            }
                            break;
                        case Constant::ROLE_MUNICIPALITY:
                            $special_school = $app->special_school;

                            if($special_school->owner_id == $role->school_owner_id) {
                                $return['go_to_teacher_portal'] = (int) $role->id;
                            } else {
                                $academic_year = $app->school_owner->activeAcademicYear();
                                $schools = $this->school_repository->findWhere(['owner_id' => $role->school_owner_id]);

                                $groups = $this->group_repository->findWhereIn('school_id', $schools->pluck('id')->toArray());

                                $groups = $groups->filter(function ($group) {
                                    return $group->academic_year_id == $academic_year->id;
                                });

                                $subgroups = $this->subgroup_repository->findWhereIn('group_id', $groups->pluck('id')->toArray());

                                foreach ($subgroups as $subgroup) {
                                    if(isset($return['teachers'][$school->id])) {
                                        $return['teachers'][$school->id]['classes'][$subgroup->id] = $subgroup;
                                    } else {
                                        $return['teachers'][$school->id] = [
                                            'school' => $school,
                                            'classes' => [$subgroup->id => $subgroup]
                                        ];
                                    }
                                }
                            }
                            break;
                        case Constant::ROLE_TEACHER:
                            $subgroup = $this->subgroup_repository->find($role->subgroup_id);
                            $group = $this->group_repository->find($subgroup->group_id);

                            if(isset($return['teachers'][$group->school_id])) {
                                $return['teachers'][$group->school_id]['classes'][$subgroup->id] = $subgroup;
                            } else {
                                $school = $this->school_repository->find($group->school_id);
                                $return['teachers'][$group->school_id] = [
                                    'school' => $school,
                                    'classes' => [$subgroup->id => $subgroup]
                                ];
                            }
                            break;

                        case Constant::ROLE_PUPIL:
                            $return['roles'][] = array(
                                'id' => (int) $role->id,
                                'user_id' => $role->user_id,
                                'title' => $user->getShortName(),
                                'name' => explode(' ', $user->firstname)[0],
                                'image_id' => $user->getImage('profile')
                            );
                            $return['is_pupil'] = true;
                        default:
                            break;
                    }
				}
			}

			$return['school_owner'] = $app->school_owner;

			return (object)$return;
		} catch (Exception $ex) {
			\Log::error($ex);
		}
    }
}