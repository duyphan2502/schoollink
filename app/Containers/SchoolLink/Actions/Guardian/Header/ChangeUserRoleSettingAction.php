<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Header;

use App\Containers\SchoolLink\Contracts\SessionRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class ChangeUserRoleSettingAction.
 *
 */
class ChangeUserRoleSettingAction extends Action
{
	private $session_repository;

	public function __construct(
        SessionRepositoryInterface $session_repository
    ) {
        $this->session_repository = $session_repository;
    }

    public function run($user_role_id, $lang = null)
    {
        try {
            $app = \Session::get('app');
            if(isset($user_role_id)) {
                $session_params = [
                    'user_role_id' => $user_role_id
                ];

                if(isset($lang)) {
                    $session_params['lang'] = $lang;
                }

                $session = $app->session;

                $app->session = $this->session_repository->update($session_params, $session->id);
                \Session::put('app', $app);

                $session->loadProperties();

                $app = \Session::get('app');
                $child_role = $app->child_role;
                $school = $app->school;

                $permissions = [
                    'parent' => false,
                    'school' => false,
                    'school_sfo_subscription' => false,
                    'report_absence' => false,
                    'activities' => false,
                    'school_type' => '',
                ];

                if (isset($child_role) && $child_role->school_sfo_subscription != null) {
                    $permissions['school_sfo_subscription'] = true;
                }

                if(isset($school)){
                    $permissions['school'] = true;
                    $permissions['school_type'] = $school->type_id;

                    if($school->hasModule(Constant::MODULE_GUARDIANPOTAL_REPORT_ABSENCE)) {
                        $permissions['report_absence'] = true;
                    }

                    if($school->hasModule(Constant::MODULE_GUARDIANPOTAL_ACTIVITIES)) {
                        $permissions['activities'] = true;
                    }
                }
                $return['permissions'] = $permissions;
            } else {
                $app->user_role = null;
                $app->school = null;
                \Session::put('app', $app);

                $permissions = [
                    'parent' => true,
                    'school' => false,
                    'school_sfo_subscription' => false,
                    'report_absence' => false,
                    'activities' => false,
                    'school_type' => '',
                ];
                $return['permissions'] = $permissions;
            }
            return $return;
        } catch (Exception $ex){
            Log::error($ex);
        }

    }
}