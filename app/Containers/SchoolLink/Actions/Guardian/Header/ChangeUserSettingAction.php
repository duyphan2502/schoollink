<?php

namespace App\Containers\SchoolLink\Actions\Guardian\Header;

use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SessionRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;
use App\Core\Exception\Abstracts\Exception;

/**
 * Class ChangeUserSettingAction.
 *
 */
class ChangeUserSettingAction extends Action
{
    private $session_repository;
    private $user_role_repository;
    private $user_repository;

	public function __construct(
        SessionRepositoryInterface $session_repository,
        UserRoleRepositoryInterface $user_role_repository,
        UserRepositoryInterface $user_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository,
        SchoolOwnerRepositoryInterface $school_owner_repository
    ) {
        $this->session_repository = $session_repository;
        $this->user_repository = $user_repository;
        $this->user_role_repository = $user_role_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->school_repository = $school_repository;
        $this->school_owner_repository = $school_owner_repository;
    }

    public function run($user_role_id = null, $user_id = null)
    {
        try {
            $app = \Session::get('app');

            if(isset($user_role_id)) {
                $session_params = [
                    'user_role_id' => $user_role_id
                ];

                if(isset($lang)) {
                    $session_params['lang'] = $lang;
                }

                $session = $app->session;

                $app->session = $this->session_repository->update($session_params, $session->id);
                \Session::put('app', $app);

                $session->loadProperties();

                $app = \Session::get('app');
            } else if($user_id != null){
                $children = $this->user_repository->find($user_id);
                $child_role = $children->getRole(Constant::ROLE_PUPIL, new RoleParam(['current_year' => true]));

                if(isset($child_role)) {
                    $subgroup = $this->subgroup_repository->find($child_role->subgroup_id);
                    $group = $this->group_repository->find($subgroup->group_id);
                    $school = $this->school_repository->find($group->school_id);
                    $school_owner = $this->school_owner_repository->find($school->owner_id);
                    $academic_year = $school_owner->activeAcademicYear();

                    $app->child_role = $child_role;
                    $app->child = $children;
                    $app->subgroup = $subgroup;
                    $app->school = $school;
                    $app->group = $group;
                    //$app->school_owner = $school_owner;
                    $app->academic_year = $academic_year;
                }
            }

            \Session::put('app', $app);
            return ['result' => true];
        } catch (Exception $ex){
            Log::error($ex);
        }
    }
}