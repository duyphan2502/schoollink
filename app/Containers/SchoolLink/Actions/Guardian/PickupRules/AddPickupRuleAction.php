<?php

namespace App\Containers\SchoolLink\Actions\Guardian\PickupRules;

use App\Containers\SchoolLink\Contracts\UserMessagesRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;
use App\Core\Exception\Abstracts\Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class AddPickupRuleAction.
 *
 */
class AddPickupRuleAction extends Action
{
    private $user_messages_repository;

    public function __construct(
        UserMessagesRepositoryInterface $user_messages_repository
    ) {
        $this->user_messages_repository = $user_messages_repository;
    }

    public function run($id)
    {
        try{
            $app = \Session::get('app');
            //$child = $app->child;
            $user = $app->current_user;

            $pickupRule = $this->user_messages_repository->find($id);
            if (!$pickupRule) {
                return;
            }
            /* I will come back to continue as soon */
            $data_pickup = [
//                'date_created' => Carbon::now(),
//                'created_by' => $user->id,
//                'comment' => $description,
//                'user_id' => $child->id
            ];

            $pickup_rule_delete = $this->user_messages_repository->update($data_update, $id);
            if (strtotime($pickup_rule_delete->date_end) > time()) {
                $pickup_rule_delete->active_pickup_rules = 1;
            } else {
                $pickup_rule_delete->deactive_pickup_rules = 1;
            }
            $pickupRule->addChangeLog($user, $pickup_rule_delete, Constant::CHANGE_LOG_DELETE);
            return $pickup_rule_delete;
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}