<?php

namespace App\Containers\SchoolLink\Actions\Guardian\PickupRules;

use App\Containers\SchoolLink\Contracts\UserMessagesRepositoryInterface;
use App\Containers\SchoolLink\Criterias\PickupRules\GetActivePickupRulesCriteria;
use App\Containers\SchoolLink\Criterias\PickupRules\GetDeactivePickupRulesCriteria;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Core\Action\Abstracts\Action;
use App\Containers\SchoolLink\Constants\Constant;
use Carbon\Carbon;

/**
 * Class GetAllPickupRulesOfTypeRuleAction.
 *
 */
class GetAllPickupRulesOfTypeRuleAction extends Action
{
    private $user_messages_repository;

	public function __construct(
        UserMessagesRepositoryInterface $user_messages_repository
    ) {
        $this->user_messages_repository = $user_messages_repository;
    }

    public function run($type_pickup_rule)
    {
        $app = \Session::get('app');
        $user = $app->child;
        $user_role = $user->getRole(Constant::ROLE_PUPIL, new RoleParam(['current_year' => true]));
        $return = array();
        if($user_role){
            if ($type_pickup_rule === Constant::PICKUP_RULE_ACTIVE_RULE) {
                $active_pickup_rules = $this->user_messages_repository->pushCriteria(new GetActivePickupRulesCriteria($user->id, Constant::PICKUP_RULE_ACTIVE_RULE))->paginate(Constant::PICKUP_RULE_PAGINATION);
                $return['pickup']['active_pickup_rules'] = $active_pickup_rules;
            } else {
                $deactive_pickup_rules = $this->user_messages_repository->pushCriteria(new GetDeactivePickupRulesCriteria($user->id, Constant::PICKUP_RULE_DEACTIVE_RULE))->paginate(Constant::PICKUP_RULE_PAGINATION);
                $return['pickup']['deactive_pickup_rules'] = $deactive_pickup_rules;
            }
        }
        return $return;
    }
}