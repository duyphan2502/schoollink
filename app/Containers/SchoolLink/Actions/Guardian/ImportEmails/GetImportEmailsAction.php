<?php

namespace App\Containers\SchoolLink\Actions\Guardian\ImportEmails;


use App\Core\Action\Abstracts\Action;

/**
 * Class GetImportEmailsAction.
 *
 */
class GetImportEmailsAction extends Action
{

    public function run()
    {
        $return = [];
        $app = \Session::get('app');
        $child = $app->child;

        $return['user_emails'] = $child->getEmailsExceptApplicationType();
        return $return;
    }
}