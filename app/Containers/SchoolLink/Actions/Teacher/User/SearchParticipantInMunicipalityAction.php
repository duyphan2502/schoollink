<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 03/10/2016
 * Time: 11:44 AM
 */

namespace App\Containers\SchoolLink\Actions\Teacher\User;


use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Criterias\User\GetPupilIdsOfGroupsCriteria;
use App\Containers\SchoolLink\Criterias\User\SearchParticipantsCriteria;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;

class SearchParticipantInMunicipalityAction extends Action
{
    private $user_role_repository;
    private $user_repository;
    private $subgroup_repository;
    private $group_repository;
    private $school_repository;
    private $activity_repository;

    public function __construct(
        UserRoleRepositoryInterface $user_role_repository,
        UserRepositoryInterface $user_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository,
        ActivityRepositoryInterface $activity_repository
    ) {
        $this->user_role_repository = $user_role_repository;
        $this->user_repository = $user_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->school_repository = $school_repository;
        $this->activity_repository = $activity_repository;
    }

    public function run($id, $text)
    {
        try {
            $activity = $this->getActivitiy($id);
            if($activity == null)
                return ['result' => false];

            $users = $this->user_repository->getByCriteria(new SearchParticipantsCriteria($activity->school_owner_id, $text));
            return $users->toArray();

        } catch (Exception $e) {
            Log::error($e);
        }
        return;
    }

    /**
     * @param $id
     * @return mixed|null
     */
    private function getActivitiy($id)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;

        $activity = $this->activity_repository->find($id);

        if(!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }
        return $activity;
    }
}