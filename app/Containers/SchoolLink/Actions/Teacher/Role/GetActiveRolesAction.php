<?php

namespace App\Containers\SchoolLink\Actions\Teacher\Role;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use Carbon\Carbon;


/**
 * Class GetActiveRolesAction.
 *
 */
class GetActiveRolesAction extends Action
{
    private $user_role_repository;
    private $subgroup_repository;
    private $group_repository;
    private $school_repository;

	public function __construct(
        UserRoleRepositoryInterface $user_role_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository
    ) {
        $this->user_role_repository = $user_role_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->school_repository = $school_repository;
    }

    public function run()
    {
        $current_user = \Session::get('app')->current_user;
        $roles = $current_user->roles;

        foreach ($roles as $role) {
            switch ($role->role_id) {
                case Constant::ROLE_SFO:
                    $school = $this->school_repository->find($role->school_id);
                    if (!isset($return['schools'][(int)$school->id])) {
                        $return['schools'][(int)$school->id] = array(
                            'school_id' => (int)$school->id,
                            'school_name' => $school->name,
                            'school_SFO' => false,
                            'school_administration' => false,
                            'teacher' => false
                        );
                    }
                    $return['schools'][(int)$school->id]['school_SFO'] = true;
                    break;
                case Constant::ROLE_SCHOOL_ADMIN:
                    $school = $this->school_repository->find($role->school_id);
                    if (!isset($return['schools'][(int)$school->id])) {
                        $return['schools'][(int)$school->id] = array(
                            'school_id' => (int)$school->id,
                            'school_name' => $school->name,
                            'school_SFO' => false,
                            'school_administration' => false,
                            'teacher' => false
                        );
                    }
                    $return['schools'][(int)$school->id]['school_administration'] = true;
                    break;
                case Constant::ROLE_TEACHER:
                    $subgroup = $this->subgroup_repository->find($role->subgroup_id);
                    $group = $this->group_repository->find($subgroup->group_id);
                    $school = $this->subgroup_repository->find($group->school_id);

                    if (!isset($return['schools'][(int)$school->id])) {
                        $return['schools'][(int)$school->id] = array(
                            'school_id' => (int)$school->id,
                            'school_name' => $school->name,
                            'school_SFO' => false,
                            'school_administration' => false,
                            'teacher' => false
                        );
                    }
                    $return['schools'][(int)$school->id]['teacher'] = true;
                    break;
            }
        }

        return $return;
    }
}