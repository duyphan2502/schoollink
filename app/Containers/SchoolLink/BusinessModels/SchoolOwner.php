<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\AcademicYearRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRoleAccessLevelRepositoryInterface;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Core\Model\Abstracts\BusinessModel;
use Carbon\Carbon;

/**
 * Class SchoolOwner.
 *
 */
class SchoolOwner extends BusinessModel
{
    public function active() {
        return true;
    }

    public function activeAcademicYear()
    {
        $academic_year_repository = app()->make(AcademicYearRepositoryInterface::class);

        $active_academic_year = null;

        $academic_years = $academic_year_repository
            ->findWhere(['school_owner_id' => $this->id])
            ->sortBy('date_start');

        foreach($academic_years as $academic_year){
            if(Carbon::parse($academic_year->date_start)->getTimestamp() <= Carbon::now()->getTimestamp() && Carbon::parse($academic_year->date_end)->getTimestamp() > Carbon::now()->getTimestamp() && $academic_year->date_deleted === null)
                $active_academic_year = $academic_year;
        }
        return $active_academic_year;
    }

    public function permissionAccessSicknesses(){
        $app = \Session::get('app');
        $user = $app->current_user;

        if($user->hasRole(Constant::ROLE_MUNICIPALITY, $this->id))
            return true;
        if($user->hasRole(Constant::ROLE_SFO, new RoleParam(['school_owner_id' => $this->id])))
            return true;
        if($user->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_owner_id' => $this->id])))
            return true;
        if($user->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_owner_id' => $this->id])))
            return true;
        if($user->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_owner_id' => $this->id])))
            return true;
        return false;
    }
    public function getSchoolOwnerGuardianAccessLevels(){
        $school_owner_role_access_levels_repository = app()->make(SchoolOwnerRoleAccessLevelRepositoryInterface::class);
        $guardian_access_levels = $school_owner_role_access_levels_repository->findWhere([
            'school_owner_id' => $this->id,
            'deleted' => false,
            'role_id' => Constant::ROLE_GUARDIAN,
            'type_id' => Constant::SCHOOL_TYPE_SCHOOL
        ]);
        return $guardian_access_levels;
    }
}