<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Core\Model\Abstracts\BusinessModel;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;

/**
 * Class SubGroup.
 *
 */
class SubGroup extends BusinessModel
{
    public function active()
    {
        $group_repository = app()->make(GroupRepositoryInterface::class);
        $group = $group_repository->find($this->group_id);
        return $group->active();
    }
}
