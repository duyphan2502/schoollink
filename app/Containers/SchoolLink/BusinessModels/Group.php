<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Core\Model\Abstracts\BusinessModel;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Contracts\AcademicYearRepositoryInterface;
use Carbon\Carbon;

/**
 * Class Group.
 *
 */
class Group extends BusinessModel
{
    public function active()
    {
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $school_owner_repository = app()->make(SchoolOwnerRepositoryInterface::class);

        $school = $school_repository->find($this->school_id);
        $school_owner = $school_owner_repository->find($school->owner_id);

        $active_academic_year = $school_owner->activeAcademicYear();

        return $school->active() && isset($active_academic_year) && $active_academic_year->id == $this->academic_year_id;
    }

    public function validateAcademicYear()
    {
        $academic_year_repository = app()->make(AcademicYearRepositoryInterface::class);
        $academic_year = $academic_year_repository->find($this->academic_year_id);
        return (Carbon::parse($academic_year->date_start)->getTimestamp() <= time() && Carbon::parse($academic_year->date_end)->getTimestamp() > time() && $academic_year->date_deleted === null);
    }
}
