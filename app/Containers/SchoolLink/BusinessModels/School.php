<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Containers\SchoolLink\Contracts\SchoolModuleRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant as SummerConstant;
use App\Containers\SummerSchool\Contracts\CustomFieldRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CustomFieldValueRepositoryInterface;
use App\Core\Model\Abstracts\BusinessModel;
use MongoDB\BSON\ObjectID;

/**
 * Class School.
 *
 */
class School extends BusinessModel
{
    public function hasModule($module_name)
    {
        $school_module_repository = app()->make(SchoolModuleRepositoryInterface::class);
        $modules = $school_module_repository->findWhere(['school_id' => $this->id]);
        if(isset($modules)) {
            $result = false;
            foreach ($modules as $module) {
                if($module->module_id == $module_name) {
                    $result = true;
                    break;
                }

            }
            return $result;
        }
        return false;
    }

    public function active()
    {
        return $this->date_deleted === null;
    }

    public function academicYear()
    {
        $school_owner_repository = app()->make(SchoolOwnerRepositoryInterface::class);
        $school_owner = $school_owner_repository->find($this->owner_id);
        return $school_owner->activeAcademicYear();
    }
    public function getGpsCoordinates()
    {
        $current_school = $this;
        if(!$current_school){
            return false;
        }
        $custom_field_repository = app()->make(CustomFieldRepositoryInterface::class);
        $custom_field_value_repository = app()->make(CustomFieldValueRepositoryInterface::class);
        $custom_field = $custom_field_repository->findWhere([
            'SchoolOwnerId' => $this->owner_id,
            'UniqueName' => SummerConstant::CUSTOM_FIELD_GPS_COORDINATES
        ])->first();
        $current_school->gps_coordinates = new \stdClass();
        $current_school->gps_coordinates->latitude = 0;
        $current_school->gps_coordinates->longitude = 0;
        if(isset($custom_field->id)){
            $custom_field_value = $custom_field_value_repository->findWhere([
                'CustomFieldId' => new ObjectID($custom_field->id),
                'ResourceId.Id' => $this->id
            ])->first();
            if($custom_field_value) {
                $gps_coordinate = explode(';', $custom_field_value->value->value);
                if (!empty($gps_coordinate[1])) {
                    $current_school->gps_coordinates->latitude = trim($gps_coordinate[0]);
                    $current_school->gps_coordinates->longitude = trim($gps_coordinate[1]);
                }
            }
        }
        return $current_school;
    }
}
