<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SicknessRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserEmailRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Criterias\UserEmail\GetUserEmailByUserIdCriteria;
use App\Containers\SchoolLink\Models\RoleParam;
use App\Core\Model\Abstracts\BusinessModel;
use Illuminate\Support\Facades\App;

class User extends BusinessModel
{
    public function hasRole($role_id, $role_params = null)
    {
        return $this->getRole($role_id, $role_params) !== null;
    }
    public function getRole($role_id, $role_params = null)
    {
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);
        $user_repository = app()->make(UserRepositoryInterface::class);
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);

        if(isset($this->roles)) {
            $roles = $this->roles;
        } else {
            $roles = $user_role_repository->findWhere(['user_id' => $this->id]);
        }

        if (is_object($role_params)) {
            foreach ($roles as $role) {
                if ($role->role_id === $role_id) {
                    switch ($role->role_id) {
                        case Constant::ROLE_SFO:
                            if(isset($role_params->subgroup_id)) {
                                $subgroup = $subgroup_repository->find($role_params->subgroup_id);
                                $group = $group_repository->find($subgroup->group_id);
                                $school_id = $group->school_id;
                                if((int) $role->school_id !== (int) $school_id)
                                    break;
                            }

                            if(isset($role_params->group_id)) {
                                $group = $group_repository->find($role_params->group_id);
                                if((int) $role->school_id !== (int)$group->school_id)
                                    break;
                            }

                            if(isset($role_params->school_id)) {
                                if((int) $role->school_id !== (int)$role_params->school_id)
                                    break;
                            }

                            if(isset($role_params->school_owner_id)) {
                                $school = $school_repository->find($role->school_id);
                                if((int) $school->owner_id !== (int)$role_params->school_owner_id)
                                    break;
                            }

                            if(isset($role_params->permission)) {
                                $found = false;
                                foreach ($role_params->permission as $permission) {
                                    if ($role->hasPermission($permission) === false) {
                                        $found = true;
                                    }
                                }
                                if($found)
                                    break;
                            }

                            return $role;
                        case Constant::ROLE_SCHOOL_ADMIN:
                            if(isset($role_params->subgroup_id)) {
                                $subgroup = $subgroup_repository->find($role_params->subgroup_id);
                                $group = $group_repository->find($subgroup->group_id);
                                $school_id = $group->school_id;
                                if((int) $role->school_id !== (int) $school_id)
                                    break;
                            }

                            if(isset($role_params->group_id)) {
                                $group = $group_repository->find($role_params->group_id);
                                if((int) $role->school_id !== (int)$group->school_id)
                                    break;
                            }

                            if(isset($role_params->school_id)) {
                                if((int) $role->school_id !== (int)$role_params->school_id)
                                    break;
                            }

                            if(isset($role_params->school_owner_id)) {
                                $school = $school_repository->find($role->school_id);
                                if((int) $school->owner_id !== (int)$role_params->school_owner_id)
                                    break;
                            }

                            if(isset($role_params->permission)) {
                                $found = false;
                                foreach ($role_params->permission as $permission) {
                                    if ($role->hasPermission($permission) === false) {
                                        $found = true;
                                    }
                                }
                                if($found)
                                    break;
                            }

                            if (isset($role_params->module)) {
                                $school = $school_repository->find($role->school_id);
                                $found = false;
                                foreach ($role_params->module as $module) {
                                    if ($school->hasModule($module) === false) {
                                        $found = true;
                                    }
                                }
                                if($found)
                                    break;
                            }

                            return $role;
                        case Constant::ROLE_TEACHER:
                            if(isset($role_params->subgroup_id) && (int)$role->subgroup_id !== (int)$role_params->subgroup_id)
                                break;

                            if(isset($role_params->group_id)) {
                                $subgroup = $subgroup_repository->find($role->subgroup_id);
                                if((int)$subgroup->group_id !== (int)$role_params->group_id)
                                    break;
                            }

                            if(isset($role_params->school_id)) {
                                $subgroup = $subgroup_repository->find($role->subgroup_id);
                                $group = $group_repository->find($subgroup->group_id);
                                if((int)$group->school_id  !== (int)$role_params->school_id)
                                    break;
                            }

                            if(isset($role_params->school_owner_id)) {
                                $subgroup = $subgroup_repository->find($role->subgroup_id);
                                $group = $group_repository->find($subgroup->group_id);
                                $school = $school_repository->find($group->school_id);
                                if((int)$school->owner_id  !== (int)$role_params->school_owner_id)
                                    break;
                            }

                            if(isset($role_params->school_type)) {
                                $subgroup = $subgroup_repository->find($role->subgroup_id);
                                $group = $group_repository->find($subgroup->group_id);
                                $school = $school_repository->find($group->school_id);
                                if((int)$school->type_id  !== (int)$role_params->school_type)
                                    break;
                            }

                            if(isset($role_params->permission)) {
                                $found = false;
                                foreach ($role_params->permission as $permission) {
                                    if ($role->hasPermission($permission) === false) {
                                        $found = true;
                                    }
                                }
                                if($found)
                                    break;
                            }

                            return $role;
                        case Constant::ROLE_MUNICIPALITY:
                            if(isset($role_params->school_owner_id)) {
                                if((int) $role->school_owner_id !== (int)$role_params->school_owner_id)
                                    break;
                            }

                            if(isset($role_params->school_id)) {
                                $school = $school_repository->find($role_params->school_id);

                                if((int) $role->school_owner_id !== (int)$school->owner_id)
                                    break;
                            }

                            if(isset($role_params->group_id)) {
                                $group = $group_repository->find($role_params->group_id);
                                $school = $school_repository->find($group->school_id);

                                if((int) $role->school_owner_id !== (int)$school->owner_id)
                                    break;
                            }

                            if(isset($role_params->subgroup_id)) {
                                $subgroup = $subgroup_repository->find($role_params->subgroup_id);
                                $group = $group_repository->find($subgroup->group_id);
                                $school = $school_repository->find($group->school_id);

                                if((int) $role->school_owner_id !== (int)$school->owner_id)
                                    break;
                            }

                            if(isset($role_params->permission)) {
                                $found = false;
                                foreach ($role_params->permission as $permission) {
                                    if ($role->hasPermission($permission) === false)
                                        $found = true;
                                }
                                if($found)
                                    break;
                            }

                            return $role;
                        case Constant::ROLE_GUARDIAN:
                            $child = $user_repository->find($role->guardian_user_id);
                            if(isset($role_params->school_owner_id) && !($child->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_owner_id' => $role_params->school_owner_id]))))
                                break;
                            if(isset($role_params->school_id) && !($child->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => $role_params->school_id]))))
                                break;
                            if(isset($role_params->group_id) && !($child->hasRole(Constant::ROLE_PUPIL, new RoleParam(['group_id' => $role_params->group_id]))))
                                break;
                            if(isset($role_params->subgroup_id) && !($child->hasRole(Constant::ROLE_PUPIL, new RoleParam(['subgroup_id' => $role_params->subgroup_id]))))
                                break;

                            if(isset($role_params->permission)) {
                                $found = false;
                                foreach ($role_params->permission as $permission) {
                                    if ($role->hasPermission($permission) === false)
                                        $found = true;
                                }
                                if($found)
                                    break;
                            }

                            return $role;
                        case Constant::ROLE_PUPIL:
                            if (isset($role_params->school_sfo_subscription)) {
                                if ($role_params->school_sfo_subscription === true) {
                                    if ($role->school_sfo_subscription === null)
                                        break;
                                } else {
                                    if ($role->school_sfo_subscription !== $role_params->school_sfo_subscription)
                                        break;
                                }
                            }

                            if (isset($role_params->school_id) || isset($role_params->academic_year_id) || isset($role_params->school_owner_id) || isset($role_params->current_year)) {
                                $subgroup = $subgroup_repository->find($role->subgroup_id);
                                $group = $group_repository->find($subgroup->group_id);

                                if(isset($role_params->school_owner_id)) {
                                    $school = $school_repository->find($group->school_id);
                                    if((int)$school->owner_id !== (int)$role_params->school_owner_id)
                                        break;
                                }

                                if(isset($role_params->school_id) && (int)$group->school_id !== (int)$role_params->school_id)
                                    break;

                                if(isset($role_params->academic_year_id) && (int)$group->academic_year_id !== (int)$role_params->academic_year_id)
                                    break;

                                if (isset($role_params->current_year)) {
                                    $school = $school_repository->find($group->school_id);
                                    if (!$school)
                                        break;

                                    $academicYear = $school->academicYear();
                                    if (!$academicYear)
                                        break;

                                    if ((int)$group->academic_year_id !== (int)$academicYear->id)
                                        break;
                                }
                            }

                            if (isset($role_params->group_id)) {
                                $subgroup = $subgroup_repository->find($role->subgroup_id);
                                if((int)$subgroup->group_id !== (int)$role_params->group_id)
                                    break;
                            }


                            if (isset($role_params->subgroup_id) && (int)$role->subgroup_id !== (int)$role_params->subgroup_id) {
                                break;
                            }
                            if (isset($role_params->active) && $role->active() !== $role_params->active) {
                                break;
                            }
                            return $role;
                    }
                }
            }
        } else {
            foreach ($roles as $role) {
                if ($role->role_id === $role_id) {
                    if($role_params === null){
                        return $role;
                    }

                    $role_column = Constant::$role_columns[$role_id];

                    if ((int)$role->$role_column === (int)$role_params) {
                        return $role;
                    }
                }
            }
        }
        return null;
    }

    public function permissionAccess($element = null)
    {
        /*
            null
            adfs_id
            birthnumber
            children
            message
        */
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);

        $app = \Session::get('app');
        $current_user = $app->current_user;

        if((int)$this->id === (int)$current_user->id){
            return true;
        }

        foreach ($current_user->roles as $r) {
            switch ($r->role_id) {
                case Constant::ROLE_MUNICIPALITY:
                    switch ($element) {
                        case Constant::PERMISSION_ACCESS_ADFS_ID:
                            if ($r->hasPermission(Constant::PERMISSION_PROFILE_VIEW_ADFS_ID)
                                && ($this->hasRole( Constant::ROLE_SFO, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_owner_id' => $r->school_owner_id])))
                            ) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_ACCESS_BIRTHNUMBER:
                            if ($r->hasPermission(Constant::PERMISSION_PROFILE_VIEW_BIRTHNUMBER)
                                && ($this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_owner_id' => $r->school_owner_id])))
                            ) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_ACCESS_CHILDREN:
                            if ($this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_owner_id' => $r->school_owner_id]))) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_ACCESS_MESSAGE:
                            if (
                                $this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                || $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                            ) {
                                return true;
                            }
                            break;
                    }
                    break;
                case Constant::ROLE_SFO:
                    switch ($element) {
                        case Constant::PERMISSION_ACCESS_CHILDREN:
                            if ($this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_id' => $r->school_id]))) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_ACCESS_MESSAGE:
                            if (
                                $this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_id' => $r->school_id]))
                                || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_id' => $r->school_id]))
                                || $user->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $r->school_id]))
                                || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_id' => $r->school_id]))
                                || $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => $r->school_id]))
                                || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_id' => $r->school_id]))
                            ) {
                                return true;
                            }
                            break;
                    }
                    break;
                case Constant::ROLE_SCHOOL_ADMIN:
                    switch ($element) {
                        case Constant::PERMISSION_ACCESS_ADFS_ID:
                            if ($r->hasPermission(Constant::PERMISSION_PROFILE_VIEW_ADFS_ID)
                                && ($this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_id' => $r->school_id])))
                            ) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_ACCESS_BIRTHNUMBER:
                            if ($r->hasPermission(Constant::PERMISSION_PROFILE_VIEW_BIRTHNUMBER)
                                && ($this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_id' => $r->school_id])))
                            ) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_ACCESS_CHILDREN:
                            if ($this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_id' => $r->school_id]))) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_ACCESS_MESSAGE:
                            if (
                                $this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_id' => $r->school_id]))
                                || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_id' => $r->school_id]))
                                || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $r->school_id]))
                                || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_id' => $r->school_id]))
                                || $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => $r->school_id]))
                                || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_id' => $r->school_id]))
                            ) {
                                return true;
                            }
                            break;
                    }
                    break;
                case Constant::ROLE_TEACHER:
                    switch ($element) {
                        case Constant::PERMISSION_ACCESS_CHILDREN:
                            $subgroup = $subgroup_repository->find($r->subgroup_id);
                            if ($this->hasRole(Constant::ROLE_GUARDIAN,
                                new RoleParam(['group_id' => $subgroup->group_id]))) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_ACCESS_MESSAGE:
                            $subgroup = $subgroup_repository->find($r->subgroup_id);
                            $group = $group_repository->find($subgroup->group_id);
                            $school = $school_repository->find($group->school_id);

                            if (
                                $this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_id' => $school->id]))
                                || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_id' => $school->id]))
                                || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $school->id]))
                                || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_id' => $school->id]))
                                || $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => $school->id]))
                                || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_id' => $school->id]))
                            ) {
                                return true;
                            }
                            break;
                    }
                    break;
                case Constant::ROLE_GUARDIAN:
                    $child_roles = $user_role_repository->findWhere(['user_id' => $r->guardian_user_id]);
                    switch ($element) {
                        case Constant::PERMISSION_ACCESS_MESSAGE:
                            foreach($child_roles as $role){
                                if($role->role_id === Constant::ROLE_PUPIL){
                                    $subgroup = $subgroup_repository->find($role->subgroup_id);
                                    $group = $group_repository->find($subgroup->group_id);
                                    $school = $school_repository->find($group->school_id);

                                    if (
                                        $this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_id' => $school->id]))
                                        || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_id' => $school->id]))
                                        || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $school->id]))
                                        || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_id' => $school->id]))
                                        || $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => $school->id]))
                                        || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_id' => $school->id]))
                                    ) {
                                        return true;
                                    }
                                    break;
                                }
                            }
                    }
                    break;
            }
        }

        return false;
    }

    public function permissionEdit($element, $profile_id=null)
    {
        /*
            guardianportal_main // Can edit at least one of the fields in guardianportal main form
            adfs_id
            firstname
            surname
            email
            birthdate
            birthnumber
            gender
            email
            mobilephone
            telephone
            address
            postal_code
            postal_name
            image_id
            password
            send_password_reset_email
            student_permissions
            sicknesses
            pickup_notifications
            guardians.block_portal_access
            message_delivery_need_sms
        */
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);
        $user_repository = app()->make(UserRepositoryInterface::class);

        $app = \Session::get('app');

        $current_user = $app->current_user;

        if(isset($this->roles)) {
            $user_roles = $this->roles;
        } else {
            $user_roles = $user_role_repository->findWhere(['user_id' => $this->id]);
        }

        if ($current_user->id === $this->id && in_array($element, array(Constant::PERMISSION_EDIT_IMAGE_ID,
                Constant::PERMISSION_EDIT_PASSWORD))) {
            return true;
        }

        foreach ($current_user->roles as $r) {
            switch ($r->role_id) {
                case Constant::ROLE_MUNICIPALITY:
                    if ($current_user->id === $this->id
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_FIRSTNAME, Constant::PERMISSION_EDIT_SURNAME,
                                Constant::PERMISSION_EDIT_BIRTHDATE, Constant::PERMISSION_EDIT_EMAIL,
                                Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_TELEPHONE,
                                Constant::PERMISSION_EDIT_ADDRESS, Constant::PERMISSION_EDIT_POSTAL_CODE,
                                Constant::PERMISSION_EDIT_POSTAL_NAME, Constant::PERMISSION_EDIT_IMAGE_ID)
                        )
                    ) {
                        return true;
                    }
                    if ($current_user->hasRole(Constant::ROLE_MUNICIPALITY, $r->school_owner_id)
                        && $r->hasPermission(Constant::PERMISSION_USERS)
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_FIRSTNAME, Constant::PERMISSION_EDIT_SURNAME,
                                Constant::PERMISSION_EDIT_BIRTHDATE, Constant::PERMISSION_EDIT_EMAIL,
                                Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_TELEPHONE,
                                Constant::PERMISSION_EDIT_ADDRESS, Constant::PERMISSION_EDIT_POSTAL_CODE,
                                Constant::PERMISSION_EDIT_POSTAL_NAME, Constant::PERMISSION_EDIT_IMAGE_ID)
                        )
                    ) {
                        return true;
                    }

                    switch ($element) {
                        case Constant::PERMISSION_EDIT_ADFS_ID:
                            if ($r->hasPermission(Constant::PERMISSION_PROFILE_EDIT_ADFS_ID)
                                && ($this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_owner_id' => $r->school_owner_id])))
                            ) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_EDIT_BIRTHNUMBER:
                            if ($r->hasPermission(Constant::PERMISSION_PROFILE_EDIT_BIRTHNUMBER)
                                && ($this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_owner_id' => $r->school_owner_id]))))
                            {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_EDIT_SEND_PASSWORD_RESET_EMAIL:
                            if ($r->hasPermission(Constant::PERMISSION_PROFILE_SEND_PASSWORD_RESET_EMAIL)
                                && ($this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    ||  $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_owner_id' => $r->school_owner_id]))
                                    || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_owner_id' => $r->school_owner_id])))
                            ) {
                                return true;
                            }
                            break;
                    }

                    break;
                case Constant::ROLE_SFO:
                    if ($current_user->id === $this->id && $r->hasPermission(Constant::PERMISSION_EDIT_PROFILE_NAME_EMAIL)
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_FIRSTNAME, Constant::PERMISSION_EDIT_SURNAME,
                                Constant::PERMISSION_EDIT_BIRTHDATE, Constant::PERMISSION_EDIT_EMAIL,
                                Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_TELEPHONE,
                                Constant::PERMISSION_EDIT_ADDRESS, Constant::PERMISSION_EDIT_POSTAL_CODE,
                                Constant::PERMISSION_EDIT_POSTAL_NAME, Constant::PERMISSION_EDIT_IMAGE_ID)
                        )
                    ) {
                        return true;
                    }

                    if ($current_user->id === $this->id
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_TELEPHONE,
                                Constant::PERMISSION_EDIT_ADDRESS, Constant::PERMISSION_EDIT_POSTAL_CODE,
                                Constant::PERMISSION_EDIT_POSTAL_NAME, Constant::PERMISSION_EDIT_IMAGE_ID)
                        )
                    ) {
                        return true;
                    }

                    if (
                        $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => (int)$r->school_id]))
                        && $r->hasPermission(Constant::PERMISSION_STUDENTS)
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_FIRSTNAME, Constant::PERMISSION_EDIT_SURNAME,
                                Constant::PERMISSION_EDIT_BIRTHDATE, Constant::PERMISSION_EDIT_EMAIL,
                                Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_TELEPHONE,
                                Constant::PERMISSION_EDIT_ADDRESS, Constant::PERMISSION_EDIT_POSTAL_CODE,
                                Constant::PERMISSION_EDIT_POSTAL_NAME, Constant::PERMISSION_EDIT_IMAGE_ID)
                        )
                    ) {
                        return true;
                    }

                    if (
                        $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => (int)$r->school_id]))
                        && $r->hasPermission(Constant::PERMISSION_STUDENTS_SICKNESSES_EDIT)
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_SICKNESSES)
                        )
                    ) {
                        return true;
                    }

                    if (
                        $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => (int)$r->school_id]))
                        && $r->hasPermission(Constant::PERMISSION_EDIT_PUPIL_LEAVE_PICKEDUP_NOTIFICATION)
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_PICKUP_NOTIFICATIONS)
                        )
                    ) {
                        return true;
                    }

                    foreach ($user_roles as $role_of_this) {
                        switch ($role_of_this->role_id) {
                            case Constant::ROLE_GUARDIAN:
                                $child = $user_repository->find($role_of_this->guardian_user_id);
                                if (
                                    $child->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => (int)$r->school_id]))
                                    && $r->hasPermission(Constant::PERMISSION_STUDENTS)
                                    && in_array(
                                        $element,
                                        array(Constant::PERMISSION_EDIT_FIRSTNAME, Constant::PERMISSION_EDIT_SURNAME,
                                            Constant::PERMISSION_EDIT_MESSAGE_DELIVERY_NEED_SMS, Constant::PERMISSION_EDIT_EMAIL,
                                            Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_TELEPHONE,
                                            Constant::PERMISSION_EDIT_ADDRESS, Constant::PERMISSION_EDIT_POSTAL_CODE,
                                            Constant::PERMISSION_EDIT_POSTAL_NAME, Constant::PERMISSION_EDIT_IMAGE_ID)
                                    )
                                ) {
                                    return true;
                                }
                                if ($element === Constant::PERMISSION_EDIT_GUARDIANS_BLOCK_PORTAL_ACCESS
                                    && $r->hasPermission(Constant::PERMISSION_GUARDIANS_BLOCK_PORTAL_ACCESS)
                                    && $child->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => (int)$r->school_id])
                                    )
                                ) {
                                    return true;
                                }
                        }
                    }
                    break;
                case Constant::ROLE_SCHOOL_ADMIN:
                    if ($current_user->id === $this->id
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_FIRSTNAME, Constant::PERMISSION_EDIT_SURNAME,
                                Constant::PERMISSION_EDIT_BIRTHDATE, Constant::PERMISSION_EDIT_EMAIL,
                                Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_TELEPHONE,
                                Constant::PERMISSION_EDIT_ADDRESS, Constant::PERMISSION_EDIT_POSTAL_CODE,
                                Constant::PERMISSION_EDIT_POSTAL_NAME, Constant::PERMISSION_EDIT_IMAGE_ID)
                        )
                    ) {
                        return true;
                    }

                    if (
                        $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => (int)$r->school_id]))
                        && $r->hasPermission(Constant::PERMISSION_STUDENTS)
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_FIRSTNAME, Constant::PERMISSION_EDIT_SURNAME,
                                Constant::PERMISSION_EDIT_BIRTHDATE, Constant::PERMISSION_EDIT_EMAIL,
                                Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_TELEPHONE,
                                Constant::PERMISSION_EDIT_ADDRESS, Constant::PERMISSION_EDIT_POSTAL_CODE,
                                Constant::PERMISSION_EDIT_POSTAL_NAME, Constant::PERMISSION_EDIT_IMAGE_ID)
                        )
                    ) {
                        return true;
                    }

                    if (
                        $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => (int)$r->school_id]))
                        && $r->hasPermission(Constant::PERMISSION_STUDENTS_SICKNESSES_EDIT)
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_SICKNESSES)
                        )
                    ) {
                        return true;
                    }

                    foreach ($user_roles as $role_of_this) {
                        switch ($role_of_this->role_id) {
                            case Constant::ROLE_GUARDIAN:
                                $child = $user_repository->find($role_of_this->guardian_user_id);
                                if (
                                    $child->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => (int)$r->school_id]))
                                    && $r->hasPermission(Constant::PERMISSION_STUDENTS)
                                    && in_array(
                                        $element,
                                        array(Constant::PERMISSION_EDIT_FIRSTNAME, Constant::PERMISSION_EDIT_SURNAME,
                                            Constant::PERMISSION_EDIT_MESSAGE_DELIVERY_NEED_SMS, Constant::PERMISSION_EDIT_EMAIL,
                                            Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_TELEPHONE,
                                            Constant::PERMISSION_EDIT_ADDRESS, Constant::PERMISSION_EDIT_POSTAL_CODE,
                                            Constant::PERMISSION_EDIT_POSTAL_NAME, Constant::PERMISSION_EDIT_IMAGE_ID)
                                    )
                                ) {
                                    return true;
                                }
                                if ($element === Constant::PERMISSION_EDIT_GUARDIANS_BLOCK_PORTAL_ACCESS
                                    && $r->hasPermission(Constant::PERMISSION_GUARDIANS_BLOCK_PORTAL_ACCESS)
                                    && $child->studentOfSchool($r->school_id)
                                ) {
                                    return true;
                                }
                        }
                    }
                    switch ($element) {
                        case Constant::PERMISSION_EDIT_ADFS_ID:
                            if ($r->hasPermission(Constant::PERMISSION_PROFILE_EDIT_ADFS_ID)
                                && ($this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_id' => $r->school_id])))
                            ) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_EDIT_BIRTHNUMBER:
                            if ($r->hasPermission(Constant::PERMISSION_PROFILE_EDIT_BIRTHNUMBER)
                                && ($this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_SCHOOL_ADMIN, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_PUPIL, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_MUNICIPALITY, new RoleParam(['school_id' => $r->school_id])))
                            ) {
                                return true;
                            }
                            break;
                        case Constant::PERMISSION_EDIT_SEND_PASSWORD_RESET_EMAIL:
                            if ($r->hasPermission(Constant::PERMISSION_PROFILE_SEND_PASSWORD_RESET_EMAIL)
                                && ($this->hasRole(Constant::ROLE_SFO, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $r->school_id]))
                                    || $this->hasRole(Constant::ROLE_GUARDIAN, new RoleParam(['school_id' => $r->school_id])))
                            ) {
                                return true;
                            }
                            break;
                    }
                    break;
                case Constant::ROLE_TEACHER:
                    if ($current_user->id === $this->id && $r->hasPermission(Constant::PERMISSION_EDIT_PROFILE_NAME_EMAIL)
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_FIRSTNAME, Constant::PERMISSION_EDIT_SURNAME,
                                Constant::PERMISSION_EDIT_BIRTHDATE, Constant::PERMISSION_EDIT_EMAIL,
                                Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_TELEPHONE,
                                Constant::PERMISSION_EDIT_ADDRESS, Constant::PERMISSION_EDIT_POSTAL_CODE,
                                Constant::PERMISSION_EDIT_POSTAL_NAME, Constant::PERMISSION_EDIT_IMAGE_ID)
                        )
                    ) {
                        return true;
                    }

                    if(in_array($element, array(Constant::PERMISSION_EDIT_IMAGE_ID)) && isset($profile_id)
                        && $r->hasPermission(Constant::PERMISSION_STUDENT_PICTURE_ADMIN))
                    {
                        $student_role = $user_role_repository->findWhere(['user_id' => $profile_id]);

                        if(isset($student_role)) {
                            $access_subgroup = array();
                            foreach($user_roles as $role_of_this) {
                                if(isset($role_of_this->subgroup_id))
                                    $access_subgroup[] = $role_of_this->subgroup_id;
                            }
                            return in_array($student_role[0]['subgroup_id'], $access_subgroup);
                        }
                    }

                    foreach ($user_roles as $role_of_this) {
                        switch ($role_of_this->role_id) {
                            case Constant::ROLE_GUARDIAN:
                                $child = $user_repository->find($role_of_this->guardian_user_id);
                                if ($element === Constant::PERMISSION_EDIT_MESSAGE_DELIVERY_NEED_SMS
                                    && $child->hasRole(Constant::ROLE_STUDENT, new RoleParam(['subgroup_id' => (int)$r->subgroup_id])
                                    )
                                ) {
                                    return true;
                                }
                                if ($element === Constant::PERMISSION_EDIT_GUARDIANS_BLOCK_PORTAL_ACCESS
                                    && $r->hasPermission(Constant::PERMISSION_EDIT_GUARDIANS_BLOCK_PORTAL_ACCESS)
                                    && $child->hasRole(Constant::ROLE_STUDENT, new RoleParam(['subgroup_id' => (int)$r->subgroup_id])
                                    )
                                ) {
                                    return true;
                                }
                        }
                    }
                    break;
                case Constant::ROLE_GUARDIAN:
                    if ($current_user->id === $this->id && in_array($element, array(Constant::PERMISSION_EDIT_EMAIL,
                            Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_IMAGE_ID))) {
                        return true;
                    }
                    if ((int)$r->guardian_user_id === (int)$this->id
                        && in_array(
                            $element, array(Constant::PERMISSION_EDIT_MOBILEPHONE, Constant::PERMISSION_EDIT_IMAGE_ID,
                                Constant::PERMISSION_EDIT_STUDENT_PERMISSIONS, Constant::PERMISSION_EDIT_SICKNESSES)
                        )
                    ) {
                        return true;
                    }
                    break;
            }
        }

        return false;
    }

    public function studentOfSchool($school_id, $academic_year_id = null)
    {
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);

        if(isset($this->roles)) {
            $roles = $this->roles;
        } else {
            $roles = $user_role_repository->findWhere(['user_id' => $this->id]);
        }

        $school_id = (int)$school_id;
        $academic_year_id = $academic_year_id !== null ? (int)$academic_year_id : null;
        foreach ($roles as $r) {
            if ($r->role_id == Constant::ROLE_PUPIL) {
                $subgroup  = $subgroup_repository->find($r->subgroup_id);
                $group = $group_repository->find($subgroup->group_id);
                if ((int)$group->school_id === $school_id
                    && ($academic_year_id === null || (int)$group->academic_year_id === $academic_year_id)) {
                    return true;
                }
            }
        }
    }

    public function studentOfSubgroup($subgroup_id)
    {
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);

        if(isset($this->roles)) {
            $roles = $this->roles;
        } else {
            $roles = $user_role_repository->findWhere(['user_id' => $this->id]);
        }

        foreach ($roles as $r) {
            if ($r->role_id == Constant::ROLE_PUPIL) {
                if ($r->subgroup_id === $subgroup_id) {
                    return true;
                }
            }
        }
    }

    public function getImage($profile)
    {
        if ($this->image_id) {
            return "/img/$profile/$this->image_id";
        } else {
            return "/img/$profile/00000-00000-00000-00000";
        }
    }

    public function getShortName()
    {
        return strtoupper(substr($this->firstname, 0, 1) . substr($this->surname, 0, 1));
    }

    public function getSetting($setting)
    {
        if (isset($this->notification_settings)) {

            if (is_array($this->notification_settings) === false) {
                $this->notification_settings = unserialize($this->notification_settings);
            }
            if (isset($this->notification_settings[$setting])) {
                return $this->notification_settings[$setting];
            }
        }
        return null;
    }

    public function accessToSubgroup($subgroup_id, $permission = null)
    {
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);

        if(isset($this->roles)) {
            $roles = $this->roles;
        } else {
            $roles = $user_role_repository->findWhere(['user_id' => $this->id]);
        }

        foreach ($roles as $role) {
            if (($permission === null || $role->hasPermission($permission))
                && $role->accessToSubgroup($role, $subgroup_id)) {
                return true;
            }
        }

        return false;
    }

    public function getSicknessListOfUserRoles()
    {
        $user_repository = app()->make(UserRepositoryInterface::class);
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $school_owner_repository = app()->make(SchoolOwnerRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);
        $sickness_repository = app()->make(SicknessRepositoryInterface::class);
        $user = $user_repository->find($this->id);
        $roles = $user_role_repository->findWhere(['user_id' => $this->id]);

        if (!$user || !$user->permissionEdit(Constant::PERMISSION_EDIT_SICKNESSES)) {
            return null;
        }

        $sicknesses = array();
        $setting_include_system_sicknesses = true;
        foreach ($roles as $role) {
            if ($role->role_id == Constant::ROLE_PUPIL) {
                $subgroup = $subgroup_repository->find($role->subgroup_id);
                $group = $group_repository->find($subgroup->group_id);
                $school = $school_repository->find($group->school_id);
                $school_owner = $school_owner_repository->find($school->owner_id);

                if ($school_owner->permissionAccessSicknesses()) {
                    $sickness_ids = $sickness_repository->orderBy('name_no_NB')->findWhere(['school_owner_id' => $school_owner->id,
                        'date_replaced' => null], ['sickness']);

                    $sickness_ids = $sickness_ids->map(function ($item) {
                        return $item->sickness;
                    })->toArray();

                    $sicknesses = array_merge($sicknesses, $sickness_ids);
                    if ($school_owner->setting_include_system_sicknesses === false) {
                        $setting_include_system_sicknesses = false;
                    }
                }
            }
        }

        $return = array();

        $lang = App::getLocale();
        $lang = $lang === 'en' ? 'en_GB' : 'no_NB';
        $variable = 'name_' . $lang;
        $sicknesses = $sickness_repository->findWhereIn('sickness', $sicknesses);

        foreach ($sicknesses as $sickness) {
            $return[$sickness->sickness] = $sickness->$variable;
        }

        if ($setting_include_system_sicknesses === true) {
            foreach (Sickness::$sicknesses as $key => $sickness) {
                $return[$key] = $sickness['name'][$lang];
            }
        }

        return $return;
    }

    public function getEmailsExceptApplicationType()
    {
        $user_email_repository = app()->make(UserEmailRepositoryInterface::class);
        $user_emails_source = $user_email_repository->getByCriteria(new GetUserEmailByUserIdCriteria($this->id));
        $user_emails = [];
        if($user_emails_source){
            foreach($user_emails_source as $email) {
                $user_emails[$email->type][] = $email;
            }
        }
        return $user_emails;
    }
    public function addRole($role_id, $id){
        $role = $this->getRole($role_id, $id);
        if($role){
            return $role;
        }
        $user_roles_repository = app()->make(UserRoleRepositoryInterface::class);
        $data = [
            'user_id' => $this->id,
            'role_id' => $role_id,
            Constant::$role_columns[$role_id] => $id
        ];

        $role = $user_roles_repository->create(
            $data
        );
        
        
        return $role;
        
    }
}
