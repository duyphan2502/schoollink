<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\ChangeLogRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SicknessRepositoryInterface;
use App\Core\Model\Abstracts\BusinessModel;
use Illuminate\Support\Facades\App;

/**
 * Class UserSickness.
 *
 */
class UserSickness extends BusinessModel
{
    public static $sicknesses = array(
        'allergy.nut' => array(
            'name' => array(
                'no_NB' => 'Nøtteallergi',
                'en_GB' => 'Nut allergy'
            )
        ),
        'allergy.pollen' => array(
            'name' => array(
                'no_NB' => 'Pollenallergi',
                'en_GB' => 'Pollen allergy'
            )
        ),

        'disease.coeliac' => array(
            'name' => array(
                'no_NB' => 'Cøliaki',
                'en_GB' => 'Coeliac disease'
            )
        ),

        'diabetes.type.1' => array(
            'name' => array(
                'no_NB' => 'Diabetes type 1',
                'en_GB' => 'Diabetes type 1'
            )
        ),
        'diabetes.type.2' => array(
            'name' => array(
                'no_NB' => 'Diabetes type 2',
                'en_GB' => 'Diabetes type 2'
            )
        )

    );
    public function getName()
    {
        $sickness_repository = app()->make(SicknessRepositoryInterface::class);

        $lang = App::getLocale();
        $lang = $lang === 'en' ? 'en_GB' : 'no_NB';

        if($this->name !== null)
            return $this->name;
        if($this->custom_id !== null){
            $lang_variable = 'name_'.$lang;
            $sicknesses = $sickness_repository->orderBy('id', 'desc')
                ->findWhere(['sickness' => $this->custom_id, 'date_replaced' => null]);

            return count($sicknesses) > 0 ? $sicknesses[0]->$lang_variable : '';
        }
        if($this->system_registered_id !== null){
            if(isset(self::$sicknesses[$this->system_registered_id]['name'][$lang]))
                return self::$sicknesses[$this->system_registered_id]['name'][$lang];
        }
    }

    public function addChangeLog($user, $action)
    {
        $change_log_repository = app()->make(ChangeLogRepositoryInterface::class);
        $json_string_old = json_encode(array(
            'user_id' => $this->user_id,
            'created_by' => $this->created_by,
            'date_created' => $this->date_created,
            'name' => $this->getName(),
            'comment' => $this->comment
        ));
        $change_log_repository->create(array(
            'editor_id' => $user->id,
            'related_user_id' => $this->user_id,
            'data_type' => Constant::CHANGE_LOG_TYPE_SICKNESS,
            'old_value' => $json_string_old,
            'new_value' => '',
            'action' => $action,
        ));
    }
}
