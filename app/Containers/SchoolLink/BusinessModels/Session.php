<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SessionRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Core\Model\Abstracts\BusinessModel;

class Session extends BusinessModel
{
    public static function loadProperties()
    {
        $app = \Session::get('app');

        $app->child = null;
        $app->child_role = null;
        $app->subgroup = null;
        $app->group = null;
        $app->school = null;
        $app->school_owner = null;
        $app->academic_year = null;
        $app->user_role = null;

        $session_repository = app()->make(SessionRepositoryInterface::class);
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $school_owner_repository = app()->make(SchoolOwnerRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);
        $user_repository = app()->make(UserRepositoryInterface::class);
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);

        $user = $app->current_user;
        $session = $app->session;

        if(!isset($user->roles[$session->user_role_id]) || $user->roles[$session->user_role_id]->loginActive() == false) {
            $set_role = null;
            foreach ($user->roles as $role) {
                if ($role->loginActive()) {
                    $set_role = $role->id;
                }
            }

            if ($set_role === null) {
                $session = $session_repository
                    ->update(['user_role_id' => null], $session->id);
            } else {
                $session = $session_repository
                    ->update(['user_role_id' => $set_role], $session->id);
            }
        }
        if(isset($session->user_role_id)) {
            $user_role = $user->roles[$session->user_role_id];
            $app->user_role = $user_role;

            switch ($user_role->role_id) {
                case Constant::ROLE_MUNICIPALITY:
                    $school_owner = $school_owner_repository->find($user_role->school_owner_id);
                    $academic_year = $school_owner->activeAcademicYear();

                    $app->academic_year = $academic_year;
                    $app->school_owner = $school_owner;
                    break;

                case Constant::ROLE_SFO:
                    $school = $school_repository->find($user_role->school_id);
                    $school_owner = $school_owner_repository->find($school->owner_id);
                    $academic_year = $school_owner->activeAcademicYear();

                    $app->school = $school;
                    $app->school_owner = $school_owner;
                    $app->academic_year = $academic_year;
                    break;

                case Constant::ROLE_SCHOOL_ADMIN:
                    $school = $school_repository->find($user_role->school_id);
                    $school_owner = $school_owner_repository->find($school->owner_id);
                    $academic_year = $school_owner->activeAcademicYear();

                    $app->school = $school;
                    $app->school_owner = $school_owner;
                    $app->academic_year = $academic_year;
                    break;

                case Constant::ROLE_TEACHER:
                    $subgroup = $subgroup_repository->find($user_role->subgroup_id);
                    $group = $group_repository->find($subgroup->group_id);
                    $school = $school_repository->find($group->school_id);
                    $school_owner = $school_owner_repository->find($school->owner_id);
                    $academic_year = $school_owner->activeAcademicYear();

                    $app->subgroup = $subgroup;
                    $app->group = $group;
                    $app->school = $school;
                    $app->school_owner = $school_owner;
                    $app->academic_year = $academic_year;
                    break;

                case Constant::ROLE_GUARDIAN:
                    $children = $user_repository->find($user_role->guardian_user_id);
                    $child_roles = $user_role_repository->findWhere(['user_id' => $children->id]);

                    $children_role = null;
                    foreach ($child_roles as $role) {
                        if($role->id == $session->role_child_role_id) {
                            $children_role = $role;
                            break;
                        }
                    }

                    if(!isset($children_role)) {
                        foreach ($child_roles as $role) {
                            if ($role->role_id === Constant::ROLE_PUPIL) {
                                if ($children_role === null) {
                                    $children_role = $role;
                                } else {
                                    $subgroup = $subgroup_repository->find($children_role->subgroup_id);
                                    $group = $group_repository->find($subgroup->group_id);
                                    $school = $school_repository->find($group->school_id);
                                    $school_owner = $school_owner_repository->find($school->owner_id);
                                    $academic_year = $school_owner->activeAcademicYear();

                                    if (isset($academic_year)
                                        && $group->academic_year_id
                                        === $academic_year->id
                                    ) {
                                        $children_role = $role;
                                    }
                                }
                            }
                        }

                        if ($children_role !== null) {
                            $session = $session_repository
                                ->update(['role_child_role_id' => $children_role->id], $session->id);
                            $app->session = $session;
                        }
                    }

                    $subgroup = $subgroup_repository->find($children_role->subgroup_id);
                    $group = $group_repository->find($subgroup->group_id);
                    $school = $school_repository->find($group->school_id);
                    $school_owner = $school_owner_repository->find($school->owner_id);
                    $academic_year = $school_owner->activeAcademicYear();

                    $app->child = $children;
                    $app->child_role = $children_role;
                    $app->subgroup = $subgroup;
                    $app->group = $group;
                    $app->school = $school;
                    $app->school_owner = $school_owner;
                    $app->academic_year = $academic_year;
                    break;

                case Constant::ROLE_PUPIL:
                    $subgroup = $subgroup_repository->find($user_role->subgroup_id);
                    $group = $group_repository->find($subgroup->group_id);
                    $school = $school_repository->find($group->school_id);
                    $school_owner = $school_owner_repository->find($school->owner_id);
                    $academic_year = $school_owner->activeAcademicYear();

                    $app->child = $user;
                    $app->subgroup = $subgroup;
                    $app->group = $group;
                    $app->school = $school;
                    $app->school_owner = $school_owner;
                    $app->academic_year = $academic_year;
                    break;
            }

        }

        if(!isset($app->school_owner)) {
            $school_owner = $school_owner_repository->findWhere(['web_domain' => \Request::server("HTTP_HOST")])->first();
            $academic_year = $school_owner->activeAcademicYear();
            $app->school_owner = $school_owner;
            $app->academic_year = $academic_year;
        }

        if(strpos(\Request::root(), 'sommerskolen') != false) {
            $summer_school = $school_repository->findWhere(['name' => 'Sommerskolen', 'owner_id' => $app->school_owner->id])->first();
            $app->special_school = $summer_school;
        } else if(strpos(\Request::root(), 'dks') != false) {
            $dks_school = $school_repository->findWhere(['name' => 'dks', 'owner_id' => $app->school_owner->id])->first();
            $app->special_school = $dks_school;
        }

        \Session::put('app', $app);
    }
}
