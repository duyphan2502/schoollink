<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Contracts\AcademicYearRepositoryInterface;
use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolOwnerRoleAccessLevelPermissionRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleSettingRepositoryInterface;
use App\Core\Model\Abstracts\BusinessModel;
use Carbon\Carbon;

class UserRole extends BusinessModel
{
    public function hasPermission($p, $non_existent_permission_throw_error = true)
    {
        $school_owner_role_access_level_permission_repository = app()->make(SchoolOwnerRoleAccessLevelPermissionRepositoryInterface::class);

        if($this->school_owner_role_access_level){
            $permissions = $school_owner_role_access_level_permission_repository
                ->findWhere(['school_owner_role_access_level' => $this->school_owner_role_access_level]);

            if($non_existent_permission_throw_error === true && !isset($permissions) &&count($permissions) == 0){
                return false;
            }

            $result = false;
            foreach ($permissions as $permission) {
                if($permission->permission_id == $p) {
                    $result = (bool)$permission->value;
                    break;
                }
            }
            return $result;
        }
        return false;
    }

    public function active()
    {
        $school_owner_repository = app()->make(SchoolOwnerRepositoryInterface::class);
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $user_repository = app()->make(UserRepositoryInterface::class);
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);

        switch($this->role_id) {
            case Constant::ROLE_MUNICIPALITY:
                $school_owner = $school_owner_repository->find($this->school_owner_id);
                if (isset($school_owner)) {
                    return $school_owner->active();
                }
                return false;
            case Constant::ROLE_SFO:
            case Constant::ROLE_SCHOOL_ADMIN:
                $school = $school_repository->find($this->school_id);
                return $school->active();
            case Constant::ROLE_TEACHER:
                $subgroup = $subgroup_repository->find($this->subgroup_id);
                return $subgroup->active();
            case Constant::ROLE_PUPIL:
                $subgroup = $subgroup_repository->find($this->subgroup_id);
                return $subgroup->active();
            case Constant::ROLE_GUARDIAN:
                $user = $user_repository->find($this->guardian_user_id);
                $user_roles = $user_role_repository->findWhere(['user_id'=> $user->id]);
                foreach($user_roles as $role) {
                    if($role->role_id === Constant::ROLE_PUPIL && $role->active())
                        return true;
                }
                return false;
        }
    }

    public function loginActive()
    {
        $school_owner_repository = app()->make(SchoolOwnerRepositoryInterface::class);
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $user_repository = app()->make(UserRepositoryInterface::class);
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);

        $app = \Session::get('app');
        $session = $app->session;

        $login_method = $session->source_id ? $session->source_id : 'email_password';
        switch($this->role_id) {
            case Constant::ROLE_MUNICIPALITY:
                $school_owner = $school_owner_repository->find($this->school_owner_id);
                if (isset($school_owner)) {
                    $setting_login_methods = unserialize($school_owner->setting_login_methods);
                    return $school_owner->active() && (is_array($setting_login_methods) === false
                        || $setting_login_methods[$this->role_id][$login_method]);
                }
                return false;

            case Constant::ROLE_SFO:
                $school = $school_repository->find($this->school_id);
                $school_owner = $school_owner_repository->find($school->owner_id);

                $setting_login_methods = unserialize($school_owner->setting_login_methods);
                return $school->active() && (is_array($setting_login_methods) === false
                    || $setting_login_methods[$this->role_id][$login_method]);

            case Constant::ROLE_SCHOOL_ADMIN:
                $school = $school_repository->find($this->school_id);
                $school_owner = $school_owner_repository->find($school->owner_id);

                $setting_login_methods = unserialize($school_owner->setting_login_methods);

                return $school->active() && (is_array($setting_login_methods) === false
                    || $setting_login_methods[$this->role_id][$login_method]);

            case Constant::ROLE_TEACHER:
                $subgroup = $subgroup_repository->find($this->subgroup_id);
                $group = $group_repository->find($subgroup->group_id);
                $school = $school_repository->find($group->school_id);
                $school_owner = $school_owner_repository->find($school->owner_id);

                $setting_login_methods = unserialize($school_owner->setting_login_methods);
                return $group->validateAcademicYear() && $subgroup->active() && (is_array($setting_login_methods) === false
                    || $setting_login_methods[$this->role_id][$login_method]);

            case Constant::ROLE_PUPIL:
                $subgroup = $subgroup_repository->find($this->subgroup_id);
                $group = $group_repository->find($subgroup->group_id);
                $school = $school_repository->find($group->school_id);
                $school_owner = $school_owner_repository->find($school->owner_id);

                $setting_login_methods = unserialize($school_owner->setting_login_methods);
                return ($group->validateAcademicYear() && $subgroup->active() && (is_array($setting_login_methods) === false
                        || $setting_login_methods[$this->role_id][$login_method]));

            case Constant::ROLE_GUARDIAN:
                $user = $user_repository->find($this->user_id, ['guardianportal_access_blocked', 'id']);

                if((bool)$user->guardianportal_access_blocked === false) {
                    $user = $user_repository->find($this->guardian_user_id, ['id']);
                    $user_roles = $user_role_repository->findWhere(['user_id'=> $user->id], ['role_id', 'subgroup_id']);

                    foreach ($user_roles as $child_role) {
                        if ($child_role->role_id === Constant::ROLE_PUPIL && $child_role->active()) {
                            $subgroup = $subgroup_repository->find($child_role->subgroup_id);
                            $group = $group_repository->find($subgroup->group_id);
                            $school = $school_repository->find($group->school_id);
                            $school_owner = $school_owner_repository->find($school->owner_id);

                            $setting_login_methods = unserialize($school_owner->setting_login_methods);

                            if(($school->hasModule(Constant::MODULE_GUARDIANPOTAL) && (is_array($setting_login_methods) === false
                                    || $setting_login_methods[$child_role->role_id][$login_method])))
                                return true;
                        }
                    }
                }
                return false;
        }
    }


    public function getTitle() {
        $school_owner_repository = app()->make(SchoolOwnerRepositoryInterface::class);
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $user_repository = app()->make(UserRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $academic_year_repository = app()->make(AcademicYearRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);

        switch($this->role_id){
            case Constant::ROLE_OWNER:
                return 'Netpower Superuser';
            case Constant::ROLE_MUNICIPALITY:
                $school_owner = $school_owner_repository->find($this->school_owner_id);
                return $school_owner->name;
            case Constant::ROLE_SFO:
                $school = $school_repository->find($this->school_id);
                return $school->name.': SFO';
            case Constant::ROLE_SCHOOL_ADMIN:
                $school = $school_repository->find($this->school_id);
                return $school->name.': Administrasjon';
            case Constant::ROLE_TEACHER:
                $subgroup = $subgroup_repository->find($this->subgroup_id);
                $group = $group_repository->find($subgroup->group_id);
                $school = $school_repository->find($group->school_id);
                $academic_year = $academic_year_repository->find($group->academic_year_id);

                if($this->school_owner_role_access_level !== null){
                    return $this->access_level->name . " for " . $subgroup->name . " (" . $school->name . " - " . $academic_year->name . ")";
                } else {
                    return "Rolle mangler aksessnivå for " . $subgroup->name . " (" . $school->name . " - " . $academic_year->name . ")";
                }
            case Constant::ROLE_PUPIL:
                $subgroup = $subgroup_repository->find($this->subgroup_id);
                $group = $group_repository->find($subgroup->group_id);
                $school = $school_repository->find($group->school_id);
                $academic_year = $academic_year_repository->find($group->academic_year_id);
                return "{$subgroup -> name} - {$school -> name} ({$academic_year -> name})";
            case Constant::ROLE_GUARDIAN:
                $child = $user_repository->find($this->guardian_user_id);
                $child_current_role = $child->getRole(Constant::ROLE_PUPIL);
                return $child->firstname.($child_current_role !== null ? ' - '.$subgroup_repository->find($child_current_role->subgroup_id)->name : '');
        }
    }

    public function getImage() {
        $school_owner_repository = app()->make(SchoolOwnerRepositoryInterface::class);
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $user_repository = app()->make(UserRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);

        switch($this->role_id){
            case Constant::ROLE_MUNICIPALITY:
                $school_owner = $school_owner_repository->find($this->school_owner_id);
                return $school_owner->image_id;
            case Constant::ROLE_SFO:
            case Constant::ROLE_SCHOOL_ADMIN:
                $school = $school_repository->find($this->school_id);
                return $school->image_id;
            case Constant::ROLE_GUARDIAN:
                $children = $user_repository->find($this->guardian_user_id);
                return $children->image_id;
            case Constant::ROLE_PUPIL:
                $subgroup = $subgroup_repository->find($this->subgroup_id);
                $group = $group_repository->find($subgroup->group_id);
                $school = $school_repository->find($group->school_id);
                return $school->image_id;
        }
    }

    public function getSetting($setting_key) {
        $settings = $this->loadSettings();
        foreach ($settings as $setting) {
            if ($setting->key === $setting_key && ($setting->date_expire === null
                    || Carbon::parse($setting->date_expire)->getTimestamp() > time()))
                return $setting;
        }
        return null;
    }

    public function setSetting($setting, $value, $date_expire = null){
        $settings = $this->loadSettings();
        $user_role_settings_repository = app()->make(UserRoleSettingRepositoryInterface::class);

        $setting_params = [
            'value' => $value,
            'date_expire' => $date_expire
        ];

        foreach($settings as $s){
            if($s->key == $setting){
                $user_role_settings_repository->update($setting_params, $s->id);
                return true;
            }
        }

        $setting_params['role_id'] = $this->id;
        $setting_params['key'] = $setting;

        $user_role_settings_repository->create($setting_params);
        return true;
    }

    public function loadSettings(){
        $user_role_settings_repository = app()->make(UserRoleSettingRepositoryInterface::class);
        $settings = $user_role_settings_repository->findWhere(['role_id' => $this->id]);

        $delete = false;
        $settings_result = [];
        foreach ($settings as $setting) {
            if($setting -> date_expire === null || Carbon::parse($setting->date_expire)->getTimestamp() > time())
                $settings_result[$setting->id] = $setting;
            else
                $delete = true;
        }

        if($delete === true) {
            $user_role_settings_repository->deleteWhere([['date_expire', '<' , Carbon::now()]]);
        }

        return $settings_result;
    }

    public function accessToSubgroup($subgroup_id)
    {
        $group_repository = app()->make(GroupRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);

        $access_settings = unserialize($this->access_settings);
        if (isset($access_settings['subgroups']) && array_key_exists((int)$subgroup_id, $access_settings['subgroups'])) {
            return (bool)$access_settings['subgroups'][(int)$subgroup_id];
        }

        switch ($this->role_id) {
            case Constant::ROLE_TEACHER:
                if ((int)$this->subgroup_id === (int)$subgroup_id)
                    return true;

                $role_subgroup = $subgroup_repository->find($this->subgroup_id);
                $request_subgroup = $subgroup_repository->find($subgroup_id);

                if ($this->hasPermission(Constant::PERMISSION_ACCESS_GROUP)
                    && (int)$role_subgroup->group_id === (int)$request_subgroup->group_id)
                    return true;

                $role_group = $group_repository->find($role_subgroup->group_id);
                $request_group = $group_repository->find($request_subgroup->group_id);

                if ($this->hasPermission(Constant::PERMISSION_ACCESS_SCHOOL)
                    && (int)$role_group->school_id === (int)$request_group->school_id
                    && (int)$role_group->academic_year_id === (int)$request_group->academic_year_id)
                    return true;

                return false;

            case Constant::ROLE_SFO:
            case Constant::ROLE_SCHOOL_ADMIN:
                $subgroup = $subgroup_repository->find($subgroup_id);
                $group = $group_repository->find($subgroup->group_id);

                if ((int)$group->school_id === (int)$this->school_id) {
                    return true;
                }
        }
        return false;
    }
}
