<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Containers\SchoolLink\Constants\Constant;
use App\Core\Model\Abstracts\BusinessModel;
use Illuminate\Support\Facades\App;

/**
 * Class Sickness.
 *
 */
class Sickness extends BusinessModel
{
    public static $sicknesses = array(
        'allergy.nut' => array(
            'name' => array(
                'no_NB' => 'Nøtteallergi',
                'en_GB' => 'Nut allergy'
            )
        ),
        'allergy.pollen' => array(
            'name' => array(
                'no_NB' => 'Pollenallergi',
                'en_GB' => 'Pollen allergy'
            )
        ),

        'disease.coeliac' => array(
            'name' => array(
                'no_NB' => 'Cøliaki',
                'en_GB' => 'Coeliac disease'
            )
        ),

        'diabetes.type.1' => array(
            'name' => array(
                'no_NB' => 'Diabetes type 1',
                'en_GB' => 'Diabetes type 1'
            )
        ),
        'diabetes.type.2' => array(
            'name' => array(
                'no_NB' => 'Diabetes type 2',
                'en_GB' => 'Diabetes type 2'
            )
        )

    );

    public function userList($user_id)
    {
        $user_repository = app()->make(UserRepositoryInterface::class);
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);
        $school_repository = app()->make(SchoolRepositoryInterface::class);
        $school_owner_repository = app()->make(SchoolOwnerRepositoryInterface::class);
        $subgroup_repository = app()->make(SubGroupRepositoryInterface::class);
        $group_repository = app()->make(GroupRepositoryInterface::class);
        $sickness_repository = app()->make(SicknessRepositoryInterface::class);
        $user = $user_repository->find($user_id);
        $roles = $user_role_repository->findWhere(['user_id' => $user_id]);

        if (!$user || !$user->permissionEdit(Constant::PERMISSION_EDIT_SICKNESSES)) {
            return null;
        }

        $sicknesses = array();
        $setting_include_system_sicknesses = true;
        foreach ($roles as $role) {
            if ($role->role_id == Constant::ROLE_PUPIL) {
                $subgroup = $subgroup_repository->find($role->subgroup_id);
                $group = $group_repository->find($subgroup->group_id);
                $school = $school_repository->find($group->school_id);
                $school_owner = $school_owner_repository->find($school->owner_id);

                if ($school_owner->permissionAccessSicknesses()) {
                    $sickness_ids = $sickness_repository->orderBy('name_no_NB')->findWhere(['school_owner_id' => $school_owner->id,
                        'date_replaced' => null], ['sickness'])->toArray();

                    $sicknesses = array_merge($sicknesses, $sickness_ids);
                    if ($school_owner->setting_include_system_sicknesses === false) {
                        $setting_include_system_sicknesses = false;
                    }
                }
            }
        }

        $return = array();

        $lang = App::getLocale();
        $lang = $lang === 'en' ? 'en_GB' : 'no_NB';
        $variable = 'name_' . $lang;
        $sicknesses = $sickness_repository->findWhereIn('sickness', $sicknesses);

        foreach ($sicknesses as $sickness) {
            $return[$sickness->sickness] = $sickness->$variable;
        }

        if ($setting_include_system_sicknesses === true) {
            foreach (self::$sicknesses as $key => $sickness) {
                $return[$key] = $sickness['name'][$lang];
            }
        }

        return $return;
    }
}
