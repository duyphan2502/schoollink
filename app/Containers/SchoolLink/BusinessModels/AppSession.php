<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 08/09/2016
 * Time: 6:03 PM
 */

namespace App\Containers\SchoolLink\BusinessModels;


class AppSession
{
    public $current_user;
    public $user_role;
    public $session;
    public $school_owner;
    public $school;
    public $child;
    public $child_role;
    public $subgroup;
    public $group;
    public $academic_year;
    public $special_school;
}