<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Containers\SchoolLink\Contracts\UserMessagesRepositoryInterface;
use App\Containers\SchoolLink\Contracts\ChangeLogRepositoryInterface;
use App\Core\Model\Abstracts\BusinessModel;
use Illuminate\Support\Facades\App;

class UserMessages extends BusinessModel
{
    public function getUserMessagesOfUserByIdUser($user_id)
    {
        $user_messages_repository = app()->make(UserMessagesRepositoryInterface::class);
        $user_id = (int)$user_id;
        $user_messages  = $user_messages_repository->findWhere(['user_id' => $user_id, 'date_deleted' => NULL])->orderBy('id');
        return $user_messages->toArray();
    }
    public function addChangeLog($user, $pickupRule, $action)
    {
        $change_log_repository = app()->make(ChangeLogRepositoryInterface::class);
        $json_string_old = json_encode(array(
            'date_start' => $pickupRule->date_start,
            'date_end' => $pickupRule->date_end,
            'date_registered' => $pickupRule->date_registered
        ));
        $change_log_repository->create(array(
            'editor_id' => $user->id,
            'related_user_id' => $this->user_id,
            'data_type' => 2,
            'old_value' => $json_string_old,
            'new_value' => '',
            'action' => $action,
        ));
    }
}
