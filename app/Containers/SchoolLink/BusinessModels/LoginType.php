<?php

namespace App\Containers\SchoolLink\BusinessModels;

use App\Core\Model\Abstracts\BusinessModel;

/**
 * Class LoginType.
 *
 */
class LoginType extends BusinessModel
{
    public static $notification_slider_login_types = array(
        'school' => array(
            'sfopro_type-collected',
            'sfopro_type-gone'
        ),
        'kindergarten' => array(
            'sfopro_type-collected'
        )
    );
}
