<?php

namespace App\Containers\SchoolLink\Middlewares;

use App\Containers\SchoolLink\BusinessModels\AppSession;
use App\Containers\SchoolLink\Contracts\SessionRepositoryInterface;
use App\Containers\SchoolLink\Criterias\Session\GetActiveSessionCriteria;
use Closure;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SchoolLink\Models\RoleParam;

class AuthenticateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = isset($_COOKIE['LOGINKEY']) ? $_COOKIE['LOGINKEY'] : null;
        if($key == null)
            redirect('/');

        $this->initAppSession($key);

        return $next($request);
    }

    private function initAppSession($key)
    {
        $user_repository = app()->make(UserRepositoryInterface::class);
        $user_role_repository = app()->make(UserRoleRepositoryInterface::class);
        $session_repository = app()->make(SessionRepositoryInterface::class);

        $session = $session_repository->pushCriteria(new GetActiveSessionCriteria($key))->first();

        if ($session == null)
            //TODO: handle when session is null
            return;

        $app = \Session::get('app');
        \App::setLocale(explode("_", $session->language)[0]);

        if(isset($app) && ($session->user_id != $app->session->user_id ||
                $session->user_role_id != $app->session->user_role_id)) {
            $app = null;
        }

        if(!isset($app)) {
            $user = $user_repository->find($session->user_id);
            $roles = $user_role_repository->findWhere(['user_id' => $user->id]);
            $convert_roles = [];
            foreach ($roles as $role) {
                if(!empty($role->guardian_user_id)){
                    $child = $user_repository->find($role->guardian_user_id);
                    $child_role = $child->getRole(Constant::ROLE_PUPIL, new RoleParam(['current_year' => true]));
                    if($role->role_id == Constant::ROLE_GUARDIAN && empty($child_role)){
                        continue;
                    }
                }
                $convert_roles[$role->id] = $role;
            }
            $user->roles = $convert_roles;

            $app = new AppSession();
            $app->current_user = $user;
            $app->session = $session;
            \Session::put('app', $app);

            $session->loadProperties();
        }
    }
}
