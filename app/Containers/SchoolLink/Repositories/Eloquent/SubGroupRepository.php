<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Models\SubGroup;
use App\Containers\SchoolLink\BusinessModels\SubGroup as BusinessSubGroup;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class SchoolRepository.
 *
 */
class SubGroupRepository extends Repository implements SubGroupRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return SubGroup::class;
    }

    public function business_model()
    {
        return BusinessSubGroup::class;
    }
}
