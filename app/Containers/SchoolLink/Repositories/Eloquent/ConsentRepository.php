<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\BusinessModels\Consent as BusinessConsent;
use App\Containers\SchoolLink\Contracts\ConsentRepositoryInterface;
use App\Containers\SchoolLink\Models\Consent;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class ConsentRepository.
 *
 */
class ConsentRepository extends Repository implements ConsentRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Consent::class;
    }

    public function business_model()
    {
        return BusinessConsent::class;
    }
}
