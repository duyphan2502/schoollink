<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\ImageRepositoryInterface;
use App\Containers\SchoolLink\BusinessModels\Image as BusinessImage;
use App\Containers\SchoolLink\Models\Image;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class ImageRepository.
 *
 */
class ImageRepository extends Repository implements ImageRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Image::class;
    }

    public function business_model()
    {
        return BusinessImage::class;
    }
}
