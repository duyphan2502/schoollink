<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\SchoolModuleRepositoryInterface;
use App\Containers\SchoolLink\Models\SchoolModule;
use App\Containers\SchoolLink\BusinessModels\SchoolModule as BusinessSchoolModule;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class SchoolModuleRepository.
 *
 */
class SchoolModuleRepository extends Repository implements SchoolModuleRepositoryInterface
{

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return SchoolModule::class;
    }

    public function business_model()
    {
        return BusinessSchoolModule::class;
    }
}
