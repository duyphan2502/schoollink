<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\UserRoleSettingRepositoryInterface;
use App\Containers\SchoolLink\Models\UserRoleSetting;
use App\Containers\SchoolLink\BusinessModels\UserRoleSetting as BusinessUserRoleSetting;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class UserRoleSettingRepository.
 *
 */
class UserRoleSettingRepository extends Repository implements UserRoleSettingRepositoryInterface
{

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UserRoleSetting::class;
    }

    public function business_model()
    {
        return BusinessUserRoleSetting::class;
    }
}
