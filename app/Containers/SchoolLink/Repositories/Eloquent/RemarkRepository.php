<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\RemarkRepositoryInterface;
use App\Containers\SchoolLink\BusinessModels\Remark as BusinessRemark;
use App\Containers\SchoolLink\Models\Remark;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class RemarkRepository.
 *
 */
class RemarkRepository extends Repository implements RemarkRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Remark::class;
    }

    public function business_model()
    {
        return BusinessRemark::class;
    }
}
