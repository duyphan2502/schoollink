<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\SchoolOwnerRoleAccessLevelPermissionRepositoryInterface;
use App\Containers\SchoolLink\Models\SchoolOwnerRoleAccessLevelPermission;
use App\Containers\SchoolLink\BusinessModels\SchoolOwnerRoleAccessLevelPermission as BusinessSchoolOwnerRoleAccessLevelPermission;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class SchoolOwnerRoleAccessLevelPermissionRepository.
 *
 */
class SchoolOwnerRoleAccessLevelPermissionRepository extends Repository implements SchoolOwnerRoleAccessLevelPermissionRepositoryInterface
{

    /**
     * @var array
     */
    // protected $fieldSearchable = [
    //     'name'  => 'like',
    //     'email' => '=',
    // ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return SchoolOwnerRoleAccessLevelPermission::class;
    }

    public function business_model()
    {
        return BusinessSchoolOwnerRoleAccessLevelPermission::class;
    }

}
