<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\LoginTypeRepositoryInterface;
use App\Containers\SchoolLink\Models\LoginType;
use App\Containers\SchoolLink\BusinessModels\LoginType as BusinessLoginType;

use App\Core\Repository\Abstracts\Repository;

/**
 * Class LoginTypeRepository.
 *
 */
class LoginTypeRepository extends Repository implements LoginTypeRepositoryInterface
{

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return LoginType::class;
    }

    public function business_model()
    {
        return BusinessLoginType::class;
    }
}
