<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\BusinessModels\Form as BusinessForm;
use App\Containers\SchoolLink\Contracts\FormRepositoryInterface;
use App\Containers\SchoolLink\Models\Form;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class FormRepository.
 *
 */
class FormRepository extends Repository implements FormRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Form::class;
    }

    public function business_model()
    {
        return BusinessForm::class;
    }
}
