<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\SchoolOwnerRoleAccessLevelRepositoryInterface;
use App\Containers\SchoolLink\Models\SchoolOwnerRoleAccessLevel;
use App\Containers\SchoolLink\BusinessModels\SchoolOwnerRoleAccessLevel as BusinessSchoolOwnerRoleAccessLevel;

use App\Core\Repository\Abstracts\Repository;

/**
 * Class SchoolOwnerRoleAccessLevelRepository.
 *
 */
class SchoolOwnerRoleAccessLevelRepository extends Repository implements SchoolOwnerRoleAccessLevelRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return SchoolOwnerRoleAccessLevel::class;
    }

    public function business_model()
    {
        return BusinessSchoolOwnerRoleAccessLevel::class;
    }
}
