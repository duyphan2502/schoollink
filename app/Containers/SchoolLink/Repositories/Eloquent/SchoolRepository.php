<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Models\School;
use App\Containers\SchoolLink\BusinessModels\School as BusinessSchool;

use App\Core\Repository\Abstracts\Repository;

/**
 * Class SchoolRepository.
 *
 */
class SchoolRepository extends Repository implements SchoolRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return School::class;
    }

    public function business_model()
    {
        return BusinessSchool::class;
    }
}
