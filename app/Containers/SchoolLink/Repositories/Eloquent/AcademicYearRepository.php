<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\AcademicYearRepositoryInterface;
use App\Containers\SchoolLink\Models\AcademicYear;
use App\Containers\SchoolLink\BusinessModels\AcademicYear as BusinessAcademicYear;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class AcademicYearRepository.
 *
 */
class AcademicYearRepository extends Repository implements AcademicYearRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return AcademicYear::class;
    }

    public function business_model()
    {
        return BusinessAcademicYear::class;
    }
}
