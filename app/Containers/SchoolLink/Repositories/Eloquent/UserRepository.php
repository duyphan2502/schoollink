<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Models\User;
use App\Containers\SchoolLink\BusinessModels\User as BusinessUser;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class UserRepository.
 *
 */
class UserRepository extends Repository implements UserRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    public function business_model()
    {
        return BusinessUser::class;
    }
}
