<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\BusinessModels\UserConsent as BusinessUserConsent;
use App\Containers\SchoolLink\Contracts\UserConsentRepositoryInterface;
use App\Containers\SchoolLink\Models\UserConsent;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class UserConsentRepository.
 *
 */
class UserConsentRepository extends Repository implements UserConsentRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UserConsent::class;
    }

    public function business_model()
    {
        return BusinessUserConsent::class;
    }
}
