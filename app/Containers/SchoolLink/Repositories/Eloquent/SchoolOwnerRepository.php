<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\SchoolOwnerRepositoryInterface;
use App\Containers\SchoolLink\Models\SchoolOwner;
use App\Containers\SchoolLink\BusinessModels\SchoolOwner as BusinessSchoolOwner;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class SchoolOwnerRepository.
 *
 */
class SchoolOwnerRepository extends Repository implements SchoolOwnerRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return SchoolOwner::class;
    }

    public function business_model()
    {
        return BusinessSchoolOwner::class;
    }
}
