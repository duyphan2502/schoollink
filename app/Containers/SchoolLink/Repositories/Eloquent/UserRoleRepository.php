<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SchoolLink\Models\UserRole;
use App\Containers\SchoolLink\BusinessModels\UserRole as BusinessUserRole;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class UserRoleRepository.
 *
 */
class UserRoleRepository extends Repository implements UserRoleRepositoryInterface
{

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UserRole::class;
    }

    public function business_model()
    {
        return BusinessUserRole::class;
    }
}
