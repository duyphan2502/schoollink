<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\SicknessRepositoryInterface;
use App\Containers\SchoolLink\Models\Sickness;
use App\Containers\SchoolLink\BusinessModels\Sickness as BusinessSickness;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class SicknessRepository.
 *
 */
class SicknessRepository extends Repository implements SicknessRepositoryInterface
{

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Sickness::class;
    }

    public function business_model()
    {
        return BusinessSickness::class;
    }
}
