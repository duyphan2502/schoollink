<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Models\Group;
use App\Containers\SchoolLink\BusinessModels\Group as BusinessGroup;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class GroupRepository.
 *
 */
class GroupRepository extends Repository implements GroupRepositoryInterface
{

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Group::class;
    }

    public function business_model()
    {
        return BusinessGroup::class;
    }
}
