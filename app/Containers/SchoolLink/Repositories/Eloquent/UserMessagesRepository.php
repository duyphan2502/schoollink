<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\UserMessagesRepositoryInterface;
use App\Containers\SchoolLink\Models\UserMessages;
use App\Containers\SchoolLink\BusinessModels\UserMessages as BusinessUserMessages;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class UserMessagesRepository.
 *
 */
class UserMessagesRepository extends Repository implements UserMessagesRepositoryInterface
{

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UserMessages::class;
    }

    public function business_model()
    {
        return BusinessUserMessages::class;
    }
}
