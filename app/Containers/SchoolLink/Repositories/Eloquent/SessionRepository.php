<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\SessionRepositoryInterface;
use App\Containers\SchoolLink\Models\Session;
use App\Containers\SchoolLink\BusinessModels\Session as BusinessSession;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class SessionRepository.
 *
 */
class SessionRepository extends Repository implements SessionRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Session::class;
    }

    public function business_model()
    {
        return BusinessSession::class;
    }
}
