<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\UserSicknessRepositoryInterface;
use App\Containers\SchoolLink\Models\UserSickness;
use App\Containers\SchoolLink\BusinessModels\UserSickness as BusinessUserSickness;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class UserSicknessRepository.
 *
 */
class UserSicknessRepository extends Repository implements UserSicknessRepositoryInterface
{

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UserSickness::class;
    }

    public function business_model()
    {
        return BusinessUserSickness::class;
    }
}
