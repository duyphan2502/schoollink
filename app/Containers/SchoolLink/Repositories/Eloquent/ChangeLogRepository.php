<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\ChangeLogRepositoryInterface;
use App\Containers\SchoolLink\Models\ChangeLog;
use App\Containers\SchoolLink\BusinessModels\ChangeLog as BusinessChangeLog;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class ChangeLogRepository.
 *
 */
class ChangeLogRepository extends Repository implements ChangeLogRepositoryInterface
{

    /**
     * @var array
     */
    // protected $fieldSearchable = [
    //     'name'  => 'like',
    //     'email' => '=',
    // ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return ChangeLog::class;
    }

    public function business_model()
    {
        return BusinessChangeLog::class;
    }
}
