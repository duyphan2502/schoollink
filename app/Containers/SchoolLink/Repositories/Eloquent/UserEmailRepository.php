<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\Contracts\UserEmailRepositoryInterface;
use App\Containers\SchoolLink\BusinessModels\UserEmail as BusinessUserEmail;
use App\Containers\SchoolLink\Models\UserEmail;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class UserEmailRepository.
 *
 */
class UserEmailRepository extends Repository implements UserEmailRepositoryInterface
{

    /**
     * @var array
     */
    // protected $fieldSearchable = [
    //     'name'  => 'like',
    //     'email' => '=',
    // ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UserEmail::class;
    }

    public function business_model()
    {
        return BusinessUserEmail::class;
    }
}
