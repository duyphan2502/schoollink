<?php

namespace App\Containers\SchoolLink\Repositories\Eloquent;

use App\Containers\SchoolLink\BusinessModels\MessageTemplate as BusinessMessageTemplate;
use App\Containers\SchoolLink\Contracts\MessageTemplateRepositoryInterface;
use App\Containers\SchoolLink\Models\MessageTemplate;
use App\Core\Repository\Abstracts\Repository;

/**
 * Class MessageTemplateRepository.
 *
 */
class MessageTemplateRepository extends Repository implements MessageTemplateRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return MessageTemplate::class;
    }

    public function business_model()
    {
        return BusinessMessageTemplate::class;
    }
}
