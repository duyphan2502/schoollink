@include('js-localization::head', ['type' => 'm_schoollink'])
<!doctype html>
<html ng-app="app" ng-strict-di>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="./{!! elixir('css/vendor.css') !!}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="./{!! elixir('css/components.css') !!}">
    <link rel="stylesheet" href="./{!! elixir('css/app.schoollink.guardian.css') !!}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <title>Schoollink</title>
    @yield('js-localization.head')
    <!--[if lte IE 10]>
    <script type="text/javascript">document.location.href = '/unsupported-browser'</script>
    <![endif]-->
</head>
<body>
    <div ui-view="header"></div>
    <div ui-view="main" class="container"></div>
    <div ui-view="footer"></div>


    <script src="./{!! elixir('js/vendor.js') !!}"></script>
    <script src="./{!! elixir('js/partials.js') !!}"></script>
    <script src="./{!! elixir('js/schoollink.guardian.app.js') !!}"></script>


    {{--livereload--}}
    @if ( env('APP_ENV') === 'local' )
    <script type="text/javascript">
        document.write('<script src="'+ location.protocol + '//' + (location.host.split(':')[0] || 'localhost') +':35729/livereload.js?snipver=1" type="text/javascript"><\/script>')
    </script>
    @endif
    <script>
        angular.module("app").constant("CSRF_TOKEN", '{{ csrf_token() }}');
    </script>
</body>
</html>
