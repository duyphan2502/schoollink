<?php

namespace App\Containers\SchoolLink\UI\WEB\Controllers;

use App\Core\Controller\Abstracts\CoreWebController;

/**
 * Class Controller
 *
 */
class Controller extends CoreWebController
{

    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('schoollink-index');
    }
}
