<?php

namespace App\Containers\SchoolLink\UI\API\Transformers;

use App\Core\Transformer\Abstracts\Transformer;

class SummerSchoolHeaderSettingsTransformer extends Transformer
{
    protected $defaultIncludes  = [
        'user'
    ];

    public function transform($result)
    {
        return [
            'go_to_teacher_portal' => $result->go_to_teacher_portal,
            'roles' => $result->roles,
            'teachers' => $result->teachers,
            'permissions' => $result->permissions,
            'school_owner' => $result->school_owner,
            'is_parent' => $result->is_parent,
            'is_pupil' => $result->is_pupil
        ];
    }

    public function includeUser($result)
    {
        return $this->item($result->user, new UserTransformer());
    }
}