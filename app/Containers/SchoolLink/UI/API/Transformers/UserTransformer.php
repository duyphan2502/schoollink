<?php

namespace App\Containers\SchoolLink\UI\API\Transformers;

use App\Containers\Countries\UI\API\Transformers\CountryTransformer;
use App\Containers\SchoolLink\Models\User;
use App\Core\Transformer\Abstracts\Transformer;

class UserTransformer extends Transformer
{
    public function transform($user)
    {
        return [
            'id' => $user->id,
            'firstname' => $user->firstname,
            'surname' => $user->surname,
            'gender' => $user->gender,
            'email' => $user->email,
            'image_id' => $user->image_id,
            'source_mobilephone' => $user->source_mobilephone,
            'source_telephone'  => $user->source_telephone,
            'mobilephone' => $user->mobilephone,
            'address' => $user->address,
            'postal_code' => $user->postal_code,
            'postal_name' => $user->postal_name
        ];
    }
}