<?php

namespace App\Containers\SchoolLink\UI\API\Transformers;

use App\Core\Transformer\Abstracts\Transformer;

class DksHeaderSettingsTransformer extends Transformer
{
    protected $defaultIncludes  = [
        'user'
    ];

    public function transform($result)
    {
        return [
            'go_to_teacher_portal' => $result->go_to_teacher_portal,
            'teachers' => $result->teachers,
            'school_owner' => $result->school_owner,
        ];
    }

    public function includeUser($result)
    {
        return $this->item($result->user, new UserTransformer());
    }
}