<?php
/**
 * Created by PhpStorm.
 * User: thuan.pham
 * Date: 9/9/2016
 * Time: 11:48 AM
 */
$router->post('contactList/getContactList', ['uses' => 'ContactListController@getContactList']);
$router->put('contactList/updateShowMyContact', ['uses' => 'ContactListController@updateShowMyContact']);