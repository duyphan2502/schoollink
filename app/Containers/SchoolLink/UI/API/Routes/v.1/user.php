<?php
$router->get('user/getProfile', ['uses' => 'UserController@getProfile']);
$router->get('user/getSummerProfile', ['uses' => 'UserController@getSummerProfile']);
$router->get('user/searchParticipantInMunicipality', ['uses' => 'UserController@searchParticipantInMunicipality']);
$router->put('user/updateProfile', ['uses' => 'UserController@updateProfile']);
//$router->post('user/uploadImage', ['uses' => 'UserController@uploadImage']);
$router->put('user/uploadImage', ['uses' => 'UserController@uploadImage']);
