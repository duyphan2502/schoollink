<?php
$router->get('precaution/getPrecautionsOfPupil', [
    'uses'       => 'PrecautionController@getPrecautionsOfPupil'
]);
$router->post('precaution/addPrecautionOfPupil', [
    'uses'       => 'PrecautionController@addPrecautionOfPupil'
]);
$router->put('precaution/deletePrecautionOfPupil', [
    'uses'       => 'PrecautionController@deletePrecautionOfPupil'
]);
$router->put('precaution/confirmSicknessOfPupil', [
    'uses'       => 'PrecautionController@confirmSicknessOfPupil'
]);