<?php
$router->get('consent/getPupilConsents', [
    'uses'       => 'ConsentController@getPupilConsents'
]);
$router->put('consent/updatePupilConsents', [
    'uses'       => 'ConsentController@updatePupilConsents'
]);