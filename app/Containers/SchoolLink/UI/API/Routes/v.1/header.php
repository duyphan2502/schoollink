<?php
$router->get('header/getHeaderSettings', [
    'uses' => 'HeaderController@getHeaderSettings'
]);

$router->get('header/getSommerSchoolHeaderSettings', [
    'uses' => 'HeaderController@getSommerSchoolHeaderSettings'
]);

$router->get('header/getDksHeaderSettings', [
    'uses' => 'HeaderController@getDksHeaderSettings'
]);

$router->post('header/changeUserRoleSetting', [
    'uses' => 'HeaderController@changeUserRoleSetting'
]);

$router->post('header/changeUserSetting', [
    'uses' => 'HeaderController@changeUserSetting'
]);


