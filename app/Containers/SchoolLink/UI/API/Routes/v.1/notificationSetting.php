<?php
$router->get('notificationSetting/getNotificationSettingOfPupil', [
    'uses' => 'NotificationSettingController@getNotificationSettingOfPupil'
]);

$router->put('notificationSetting/updateNotificationSettingOfPupil', [
    'uses' => 'NotificationSettingController@updateNotificationSettingOfPupil'
]);