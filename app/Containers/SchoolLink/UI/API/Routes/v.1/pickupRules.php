<?php
$router->post('pickupRules/getPickupRules', [
    'uses' => 'PickupRulesController@getPickupRules'
]);
$router->post('pickupRules/getAllPickupRulesOfTypeRule', [
    'uses' => 'PickupRulesController@getAllPickupRulesOfTypeRule'
]);
$router->put('pickupRules/deletePickupRule', [
    'uses'       => 'PickupRulesController@deletePickupRule'
]);