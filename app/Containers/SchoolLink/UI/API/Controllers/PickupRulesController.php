<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;


use App\Containers\SchoolLink\Actions\Guardian\PickupRules\GetPickupRulesAction;
use App\Containers\SchoolLink\Actions\Guardian\PickupRules\GetAllPickupRulesOfTypeRuleAction;
use App\Containers\SchoolLink\UI\API\Requests\PickupRules\GetAllPickupRulesOfTypeRuleRequest;
use App\Containers\SchoolLink\Actions\Guardian\PickupRules\DeletePickupRuleAction;
use App\Containers\SchoolLink\UI\API\Requests\PickupRules\DeletePickupRuleRequest;
use App\Containers\SchoolLink\Actions\Guardian\PickupRules\AddPickupRuleAction;
use App\Containers\SchoolLink\UI\API\Requests\PickupRules\AddPickupRuleRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class UserMessagesController.
 *
 */
class PickupRulesController extends CoreApiController
{
    public function getPickupRules(GetPickupRulesAction $action)
    {
        $pickup_rules = $action->run();
        return  $this->response->array($pickup_rules);
    }
    public function getAllPickupRulesOfTypeRule(GetAllPickupRulesOfTypeRuleRequest $request, GetAllPickupRulesOfTypeRuleAction $action)
    {
        $pickup_rules = $action->run($request->type_pickup_rule);
        return  $this->response->array($pickup_rules);
    }
    public function deletePickupRule(DeletePickupRuleRequest $request, DeletePickupRuleAction $action)
    {
        $pickup_rules = $action->run($request->id);
        return  $this->response->array((array)$pickup_rules);
    }
    public function addPickupRule(AddPickupRuleAction $action)
    {
        $pickup_rules = $action->run();
        return  $this->response->array($pickup_rules);
    }
}
