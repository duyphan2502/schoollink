<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;

use App\Containers\SchoolLink\Actions\Guardian\Precaution\GetPrecautionsOfPupilAction;
use App\Containers\SchoolLink\Actions\Guardian\Precaution\AddPrecautionOfPupilAction;
use App\Containers\SchoolLink\UI\API\Requests\Precaution\AddPrecautionOfPupilRequest;
use App\Containers\SchoolLink\Actions\Guardian\Precaution\DeletePrecautionOfPupilAction;
use App\Containers\SchoolLink\UI\API\Requests\Precaution\DeletePrecautionOfPupilRequest;
use App\Containers\SchoolLink\Actions\Guardian\Precaution\ConfirmSicknessOfPupilAction;
use App\Containers\SchoolLink\UI\API\Requests\Precaution\ConfirmSicknessOfPupilRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class PupilController.
 *
 */
class PrecautionController extends CoreApiController
{
    public function getPrecautionsOfPupil(GetPrecautionsOfPupilAction $action)
    {
        $profile = $action->run();
        return  $this->response->array($profile);
    }
    public function addPrecautionOfPupil(AddPrecautionOfPupilRequest $request, AddPrecautionOfPupilAction $action)
    {
        $precaution = $action->run($request->name, $request->description);
        return  $this->response->array((array)$precaution);
    }
    public function deletePrecautionOfPupil(DeletePrecautionOfPupilRequest $request, DeletePrecautionOfPupilAction $action)
    {
        $precaution = $action->run($request->id);
        return  $this->response->array((array)$precaution);
    }
    public function confirmSicknessOfPupil(ConfirmSicknessOfPupilRequest $request, ConfirmSicknessOfPupilAction $action)
    {
        $precaution = $action->run($request->sickness_confirmed);
        return  $this->response->array($precaution);
    }
}
