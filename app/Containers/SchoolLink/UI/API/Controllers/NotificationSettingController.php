<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;


use App\Containers\SchoolLink\Actions\Guardian\NotificationSetting\GetNotificationSettingOfPupilAction;
use App\Containers\SchoolLink\Actions\Guardian\NotificationSetting\UpdateNotificationSettingOfPupilAction;
use App\Containers\SchoolLink\UI\API\Requests\NotificationSetting\UpdateNotificationSettingOfPupilRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class NotificationSettingController.
 *
 */
class NotificationSettingController extends CoreApiController
{
    public function getNotificationSettingOfPupil(GetNotificationSettingOfPupilAction $action)
    {
        $profile = $action->run();
        return  $this->response->array($profile);
    }

    public function updateNotificationSettingOfPupil(UpdateNotificationSettingOfPupilRequest $request,
                                                     UpdateNotificationSettingOfPupilAction $action)
    {
        $result = $action->run($request->id, $request->value);
        return  $this->response->array($result);
    }
}
