<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;

use App\Containers\SchoolLink\Actions\Guardian\Consent\GetPupilConsentsAction;
use App\Containers\SchoolLink\UI\API\Requests\Consent\UpdatePupilConsentRequest;
use App\Containers\SchoolLink\Actions\Guardian\Consent\UpdatePupilConsentAction;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class ConsentController.
 *
 */
class ConsentController extends CoreApiController
{
    public function getPupilConsents(GetPupilConsentsAction $action)
    {
		$consents = $action->run();
    	return $this->response->array($consents);
    }
    public function updatePupilConsents(UpdatePupilConsentAction $action, UpdatePupilConsentRequest $request)
    {
        $consents = $action->run($request->all());
        return  $this->response->array($consents);
    }
}
