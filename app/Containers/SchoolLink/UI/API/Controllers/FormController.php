<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;


use App\Containers\SchoolLink\Actions\Guardian\Form\GetFormsBySchoolIdAction;
use App\Containers\SchoolLink\UI\API\Requests\Form\GetFormsBySchoolIdRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class FormController.
 *
 */
class FormController extends CoreApiController
{
    public function getBySchoolId(GetFormsBySchoolIdRequest $request,GetFormsBySchoolIdAction $action)
    {
        $pickup_rules = $action->run($request->school_id);
        return  $this->response->array($pickup_rules);
    }
}
