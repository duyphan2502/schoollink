<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;


use App\Containers\SchoolLink\Actions\Guardian\Header\ChangeUserRoleSettingAction;
use App\Containers\SchoolLink\Actions\Guardian\Header\ChangeUserSettingAction;
use App\Containers\SchoolLink\Actions\Guardian\Header\GetDksHeaderSettingsAction;
use App\Containers\SchoolLink\Actions\Guardian\Header\GetHeaderSettingsAction;
use App\Containers\SchoolLink\Actions\Guardian\Header\GetSummerSchoolHeaderSettingsAction;
use App\Containers\SchoolLink\UI\API\Requests\Header\ChangeUserRoleSettingRequest;
use App\Containers\SchoolLink\UI\API\Requests\Header\ChangeUserSettingRequest;
use App\Containers\SchoolLink\UI\API\Transformers\DksHeaderSettingsTransformer;
use App\Containers\SchoolLink\UI\API\Transformers\HeaderSettingsTransformer;
use App\Containers\SchoolLink\UI\API\Transformers\SummerSchoolHeaderSettingsTransformer;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class HeaderController.
 *
 */
class HeaderController extends CoreApiController
{
    public function getHeaderSettings(GetHeaderSettingsAction $action)
    {
		$pupils = $action->run();
    	return  $this->response->item($pupils, new HeaderSettingsTransformer());
    }

    public function getSommerSchoolHeaderSettings(GetSummerSchoolHeaderSettingsAction $action)
    {
        $pupils = $action->run();
        return  $this->response->item($pupils, new SummerSchoolHeaderSettingsTransformer());
    }
    public function getDksHeaderSettings(GetDksHeaderSettingsAction $action)
    {
        $header = $action->run();
        return  $this->response->item($header, new DksHeaderSettingsTransformer());
    }

    public function changeUserRoleSetting(ChangeUserRoleSettingRequest $request, ChangeUserRoleSettingAction $action)
    {
		$result = $action->run($request->id);
    	return  $this->response->array($result);
    }

    public function changeUserSetting(ChangeUserSettingRequest $request, ChangeUserSettingAction $action)
    {
        $result = $action->run($request->id, $request->user_id);
        return  $this->response->array($result);
    }
}
