<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;


use App\Containers\SchoolLink\Actions\Guardian\Profile\GetProfileAction;
use App\Containers\SchoolLink\Actions\Guardian\Profile\GetSummerProfileAction;
use App\Containers\SchoolLink\Actions\Guardian\Profile\UpdateProfileAction;
use App\Containers\SchoolLink\Actions\Teacher\User\SearchParticipantInMunicipalityAction;
use App\Containers\SchoolLink\UI\API\Requests\Profile\UpdateProfileRequest;
use App\Containers\SchoolLink\Actions\Guardian\Profile\UploadImageProfileAction;
use App\Containers\SchoolLink\UI\API\Requests\Profile\UploadImageProfileRequest;
use App\Containers\SchoolLink\UI\API\Requests\User\SearchParticipantInMunicipalityRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class UserController.
 *
 */
class UserController extends CoreApiController
{
    public function getProfile(GetProfileAction $action)
    {
		$profile = $action->run();
    	return  $this->response->array($profile);
    }
    public function updateProfile(UpdateProfileAction $action, UpdateProfileRequest $request)
    {
        $profile = $action->run($request->all());
        $result['result'] = empty($profile) ? false : true;
        return  $this->response->array($result);
    }
    /*public function uploadImage(UploadImageProfileAction $action, UploadImageProfileRequest $request)
    {
        $profile = $action->run($request->file);
        return  $this->response->array($profile);
    }*/
    public function uploadImage(UploadImageProfileAction $action, UploadImageProfileRequest $request)
    {
        $profile = $action->run($request->all());
        return  $this->response->array($profile);
    }
    public function getSummerProfile(GetSummerProfileAction $action)
    {
        $profile = $action->run();
        return  $this->response->array($profile);
    }

    public function searchParticipantInMunicipality(SearchParticipantInMunicipalityRequest $request,
                                                    SearchParticipantInMunicipalityAction $action)
    {
        $particpants = $action->run($request->id, $request->text);
        return  $this->response->array($particpants);
    }
}
