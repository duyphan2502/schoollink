<?php
/**
 * Created by PhpStorm.
 * User: thuan.pham
 * Date: 9/9/2016
 * Time: 11:39 AM
 */

namespace App\Containers\SchoolLink\UI\API\Controllers;


use App\Containers\SchoolLink\Actions\Guardian\ContactList\GetContactListAction;
use App\Containers\SchoolLink\Actions\Guardian\ContactList\UpdateShowMyContactAction;
use App\Containers\SchoolLink\UI\API\Requests\ContactList\GetContactListRequest;
use App\Containers\SchoolLink\UI\API\Requests\ContactList\UpdateShowMyContactRequest;
use App\Core\Controller\Abstracts\CoreApiController;

class ContactListController extends CoreApiController
{
    public function getContactList(GetContactListAction $action, GetContactListRequest $request)
    {
        $contactList = $action->run($request->all());
        return $this->response->array($contactList);
    }
    public function updateShowMyContact(UpdateShowMyContactAction $action, UpdateShowMyContactRequest $request){
        $update = $action->run($request->all());
        return $this->response->array($update);
    }
}