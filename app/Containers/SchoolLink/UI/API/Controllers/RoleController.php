<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;


use App\Containers\SchoolLink\Actions\Teacher\Role\GetActiveRolesAction;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class RoleController.
 *
 */
class RoleController extends CoreApiController
{
    public function getActiveRoles(GetActiveRolesAction $action)
    {
		$roles = $action->run();
    	return  $this->response->array($roles);
    }
}
