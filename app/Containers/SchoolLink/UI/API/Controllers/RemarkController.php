<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;

use App\Containers\SchoolLink\Actions\Guardian\Remark\GetPupilRemarksAction;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class RemarkController.
 *
 */
class RemarkController extends CoreApiController
{
    public function getPupilRemarks(GetPupilRemarksAction $action)
    {
		$remarks = $action->run();
    	return $this->response->array($remarks);
    }
}
