<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;


use App\Containers\SchoolLink\Actions\Guardian\School\GetSchoolsInMunicipalityAction;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class SchoolController.
 *
 */
class SchoolController extends CoreApiController
{
    public function getSchoolsInMunicipality(GetSchoolsInMunicipalityAction $action)
    {
        $schools = $action->run();
        return  $this->response->array($schools);
    }
}
