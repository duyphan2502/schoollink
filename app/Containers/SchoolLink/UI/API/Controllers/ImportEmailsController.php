<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;


use App\Containers\SchoolLink\Actions\Guardian\ImportEmails\GetImportEmailsAction;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class ImportEmailsController.
 *
 */
class ImportEmailsController extends CoreApiController
{
    public function getImportEmails(GetImportEmailsAction $action)
    {
        $profile = $action->run();
        return  $this->response->array($profile);
    }
}
