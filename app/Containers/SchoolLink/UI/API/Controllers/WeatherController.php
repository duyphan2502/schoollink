<?php

namespace App\Containers\SchoolLink\UI\API\Controllers;

use App\Containers\SchoolLink\Actions\Guardian\Weather\GetWeatherAction;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class WeatherController.
 *
 */
class WeatherController extends CoreApiController
{
    public function getWeather(GetWeatherAction $action)
    {
		$weather = $action->run();
    	return $this->response->array($weather);
    }
}
