<?php

/**
 * Created by PhpStorm.
 * User: thuan.pham
 * Date: 9/9/2016
 * Time: 2:56 PM
 */
namespace App\Containers\SchoolLink\UI\API\Requests\ContactList;

use App\Core\Request\Abstracts\Request;

class GetContactListRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subgroup_id' => 'integer', // url parameter
        ];
    }

    public function messages()
    {
        return [
            'id.integer' => trans('message.message_id_validate_integer'),
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}