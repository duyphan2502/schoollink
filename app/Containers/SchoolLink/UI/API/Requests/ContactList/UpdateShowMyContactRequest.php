<?php
/**
 * Created by PhpStorm.
 * User: phamt
 * Date: 9/16/2016
 * Time: 12:47 AM
 */

namespace App\Containers\SchoolLink\UI\API\Requests\ContactList;


use App\Core\Request\Abstracts\Request;

class UpdateShowMyContactRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'show_my_contact' => 'integer', // url parameter
        ];
    }

    public function messages()
    {
        return [
            'show_my_contact.integer' => trans('message.message_id_validate_integer'),
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}