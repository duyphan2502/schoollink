<?php

namespace App\Containers\SchoolLink\UI\API\Requests\Profile;

use App\Core\Request\Abstracts\Request;

/**
 * Class UploadImageProfileRequest.
 *
 */
class UploadImageProfileRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $app = \Session::get('app');
        /*$rules = [
            'file' => 'required|image',
        ];*/
        $rules = [
            'image_id' => 'required',
        ];
        return $rules;
    }
    public function messages()
    {
        return [
            'file.required' => 'required'
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}
