<?php

namespace App\Containers\SchoolLink\UI\API\Requests\Profile;

use App\Core\Request\Abstracts\Request;

/**
 * Class UpdateProfileRequest.
 *
 */
class UpdateProfileRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $app = \Session::get('app');
        $rules = [
            'id'    => 'required|integer',
            'email' => 'required|email|unique:users,email,'.$this->id,
            'mobilephone' => 'max:25'
        ];
        // child does not require email
        if($app->user_role){
            $rules['email'] = 'email|unique:users,email,'.$this->id;
        }
        return $rules;
    }
    public function messages()
    {
        return [
            'id.required' => trans('message.message_id_validate_required'),
            'id.integer' => trans('message.message_id_validate_integer'),
            'email.required' => trans('message.message_email_validate_required'),
            'email.email' => trans('message.message_email_validate_email'),
            'email.unique' => trans('message.message_email_validate_unique'),
            'mobilephone.max' => trans('message.message_mobilephone_validate_max')
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}
