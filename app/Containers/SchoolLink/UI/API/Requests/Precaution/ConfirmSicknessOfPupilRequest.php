<?php

namespace App\Containers\SchoolLink\UI\API\Requests\Precaution;

use App\Core\Request\Abstracts\Request;

/**
 * Class ConfirmSicknessOfPupilRequest.
 *
 */
class ConfirmSicknessOfPupilRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sickness_confirmed'    => 'required',
        ];
    }
    public function messages()
    {
        return [
            'sickness_confirmed.required' => trans('message.message_sickness_confirmed_validate_required'),

        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}
