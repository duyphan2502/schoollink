<?php

namespace App\Containers\SchoolLink\UI\API\Requests\Precaution;

use App\Core\Request\Abstracts\Request;

/**
 * Class AddPrecautionOfPupilRequest.
 *
 */
class AddPrecautionOfPupilRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => trans('message.message_name_validate_required'),
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }


}
