<?php

/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 11/08/2016
 */
namespace App\Containers\SchoolLink\UI\API\Requests\Form;
use App\Core\Request\Abstracts\Request;

class GetFormsBySchoolIdRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id' => 'integer', // url parameter
        ];
    }

    public function messages()
    {
        return [
            'id.integer' => trans('message.message_id_validate_integer'),
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}