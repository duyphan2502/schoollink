<?php

namespace App\Containers\SchoolLink\UI\API\Requests\Header;

use App\Core\Request\Abstracts\Request;

/**
 * Class ChangeUserSettingRequest.
 *
 */
class ChangeUserSettingRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'id'    => 'required|integer', // url parameter
        ];
    }
    public function messages()
    {
        return [
//            'id.required' => trans('message.message_id_validate_required'),
//            'id.integer' => trans('message.message_id_validate_integer'),
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}
