<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 03/10/2016
 * Time: 11:43 AM
 */

namespace App\Containers\SchoolLink\UI\API\Requests\User;


use App\Core\Request\Abstracts\Request;

class SearchParticipantInMunicipalityRequest extends Request
{
    public function rules()
    {
        $rules = [
        ];
        return $rules;
    }
    public function messages()
    {
        return [
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}