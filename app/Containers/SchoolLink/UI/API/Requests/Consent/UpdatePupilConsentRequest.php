<?php

namespace App\Containers\SchoolLink\UI\API\Requests\Consent;

use App\Core\Request\Abstracts\Request;

/**
 * Class UpdatePupilConsentRequest.
 *
 */
class UpdatePupilConsentRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}
