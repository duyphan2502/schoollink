<?php

namespace App\Containers\SchoolLink\UI\API\Requests\PickupRules;

use App\Core\Request\Abstracts\Request;

/**
 * Class AddPickupRuleRequest.
 *
 */
class AddPickupRuleRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text'    => 'required',
        ];
    }
    public function messages()
    {
        return [
            'text.required' => trans('message.message_id_validate_required'),
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}
