<?php

namespace App\Containers\SchoolLink\UI\API\Requests\PickupRules;

use App\Core\Request\Abstracts\Request;

/**
 * Class GetAllPickupRulesOfTypeRuleRequest.
 *
 */
class GetAllPickupRulesOfTypeRuleRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_pickup_rule'    => 'required',
        ];
    }
    public function messages()
    {
        return [
            'type_pickup_rule.required' => trans('message.message_id_validate_required'),
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}
