<?php
namespace App\Containers\SchoolLink\Constants;

/**The first character is wrapper of constants*/

class Constant {

	static $role_columns = array(
		'A' => 'school_id',
		'B' => 'school_id',
		'C' => 'subgroup_id',
		'G' => 'guardian_user_id',
		'O' => 'school_owner_id',
		'S' => 'subgroup_id',
		'R' => 'school_id',
		'T' => 'school_id',
		'Y' => 'school_owner_id'
	);

	/** Role Constants **/

	const ROLE_SFO 			= 'A';
	const ROLE_SCHOOL_ADMIN = 'B';
	const ROLE_TEACHER 		= 'C';
	const ROLE_GUARDIAN 	= 'G';
	const ROLE_MUNICIPALITY = 'O';
	const ROLE_OWNER 		= 'N';
	const ROLE_PUPIL 		= 'S';
	const ROLE_RELATED_SCHOOL = 'T';
	const ROLE_RELATED_SCHOOL_OWNER = 'Y';

	/** End Role Constants **/


	/** Login Method Constants **/

	const LOGIN_METHOD_ADFS 		= 'adfs';
	const LOGIN_METHOD_FEIDE 		= 'feide';
	const LOGIN_METHOD_IDPORTEN 	= 'idporten';
	const LOGIN_METHOD_EMAILPASSWORD = 'email_password';

	/** End Login Method Constants **/


	/** Permission Access Constants **/

	const PERMISSION_ACCESS_ADFS_ID		= 'adfs_id';
	const PERMISSION_ACCESS_BIRTHNUMBER = 'birthnumber';
	const PERMISSION_ACCESS_CHILDREN 	= 'children';
	const PERMISSION_ACCESS_MESSAGE 	= 'message';

	/** End Permission Access Constants **/


	/** Permission Edit Constants **/

	const PERMISSION_EDIT_GUARDIANPORTAL	= 'guardianportal_main';
	const PERMISSION_EDIT_ADFS_ID 			= 'adfs_id';
	const PERMISSION_EDIT_FIRSTNAME 		= 'firstname';
	const PERMISSION_EDIT_SURNAME 			= 'surname';
	const PERMISSION_EDIT_EMAIL 			= 'email';
	const PERMISSION_EDIT_BIRTHDATE 		= 'birthdate';
	const PERMISSION_EDIT_BIRTHNUMBER 		= 'birthnumber';
	const PERMISSION_EDIT_GENDER 			= 'gender';
	const PERMISSION_EDIT_MOBILEPHONE 		= 'mobilephone';
	const PERMISSION_EDIT_TELEPHONE 		= 'telephone';
	const PERMISSION_EDIT_ADDRESS 			= 'address';
	const PERMISSION_EDIT_POSTAL_CODE 		= 'postal_code';
	const PERMISSION_EDIT_POSTAL_NAME 		= 'postal_name';
	const PERMISSION_EDIT_IMAGE_ID 			= 'image_id';
	const PERMISSION_EDIT_PASSWORD 			= 'password';
	const PERMISSION_EDIT_SEND_PASSWORD_RESET_EMAIL 	= 'send_password_reset_email';
	const PERMISSION_EDIT_STUDENT_PERMISSIONS 			= 'student_permissions';
	const PERMISSION_EDIT_SICKNESSES 					= 'sicknesses';
	const PERMISSION_EDIT_PICKUP_NOTIFICATIONS 			= 'pickup_notifications';
	const PERMISSION_EDIT_GUARDIANS_BLOCK_PORTAL_ACCESS = 'guardians.block_portal_access';
	const PERMISSION_EDIT_MESSAGE_DELIVERY_NEED_SMS 	= 'message_delivery_need_sms';
	const PERMISSION_EDIT_PUPIL_LEAVE_PICKEDUP_NOTIFICATION = 'edit_pupil_leave_pickedup_notification';
	/** End Permission Edit Constants **/


	/** Permission Constants **/

	const PERMISSION_PROFILE_VIEW_ADFS_ID			= 'profile.view.adfs_id';
	const PERMISSION_PROFILE_EDIT_ADFS_ID			= 'profile.edit.adfs_id';
	const PERMISSION_PROFILE_VIEW_BIRTHNUMBER 		= 'profile.view.birthnumber';
	const PERMISSION_PROFILE_EDIT_BIRTHNUMBER 		= 'profile.edit.birthnumber';
	const PERMISSION_USERS 							= 'users';
	const PERMISSION_PROFILE_SEND_PASSWORD_RESET_EMAIL	='profile.send_password_reset_email';
	const PERMISSION_EDIT_PROFILE_NAME_EMAIL 			= 'self.edit_profile_name_email';
	const PERMISSION_STUDENTS 							= 'students';
	const PERMISSION_STUDENTS_SICKNESSES_EDIT 			= 'students.sicknesses.edit';
	const PERMISSION_GUARDIANS_BLOCK_PORTAL_ACCESS 			= 'guardians.block_portal_access';
	const PERMISSION_STUDENT_PICTURE_ADMIN 					= 'student_picture_admin';
	const PERMISSION_DEBUGDATA  = 'DebugData';
	const PERMISSION_ACCESS_SUBGROUP = 'Access_Subgroup';
	const PERMISSION_ACCESS_GROUP = 'Access_Group';
	const PERMISSION_ACCESS_SCHOOL  = 'Access_School';

	const PERMISSION_SFO_COLLECT_RULES_MESSAGES = 'SFO_Collect_Rules_Messages';
	const PERMISSION_SFO_STUDENT_PROFILE_STATUS_VIEW = 'SFO_Student_Profile_Status_View';
	const PERMISSION_SFO_STUDENT_PROFILE_STATUS_EDIT = 'SFO_Student_Profile_Status_Edit';
	const PERMISSION_SFO_STUDENT_OVERVIEW  = 'Student_Overview';
	const PERMISSION_SFO_COLLECT_LIST = 'SFO_Collection_List';
	const PERMISSION_SFO_GROUP_CHECKIN = 'SFO_Group_CheckIn';

	const PERMISSION_MESSAGES_VIEW = 'Messages_View';
	const PERMISSION_MESSAGES_SEND = 'Messages_Send';

	/** End Permission Constants **/


	/** Module Constants **/

	const MODULE_GUARDIANPOTAL = 'guardianportal';
	const MODULE_GUARDIANPOTAL_REPORT_ABSENCE = 'guardianportal.report_absence';
	const MODULE_GUARDIANPOTAL_ACTIVITIES = 'guardianportal.activities';
	const MODULE_NOTIFICATION_WHEN_LEAVE_PICKED_UP = 'notification_when_leave_picked_up';

	/** End Module Constants **/
	/** Change Log Constants **/
	const CHANGE_LOG_UPDATE = 'U';
	const CHANGE_LOG_ADD = 'A';
	const CHANGE_LOG_DELETE = 'D';
	const CHANGE_LOG_TYPE_PICKUP_MESSAGE = 0;
	const CHANGE_LOG_TYPE_PICKUP_RULES = 1;
	const CHANGE_LOG_TYPE_SICKNESS = 2;
	const CHANGE_LOG_TYPE_USER_CONSENT = 3;
	/** End Change Log Constants **/
	/** User Email Constants **/
	const USER_EMAIL_SOURCE_APPLICATION = 'Source_Application';
	/** End User Email Constants **/
	/** Remark Constants **/
	const REMARK_STATUS_FINAL = 'final';
	const REMARK_NEGATIVE = 'Negative';
	/** End Remark Constants **/
	/** School Constants **/
	const SCHOOL_TYPE_KINDERGARTEN = 'kindergarten';
	const SCHOOL_TYPE_SCHOOL = 'school';
	/** End School Constants **/

	/** Pickup Rules Constants **/
	const PICKUP_RULE_PAGINATION = '1';
	const PICKUP_RULE_ACTIVE_RULE = 'active_pickup_rules';
	const PICKUP_RULE_DEACTIVE_RULE = 'deactive_pickup_rules';
	/** End Pickup Rules Constants **/

}


