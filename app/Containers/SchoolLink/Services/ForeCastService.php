<?php
/**
 * Created by PhpStorm.
 * User: Nhan Vu
 * Date: 05/09/2016
 * Time: 1:23 PM
 */

namespace App\Containers\SchoolLink\Services;


use App\Containers\SchoolLink\Contracts\ForeCastServiceInterface;
use App\Core\Exception\Abstracts\Exception;
use App\Core\Services\Abstracts\Service;

class ForeCastService extends Service implements ForeCastServiceInterface
{
    public function getForeCast($school)
    {
        try {
            $yr = $this->file_get_contents_curl($school->yr."varsel.xml");
            if($yr != false){
                $yr_data = array();
                $yr = new \SimpleXMLElement($yr);
                foreach($yr->forecast->tabular->time as $t){
                    $timestamp = strtotime($t['from']);
                    $yr_data[$timestamp] = array(
                        'temperature' => (string) $t -> temperature[0]['value'],
                        'symbol' => (string) $t -> symbol[0]['number'],
                        'name' => (string) $t -> symbol[0]['name'],
                        'value' => (string) $t -> symbol[0]['var']);
                }
                $yr_data_perfect = null;
                foreach($yr_data as $k => $v) {
                    $daytime = mktime(0, 0, 0, date("m", $k), date("d", $k), date("Y", $k));
                    if (date('d.m.Y', $daytime) == date('d.m.Y', time() + 24 * 3600))
                    {
                        if(date("G:i:00", $k) === "12:00:00")
                            $yr_data_perfect = $v;
                    }
                }
                return $yr_data_perfect;
            }
        }
        catch(Exception  $e){
            Log::error($e);
            return "";
        }
    }

    protected function file_get_contents_curl($url, $timeout = 3) {
        return @file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'timeout' => 2))));
    }
}