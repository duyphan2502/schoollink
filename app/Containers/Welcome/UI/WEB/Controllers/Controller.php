<?php

namespace App\Containers\Welcome\UI\WEB\Controllers;

use App\Core\Controller\Abstracts\CoreWebController;

/**
 * Class Controller
 *
 */
class Controller extends CoreWebController
{

    /**
     * @return  string
     */
    public function index()
    {
        return view('index');
    }
}
