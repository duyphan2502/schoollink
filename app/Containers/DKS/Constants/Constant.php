<?php
namespace App\Containers\DKS\Constants;

class Constant
{
	const TYPE_SCHOOL_SFO = 'SchoolSFO';
    const TYPE_SCHOOL_ADMIN = 'SchoolAdministration';
    const TYPE_TEACHER = 'Teacher';

    const COURSE_TYPE_DKS = 'DKS';
    const COURSE_TYPE_SOMMERSKOLEN = 'SommerSkolen';
    const COURSE_TYPE_CODE_NORMAL = 'Normal';
    const COURSE_TYPE_CODE_SPECIAL = 'Special';

    const ACTIVITY_PARTICIPANT_ACTION_DELETE = 'delete';

    const PARTICIPANT_STATUS_REGISTERED = 'Registered';
    const PARTICIPANT_STATUS_ALLOCATED = 'Allocated';
    const PARTICIPANT_STATUS_BOOKED_DIRECTLY = 'BookedDirectly';
    const PARTICIPANT_STATUS_WAITING_LIST = 'WaitingList';
}