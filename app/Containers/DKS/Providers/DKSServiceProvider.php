<?php

namespace App\Containers\SummerSchool\Providers;

#register_repository_contract
use App\Core\Provider\Abstracts\ServiceProviderAbstract;

#register_repository_eloquent

/**
 * Class SummerSchoolServiceProvider.
 *
 * The Main Service Provider of this Module.
 * Will be automatically registered in the framework after
 * adding the Module name to containers config file.
 *
 */
class DKSServiceProvider extends ServiceProviderAbstract
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Container internal Service Provides.
     *
     * @var array
     */
    private $containerServiceProviders = [
    ];

    /**
     * Perform post-registration booting of services.
     */
    public function boot()
    {
        $this->registerServiceProviders($this->containerServiceProviders);
    }

    /**
     * Register bindings in the container.
     */
    public function register()
    {
        #binding_repository_flag
    }
}
