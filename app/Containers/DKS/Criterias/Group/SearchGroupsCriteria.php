<?php

namespace App\Containers\DKS\Criterias\Group;

use App\Core\Criterias\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 03/11/2016
 * Time: 4:46 PM
 */
class SearchGroupsCriteria extends Criteria
{
    private $group_ids;
    private $name;

    public function __construct($group_ids, $name)
    {
        $this->group_ids = $group_ids;
        $this->name = $name;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        $name = $this->name;
        return
            $model
                ->whereIn('group_id', $this->group_ids)
                ->where(function($query) use($name) {
                    $query->where('name', 'like', '%'.$name.'%');
                })
                ->orderBy('name');
    }
}