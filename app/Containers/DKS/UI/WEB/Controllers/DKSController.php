<?php

namespace App\Containers\DKS\UI\WEB\Controllers;

use App\Core\Controller\Abstracts\CoreWebController;
use Carbon\Carbon;

/**
 * Class SummerSchoolController
 *
 */
class DKSController extends CoreWebController
{

    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('dks-index');
    }

    public function admin()
    {
        return view('dks-admin');
    }
}
