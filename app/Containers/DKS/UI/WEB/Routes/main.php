<?php

$router->get('/dks', [
    'uses' => 'DKSController@index',
]);


$router->get('/dks/admin', [
    'uses' => 'DKSController@admin',
]);