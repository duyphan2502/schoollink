<?php

$router->post('dks-activity/add', [ 'uses' => 'DksActivityController@add']);
$router->post('dks-activity/update', [ 'uses' => 'DksActivityController@update']);
$router->post('dks-activity/clone', [ 'uses' => 'DksActivityController@copy']);

$router->get('dks-activity/getTourPlan', [ 'uses' => 'DksActivityController@getTourPlan']);
$router->get('dks-activity/getActivitiesByEnrollmentGroupId', [ 'uses' => 'DksActivityController@getActivitiesByEnrollmentGroupId']);