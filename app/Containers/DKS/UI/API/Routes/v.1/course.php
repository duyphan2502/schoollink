<?php

$router->get('dks-course/getByCourseTypeId',
    ['uses' => 'DksCourseController@getByCourseTypeId']
);


$router->post('dks-course/update', [ 'uses' => 'DksCourseController@update']);