<?php

$router->get('dks-enrollmentGroup/getByCourseTypeId', [ 'uses' => 'DksEnrollmentGroupController@getByCourseTypeId']);

$router->post('enrollmentGroup/getDksBookingData', [ 'uses' => 'GroupController@getDksBookingData']);