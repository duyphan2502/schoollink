<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 23/11/2016
 * Time: 5:55 PM
 */

namespace App\Containers\DKS\UI\API\Controllers;


use App\Containers\SummerSchool\Actions\EnrollmentGroup\GetEnrollmentGroupsByCourseTypeIdAction;
use App\Containers\SummerSchool\UI\API\Controllers\EnrollmentGroupController;
use App\Containers\SummerSchool\UI\API\Requests\EnrollmentGroup\GetEnrollmentGroupsByCourseTypeIdRequest;

class DksEnrollmentGroupController extends EnrollmentGroupController
{
    public function getByCourseTypeId(GetEnrollmentGroupsByCourseTypeIdRequest $request, GetEnrollmentGroupsByCourseTypeIdAction $action)
    {
        $groups = $action->run($request);
        return  $this->response->array($groups);
    }
}