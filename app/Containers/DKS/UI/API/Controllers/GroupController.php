<?php

namespace App\Containers\DKS\UI\API\Controllers;

use App\Containers\DKS\Actions\Group\GetDksBookingDataAction;
use App\Containers\DKS\Actions\Group\SearchGroupInMunicipalityAction;
use App\Containers\DKS\UI\API\Requests\Group\GetGroupsByClassRequest;
use App\Containers\DKS\UI\API\Requests\Group\SearchGroupInMunicipalityRequest;
use App\Core\Controller\Abstracts\CoreApiController;

/**
 * Class CourseController.
 *
 */
class GroupController extends CoreApiController
{
	public function search(SearchGroupInMunicipalityRequest $request, SearchGroupInMunicipalityAction $action)
    {
        $result = $action->run($request->id, $request->text);
        return  $this->response->array($result);
    }
    public function getDksBookingData(GetDksBookingDataAction $action, GetGroupsByClassRequest $request)
    {
        $result = $action->run($request->class_id);
        return  $this->response->array($result);
    }
}
