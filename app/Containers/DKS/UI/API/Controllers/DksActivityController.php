<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 08/11/2016
 * Time: 5:42 PM
 */

namespace App\Containers\DKS\UI\API\Controllers;

use App\Containers\DKS\Actions\Activity\DksAddActivityAction;
use App\Containers\DKS\Actions\Activity\DksCopyActivityAction;
use App\Containers\DKS\Actions\Activity\DksGetAllByEnrollmentGroupIdAction;
use App\Containers\DKS\Actions\Activity\DksUpdateActivityAction;
use App\Containers\DKS\Actions\Activity\GetTourPlanAction;
use App\Containers\DKS\UI\API\Requests\Activity\GetTourPlanRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\AddActivityRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\CopyActivityRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\GetAllByEnrollmentGroupIdRequest;
use App\Containers\SummerSchool\UI\API\Requests\Activity\UpdateActivityRequest;
use App\Core\Controller\Abstracts\CoreApiController;

class DksActivityController extends CoreApiController
{
    public function add(AddActivityRequest $request, DksAddActivityAction $action)
    {
        $activityId = $action->run($request->enrollment_group_id, $request->course_id);
        return $this->response->array($activityId);
    }

    public function update(UpdateActivityRequest $request, DksUpdateActivityAction $action)
    {
        $activity = $action->run($request->all());
        return $this->response->array($activity);
    }

    public function getTourPlan(GetTourPlanRequest $request, GetTourPlanAction $action)
    {
        $activities = $action->run($request->all());
        return $this->response->array($activities);
    }

    public function getActivitiesByEnrollmentGroupId(GetAllByEnrollmentGroupIdRequest $request, DksGetAllByEnrollmentGroupIdAction $action)
    {
        $activities = $action->run($request->enrollment_group_id);
        return $this->response->array($activities);
    }

    public function copy(CopyActivityRequest $request, DksCopyActivityAction $action)
    {
        $activity = $action->run($request->all());
        return $this->response->array($activity);
    }
}