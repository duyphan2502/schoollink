<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 08/11/2016
 * Time: 5:42 PM
 */

namespace App\Containers\DKS\UI\API\Controllers;

use App\Containers\DKS\Actions\Course\DksGetCoursesByCourseTypeIdAction;
use App\Containers\DKS\Actions\Course\DksUpdateCourseAction;
use App\Containers\SummerSchool\UI\API\Requests\Course\GetCoursesByCourseTypeIdRequest;
use App\Containers\SummerSchool\UI\API\Requests\Course\UpdateCourseRequest;
use App\Core\Controller\Abstracts\CoreApiController;

class DksCourseController extends CoreApiController
{
    public function getByCourseTypeId(GetCoursesByCourseTypeIdRequest $request, DksGetCoursesByCourseTypeIdAction $action)
    {
        $courses = $action->run($request->id);
        return  $this->response->array($courses);
    }

    public function update(UpdateCourseRequest $request, DksUpdateCourseAction $action)
    {
        $course_id = $action->run($request->all());
        return  $this->response->array($course_id);
    }
}