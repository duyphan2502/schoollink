<?php

namespace App\Containers\DKS\UI\API\Requests\Group;

use App\Core\Request\Abstracts\Request;

class GetGroupsByClassRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class_id' => 'required'
        ];
    }
    

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}