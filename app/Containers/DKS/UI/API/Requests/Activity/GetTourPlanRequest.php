<?php

namespace App\Containers\DKS\UI\API\Requests\Activity;

use App\Core\Request\Abstracts\Request;

class GetTourPlanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_id' => 'required',
            'enrollment_group_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'enrollment_group_id.required' => trans('message.message_id_validate_required'),
            'course_id.required' => trans('message.message_id_validate_required')
        ];
    }

    /**
     * Override the all() to automatically apply validation rules to the URL parameters
     *
     * @return  array
     */
    public function all()
    {
        $data = parent::all();
        return $data;
    }
}