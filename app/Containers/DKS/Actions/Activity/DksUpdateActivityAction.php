<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\DKS\Actions\Activity;

use App\Containers\SummerSchool\Actions\Activity\UpdateActivityAction;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

class DksUpdateActivityAction extends UpdateActivityAction
{
    protected function updateActivity($activity_id, $activity)
    {
        $data = array();
        $exist_activity = $this->activity_repository->find($activity_id);

        $app = \Session::get('app');
        $current_user = $app->current_user;

        if (!isset($exist_activity) || !$exist_activity->permissionEdit($current_user)) {
            return null;
        }

        $data['ActivatedBy'] = $current_user->id;
        $data['DateActivated'] = new UTCDatetime(Carbon::now()->getTimestamp() * 1000);

        if (isset($activity['date_start'])) {
            $data['DateStart'] = new UTCDatetime(Carbon::parse($activity['date_start'])->getTimestamp() * 1000);
            $data['DateEnd'] = new UTCDatetime(Carbon::parse($activity['date_start'])->getTimestamp() * 1000);
        }

        //for workshop
        if (isset($activity['date_range'])) {
            $data['DateStart'] = isset($activity['date_range']['startDate']) ? new UTCDatetime(Carbon::parse($activity['date_range']['startDate'])->getTimestamp() * 1000) : null;
            $data['DateEnd'] = isset($activity['date_range']['endDate']) ? new UTCDatetime(Carbon::parse($activity['date_range']['endDate'])->getTimestamp() * 1000) : null;
        }

        if (isset($activity['course_id'])) {
            $course = $this->course_repository->find($activity['course_id']);
            if ($course) {
                $data['CourseId'] = new ObjectID($course->id);
            }

            if($exist_activity->course_id != $data['CourseId']) {
                $is_default = true;
                $has_default_activity = $this->hasDefaultActivity($exist_activity->enrollment_group_id, $data['CourseId']);
                if ($has_default_activity) {
                    $is_default = false;
                }
                $data['IsDefault'] = $is_default;
            }
        }

        $this->setData($data, $activity, 'Name', 'name');
        $this->setData($data, $activity, 'Description', 'description');
        $this->setData($data, $activity, 'AllowWaitingQueue', 'allow_waiting_queue');

        $this->setData($data, $activity, 'MondayTimeStart', 'monday_time_start');
        $this->setData($data, $activity, 'MondayTimeEnd', 'monday_time_end');

        $this->setData($data, $activity, 'TuesdayTimeStart', 'tuesday_time_start');
        $this->setData($data, $activity, 'TuesdayTimeEnd', 'tuesday_time_end');

        $this->setData($data, $activity, 'WednesdayTimeStart', 'wednesday_time_start');
        $this->setData($data, $activity, 'WednesdayTimeEnd', 'wednesday_time_end');

        $this->setData($data, $activity, 'ThursdayTimeStart', 'thursday_time_start');
        $this->setData($data, $activity, 'ThursdayTimeEnd', 'thursday_time_end');

        $this->setData($data, $activity, 'FridayTimeStart', 'friday_time_start');
        $this->setData($data, $activity, 'FridayTimeEnd', 'friday_time_end');

        $this->setData($data, $activity, 'SaturdayTimeStart', 'saturday_time_start');
        $this->setData($data, $activity, 'SaturdayTimeEnd', 'saturday_time_end');

        $this->setData($data, $activity, 'SundayTimeStart', 'sunday_time_start');
        $this->setData($data, $activity, 'SundayTimeEnd', 'sunday_time_end');

        $this->setData($data, $activity, 'NumberOfGroups', 'number_of_groups');

        $options_data = array();
        if (isset($activity['options']['summer_school_school_id'])) {
            $summer_school = $this->school_repository->find($activity['options']['summer_school_school_id']);

            if ($summer_school && $summer_school->owner_id == $activity['school_owner_id']) {
                $options_data['SummerSchoolSchoolId'] = (int)$summer_school->id;
            }
        }

        if(isset($activity['options']['dks_note_field_school'])) {
            $options_data['DksNoteFieldSchool'] = $activity['options']['dks_note_field_school'];
        }

        if(isset($activity['options']['dks_note_field_artist'])) {
            $options_data['DksNoteFieldArtist'] = $activity['options']['dks_note_field_artist'];
        }

        if(isset($activity['options']['dks_school_participants_limit'])) {
            $options_data['DksSchoolParticipantsLimit'] = $activity['options']['dks_school_participants_limit'];
        }

        if(isset($activity['options']['dks_school_tour_start_time'])) {
            $options_data['DksSchoolTourStartTime'] = $activity['options']['dks_school_tour_start_time'];
        }

        if(isset($activity['options']['dks_school_tour_end_time'])) {
            $options_data['DksSchoolTourEndTime'] = $activity['options']['dks_school_tour_end_time'];
        }

        if(isset($activity['options']['dks_tour_activity'])) {
            $options_data['DksTourActivity'] = $activity['options']['dks_tour_activity'];
        }

        $data['Options'] = $options_data;

        $result = $this->activity_repository->update($data, $activity_id);
        return $result;
    }

    protected function hasDefaultActivity($enrollment_group_id, $course_id) {
        $count = $this->activity_repository->count([
            'IsDefault' => true,
            'CourseId' => new ObjectID($course_id),
            'EnrollmentGroupId' => new ObjectID($enrollment_group_id),
            ['DateActivated', '<>', null],
            ['DateDeleted', '=', null]
        ]);

        return ($count != 0);
    }
}