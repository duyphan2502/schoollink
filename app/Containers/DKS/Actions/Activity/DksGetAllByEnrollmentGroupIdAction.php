<?php

namespace App\Containers\DKS\Actions\Activity;

use App\Containers\SummerSchool\Actions\Activity\GetAllByEnrollmentGroupIdAction;
use App\Containers\SummerSchool\Criterias\Activity\GetAllActivitiesCriteria;
use App\Containers\SummerSchool\Criterias\Course\GetAllCoursesCriteria;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;

class DksGetAllByEnrollmentGroupIdAction extends GetAllByEnrollmentGroupIdAction
{
    public function run($enrollment_group_id)
    {
        try {
            $this->initParams($school_owner_id, $school_id, $owner_ors);

            $activities = $this->getActivities($school_owner_id, $school_id, $owner_ors, $enrollment_group_id);
            $enrollment_group =  $this->enrollment_group_repository->find($enrollment_group_id);

            return ['enrollment_group' => $enrollment_group, 'activities' => $activities];
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function getActivities($school_owner_id, $school_id, $owner_ors, $enrollment_group_id)
    {
        //Get mongo activities
        $activity_params = array(
            'school_owner_ids' => [$school_owner_id],
            'school_ids' => [$school_id],
            'owner_ors' => $owner_ors,
            'enrollment_group_id' => $enrollment_group_id
        );

        $mongo_activities = $this->activity_repository
            ->getByCriteria(new GetAllActivitiesCriteria($activity_params));

        //GET courses by list ids
        $course_ids = [];
        $activities = [];

        foreach ($mongo_activities as $activity) {
            $activities[(string)$activity->id] = $activity;
            if ($activity->course_id != '') {
                $course_ids[(string)$activity->course_id] = $activity->course_id;
            }
        }

        $course_params = array(
            'school_owner_ids' => [$school_owner_id],
            'school_ids' => [$school_id],
            'course_ids' => $course_ids,
        );

        $mongo_courses = $this->course_repository
            ->getByCriteria(new GetAllCoursesCriteria($course_params));

        $courses = [];
        foreach ($mongo_courses as $mongo_course) {
            $courses[(string)$mongo_course->id] = $mongo_course;
        }

        $final_activities = [];

        $schools = $this->school_repository->findWhere(['owner_id' => $school_owner_id, 'date_deleted' => null])->keyBy('id');

        foreach ($activities as $id => $activity) {
            $return_object = array(
                'activity' => array(
                    'school_id' => $activity->school_id,
                    'id' => (string)$activity->id,
                    'course_id' => $activity->course_id,
                    'enrollment_group_id' => $enrollment_group_id,
                    'name' => $activity->name,
                    'course_type_id' => $activity->course_type_id,
                    'date_start' => $activity->date_start,
                    'date_end' => $activity->date_end,
                    'active' => $activity->active,
                    'number_of_groups' => $activity->number_of_groups ? $activity->number_of_groups : 1,
                    'is_default' => $activity->is_default,
                    'options' => $activity->options
                ),
                'summer_school' => array(
                    'school' => null,
                    'participants_maximum' => isset($activity->options->summer_school_participants_maximum) ? $activity->options->summer_school_participants_maximum:0,
                    'participants_confirmed' => 0,
                    'participants_waiting' => 0,
                )
            );


            if (isset($activity->participants) && is_array($activity->participants)) {
                foreach ($activity->participants as $participant) {
                    if ($participant->summer_school_status === SummerSchoolConstant::PARTICIPANT_STATUS_ALLOCATED ||
                        $participant->summer_school_status === SummerSchoolConstant::PARTICIPANT_STATUS_BOOKED_DIRECTLY) {
                        $return_object['summer_school']['participants_confirmed']++;
                    }
                    if ($participant->summer_school_status === SummerSchoolConstant::PARTICIPANT_STATUS_WAITING_LIST) {
                        $return_object['summer_school']['participants_waiting']++;
                    }
                }
            }

            if (isset($activity->options->summer_school_school_id)) {
                $summer_school = isset($schools[$activity->options->summer_school_school_id]) ? $schools[$activity->options->summer_school_school_id] : null;
                if($summer_school != null) {
                    $return_object['summer_school']['school'] = array(
                        'id' => (int)$summer_school->id,
                        'name' => $summer_school->name
                    );
                }
            }

            if (isset($activity->course_id)) {
                $course = $courses[(string)$activity->course_id];
                $return_object['activity']['year_grade_minimum'] = $course->year_grade_minimum;
                $return_object['activity']['year_grade_maximum'] = $course->year_grade_maximum;
                $return_object['course'] = array(
                    'id' => (string)$course->id,
                    'name' => $course->name,
                    'subject' => $course->subject,
                    'book_on_course' => isset($course->book_on_course) ? $course->book_on_course : false,
                    'year_grade_minimum' => $course->year_grade_minimum,
                    'year_grade_maximum' => $course->year_grade_maximum,
                    'dks_semester' => $course->options->dks_semester,
                    'dks_start_date' => $course->options->dks_start_date,
                    'dks_end_date' => $course->options->dks_end_date
                );
            }
            $final_activities[] = $return_object;
        }

        return $final_activities;
    }
}