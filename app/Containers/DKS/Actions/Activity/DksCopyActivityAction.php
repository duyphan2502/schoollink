<?php

namespace App\Containers\DKS\Actions\Activity;

use App\Containers\SummerSchool\Actions\Activity\CopyActivityAction;
use Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;


class DksCopyActivityAction extends CopyActivityAction
{
    protected function mapping($activity, $enrollment_group_id)
    {
        $now = Carbon::now();

        $result = array();
        $this->setData($result, $activity->school_owner_id, 'SchoolOwnerId');
        $this->setData($result, $activity->school_id, 'SchoolId');
        $this->setData($result, $activity->course_type_id, 'CourseTypeId');
        $this->setData($result, $activity->description, 'Description');
        $this->setData($result, $activity->name, 'Name');
        $this->setData($result, $activity->created_by, 'CreatedBy');
        $this->setData($result, $activity->activated_by, 'ActivatedBy');
        $this->setData($result, $activity->date_activated, 'DateActivated');
        $this->setData($result, $activity->monday_time_start, 'MondayTimeStart');
        $this->setData($result, $activity->monday_time_end, 'MondayTimeEnd');
        $this->setData($result, $activity->tuesday_time_start, 'TuesdayTimeStart');
        $this->setData($result, $activity->tuesday_time_end, 'TuesdayTimeEnd');
        $this->setData($result, $activity->wednesday_time_start, 'WednesdayTimeStart');
        $this->setData($result, $activity->wednesday_time_end, 'WednesdayTimeEnd');
        $this->setData($result, $activity->thursday_time_start, 'ThursdayTimeStart');
        $this->setData($result, $activity->thursday_time_end, 'ThursdayTimeEnd');
        $this->setData($result, $activity->friday_time_start, 'FridayTimeStart');
        $this->setData($result, $activity->friday_time_end, 'FridayTimeEnd');
        $this->setData($result, $activity->saturday_time_start, 'SaturdayTimeStart');
        $this->setData($result, $activity->saturday_time_end, 'SaturdayTimeEnd');
        $this->setData($result, $activity->sunday_time_start, 'SundayTimeStart');
        $this->setData($result, $activity->sunday_time_end, 'SundayTimeEnd');
        $this->setData($result, $activity->number_of_groups, 'NumberOfGroups');
        $this->setData($result, $activity->allow_waiting_queue, 'AllowWaitingQueue');
        $this->setData($result, $activity->is_default, 'IsDefault');
        $this->setData($result, $activity->notes, 'Notes');

        $result['Active'] = true;
        $result['DateStart'] = isset($activity->date_start) ? new UTCDatetime($activity->date_start * 1000) : null;
        $result['DateEnd'] = isset($activity->date_end) ? new UTCDatetime($activity->date_end * 1000) : null;
        $result['DateCreated'] = new UTCDatetime($now->getTimestamp() * 1000);
        $result['CourseId'] = isset($activity->course_id) ? new ObjectID($activity->course_id) : null;
        $result['EnrollmentGroupId'] = isset($enrollment_group_id) ? new ObjectID($enrollment_group_id) : new ObjectID($activity->enrollment_group_id);

        if (isset($activity->owner)) {
            $result['Owner'] = array(
                'Type' => (string)$activity->owner->type,
                'SchoolId' => (int)$activity->owner->school_id
            );
        }

        if (isset($activity->options)) {
            $result['Options'] = array();

            if(isset($activity->options->dks_school_tour_start_time)) {
                $result['Options']['DksSchoolTourStartTime'] = $activity->options->dks_school_tour_start_time;
            }
            if(isset($activity->options->dks_school_tour_end_time)) {
                $result['Options']['DksSchoolTourEndTime'] = $activity->options->dks_school_tour_end_time;
            }
            if(isset($activity->options->dks_tour_activity)) {
                $result['Options']['DksTourActivity'] = $activity->options->dks_tour_activity;
            }
        }
        
        return $result;
    }
}