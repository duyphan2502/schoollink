<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21/09/2016
 * Time: 1:46 PM
 */

namespace App\Containers\DKS\Actions\Activity;

use App\Containers\SchoolLink\Constants\Constant;
use App\Containers\SummerSchool\Actions\Activity\AddActivityAction;
use App\Containers\SummerSchool\Constants\Constant as SummerSchoolConstant;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

class DksAddActivityAction extends AddActivityAction
{
    protected function addActivity($enrollment_group_id, $course_id) {
        $is_default = true;
        $has_default_activity = $this->hasDefaultActivity($enrollment_group_id, $course_id);
        if($has_default_activity) {
            $is_default = false;
        }

        $enrollment_group = $this->enrollment_group_repository->find($enrollment_group_id);
        if($enrollment_group == null)
            return false;

        $school_owner_type = $enrollment_group->owner->type;
        $school_id = $enrollment_group->school_id;

        $school = $this->school_repository->find($school_id);
        if (!$school) {
            return false;
        }

        $app = \Session::get('app');
        $current_user = $app->current_user;

        switch ($school_owner_type) {
            case SummerSchoolConstant::TYPE_SCHOOL_SFO:
                if (!$current_user->hasRole(Constant::ROLE_SFO, $school->id))
                    return false;
                break;
            case SummerSchoolConstant::TYPE_SCHOOL_ADMIN:
                if (!$current_user->hasRole(Constant::ROLE_SCHOOL_ADMIN, $school->id))
                    return false;
                break;
            case SummerSchoolConstant::TYPE_TEACHER:
                if (!$current_user->hasRole(Constant::ROLE_TEACHER, new RoleParam(['school_id' => $school->id])))
                    return false;
                break;
            default:
                return false;
        }

        $academic_year = $school->academicYear();

        $data = [
            'EnrollmentGroupId' => new ObjectID($enrollment_group_id),
            'Active' => true,
            'Notes' => '',
            'SchoolOwnerId' => (int)$school->owner_id,
            'SchoolId' => (int)$school->id,
            'CourseTypeId' => new ObjectID($enrollment_group->course_type_id),
            'Description' => '',
            'Name' => '',
            'Owner' => array(
                'Type' => $school_owner_type,
                'SchoolId' => $school_owner_type === SummerSchoolConstant::TYPE_SCHOOL_SFO || $school_owner_type ===
                SummerSchoolConstant::TYPE_SCHOOL_ADMIN ? (int)$school->id : null),
            'DateStart' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000),
            'DateEnd' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000),
            'CreatedBy' => (int)$current_user->id,
            'DateCreated' => new UTCDatetime(Carbon::now()->getTimestamp() * 1000),
            'NumberOfGroups' => 3,
            'CourseId' => new ObjectID($course_id),
            'Participants' => [],
            'IsDefault' => $is_default
        ];

        $activity = $this->activity_repository->create($data);
        return $activity->id;
    }

    protected function hasDefaultActivity($enrollment_group_id, $course_id) {
        $activity = $this->activity_repository->findWhere([
            'IsDefault' => true,
            'CourseId' => new ObjectID($course_id),
            'EnrollmentGroupId' => new ObjectID($enrollment_group_id),
            ['DateActivated', '<>', null],
            ['DateDeleted', '=', null]
        ])->first();

        return isset($activity);
    }
}