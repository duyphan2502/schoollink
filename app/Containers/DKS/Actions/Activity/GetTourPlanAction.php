<?php

namespace App\Containers\DKS\Actions\Activity;

use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use Exception;
use MongoDB\BSON\ObjectID;

class GetTourPlanAction extends Action
{
    protected $activity_repository;
    protected $course_repository;
    protected $enrollment_group_repository;

    /**
     * GetTourPlanAction constructor.
     *
     */
    public function __construct(
        ActivityRepositoryInterface $activity_repository,
        CourseRepositoryInterface $course_repository,
        EnrollmentGroupRepositoryInterface $enrollment_group_repository
    ) {
        $this->activity_repository = $activity_repository;
        $this->enrollment_group_repository = $enrollment_group_repository;
        $this->course_repository = $course_repository;
    }


    public function run($data)
    {
        try {
            $course_id = $data['course_id'];
            $enrollment_group_id = $data['enrollment_group_id'];

            $activities = $this->getTourPlanActivities($course_id, $enrollment_group_id);
            $enrollment_group = $this->getEnrollmentGroup($enrollment_group_id);
            $course = $this->getCourse($course_id);

            $result = [
                'activities' => $activities,
                'enrollment_group' => $enrollment_group,
                'course' => $course
            ];
            return ['result' => $result];
        } catch (Exception $e) {
            return ['result' => null, 'message' => $e->getMessage()];
        }
    }

    protected function getCourse($course_id) {
        return $this->course_repository->find($course_id);
    }

    protected function getEnrollmentGroup($enrollment_group_id) {
        return $this->enrollment_group_repository->find($enrollment_group_id);
    }

    protected function getTourPlanActivities($course_id, $enrollment_group_id) {
        $activities = $this->activity_repository->findWhere(
            [
                'CourseId' => new ObjectID($course_id),
                'EnrollmentGroupId' => new ObjectID($enrollment_group_id),
                ['DateActivated', '<>', null],
                ['DateDeleted', '=', null],
                'IsDefault' => false
            ]
        );

        return $activities;
    }
}