<?php

namespace App\Containers\DKS\Actions\Group;

use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SummerSchool\Constants\Constant;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseTypeRepositoryInterface;
use App\Containers\SummerSchool\Contracts\EnrollmentGroupRepositoryInterface;
use App\Containers\SummerSchool\Contracts\PostRepositoryInterface;
use App\Core\Action\Abstracts\Action;
use App\Core\Exception\Abstracts\Exception;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

/**
 * Class GetDksBookingDataAction.
 *
 */
class GetDksBookingDataAction extends Action
{
    private $enroll_group_repository;
    private $activity_repository;
    private $course_repository;
    private $school_repository;
    private $course_type_repository;
    private $post_repository;

    public function __construct(
        EnrollmentGroupRepositoryInterface $enroll_group_repository,
        ActivityRepositoryInterface $activity_repository,
        CourseRepositoryInterface $course_repository,
        SchoolRepositoryInterface $school_repository,
        CourseTypeRepositoryInterface $course_type_repository,
        PostRepositoryInterface $post_repository
    )
    {
        $this->enroll_group_repository = $enroll_group_repository;;
        $this->activity_repository = $activity_repository;
        $this->course_repository = $course_repository;
        $this->school_repository = $school_repository;
        $this->course_type_repository = $course_type_repository;
        $this->post_repository = $post_repository;
    }

    public function run($class_id)
    {
        try {
            $app = \Session::get('app');
            if(empty($app->special_school)){
                return false;
            }
            $current_date = new UTCDatetime(Carbon::now()->getTimestamp() * 1000);
            $groups = $this->enroll_group_repository->findWhere([
                'SchoolId' => $app->special_school->id,
                'SchoolOwnerId' => $app->school_owner->id,
                'DateDeleted' => NULL,
                'DateArchived' => NULL,
                ['DateActivated', '<>', NULL],
                ['DateStart', '<=', $current_date],
                ['DateEnd', '>=', $current_date],
            ]);
            $enroll_groups = [];
            if ($groups) {
                $subjects = [];
                $subjects_code = [];
                $weeks = [];
                $courses_type = [];
                $now = Carbon::now();

                $courses = [];
                $activities_interested = [];
                $activities_registered = [];
                $activities_waiting = [];
                $activities_all = [];
                $activities_group = [];
                $sort_default = [];
                foreach ($groups as $kg => $group) {
                    $sort_default[$kg] = $group->is_default;
                    $enroll_groups[$group->id] = $this->transformGroup($group);
                    // get course type attribute for filter
                    if (!empty($group->course_type_id)) {
                        $course_type = $this->course_type_repository->findWhere(['_id' => $group->course_type_id])->first();
                        $courses_type[$group->course_type_id] = $course_type;
                        // get subjects
                        foreach ($course_type->subjects as $subject) {
                            $subjects_code[$subject->code] = $subject;
                            $subject->group_id = $group->id;
                            $subjects[$subject->code . '_' . $group->id] = $subject;
                        }
                        // get weeks
                        foreach ($course_type->schedules as $schedule) {
                            $schedule->date_of_week = $this->getDateOfWeek($schedule->start_week, $now->year);
                            $schedule->group_id = $group->id;
                            $weeks[$schedule->start_week . '_' . $group->id] = $schedule;
                        }
                    }
                    $activities = $this->activity_repository->findWhere(
                        [
                            'EnrollmentGroupId' => new ObjectID($group->id),
                            'Active' => true,
                            ['DateActivated', '<>', NULL],
                            ['CourseId', '<>', NULL],
                            'DateDeleted' => NULL
                        ]
                    )->toArray();
                    if (!empty($activities)) {
                        foreach ($activities as $act) {
                            $act->course_id = (string)$act->course_id;
                            if (!isset($courses[$act->course_id])) {
                                $courses[$act->course_id] = $this->course_repository->findWhere(['_id' => $act->course_id])->first();
                                if(!empty($courses[$act->course_id]->post_detail_id)){
                                    $post = $this->post_repository->findWhere(['ID' => $courses[$act->course_id]->post_detail_id])->first();
                                    $courses[$act->course_id]->description = $post->post_content;
                                }
                            }
                            if (!isset($activities_group[$group->id][$act->course_id])) {
                                $activities_group[$group->id][$act->course_id] = $this->transformCourse($courses, $act, $courses_type, $subjects_code);
                            }

                            $week_start = (int)Carbon::createFromTimeStamp($act->date_start)->format('W');
                            $school = $this->school_repository->find($act->options->summer_school_school_id);
                            if ($school) {
                                $school = $school->getGpsCoordinates();
                                $in_activity = false;
                                if (!empty($act->participants)) {
                                    foreach ($act->participants as $participant) {
                                        $activity_participant = $this->transformActivityParticipant($courses, $act, $week_start, $participant, $enroll_groups, $school);
                                        if (isset($participant->class_id) && $participant->class_id == $class_id) {
                                            $in_activity = true;
                                            if ($participant->summer_school_status == Constant::PARTICIPANT_STATUS_REGISTERED) {
                                                $activities_interested[] = $activity_participant;
                                            } else if ($participant->summer_school_status == Constant::PARTICIPANT_STATUS_ALLOCATED
                                                    || $participant->summer_school_status == Constant::PARTICIPANT_STATUS_BOOKED_DIRECTLY) {
                                                $activities_registered[] = $activity_participant;
                                            } else if ($participant->summer_school_status == Constant::PARTICIPANT_STATUS_WAITING_LIST){
                                                $activities_waiting[] = $activity_participant;
                                            }
                                        }
                                    }
                                }
                                $activities_all[$act->id] = $this->transformActivity($courses, $act, $school, $enroll_groups, $week_start);
                                $week_end = (int)Carbon::createFromTimeStamp($act->date_end)->format('W');
                                for ($i = $week_start; $i <= $week_end; $i++) {
                                    $activities_group[$group->id][$act->course_id]['weeks'][] = $i;
                                }
                                $activities_group[$group->id][$act->course_id]['activities_school'][] = [
                                    'name' => $act->name,
                                    'week_start' => $week_start,
                                    'week_end' => $week_end,
                                    'id' => $act->id,
                                    'in_activity' => $in_activity,
                                    'school' => $school,
                                    'on_waiting_list' => $act->getTotalWaitingList(),
                                    'available_space' => $act->getAvailableSpace(),
                                    'allow_waiting' => $act->allow_waiting_queue
                                ];
                            }
                        }
                    }
                }
                // sort to set enrollment group default
                array_multisort($sort_default, SORT_DESC, $enroll_groups);
            }
            if (!empty($activities_interested)) {
                usort($activities_interested, function ($a, $b) {
                    return $a['summer_school_priority'] > $b['summer_school_priority'];
                });
            }
            return [
                'activities_course' => $activities_group,
                'activities_interested' => $activities_interested,
                'activities_registered' => $activities_registered,
                'activities_waiting' => $activities_waiting,
                'activities_all' => $activities_all,
                'course_type' => [
                    'subjects' => $subjects,
                    'schedule' => $weeks
                ],
                'enroll_groups' => $enroll_groups
            ];
        } catch (Exception $ex) {
            \Log::error($ex);
        }

    }

    protected function transformGroup($group)
    {
        $current_date = Carbon::now()->getTimestamp();
        return [
            'id' => $group->id,
            'name' => $group->name,
            'description_random_allocation' => $group->description_random_allocation,
            'description_direct_entry' => $group->description_direct_entry,
            'enrollment_date_start' => $group->enrollment_date_start,
            'enrollment_date_end' => $group->enrollment_date_end,
            'direct_entry_date_start' => $group->direct_entry_date_start,
            'direct_entry_date_end' => $group->direct_entry_date_end,
            'random_allocation' => $group->enrollment_date_start < $current_date && $current_date < $group->enrollment_date_end,
            'direct_entry' => $group->direct_entry_date_start < $current_date && $current_date < $group->direct_entry_date_end,
            'withdraw' => $group->withdraw_date_start < $current_date && $current_date < $group->withdraw_date_end,
            'allow_booking' => $current_date <= $group->direct_entry_date_end,
            'maximum_course_registration' => $group->maximum_course_random_registration,
            'maximum_course_direct_registration' => $group->maximum_course_direct_registration,
            'is_default' => $group->is_default

        ];
    }

    protected function getDateOfWeek($week, $year)
    {
        $dto = Carbon::now();
        $dto->setISODate($year, $week);
        $ret['date_start'] = $dto->format('d/m');
        $dto->modify('+6 days');
        $ret['date_end'] = $dto->format('d/m');
        return $ret;
    }

    protected function transformCourse($courses, $act, $courses_type, $subjects_code)
    {
        return [
            'name' => $courses[$act->course_id]->name,
            'description' => $courses[$act->course_id]->description,
            'course_id' => $courses[$act->course_id]->id,
            'subject' => $courses[$act->course_id]->subject,
            'subject_color' => $courses[$act->course_id]->subject != NULL ? $subjects_code[$courses[$act->course_id]->subject]->color : NULL,
            'school_id' => $courses[$act->course_id]->school_id,
            'year_grade_maximum' => $courses[$act->course_id]->year_grade_maximum,
            'year_grade_minimum' => $courses[$act->course_id]->year_grade_minimum,
            'weeks' => [],
            'course_type_code' => $courses_type[$act->course_type_id]->code,
            'enroll_group' => $act->enrollment_group_id,
            'book_default' => false
        ];
    }

    protected function transformActivityParticipant($courses, $act, $week_start, $participant, $enroll_groups, $school)
    {
        $now = Carbon::now();
        return [
            'course_name' => $courses[$act->course_id]->name,
            'course_description' => $courses[$act->course_id]->description,
            'course_subject' => $courses[$act->course_id]->subject,
            'activity_id' => $act->id,
            'activity_name' => $act->name,
            'week' => $week_start,
            'date_of_week' => $this->getDateOfWeek($week_start, $now->year),
            'date_start' => $act->date_start,
            'date_end' => $act->date_end,
            'school_name' => $school->name,
            'school_location' => $school->gps_coordinates,
            'summer_school_priority' => $participant->summer_school_priority,
            'summer_school_status' => $participant->summer_school_status,
            'year_grade_maximum' => $courses[$act->course_id]->year_grade_maximum,
            'year_grade_minimum' => $courses[$act->course_id]->year_grade_minimum,
            'allow_withdraw' => $enroll_groups[$act->enrollment_group_id]['withdraw'],
            'enroll_group_id' => $act->enrollment_group_id,
            'position' => !empty($participant->position) ? $participant->position : 0
        ];
    }

    protected function transformActivity($courses, $act, $school, $enroll_groups, $week_start)
    {
        $now = Carbon::now();
        return [
            'course_id' => $courses[$act->course_id]->id,
            'course_name' => $courses[$act->course_id]->name,
            'course_description' => $courses[$act->course_id]->description,
            'course_subject' => $courses[$act->course_id]->subject,
            'activity_id' => $act->id,
            'activity_name' => $act->name,
            'date_start' => $act->date_start,
            'date_end' => $act->date_end,
            'school_name' => $school->name,
            'week' => $week_start,
            'date_of_week' => $this->getDateOfWeek($week_start, $now->year),
            'year_grade_maximum' => $courses[$act->course_id]->year_grade_maximum,
            'year_grade_minimum' => $courses[$act->course_id]->year_grade_minimum,
            'allow_waiting_queue' => $act->allow_waiting_queue,
            'limit_booking' => $act->options->summer_school_participants_limit ? $act->options->summer_school_participants_limit : $act->options->summer_school_participants_maximum,
            'allow_withdraw' => $enroll_groups[$act->enrollment_group_id]['withdraw'],
            'enroll_group_id' => $act->enrollment_group_id
        ];
    }
}