<?php

namespace App\Containers\DKS\Actions\Group;

use App\Containers\DKS\Criterias\Group\SearchGroupsCriteria;
use Exception;
use Carbon\Carbon;

use App\Containers\SchoolLink\Contracts\GroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SchoolRepositoryInterface;
use App\Containers\SchoolLink\Contracts\SubGroupRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRepositoryInterface;
use App\Containers\SchoolLink\Contracts\UserRoleRepositoryInterface;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;

use App\Core\Action\Abstracts\Action;

class SearchGroupInMunicipalityAction extends Action
{
    private $user_role_repository;
    private $user_repository;
    private $subgroup_repository;
    private $group_repository;
    private $school_repository;
    private $activity_repository;

    /**
     * SearchGroupInMunicipalityAction constructor.
     *
     */
    public function __construct(
        UserRoleRepositoryInterface $user_role_repository,
        UserRepositoryInterface $user_repository,
        SubGroupRepositoryInterface $subgroup_repository,
        GroupRepositoryInterface $group_repository,
        SchoolRepositoryInterface $school_repository,
        ActivityRepositoryInterface $activity_repository
    ) {
        $this->user_role_repository = $user_role_repository;
        $this->user_repository = $user_repository;
        $this->subgroup_repository = $subgroup_repository;
        $this->group_repository = $group_repository;
        $this->school_repository = $school_repository;
        $this->activity_repository = $activity_repository;
    }


    public function run($id, $text)
    {
        $classes = [];
        try {
            $activity = $this->getActivitiy($id);
            if($activity == null) {
                return ['result' => false];
            }

            $schools = $this->school_repository->findWhere(['owner_id' => $activity->school_owner_id])->keyBy('id');
            if(count($schools) > 0) {
                $school_ids = $schools->map(function ($school) {
                    return $school->id;
                });

                $groups = $this->group_repository->findWhereIn('school_id', $school_ids->toArray())->keyBy('id');
                $group_ids = $groups->map(function ($group) {
                    return $group->id;
                });

                $classes = $this->subgroup_repository->getByCriteria(new SearchGroupsCriteria($group_ids, $text));

                foreach ($groups as $group) {
                    foreach ($classes as $class) {
                        if($group->id == $class->group_id) {
                            $school = $schools[$group->school_id];
                            $class->school = $school;
                        }
                    }
                }
            }
        } catch (Exception $e) {
            Log::error($e);
        }
        return $classes->toArray();
    }

    /**
     * @param $id
     * @return mixed|null
     */
    private function getActivitiy($id)
    {
        $app = \Session::get('app');
        $current_user = $app->current_user;

        $activity = $this->activity_repository->find($id);

        if(!isset($activity) || !$activity->permissionEdit($current_user)) {
            return null;
        }
        return $activity;
    }
}