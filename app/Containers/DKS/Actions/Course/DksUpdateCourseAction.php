<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 29/09/2016
 * Time: 1:56 PM
 */

namespace App\Containers\DKS\Actions\Course;

use App\Containers\SummerSchool\Actions\Course\UpdateCourseAction;
use Carbon\Carbon;
use MongoDB\BSON\UTCDatetime;

class DksUpdateCourseAction extends UpdateCourseAction
{
    protected function updateCourse($course_id, $course)
    {
        $course_data = array(
            'Name' => $course['name'],
            'Active' => true,
            'BookOnCourse' => isset($course['book_on_course']) ? $course['book_on_course'] : false,
            'Description' => $course['description'],
            'Subject' => $course['subject'],
            'YearGradeMaximum' => $course['year_grade_maximum'],
            'YearGradeMinimum' => $course['year_grade_minimum'],
            'PostDetailId' => (int)$course['post_detail_id'],
            'Options' => array(
                'DksSemester' => $course['options']['semester'],
                'DksStartDate' => isset($course['options']['dks_start_date']) ? new UTCDatetime(Carbon::parse($course['options']['dks_start_date'])->getTimestamp() * 1000) : null,
                'DksEndDate' => isset($course['options']['dks_end_date']) ? new UTCDatetime(Carbon::parse($course['options']['dks_end_date'])->getTimestamp() * 1000) : null,
            )
        );

        $course = $this->course_repository->update($course_data, $course_id);
        return $course->id;
    }
}