<?php

namespace App\Containers\DKS\Actions\Course;

use App\Containers\SummerSchool\Actions\Course\GetCoursesByCourseTypeIdAction;
use App\Containers\SummerSchool\Contracts\ActivityRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseRepositoryInterface;
use App\Containers\SummerSchool\Contracts\CourseTypeRepositoryInterface;
use MongoDB\BSON\ObjectID;

class DksGetCoursesByCourseTypeIdAction extends GetCoursesByCourseTypeIdAction
{
    protected $activity_repository;

    public function __construct(
        CourseTypeRepositoryInterface $course_type_repository,
        CourseRepositoryInterface $course_repository,
        ActivityRepositoryInterface $activity_repository
    )
    {
        parent::__construct($course_type_repository, $course_repository);

        $this->activity_repository = $activity_repository;
    }

    protected function getCourses($course_type_id)
    {
        $course_type = $this->course_type_repository->find($course_type_id);
        if (!isset($course_type)) {
            return null;
        }

        $courses = $this->course_repository->findWhere([
            'DateDeleted' => null,
            'DeletedBy' => null,
            ['Name', '<>', null],
            'CourseTypeId' => new ObjectID($course_type_id),
            'SchoolOwnerId' => (int)$course_type->school_owner_id,
            'SchoolId' => (int)$course_type->school_id
        ]);

        return ['courses' => $courses];
    }
}