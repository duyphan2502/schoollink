/**
 * Created by Admin on 06/10/2016.
 */

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    gulpIf = require('gulp-if'),
    concat = require('gulp-concat'),
    concat_sm = require('gulp-concat-sourcemap'),
    elixir = require('laravel-elixir'),
    sourcemaps = require('gulp-sourcemaps'),
    config = elixir.config,
    _  = require('underscore'),
    Elixir = require('laravel-elixir'),
    Task = Elixir.Task;

Elixir.extend('customJsLibs', function(jsOutputFile, jsOutputFolder) {

    var baseDir = Elixir.config.assetsPath + '/libs/';
    var jsCombinedFile = jsOutputFile || 'customJsLibs.js';

    if (!Elixir.config.production){
        concat = concat_sm;
    }

    var onError = function (err) {
        notify.onError({
            title: "Javascript lib Builder",
            subtitle: "Js Files Compilation Failed!",
            message: "Error: <%= error.message %>",
            icon: __dirname + '/../node_modules/laravel-elixir/icons/fail.png'
        })(err);
        this.emit('end');
    };

    var jsFiles = ['./resources/assets/libs/**/*.js'];
    new Task('resources-libs', function(){
        return gulp.src(jsFiles)
            .on('error', onError)
            .pipe(concat(jsCombinedFile, {sourcesContent: true}))
            .pipe(gulpIf(!config.production, sourcemaps.write()))
            .pipe(gulpIf(config.production, uglify()))
            .pipe(gulp.dest(jsOutputFolder || config.js.outputFolder));
    }).watch(baseDir + '/**/*.js');


});

