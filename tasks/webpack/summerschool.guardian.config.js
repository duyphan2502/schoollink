module.exports = {
	// configuration
	entry: "./angular/portals/summerschool/guardian/summerschool.guardian.main.js",
	output: {
		filename: "summerschool.guardian.app.js"
	},
	module: {
		loaders: [{
			test: /\.js?$/,
			exclude: /(node_modules|bower_components)/,
			loader: 'babel', // 'babel-loader' is also a legal name to reference
			query: {
				presets: ['es2015'],
				cacheDirectory: true
			}
		}]
	}
};
