module.exports = {
	// configuration
	entry: "./angular/portals/schoollink/guardian/schoollink.guardian.main.js",
	output: {
		filename: "schoollink.guardian.app.js"
	},
	module: {
		loaders: [{
			test: /\.js?$/,
			exclude: /(node_modules|bower_components)/,
			loader: 'babel', // 'babel-loader' is also a legal name to reference
			query: {
				presets: ['es2015'],
				cacheDirectory: true
			}
		}]
	}
};
