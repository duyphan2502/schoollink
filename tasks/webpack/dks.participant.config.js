module.exports = {
	// configuration
	entry: "./angular/portals/dks/participant/dks.participant.main.js",
	output: {
		filename: "dks.participant.app.js"
	},
	module: {
		loaders: [{
			test: /\.js?$/,
			exclude: /(node_modules|bower_components)/,
			loader: 'babel', // 'babel-loader' is also a legal name to reference
			query: {
				presets: ['es2015'],
				cacheDirectory: true
			}
		}]
	}
};
