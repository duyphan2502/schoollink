/*Elixir Task
 *copyrights to https://github.com/HRcc/laravel-elixir-angular
 */
var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var eslint = require('gulp-eslint');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var notify = require('gulp-notify');
var gulpif = require('gulp-if');

var webpack = require('webpack-stream');
var schoollinkWebpackConfig = require('./webpack/schoollink.guardian.config.js');
var summerschoolWebpackConfig = require('./webpack/summerschool.guardian.config.js');
var summerschoolTeacherWebpackConfig = require('./webpack/summerschool.teacher.config.js');
var dksParticipantWebpackConfig = require('./webpack/dks.participant.config.js');
var dksAdminWebpackConfig = require('./webpack/dks.admin.config.js');

var Elixir = require('laravel-elixir');

var Task = Elixir.Task;

Elixir.extend('angular', function(src, output, outputFilename) {

	var baseDir = src || Elixir.config.assetsPath + '/angular/';

	new Task('angular in ' + baseDir, function() {
		// Main file has to be included first.
		return gulp.src([baseDir + "schoollink.guardian.main.js", baseDir + "**/*.*.js"])
			.pipe(eslint())
			.pipe(eslint.format())
			.pipe(gulpif(!config.production, sourcemaps.init()))
			.pipe(webpack(schoollinkWebpackConfig))
			.pipe(ngAnnotate())
			.pipe(gulpif(config.production, uglify()))
			.pipe(gulpif(!config.production, sourcemaps.write()))
			.pipe(gulp.dest(output || config.js.outputFolder))
			.pipe(notify({
				title: 'Laravel Elixir',
				subtitle: 'SchoolLink Guardian Compiled!',
				icon: __dirname + '/../node_modules/laravel-elixir/icons/laravel.png',
				message: ' '
			}));
	}).watch(baseDir + '/**/*.js');


	new Task('angular in ' + baseDir, function() {
		// Main file has to be included first.
		return gulp.src([baseDir + "summerschool.guardian.main.js", baseDir + "**/*.*.js"])
			.pipe(eslint())
			.pipe(eslint.format())
			.pipe(gulpif(!config.production, sourcemaps.init()))
			.pipe(webpack(summerschoolWebpackConfig))
			.pipe(ngAnnotate())
			.pipe(gulpif(config.production, uglify()))
			.pipe(gulpif(!config.production, sourcemaps.write()))
			.pipe(gulp.dest(output || config.js.outputFolder))
			.pipe(notify({
				title: 'Laravel Elixir',
				subtitle: 'Summerschool Guardian Compiled!',
				icon: __dirname + '/../node_modules/laravel-elixir/icons/laravel.png',
				message: ' '
			}));
	}).watch(baseDir + '/**/*.js');

	new Task('angular in ' + baseDir, function() {
		// Main file has to be included first.
		return gulp.src([baseDir + "summerschool.teacher.main.js", baseDir + "**/*.*.js"])
			.pipe(eslint())
			.pipe(eslint.format())
			.pipe(gulpif(!config.production, sourcemaps.init()))
			.pipe(webpack(summerschoolTeacherWebpackConfig))
			.pipe(ngAnnotate())
			.pipe(gulpif(config.production, uglify()))
			.pipe(gulpif(!config.production, sourcemaps.write()))
			.pipe(gulp.dest(output || config.js.outputFolder))
			.pipe(notify({
				title: 'Laravel Elixir',
				subtitle: 'Summerschool Teacher Compiled!',
				icon: __dirname + '/../node_modules/laravel-elixir/icons/laravel.png',
				message: ' '
			}));
	}).watch(baseDir + '/**/*.js');

	new Task('angular in ' + baseDir, function() {
		// Main file has to be included first.
		return gulp.src([baseDir + "dks.participant.main.js", baseDir + "**/*.*.js"])
			.pipe(eslint())
			.pipe(eslint.format())
			.pipe(gulpif(!config.production, sourcemaps.init()))
			.pipe(webpack(dksParticipantWebpackConfig))
			.pipe(ngAnnotate())
			.pipe(gulpif(config.production, uglify()))
			.pipe(gulpif(!config.production, sourcemaps.write()))
			.pipe(gulp.dest(output || config.js.outputFolder))
			.pipe(notify({
				title: 'Laravel Elixir',
				subtitle: 'DKS Participant Compiled!',
				icon: __dirname + '/../node_modules/laravel-elixir/icons/laravel.png',
				message: ' '
			}));
	}).watch(baseDir + '/**/*.js');

	new Task('angular in ' + baseDir, function() {
		// Main file has to be included first.
		return gulp.src([baseDir + "dks.admin.main.js", baseDir + "**/*.*.js"])
			.pipe(eslint())
			.pipe(eslint.format())
			.pipe(gulpif(!config.production, sourcemaps.init()))
			.pipe(webpack(dksAdminWebpackConfig))
			.pipe(ngAnnotate())
			.pipe(gulpif(config.production, uglify()))
			.pipe(gulpif(!config.production, sourcemaps.write()))
			.pipe(gulp.dest(output || config.js.outputFolder))
			.pipe(notify({
				title: 'Laravel Elixir',
				subtitle: 'DKS Admin Compiled!',
				icon: __dirname + '/../node_modules/laravel-elixir/icons/laravel.png',
				message: ' '
			}));
	}).watch(baseDir + '/**/*.js');

	new Task('app-imgs', function() {
		return gulp.src('./resources/assets/sass/images/*')
			.pipe(gulp.dest(config.img.outputFolder))
			.pipe(notify({
				title: 'Laravel Elixir',
				subtitle: 'Images Application Files Imported!',
				icon: __dirname + '/../node_modules/laravel-elixir/icons/laravel.png',
				message: ' '
			}));
	}).watch('./resources/assets/sass/images/*');
});
