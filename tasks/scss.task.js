/*Elixir Task for bower
* Upgraded from https://github.com/ansata-biz/laravel-elixir-bower
*/
var gulp = require('gulp');
var mainBowerFiles = require('main-bower-files');
var filter = require('gulp-filter');
var notify = require('gulp-notify');
var cssnano = require('gulp-cssnano');
var uglify = require('gulp-uglify');
var concat_sm = require('gulp-concat-sourcemap');
var concat = require('gulp-concat');
var gulpIf = require('gulp-if');
var sourcemaps = require('gulp-sourcemaps');
var scss = require('gulp-sass');

var Elixir = require('laravel-elixir');

var Task = Elixir.Task;

Elixir.extend('resources', function(jsOutputFile, jsOutputFolder, cssOutputFile, cssOutputFolder) {

	var baseDir = Elixir.config.assetsPath + '/sass/';
	var cssFile = cssOutputFile || 'components.css';

	if (!Elixir.config.production){
		concat = concat_sm;
	}

	var onError = function (err) {
		notify.onError({
			title: "Scss Builder",
			subtitle: "Scss Files Compilation Failed!",
			message: "Error: <%= error.message %>",
			icon: __dirname + '/../node_modules/laravel-elixir/icons/fail.png'
		})(err);
		this.emit('end');
	};

	var sassFiles = ['./resources/assets/sass/*.scss'];
	new Task('resources-scss', function(){
		return gulp.src(sassFiles)
			.on('error', onError)
			.pipe(scss())
			.pipe(gulpIf(!config.production, sourcemaps.write()))
			.pipe(gulp.dest(cssOutputFolder || config.css.outputFolder))
			.pipe(notify({
				title: 'Laravel Elixir',
				subtitle: 'CSS Bower Files Imported!',
				icon: __dirname + '/../node_modules/laravel-elixir/icons/laravel.png',
				message: ' '
			}));
	}).watch(baseDir + '/**/*.scss');

	var componentsFiles = ['./angular/**/*.scss'];
	new Task('components-scss', function(){
		return gulp.src(componentsFiles)
			.on('error', onError)
			.pipe(scss())
			.pipe(concat(cssFile, {sourcesContent: true}))
			.pipe(gulpIf(!config.production, sourcemaps.write()))
			.pipe(gulp.dest(cssOutputFolder || config.css.outputFolder))
			.pipe(notify({
				title: 'Laravel Elixir',
				subtitle: 'CSS Bower Files Imported!',
				icon: __dirname + '/../node_modules/laravel-elixir/icons/laravel.png',
				message: ' '
			}));
	}).watch('./angular/**/*.scss');
});
